<?php

class m130801_134810_add_full_description_to_support_file extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE `support_file` ADD `full_description` TEXT NOT NULL;");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `support_file` DROP `full_description`;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}