<?php

class m130706_175924_add_seo_field_to_category_metadata extends CDbMigration
{
	public function up()
	{
		$this->dropTable('category_metadata');
		$this->execute("
			CREATE TABLE IF NOT EXISTS `category_metadata` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `title` varchar(150) NOT NULL,
			  `description` varchar(255) NOT NULL,
			  `keywords` varchar(255) NOT NULL,
			  `seo` TEXT DEFAULT  NULL,
			  `url` varchar(255) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->dropTable('category_metadata');
		$this->execute("
			CREATE TABLE IF NOT EXISTS `category_metadata` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `title` varchar(50) NOT NULL,
			  `description` varchar(255) NOT NULL,
			  `keywords` varchar(255) NOT NULL,
			  `category_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  CONSTRAINT `FK_category` FOREIGN KEY (`category_id`)
						  REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}