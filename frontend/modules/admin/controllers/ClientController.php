<?php

class ClientController extends Controller
{		
	public function actionEdit($id, $page = "")
	{
        $page = !is_numeric($page) ? "" : "/index/Client_page/$page";
		$model = Client::model()->findByPk($id);
        $phones = Phone::model()->findAllByAttributes(array('client_id' => $id));
        $hasSubErrors = false;
		if (isset($_POST['Client']))
		{
            if(isset($_POST['Phones']))
            {
                $oldPhones = $phones; //old phones, all value, that not unseted, will be deleted
                $phonesValues = $_POST['Phones']; //new values for old phones
                $newPhones = isset($_POST['Phones']['new']) ? $_POST['Phones']['new'] : array(); //new phones, that must be inserted into db, and $phones
                $phones = array();

                foreach ($phonesValues as $key => $phone)
                {
                    if($key !== 'new')
                    {
                        if(isset($oldPhones[$key]))
                        {
                            $phones[$key] = $oldPhones[$key];
                            unset($oldPhones[$key]);
                            $phones[$key]->attributes = $phone;
                            $phones[$key]->save();
                            $hasSubErrors = $phones[$key]->hasErrors();
                        }
                    }
                }
                foreach ($oldPhones as $oldPhone)
                {
                    $oldPhone->delete();
                }
                for ($i = 0; $i < sizeof(end($newPhones)); $i++)
                {
                    $newPhone = new Phone();
                    $newPhone->client_id = $id;
                    $newPhone->type = $newPhones['type'][$i];
                    $newPhone->value = $newPhones['value'][$i];
                    $newPhone->save();
                    if(!empty($newPhones['value'][$i]))
                        $hasSubErrors = $newPhone->hasErrors();
                    array_push($phones, $newPhone);
                }
            }
			$model->attributes = $_POST['Client'];
			if ($model->save())
            {
                if(!$hasSubErrors)
                    $this->redirect(array("/admin/client$page"));
                else
                    $model->addError('phone', 'Необходимо исправить ощибки в менеджере телефонов');
            }
		}
				
		$this->render('edit', array(
			'model' => $model,
            'phones' => $phones,
            'newPhone' => new Phone(),
		));
	}

    public function countOld(array $array)
    {
        $count = 0;
        foreach($array as $model)
        {
            $count += $model->id == null ? 0 : 1;
        }
        return $count;
    }

	public function actionIndex()
	{		
		$model = new Client('search');
		
		if (isset($_GET['Client']))
			$model->attributes = $_GET['Client'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Client');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Профиль успешно отредактирован')));
	}

    public function actionOrderInfo($id, $page = "")
    {
        $page = !is_numeric($page) ? "/admin/client" : "/admin/client/index/Client_page/$page";
        $client = Client::model()->findByPk($id);
        $carts = Cart::model()->findAllByAttributes(array('client_id' => $id));
        $orderIds = array();
        foreach($carts as $cart)
        {
            array_push($orderIds, $cart->order_id);
        }
        $orders = Order::model()->findAllByAttributes(array('id' => $orderIds));
        //echo 'ertert';
        $this->render('order_info', array(
                'client' => $client,
                'orders' => $orders,
                'page' => $page,
            )
        );
    }
    public function actionSync()
    {
         $json_value = new stdClass();  
         $contacts= array();
         $clients = Client::model()->findAll();
         foreach ($clients as $client){
            $contact = new stdClass();
            $contact->id = $client->id;
            $contact->firstName = $client->name;
            $contact->lastName = $client->lastname;
            $channels=array(array('type'=>'email', 'value' => $client->email));
            if (isset($client->phones0[0]->value)) array_push($channels, array('type'=>'sms', 'value' => $client->phones0[0]->value));
            $contact->channels = $channels;
             if (isset($client->address0[0]->town))
             {
                $contact->address = new stdClass();
                $contact->address->town=$client->address0[0]->town;
                $contact->address->address=$client->address0[0]->street.' д.'.$client->address0[0]->house.' кв.'.$client->address0[0]->flat; //: string, "postcode": string}';
                }
            array_push($contacts, $contact);
         }
         
         $json_value->contacts=$contacts;
         $json_value->dedupeOn=array("email");
         $json_value->contactFields=array('firstName','lastName', 'address', 'email', 'sms', 'town', 'region');
         //echo json_encode($json_value);
         $ch = curl_init();  
         curl_setopt($ch, CURLOPT_POST, 1);  
         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_value));  
         curl_setopt($ch, CURLOPT_HEADER, 1);  
         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));  
         curl_setopt($ch, CURLOPT_URL, 'http://esputnik.com.ua/api/v1/contacts');
         //curl_setopt($ch, CURLOPT_URL, 'http://esputnik.com.ua/api/v1/сontacts?');  
         curl_setopt($ch,CURLOPT_USERPWD, 'ua.brama@gmail.com:170989');  
         curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);  
         $output = curl_exec($ch);
         curl_close($ch);
         preg_match_all('/(HTTP\/1\.1 200 OK)|(\{.*\})/',$output,$matches);
          $this->render('sync', array(
                'output' => $output,
                'matches' => $matches,
            )
        );
         
         
    }
    
}
