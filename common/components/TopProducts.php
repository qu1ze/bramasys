<?php
class TopProducts extends CWidget
{
	public $categoriesIds = null;
	
    public function run()
    {
		$criteria = new CDbCriteria;
		$criteria->alias = 'products';
		$criteria->select = 'products.*';
		$criteria->select .= ", (SELECT count(*) FROM `order_product`
							RIGHT OUTER JOIN `order` ON `order_product`.`order_id` = `order`.`id`
							WHERE `order_product`.`product_id` = `products`.`id` AND `order_product`.`is_complect` = 0 AND `order`.`status` = ".Order::ORDER_STATUS_APPROVED.") as `buy_count`";
		$criteria->select .= ", (SELECT count(*) FROM `comment_vote`
							RIGHT OUTER JOIN `comment` ON `comment_vote`.`comment_id` = `comment`.`id`
							WHERE `comment`.`product_id` = `products`.`id`) as `rating`";
		$criteria->addCondition('hide != 1');
		if(!empty($this->categoriesIds))
			$criteria->addInCondition('category', $this->categoriesIds);
		$criteria->order = 'buy_count DESC, rating DESC';
		$criteria->limit = 5;
        $this->render("top_products", array(
            'products' => Products::model()->findAll($criteria),
        ));
    }
}
?>
