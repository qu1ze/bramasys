<?php
//	Yii::app()->clientScript->registerScript("auto_desc", "
//        $('.full_desc_field').on('change', function(){
//            var full_desc_field = $(this);
//            var desc_field = $('.desc_field');
//            if (full_desc_field.val())
//            {
//                var desc = full_desc_field.val().substring(0, 255);
//                if (desc_field.val() != desc)
//                {
//                    if (!desc_field.val())
//                        desc_field.val(desc);
//                    else
//                    {
//                        $('.desc').val(desc);
//                        $('#confirmReplace').dialog('open');
//                    }
//                }
//            }
//        });
//    ", CClientScript::POS_READY);
	
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'options'=>array(
                        'title'=>'Заменить описание?',
                        'modal'=>true,
                        'autoOpen'=>false,
                        'buttons'=>array(
                            'Yes'=>'js:function(){$(".desc_field").val($(".desc").val()); $(this).dialog("close")}',
                            'No'=>'js:function(){$(this).dialog("close")}'
                            )
                    ),
                    'id'=>'confirmReplace',
                    
            )
        );

        echo CHtml::textArea('desc', '', array('class' => 'desc'));

        $this->endWidget('zii.widgets.jui.CJuiDialog');
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'name'); ?>
	<?php echo $form->ckEditorRow($model, 'full_description', array('class'=>'full_desc_field', 'rows'=>5)); ?>
	<?php echo $form->textAreaRow($model , 'short_description', array('class' => 'desc_field'));?>
	<?php echo $form->textFieldRow($model, 'image_url');?>
	<?php echo $form->textFieldRow($model, 'url');?>
	<?php echo $form->checkBoxRow($model, 'partner'); ?>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Создать')); ?>
	
<?php $this->endWidget(); ?>