<?php

class m130728_182150_add_category_in_url_field extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE `category` ADD `in_url` TINYINT(1) NOT NULL DEFAULT 1;");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `category` DROP `in_url`;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}