<table>
	<?php 
	$isTransporent = true;
	foreach($specifications as $specification):?>
	<tr <?php echo $isTransporent ? '' : 'class="table_color"'; ?>>
		<td>
			<?php echo $specification->spec->name?>
			<a class="question" spec-id="<?php echo $specification->spec->id; ?>" href="#">
				<img src="/images/questionitem.png">
			</a>
		</td>
		<td><?php echo $specification->value?></td>
	</tr>
	<?php 
	$isTransporent = !$isTransporent;
	endforeach;?>
</table>