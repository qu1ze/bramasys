<?php
/**
 * index.php
 *    
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 8:30 PM
 */
?>
<div id="catalog-and-banner_block">
    <div id="banner">
        <?php $this->widget('common.components.NaviSlider');?>
    </div>
</div>
<?php
$categories = app()->categoryController->getChilds(ProductsController::getTag()); ?>
<div id="categories_block">
	<div id="categories-block-row">
	<?php 
	$count = 0;
	foreach ($categories as $key => $category):?>
		<?php 
		if($count > 5)
			break(1);
			?>
		<div class="category-item">
			<div class="category-name"><a href="<?php echo app()->categoryController->getCategoryAddress($category['data']['name']);?>"><?php echo $category['data']['title'];?></a></div>
			<img alt="" src="<?php echo $category['data']['image']?>" title="">
			<ul class="sub-category">
				<?php 
				$subCategories = app()->categoryController->getChilds($category['id']);
				$subCount = 0;
				foreach ($subCategories as $key => $subCategory):?>
				<?php if($subCategory['data']['in_url'] > 0): ?>
					<?php if($subCount > 5) { break(1); }; ?>
						<li>
							<a href="<?php echo app()->categoryController->getCategoryAddress($subCategory['data']['name']);?>"><?php echo $subCategory['data']['title'];?></a>
						</li>
						<?php $subCount++; ?>
				<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php 
	$count++;
	endforeach; ?>
	</div>
</div>

<?php $this->widget('common.components.UsefulTabs');?>

<div class="news-and-social-plugins">
	<div class="index-columns news-column">
		<a href="<?php echo app()->getBaseUrl(true); ?>/rss/news"><img  src="/images/rssIconThumb.png" alt=""></a>
		<p class="title">НОВОСТИ И АКЦИИ</p>
		<div class="blue-line"></div>
		<div class="news-rows">
			<?php foreach ($last_news as $last_news_item): ?>
				<div class="item news-item">
					<div class="date"><?php  echo date('d.m.Y', $last_news_item->create_time) ?></div>
					<div class="title"><a href="company/news/<?php echo $last_news_item->name ?>"><?php echo $last_news_item->title ?></a></div>
					<div class="text"><?php echo $last_news_item->short_description ?></div>
				</div>
			<?php endforeach ?>	
		</div>
		<div class="clear"></div>
		<a class="all" href="<?php echo app()->getBaseUrl(true); ?>/company/news">Все новости и акции</a>
	</div>

	<div class="index-columns">
		<a href="<?php echo app()->getBaseUrl(true); ?>/rss/articles"><img src="/images/rssIconThumb.png" alt=""></a>
		<p class="title">ПУБЛИКАЦИИ И ОБЗОРЫ</p>
		<div class="blue-line"></div>
		<div class="news-rows">
			<?php foreach ($last_articles as $last_articles_item): ?>
				<div class="item news-item">
					<div class="date"><?php  echo date('d.m.Y', $last_articles_item->create_time) ?></div>
					<div class="title"><a href="support/stati-publikatcii-obzory/<?php echo $last_articles_item->name ?>"><?php echo $last_articles_item->title ?></a></div>
					<div class="text"><?php echo $last_articles_item->short_description ?></div>
				</div>
			<?php endforeach ?>	
		</div>
		<a class="all" href="<?php echo app()->getBaseUrl(true); ?>/support/stati-publikatcii-obzory">Все публикации и обзоры</a>
	</div>

	<div class="index-columns">
		<div class="facebook-like-box">
			<div class="fb-like-box" data-href="http://www.facebook.com/BramaSys" data-width="316" data-height="230" data-show-faces="true" data-border-color="#9fa4a6" data-stream="false" data-header="true"></div>
		</div>
		<div class="like-question">
			Понравился наш сайт?
		</div>
		<div class="google-plus soc-like-btn">
            <script> document.write("<g:plusone annotation=\"inline\"></g:plusone>");</script>
		</div>
		<div class="facebook-recommend soc-like-btn">
			<div class="fb-like" data-layout="button_count" data-href="http://bramasys.com.ua" data-send="false" data-width="100" data-show-faces="false"></div>
		</div>
		<div class="twitter-btn soc-like-btn">
			<a href="https://twitter.com/twitter" class="twitter-follow-button" data-show-count="false" data-lang="ru" data-show-screen-name="false">Читать @twitter</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</div>
		<div class="vk-btn soc-like-btn">
			<div id="vk_like"></div><script type="text/javascript">VK.Widgets.Like("vk_like", {type: "mini"});</script>
		</div>
	</div>
</div>