<?php
class UsefulTabs extends CWidget
{
	public $category;
	public $products;
	public $complects;
	public $limit;
	
	public function init()
	{
		if(!isset($this->category) || !$this->category)
		{
            $this->limit = 5;
			$this->category = ProductsController::getTag();
		}
		else
		{
            $this->limit = 4;
            $categories = app()->categoryController->getChildrenIds($this->category);
            array_push($categories, $this->category);
        }
        $this->products = array();
        $productLabels = array('top_seller' => 'Лидеры продаж', 'novelty' => 'Новинки', 'price_of_the_week' => 'Цена недели');
        foreach($productLabels as $labelName => $labelTitle)
        {
            $conditionArray = isset($categories) ? array('category' => $categories, $labelName => 1) : array($labelName => 1);
            array_push($this->products, array(
                'model' => Products::model()->findAllByAttributes($conditionArray, array('limit' => $this->limit)),
                'title' => $labelTitle,
                'type' => $labelName,
                'count' => ceil(Products::model()->countByAttributes($conditionArray) / $this->limit),
            ));
        }
		$this->complects = array(
			'model' => Complect::model()->findAllByAttributes(array(), array('limit' => $this->limit)),
			'title' => 'Готовые комплекты',
			'count' => ceil(Complect::model()->count() / $this->limit),
		);
	}
	
    public function run()
    {
        $this->render("_useful_tabs", array(
			'count' => $this->limit,
			'category' => $this->category,
			'products' => $this->products,
			'complects' => $this->complects,
			)
        );
    }
}
?>
