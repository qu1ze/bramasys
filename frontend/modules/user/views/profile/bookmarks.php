<?php 
Yii::app()->getClientScript()->registerCssFile('/css/filter.css');
Yii::app()->getClientScript()->registerCssFile('/css/profile.css');

Yii::app()->clientScript->registerScript("dropMark", "
        $('.dropMark').on('click', function(){
			var tr = $(this).parent().parent();
			if($(tr).attr('data-isComplect') == 1)
				url = '/complect/ajaxRemoveBookmark';
			else
				url = '/products/ajaxRemoveBookmark';
			$.get(url + '?id=' + $(tr).attr('mark-id'), function(data) {
				if(data == 'Success')
				{
					$(tr).fadeOut('slow', function(){ $(tr).remove(); });
				}
			  });
        });
	", CClientScript::POS_READY);

$bookmarksCats = isset(Yii::app()->request->cookies['bookmarks']) ? json_decode(Yii::app()->request->cookies['bookmarks'], true) : array();
$this->breadcrumbs=array(
	'Мои закладки',
);
$this->renderPartial('_side'); ?>
<div id="container">
	<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
	?>
	<h3>Мои закладки</h3>
	<table class="cart-table" style="width: 100%;">
	<thead>
		<tr class="header">
			<td colspan="5"><img src="/images/bookmark.png" alt="" title=""> Мои закладки</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($bookmarksCats as $key => $bookmarks): ?>
			<?php foreach ($bookmarks as $id): 
				$model = null;
				if($key == 'product')
					$model = Products::model()->findByPk($id);
				else
					$model = Complect::model()->findByPk($id);
			?>
				<tr data-isComplect="<?php echo $key == 'complect' ? '1' : '0'; ?>" mark-id="<?php echo $model->id; ?>">
					<td><a class="img-a" href="<?php echo $model->link; ?>"><img src="<?php echo $model->image_url; ?>" alt="" title=""></a></td>
                    <td><a class="hover_color" href="<?php echo $model->link; ?>"><?php echo $model->name; ?></a></td>
					<td class="price"><?php echo CurrencySys::exchange($model->price).' '.CurrencySys::getLabel(); ?></td>
					<td class="show" style="display: table-cell;">
						<div class="<?php echo $key == 'complect' ? 'complPd' : 'pd'; ?><?php echo $model->id; ?>" style="display: inline-block;">
							<div class="pd-qty-block" style="display: none;"><input hidden value="1"></div>
							<div class="code pd-code" style="display: none;">Код товара: <span><?php echo $model->id;?></span></div>
							<div class="btn-buy-<?php echo $model->id; ?> enabled" style="<?php echo $model->inCart ? 'display: none;' : ''?>">
								<a class="catalog btn-buy-buy" data-is_complect="<?php echo $key == 'complect' ? '1' : '0'; ?>" href="#" id="<?php echo $key == 'complect' ? 'complPd' : 'pd'; ?><?php echo $model->id; ?>">Купить</a>
							</div>
							<div class="btn-buy-<?php echo $model->id; ?> disabled" id="<?php echo $key == 'complect' ? 'complPd' : 'pd'; ?><?php echo $model->id; ?>"  style="<?php echo $model->inCart ? '' : 'display: none;'?>">
								<a data-s_complect="<?php echo $key == 'complect' ? '1' : '0'; ?>" href="#" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
							</div>
						</div>
					</td>
					<td><img class="dropMark" style="padding-top:10px;" src="/images/cross.gif" width="10"/></td>
				</tr>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</tbody>
	</table>
</div>