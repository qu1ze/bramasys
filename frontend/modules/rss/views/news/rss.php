<?php echo '<' . '?xml version="1.0"?' . '>' ?>
<rss version="2.0">
  <channel>
    <title><?php echo $seo->title ?></title>
    <link><?php echo  app()->getBaseUrl(true); ?></link>
    <description><?php echo $seo->description ?></description>
    <language>ru-RU</language>
    <pubDate><?php echo date('r', time()); ?></pubDate>
    <lastBuildDate><?php echo date('r', time()); ?></lastBuildDate>
    <webMaster>webmaster@example.com</webMaster>
 	<?php foreach ($model as $value) : ?>
    <item>
      <title><?php echo $value->title ?></title>	
      <link><?php echo  app()->getBaseUrl(true); ?>/support/stati-publikatcii-obzory/<?php echo $value->name ?></link>
      <description><?php echo $value->short_description ?></description>
      <pubDate><?php echo date(DATE_RSS, $value->create_time); ?></pubDate>
    </item>
    <?php endforeach; ?>
 
  </channel>
</rss>