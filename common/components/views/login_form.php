<?php 

if($type == LoginForm::LOGIN_FORM_TYPE_AJAX):
		Yii::app()->clientScript->registerScript("log-in","
			$('#ajax-login-form input').keypress(function (e) {
				if (e.which == 13) {
					$('.login-btn').click();
					return false;
				}
			});
			
			$('#ajax-login-form').on('click', '.login-btn', function () {
			   var form = $('#ajax-login-form');
			   $(form).children('.alert').fadeOut();
			   var errorList = $(form).children('.alert').children('.error-list');
			   var errorLine = $($(errorList).children()[0]).clone();
			   $(errorList).children().remove()
			   $(errorList).append(errorLine);
			   var LoginForm = $(form).serializeArray();
			   $.ajax({
				   url: '/site/ajaxLogin',
				   data: LoginForm,
				   dataType: 'json',
				   type: 'post',
				   success: function(data){
					   if(typeof(data) == 'object' && data.title == 'Success')
					   {
						   $('#login_btn').fadeOut(200, function()
						   {
								$('#logout_btn').fadeIn(200);
						   });
						   var button = $('.sign-btn');
						   $(button).toggleClass('sign-btn');
						   $(button).toggleClass('user-btn');
						   var link = '/';
						   var text = '';
						   if(data.isAdmin)
						   {
								link = '".app()->getBaseUrl(true)."/admin';
								text = 'Админ панель';
							}
							else
							{
								link = '".app()->getBaseUrl(true)."/user';
								text = data.name + ' ' + data.lastname;
							}
						   $(button).children('a').attr('href', link);
							$(button).children('a').text(text);
							$('.login-form-wrapper').click();
							if($('.guest-author-name').length > 0)
							{
							    $('.guest-author-name').slideUp();
							    $('.is-login-author-name').slideDown();
							    text = data.name + ' ' + data.lastname;
							    $('.is-login-author-name p').text(text);

							}
							else if ($('#new-client').length > 0) {
                                location.reload();
							}
					   }
					   else if (typeof(data.message) == 'object')
					   {
						   for(var field in data.message)
						   {
								var newLine = $(errorLine).clone();
							   $(errorList).append($(newLine).text(data.message[field][0]));
							   $(newLine).fadeIn();
						   }
						   $(form).children('.alert').fadeIn();
					   }
				   }
			   });
			});
		   ", CClientScript::POS_READY);
		
		Yii::app()->clientScript->registerScript("login-show-hide","
			$('body').on('click', '.ajax-login-btn', function () {
				$('#login-form-wrapper').slideDown(600);
				$('#login-form-container').fadeIn(800);
				$('.form-overlay').fadeIn(100);
				return false;
			});
			$('.login-form-container').on('click', '.close', function () {
				$('.login-form-wrapper').click();
				return false;
			});
			$('body').on('click', '.login-form-wrapper:not(.login-form-container)', function (e) {
				if(e.target.id !== '.login-form-container' && !$(e.target).parents().is('.login-form-container'))
				{
					$('.login-form-wrapper').slideUp(600);
					$('.login-form-container').fadeOut(300);
					$('.login-form-wrapper').parent().children('.form-overlay').fadeOut(100);
					return false;
				}
			});
		   ", CClientScript::POS_READY);	
?>
<table id="login-form-wrapper" class="login-form-wrapper" style="display:none;"><tr><td>
    <div id="login-form-container" class="login-form-container" style="display:none;">
        <div class="login-form-header">
            <h3>ВХОД НА САЙТ</h3>
            <a class="close" id="close_login" href="#" style="top: -5px;">
            </a>
        </div>
        <div class="login-form-content">
	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => 'ajax-login-form',
		'type'=>'horizontal',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'clientOptions' => array(
		'validateOnSubmit' => true,
		),
	)); ?>

		<div class="alert alert-block alert-error" style="display:none;"><p>Необходимо исправить следующие ошибки:</p>
			<ul class="error-list">
				<li style="display:none;"></li>
			</ul>
		</div>
            <?php if(!empty(Yii::app()->request->cookies['old_login']->value)) $model->username = Yii::app()->request->cookies['old_login']->value; ?>
                
		<?php echo $form->textFieldRow($model, 'username'); ?>
		<?php echo $form->passwordFieldRow($model, 'password'); ?>
		<div class="right-block">
			<?php echo $form->checkBox($model, 'rememberMe', array('style' => 'margin-top: 0; margin-bottom: 1px; margin-right: 5px;')); ?><span style="font-size: 14px;">Запомнить меня</span>
			<?php if ($model->requireCaptcha): ?>
				<p>plain... :)) </p>
			<?php endif; ?>
			<div class="actions">
                <a id="newPass" style="margin-top: 20px; display: inline-block; float:left;" href="#">Забыли свой пароль?</a>
				<?php echo CHtml::button('Войти', array('class' => 'login-btn')); ?>
			</div>
                <div style="padding-top: 26px; text-align: left;margin-bottom: 5px;">Вход при помощи:</div>
                <?php  $this->widget('commonExt.eauth.EAuthWidget', array('action' => '/site/login'));//Yii::app()->eauth->renderWidget(); ?>
            
		</div>
	<?php $this->endWidget(); ?>
        </div>
    </div>
</td></tr></table>
<?php else: ?>
	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => 'login-form',
		'type' => 'horizontal',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
		'htmlOptions' => $htmlOptions,
	)); ?>
    <?php
	if (Yii::app()->user->hasFlash('error')) {
		echo '<div class="error">'.Yii::app()->user->getFlash('error').'</div>';
	}
?>
		<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldRow($model, 'username'); ?>
		<?php echo $form->passwordFieldRow($model, 'password'); ?>

		<?php echo $form->checkBox($model, 'rememberMe'); ?>
		<?php if ($model->requireCaptcha): ?>
			<p>plain... :)) </p>
		<?php endif; ?>

		<div class="actions" style="padding-top: 30px;">
			<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Войти', 'htmlOptions' => array('class' => 'red-button'))); ?>
		</div>
            
      <div>Вход при помощи:</div>
<?php $this->widget('commonExt.eauth.EAuthWidget', array('action' => '/site/login')); ?>
	<?php $this->endWidget(); 
    
   
 ?>
<?php endif; ?>
