<?php

class m130822_084747_add_fields_to_category extends CDbMigration
{
	public function up()
	{
		$this->execute("
                ALTER TABLE `category`  
					ADD `image_url` VARCHAR(255) DEFAULT NULL,
					ADD `product_title` VARCHAR(255) DEFAULT NULL AFTER `image_url`,
					ADD `product_description` VARCHAR(255) DEFAULT NULL AFTER `product_title`,
					ADD `product_keywords` VARCHAR(255) DEFAULT NULL AFTER `product_description`,
					ADD `H1` VARCHAR(255) DEFAULT NULL AFTER `product_keywords`;
            ");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `category`
                DROP `image_url`,
                DROP `product_title`,
				DROP `product_description`,
                DROP `product_keywords`,
				DROP `H1`;
                ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}