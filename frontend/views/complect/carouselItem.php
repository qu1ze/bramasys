	<?php foreach($complects as $complect): ?>
	<?php $inCart = isset(Yii::app()->session['cart']['complect'][$complect->id]);?>
<div class="tab-product complPd<?php echo $complect->id; ?>">
	<div class="pd-qty-block"><input hidden value="1"></div>
	<div class="code pd-code" style="display: none;">Код товара: <span><?php echo $complect->id;?></span></div>
	<div class="thumb">
		<a href="<?php echo $complect->link;?>">
			<span class="img-vert">
				<img style="width: 120px;max-height: 150px" src="<?php echo $complect->image_url;?>" alt="">
			</span>
		</a>
	</div>
	<div class="name"><a href="<?php echo $complect->link;?>"><?php echo $complect->name;?></a></div>
	<div class="price"><?php echo CurrencySys::exchange($complect->price).' '.CurrencySys::getLabel(); ?></div>
    <?php if ($complect->quantity != null):?>
        <?php if ($complect->quantity > 0):?>
            <div id="btn" class="btn-buy-<?php echo $complect->id; ?> enabled" style="<?php echo $complect->inCart ? 'display: none;' : ''?>">
                <a data-is_complect="1" id="<?php echo $complect->cartPattern; ?>" href="#" class="btn-buy-buy index-buy-but">Купить</a>
            </div>
            <div class="btn-buy-<?php echo $complect->id; ?> disabled" id="<?php echo $complect->cartPattern; ?>"  style="<?php echo $complect->inCart ? '' : 'display: none;'?>">
                <a data-is_complect="1" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
            </div>
        <?php else:?>
            <a class="catalog btn-buy-order" data-is_complect="1" href="#" id="pd<?php echo $complect->id; ?>" style="font-size: 20px; line-height: 30px; margin-left: 28px;">Заказать</a>
        <?php endif;?>
    <?php else:?>
        <a class="catalog btn-buy-soon" data-is_complect="1" href="#" id="pd<?php echo $complect->id; ?>" style="font-size: 15px; padding: 5px; line-height:23px; margin-left: 28px;">Уведомить о наличии</a>
    <?php endif;?>
</div>
	<?php endforeach; ?>