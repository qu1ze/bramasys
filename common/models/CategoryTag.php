<?php

/**
 * This is the model class for table "category_tag".
 *
 * The followings are the available columns in table 'category_tag':
 * @property integer $id
 * @property string $name
 */
class CategoryTag extends ActiveRecord
{
	private $systemTags = array(
		'static',
		'catalog',
	);
	
	private $origAttrs = array();
	
	public function isSystemTag()
	{
		if(in_array($this->origAttrs['name'], $this->systemTags))
		{
			return true;
		}
		return false;
	}
	
	public function getOrig($attribute)
	{
		return isset($this->origAttrs[$attribute]) ? $this->origAttrs[$attribute] : null;
	}
	
	protected function afterFind()
	{
		$this->origAttrs['name'] = $this->name;
		return parent::afterFind();
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CategoryTag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category_tag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
            array('name', 'filter', 'filter'=>'trim'),
			array('name', 'unique'),
			array('name', 'validateIsSystem'),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

	public function validateIsSystem($attribute)
	{
		if($this->isSystemTag())
		{
			if($this->getAttribute($attribute) != $this->getOrig($attribute))
				$this->addError($attribute, 'В этом теге поле '.$this->getAttributeLabel($attribute).' не редактируемое');
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categories' => array(self::HAS_MANY, 'Category', 'tag')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getListData($first = false)
	{
		$data = CHtml::listData(self::model()->findAll(array('index' => 'id')), 'id', 'name');
		return $first ? array('' => '') + $data : $data;
	}	
}