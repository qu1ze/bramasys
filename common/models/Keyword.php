<?php

/**
 * This is the model class for table "keyword".
 *
 * The followings are the available columns in table 'keyword':
 * @property integer $id
 * @property string $keyword
 * @property integer $object_id
 */
class Keyword extends ActiveRecord
{
	
	const OBJECT_TYPE_NEWS = 1;
	const OBJECT_TYPE_ARTICLES = 2;
	
	public $objectTypes = array(
		self::OBJECT_TYPE_NEWS => 'News',
		self::OBJECT_TYPE_ARTICLES => 'Articles',
	);
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Keyword the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'keyword';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('keyword, object_id, object_type', 'required'),
			array('keyword', 'filter', 'filter' => 'trim'),
			array('object_id, object_type', 'numerical', 'integerOnly'=>true),
			array('keyword', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, keyword, object_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'keyword' => 'Keyword',
			'object_id' => 'Object ID',
			'object_type' => 'Object Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('keyword',$this->keyword,true);
		$criteria->compare('object_id',$this->object_id);
		$criteria->compare('object_type',$this->object_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getObjectTypes($type = null)
	{
		return is_null($type) ? $this->objectTypes : $this->objectTypes[$type];
	}
}