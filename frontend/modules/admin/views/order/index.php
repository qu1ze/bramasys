<?php
$language = Yii::app()->getLanguage();
$dateFormat = 'dd/mm/yy';

//$js = '$.datepicker.setDefaults($.extend({"showAnim":"fold","dateFormat":"'.$dateFormat.'","changeMonth":"true","showButtonPanel":"true","changeYear":"true","constrainInput":"false"}));
//jQuery.datepicker.regional[""].dateFormat = "'.$dateFormat.'";
//';
//Yii::app()->getClientScript()->registerScript('setDateFormat', $js);
$dateFilter = '<div style="position:absolute;">'.$this->widget('system.zii.widgets.jui.CJuiDatePicker', array(
	'model' => $model,
	'attribute' => 'afterDate',
	'value' => $model->afterDate,
	'language'=>$language,
	'options'=>array(
//		'showAnim'=>'fold',
		'dateFormat'=>$dateFormat,
//		'changeMonth' => 'true',
//		'changeYear'=>'true',
//		'constrainInput' => 'false',
		),
	'htmlOptions'=>array('style'=>'width: auto;'),
	),true) . ' До ' . $this->widget('system.zii.widgets.jui.CJuiDatePicker', array(
	'model' => $model,
	'attribute' => 'beforeDate',
	'value' => $model->beforeDate,
	'language'=>$language,
	'options'=>array(
//		'showAnim'=>'fold',
		'dateFormat'=>$dateFormat,
//		'changeMonth' => 'true',
//		'changeYear'=>'true',
//		'constrainInput' => 'false',
		),
	'htmlOptions'=>array('style'=>'width: auto;'),
	),true).'</div>';

/**/

$grid_id = 'order-table';

$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $grid_id,
	'type' => 'bordered striped',
	'dataProvider' => $model->search(),
	'filter' => $model,	
	'afterAjaxUpdate' => 'js:function(){
		jQuery("#Order_afterDate").datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional["'.$language.'"], {"showAnim":"fold","dateFormat":"'.$dateFormat.'","changeMonth":"true","showButtonPanel":"true","changeYear":"true","constrainInput":"false"}));
		jQuery("#Order_beforeDate").datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional["'.$language.'"], {"showAnim":"fold","dateFormat":"'.$dateFormat.'","changeMonth":"true","showButtonPanel":"true","changeYear":"true","constrainInput":"false"}));
		jQuery(".tooltip").remove();
		jQuery("a[rel=tooltip]").tooltip();
	}',
	'template' => "{items}\n{pager}",
	'columns' => array(
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'create_time',
			'filter' => $dateFilter,
			'editable' => array(
				'type' => 'date',
				'format' => 'dd/mm/yyyy',
                'viewformat' => 'dd/mm/yyyy',
				'placement' => 'right',
				'url' => app()->createUrl('/admin/order/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
                                                brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),	
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'status',
			'filter' => $model->getOrderStatuses(),
			'editable' => array(
				'type' => 'select',
				'source' => $model->getOrderStatuses(),
				'url' => app()->createUrl('/admin/order/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('nowrap' => 'nowrap'),			
			'template' => '{update} {orderInfo}',
			'buttons'=>array
			(
				'orderInfo' => array
				(
					'label'=>'Инормация о заказе',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/admin/info.png',
					'url'=>'Yii::app()->createUrl("admin/order/orderInfo", array("id"=>$data->primaryKey))',
				),
			),
			'updateButtonUrl'=>'Yii::app()->controller->createUrl("update", array("id" => $data->primaryKey))',
			'deleteButtonUrl'=>'Yii::app()->controller->createUrl("ajaxDelete", array("id" => $data->primaryKey))',
			'deleteConfirmation' => 'Вы уверенны что хотите удалить?',
			'afterDelete' => 'function(link,success,data){
				if(success){data = $.parseJSON(data);brama.ui.notify(data.title, data.msg, false);}
				else{brama.ui.notify("Oops!", data.responseText )};
			}',
		)
	)
));

?>