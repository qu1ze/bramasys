<?php
/**
 * create.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 11/16/12
 * Time: 5:28 PM
 */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>$model->getFormId(),
	'type' => 'vertical',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=> true,
		'afterValidate'=>'js:function(f,d,e){
			if(!e)
			{
				var url = "'.$this->createUrl('/admin/relatedarticles/ajaxCreate').'";
		        $.ajax({
		            url: url,
		            data: f.serialize(),
		            dataType: "json",
		            type: "post",
		            success: function(d){						
		                if(typeof(d) == "object"){
							brama.ui.notify(d.title, d.msg, false);
						}
		                $.colorbox.close();
		                $("#' . $model->getGridId() . '").yiiGridView("update");					  						
		            }
		        });
			}
			return false;
		}'
	)
)); ?>

<?php echo $form->errorSummary($model); ?>
<?php $this->renderPartial('_form', array('form' => $form, 'model' => $model)) ?>

<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Создать')); ?>

<?php $this->endWidget(); ?>
