<?php
/**
 * Controller.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:55 AM
 */
class CompareController extends Controller
{
	private $compare;
	public $categoryId;
	
	protected function beforeAction($action)
    {
		if(isset(Yii::app()->session['compare']))
		{
			$this->compare = Yii::app()->session['compare'];
			$this->categoryId = Yii::app()->session['compareCatId'];
		}
		else
		{
			$this->compare = array();
		}
		return parent::beforeAction($action);
	}
	
	public function actionAddToCompare($productId, $lastCompare)
	{
        $this->categoryId = Products::model()->findByAttributes(array('id' => $productId))->category;
		if(!isset($this->compare[$this->categoryId]))
		{
			$this->compare[$this->categoryId] = array();
		}
		foreach ($this->compare[$this->categoryId] as $product_Id)
		{
			if($product_Id == $productId)
			{
				echo json_encode(array('title' => 'Error'));
				return;
			}
		}
		array_push($this->compare[$this->categoryId], $productId);
		echo json_encode(array(
			'title' => 'Success',
			'html' => $this->renderPartial('carouselItem', array(
					'product' => Products::model()->findByPk($productId),
				), true),
			));
	}
	
	public function actionRemoveFromCompare($productId)
	{
		foreach ($this->compare as $categoryKey => $products)
		{
			foreach ($products as $key => $product_Id)
			{
				if($product_Id == $productId)
				{
					array_splice($this->compare[$categoryKey], $key, 1);
					echo json_encode(array('title' => 'Success'));
					return;
				}
			}
		}
	}
	
	public function actionIndex()
	{
		
		$categotyIds = array();
		$productIds = array();
		foreach ($this->compare as $categoryKey => $products)
		{
			foreach ($products as $productId)
			{
				array_push($productIds, $productId);
			}
            foreach (app()->categoryController->addParentsIds($categoryKey) as $categoryId)
            {
                if(!in_array($categoryId, $categotyIds))
                    array_push($categotyIds, $categoryId);
            }
		}
		$categorySpecs = CategorySpec::model()->findAllByAttributes(array('category_id' => $categotyIds), array('order' => '`order`'));
		$specIdArray = array();
		foreach ($categorySpecs as $categorySpec)
		{
			array_push($specIdArray, $categorySpec->spec_id);
		}
		$specifications = CHtml::listData(Specification::model()->findAllByPk($specIdArray), 'id', 'name');

        $productSpecValues = array();
        /*Geting specs names*/
        foreach ($specIdArray as $specsId)
        {
            $specValues = ProductSpecValue::model()->findAllByAttributes(array('product_id' => $productIds, 'spec_id' => $specsId));
            foreach ($specValues as $specValue)
            {
                if($specValue !== null)
                    array_push($productSpecValues, $specValue);
            }
        }

		//$productSpecValues = ProductSpecValue::model()->findAllByAttributes(array('product_id' => $productIds));
		$productSpecValueArray = array();
		foreach ($productSpecValues as $productSpecValue)
		{
			$productSpecValueArray[Products::model()->findByPk($productSpecValue->product_id)->category][$productSpecValue->product_id][$productSpecValue->spec_id] = $productSpecValue->value;
		}
		$this->render('index', array(
			'specifications' => $specifications,
			'categorySpecsValues' => $productSpecValueArray,
                        'empty' => empty($this->compare),
		));
	}
	
	protected function afterAction($action)
    {
		Yii::app()->session['compare'] = $this->compare;
		Yii::app()->session['compareCatId'] = $this->categoryId;
	}
}
