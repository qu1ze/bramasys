<?php
/**
 * register.php
 *  <div class="lableoff">
                        <?php $form->type = 'vertical';
                        echo $form->textFieldRow($model, 'phone',array('style'=>'width: 150px;')); 
                        $form->type = 'horizontal';    ?>
                        </div>    
 *  <input type="text" name="Phone[value]" id="phone" value="<?php echo $phone->value; ?>" style="width: 150px;">
 */
Yii::app()->getClientScript()->registerCssFile('/css/profile.css');

Yii::app()->clientScript->registerScript("changeTab","
		$('.regTab').on('click', function(){
			$('.regTab').removeClass('active');
			$('.regTab').addClass('nonactive');
			$(this).removeClass('nonactive');
			$(this).addClass('active');
			var tabId = $(this).attr('tab-id');
			$('.tabcontent').removeClass('show');
			$('.tabcontent').addClass('hide');
			$('#reg-tab' + tabId).removeClass('hide');
			$('#reg-tab' + tabId).addClass('show');
			return false;
		});
        
        $('#Client_email').focus();
        
        $('body').on('click', '.sub_checkbox', function(){
			$.getJSON('/products/ajaxGetMesage?pageName=agreement', function(data) {
                $('#form_template #form-container .form-content').html(data.desc);
                var text =$('#form_template').html(); 
                $.colorbox({html:text}, function(){
                    $.colorbox.resize();});
            });
            return false;
		});
        
       $('body').on('click', '#close_',  function(){
        $.colorbox.close();
        });


       $('#phone').on('blur', function(){
          $.ajax(
			{
				url: '/user/profile/ajaxphonevalidation',
				data: {value: $('#phone').val()},
				dataType: 'json',
				type: 'get',
				success: function(data){
                    $('#old-phone-errors').hide();
					if(data.title == 'error')
					{
					$('#Phone_em_').show();
                    $('#Phone_em_').text(data.error);
                    $('#Phone_em_').parent().parent().parent().removeClass('success').addClass('error');
					}
                    else
                    {
                     $('#Phone_em_').parent().parent().parent().removeClass('error').addClass('success');
                    $('#Phone_em_').hide();
                    }
				}
			});
        });
        
", CClientScript::POS_READY);

$this->pageTitle = 'Регистрация';
$this->breadcrumbs = array(
	'Регистрация',
);
?>
<aside id="sideLeft" style="background: none;">
	<ul class="navigation">
	</ul>
</aside>
<div id="container">
	<div tab-id="1" class="regTab active">
		<a href="#">Регистрация</a>
	</div>
	<div tab-id="2" class="regTab nonactive">
		<a href="#">Я зарегистрированный</a>
	</div>
	<div id="reg-tab1" class="tabcontent show">
		<h3>Регистрация</h3>

		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'registration-form',
			'type' => 'horizontal',
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			
		)); ?>

              	
                        <?php echo $form->errorSummary($model); ?>
			<?php echo $form->textFieldRow($model, 'email'); ?>
			 <div class="controls"><div class="fieldWithSubLabel"> <label class="subLabelNoLeft">не менее 6-ти символов</label></div></div>
		
                <?php echo $form->passwordFieldRow($model, 'newPassword',array('style'=>'width: 150px;')); ?> 
             <div class="controls"><div class="fieldWithSubLabel"> <label class="subLabelNoLeft">не менее 6-ти символов</label></div></div>
		
          	<?php echo $form->passwordFieldRow($model, 'passwordConfirm',array('style'=>'width: 150px;')); ?>
			<?php echo $form->textFieldRow($model, 'name'); ?>
			<?php echo $form->textFieldRow($model, 'lastname'); ?>
			<?php echo $form->textFieldRow($model, 'middle_name'); ?>
             
            <div class="controls"><div class="fieldWithSubLabel"> <label class="subLabelNoLeft">в формате дд.мм.гггг</label></div></div>
			<?php echo $form->textFieldRow($model, 'date_of_birth',array('style'=>'width: 150px;')); ?>
            <label class="control-label required" style="float: none;">Номер телефона</label>
            <div class="control-group">
                <?php echo CHtml::dropDownList("Phone[type]", $phone->type, $phone->getPhoneTypes(), array('class' => 'control-label', 'style' => 'width: 145px;'))?>
                <div class="controls">
                    <div class="fieldWithSubLabel">
                       <input type="text" name="Phone[value]" id="phone" value="<?php echo $phone->value; ?>" style="width: 150px;">
                        
                       <label class="subLabelNoLeft">В формате (067)676-76-76</label>
                       <span class="help-inline error" id="Phone_em_" style="display: none;"></span> 
                        <?php echo CHtml::error($phone, 'value',array('id'=>'old-phone-errors')); ?>
                    </div>
                </div>
            </div>
			<div class="sub_checkbox">
				<label>
					Регистрируясь, я соглашаюсь с
					<a href="#">пользовательским соглашением</a>
				</label>
			</div><br>
            <div><label><span style="color:red">*</span>Поля отмеченные звездочкой, обязательны для заполнения</label></div>
			<div class="actions" style="padding-top: 10px;">
				<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Зарегистрироватся', 'htmlOptions' => array('class' => 'red-button'))); ?>
			</div>
              <div id="social_reg_div">
    <h4>Связь с социальными сетями</h4>
    <div>Вы можите зарегестрироваться на Браме, используя учетную запись социальных сетей</div>
    <?php $this->widget('commonExt.eauth.EAuthWidget', array('action' => '/site/login')); ?>
    </div>
		<?php $this->endWidget(); ?>
          
	</div>
    
	<div id="reg-tab2" class="tabcontent hide">
		<?php $this->widget('common.components.LoginFormWidget', array('model' => $login, 'type' => LoginForm::LOGIN_FORM_TYPE_SUBMIT)); ?>
	</div>
</div>

<div id="form_template" style="display: none">
<div id="form-container" >
		<div class="form-header">
			<h3>&nbsp;</h3>
			<a class="close hover" id="close_" href="#" style="top: -5px;">
			</a>
		</div>
		<div class="form-content">
		</div>
	</div>
</div>