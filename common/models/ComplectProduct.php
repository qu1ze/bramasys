<?php

/**
 * This is the model class for table "complect_product".
 *
 * The followings are the available columns in table 'complect_product':
 * @property integer $id
 * @property integer $complect_id
 * @property integer $product_id
 * @property integer $count
 *
 * The followings are the available model relations:
 * @property Products $product
 * @property Complect $complect
 */
class ComplectProduct extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ComplectProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'complect_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('complect_id, product_id, count', 'required'),
			array('complect_id', 'uniqueWithProduct_id'),
			array('complect_id, product_id, count', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, complect_id, product_id, count', 'safe', 'on'=>'search'),
		);
	}

	public function uniqueWithProduct_id()
	{
		if(($model = ComplectProduct::model()->findByAttributes(array('complect_id' => $this->complect_id, 'product_id' => $this->product_id))) !== null)
		{
			if($model->id != $this->id)
				$this->addError('complect_id', 'Поля комплект-продукт должны быть уникальными');
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
			'complect' => array(self::BELONGS_TO, 'Complect', 'complect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'complect_id' => 'Комплект',
			'product_id' => 'Продукт',
			'count' => 'Количество',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('complect_id',$this->complect_id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('count',$this->count);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}