<?php

class m130706_173649_add_article_fields extends CDbMigration
{
	public function up()
	{
		$this->addColumn('article', 'short_description', 'text');
		$this->addColumn('article', 'keywords', 'varchar(255)');
	}

	public function down()
	{
		$this->dropColumn('article', 'short_description');
		$this->dropColumn('article', 'keywords');
	}
}