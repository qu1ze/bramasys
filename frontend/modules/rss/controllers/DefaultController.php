<?php

class DefaultController extends Controller
{
	
	public function accessRules()
	{
		return array(
			// not logged in users should be able to login and view captcha images as well as errors
			array('allow', 'actions' => array('index','test')),
			// logged in users can do whatever they want to
			array('allow', 'users' => array('@')),
			// not logged in users can't do anything except above
			array('deny'),
		);
	}
	
	public function actionIndex()
	{
		$this->render('rss');
	}
	public function actionArticles()
	{
		$seo = CategoryMetadata::model()->findByAttributes(array('url'=>'/'));
        
        $criteria_articles = new CDbCriteria();
		$criteria_articles->order = 'create_time DESC';
		$criteria_articles->limit = 20;
		$last_articles = Article::model()->findAll($criteria_articles);
        
        $this->renderPartial('rss', array(
			'seo' => $seo,
			'model' => $last_articles,
		));   
		
	}
	public function actionTest()
	{
		$seo = CategoryMetadata::model()->findByAttributes(array('url'=>'/'));
        
        $criteria_articles = new CDbCriteria();
		$criteria_articles->order = 'create_time DESC';
		$criteria_articles->limit = 20;
		$last_articles = Article::model()->findAll($criteria_articles);
        
      $this->render('index');
		
	}
}