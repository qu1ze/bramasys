<?php

class m130526_203418_add_article_tables extends CDbMigration
{
	public function up()
	{
		//article table
		$this->execute("
			CREATE TABLE IF NOT EXISTS `article` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` varchar(255) NOT NULL,
			  `title` varchar(255) NOT NULL,
			  `text` longtext NOT NULL,
			  `category_id` int(11) NOT NULL,
			  `views` int(11) NOT NULL,
			  `thumb` varchar(255) NOT NULL,
			  `create_time` int(11) NOT NULL,
			  `update_time` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
		
		//article_product table
		$this->execute("
			
			CREATE TABLE IF NOT EXISTS `article_product` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `article_id` int(11) NOT NULL,
			  `product_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `article_id` (`article_id`),
			  KEY `product_id` (`product_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


			ALTER TABLE `article_product`
			ADD CONSTRAINT `article_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
			ADD CONSTRAINT `article_product_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);
		");
		
		//related article table
		$this->execute("
			CREATE TABLE IF NOT EXISTS `related_article` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `article_id` int(11) NOT NULL,
			  `related_article` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `article_id` (`article_id`),
			  KEY `related_article` (`related_article`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


			ALTER TABLE `related_article`
			ADD CONSTRAINT `related_article_ibfk_2` FOREIGN KEY (`related_article`) REFERENCES `article` (`id`),
			ADD CONSTRAINT `related_article_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);
		");
	}

	public function down()
	{
		$this->dropTable('related_article');
		$this->dropTable('article_product');
		$this->dropTable('article');
	}

}