<?php
class NaviSlider extends CWidget
{
    public function run()
    {
        $this->render("navi_slider", array(
            'slides' => Slides::model()->findAll(),
        ));
    }
}
?>
