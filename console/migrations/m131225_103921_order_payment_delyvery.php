<?php

class m131225_103921_order_payment_delyvery extends CDbMigration
{
	/*public function up()
	{
		
	}

	public function down()
	{
		
	}

	*/
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
     $this->execute("ALTER TABLE `order` ADD `company_name` VARCHAR( 150 ) NULL;"
            . "DELETE `delivery_type`;
                DELETE `delivery_payment`;
                DELETE `payment_type`;
            INSERT INTO `payment_type` (`id`, `title`, `commission`) VALUES
            (1, 'Online. Картой Visa или MasterCard (Приват Банка)', 0),
            (2, 'Online. Картой Visa или MasterCard (других банков)', 3),
            (3, 'По счету (в кассе банка, через Приват24)', 0),
            (4, 'Наложенный платеж', 1),
            (5, 'Безналичная форма оплаты для юр.лиц', 0);
            
            INSERT INTO `delivery_type` (`id`, `title`, `description`) VALUES
            (2, 'Новой Почтой (по всей Украине)', 'Не дорого'),
            (3, 'Курьером (в Запорожье)', '50'),
            (4, 'Курьером (в Днепропетровске)', '60'),
            (5, 'Самовывоз (в Запорожье)', 'Бесплатно');
            
            INSERT INTO `delivery_payment` (`id`, `delivery_type_id`, `payment_type_id`) VALUES
            (8, 2, 4), (9, 2, 3), (10, 3, 1), (11, 3, 2), (12, 3, 3), (14, 4, 1), (15, 4, 2),
            (16, 4, 3), (17, 5, 1), (18, 5, 2), (19, 5, 3), (20, 5, 5), (21, 3, 5), (22, 4, 5),
            (23, 2, 5);"
            );
		$currency = new Currency();
		$currency->id=2;
        $currency->name = 'UAH б/н';
 		$currency->koef = 9;
 		$currency->label= 'грн.';
        
 		$currency->save(); 
	}

	public function safeDown()
	{
        $this->execute("ALTER TABLE `order` DROP `company_name`;");
		$currency = Currency::model()->findByAttributes(array('name' => 'UAH б/н'));
		$currency->delete();
	}
	
}