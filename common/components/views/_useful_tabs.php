<?php
/* @var $this UsefulTabs */
/* @var $product Products */
/* @var $complect Complect 

 * id="<?php echo $product->cartPattern; ?>" 
id="pd<?php echo $product->id; ?>" */

$sub = '';
if($count < 5)
{
	$sub = ' sub';
}
Yii::app()->clientScript->registerScript("SlideTabs", "
	$('.carousel-control.left').on('click', function() {
		var url = '/products/ajaxCarouselNext';
		var carouselId = $(this).parent(0).attr('id');
		if (carouselId == 'tabcontent_1')
		{
			url = '/complect/ajaxCarouselNext';
		}
		var activeItem = $('#' + carouselId + ' .item.active');
		var length = $('#' + carouselId + ' .item').length;
		var count = $('#' + carouselId).attr('data-count');
		var itemId = 0;
		if($(activeItem).attr('data-itemId') == 1)
		{
			itemId = count;
		}
		else
		{
			itemId = $(activeItem).attr('data-itemId') - 1;
		}
		if(length < count && $(activeItem).index() == 0)
		{
			$('#' + carouselId + ' .carousel-inner').prepend('<div class=\"item$sub\" data-itemId=\"' + itemId + '\"></div>');
			$.ajax({
					url: url,
					data: {offset: itemId - 1, count: $count, type: $('#' + carouselId).attr('data-type'), category: '$category'},
					dataType: 'json',
					type: 'post',
					success: function(data){
						if(data.msg == 'next')
						{
							$('#' + carouselId + ' .item[data-itemId^=' + itemId + ']').html(data.item);
							$('#' + carouselId).carousel('prev');
						}
					}
			});
		}
		else
		{
			$('#' + carouselId).carousel('prev');
		}
	});

	$('.carousel-control.right').on('click', function() {
		var url = '/products/ajaxCarouselNext';
		var carouselId = $(this).parent(0).attr('id');
		var activeItem = $('#' + carouselId + ' .item.active');
		var length = $('#' + carouselId + ' .item').length;
		var count = $('#' + carouselId).attr('data-count');
		var itemId = Number($(activeItem).attr('data-itemId')) + 1;
		if(length < count && $(activeItem).index() + 1 == length)
		{
			$('#' + carouselId + ' .carousel-inner').append('<div class=\"item$sub\" data-itemId=\"' + itemId + '\"></div>');
			$.ajax({
					url: url,
					data: {offset: itemId - 1, count: $count, type: $('#' + carouselId).attr('data-type'), category: '$category'},
					dataType: 'json',
					type: 'post',
					success: function(data){
						if(data.msg == 'next')
						{
							$('#' + carouselId + ' .item[data-itemId^=' + itemId + ']').html(data.item);
							$('#' + carouselId).carousel('next');
						}
					}
			});
		}
		else
		{
			$('#' + carouselId).carousel('next');
		}
	});
", CClientScript::POS_READY);
?>

<script>
	function showtab(id){
		names = new Array ('tabname_1','tabname_2','tabname_3','tabname_4'); //массив id заголовков табов
		conts= new Array ('tabcontent_1','tabcontent_2','tabcontent_3','tabcontent_4'); //массив id табов
		for(i=0;i<names.length;i++) {
			$('#' + names[i]).removeClass('active');
			$('#' + names[i]).addClass('nonactive');
		}
		for(i=0;i<conts.length;i++) {
			$('#' + conts[i]).removeClass('show');
			$('#' + conts[i]).addClass('hide');
		}
		$('#' + 'tabname_' + id).addClass('active');
		$('#' + 'tabcontent_' + id).addClass('show');
	}
</script>

<div class="various-product-block">
    <div class="tabbs">
        <table>
			<tbody>
				<tr>
					<td class="name">
						<div id="tabname_1" onclick="showtab('1')" class="active"><span><?php echo $complects['title'];?></span></div>
						<?php foreach($products as $key => $product): ?>
						<div id="tabname_<?php echo $key + 2?>" onclick="showtab('<?php echo $key + 2?>')" class="nonactive"><span><?php echo $product['title'];?></span></div>
						<?php endforeach;?>
					</td>
				</tr>
				<tr>
					<td class="content">
						<div id="tabcontent_1" data-count="<?php echo $complects['count'];?>" class="carousel slide show">
							<div class="carousel-inner">
								<div class="item active <?php echo $category != ComplectController::getTag() ? 'sub' : '' ?>" data-itemId="1">
									<?php foreach($complects['model'] as $complect): ?>
									<div class="tab-product <?php echo $complect->cartPattern; ?>">
										<div class="pd-qty-block"><input hidden value="1"></div>
                                        <div class="code pd-code"  style="display: none;">Код товара: <span><?php echo $complect->id;?></span></div>
										<div class="thumb">
											<a href="<?php echo $complect->link;?>">
												<span style="background-image: url('<?php echo $complect->image_url;?>')" class="img-vert">
                                                    <img style="display:none" src="<?php echo $complect->image_url;?>" alt="">
                                                </span>
                                                
											</a>
										</div>
										<div class="name"><a href="<?php echo $complect->link;?>"><?php echo $complect->name;?></a></div>
										<div class="price"><?php echo CurrencySys::exchange($complect->price).' '.CurrencySys::getLabel(); ?></div>
                                        <?php if ($complect->quantity != null):?>
                                            <?php if ($complect->quantity > 0):?>
                                                <div class="btn btn-buy-<?php echo $complect->id; ?> enabled" style="<?php echo $complect->inCart ? 'display: none;' : ''?>">
                                                    <a data-is_complect="1" data-id="<?php echo $complect->cartPattern; ?>" href="#" class="btn-buy-buy index-buy-but">Купить</a>
                                                </div>
                                                <div class="btn-buy-<?php echo $complect->id; ?> disabled"  style="<?php echo $complect->inCart ? '' : 'display: none;'?>">
                                                    <a data-is_complect="1" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
                                                </div>
                                            <?php else:?>
                                                <a class="catalog btn-buy-order" data-is_complect="1" href="#" id="pdc<?php echo $complect->id; ?>" style="font-size: 20px; line-height: 30px; margin-left: 28px;">Заказать</a>
                                            <?php endif;?>
                                        <?php else:?>
                                            <a class="catalog btn-buy-soon" data-is_complect="1" href="#" id="pdc<?php echo $complect->id; ?>" style="font-size: 15px; padding: 5px; line-height:23px; margin-left: 28px;">Уведомить о наличии</a>
                                        <?php endif;?>
									</div>
									<?php endforeach; ?>
								</div>
							</div>
                            <a class="carousel-control left" style="margin-top: 0;" href="#" data-slide="prev">
                                <img alt="" src="/images/pageListLeft.png" height="24" width="15">
                            </a>
                            <a class="carousel-control right" style="margin-top: 0;" href="#" data-slide="next">
                                <img alt="" src="/images/pageListRight.png" height="24" width="15">
                            </a>
						</div>
						<?php foreach($products as $key => $productGroup): ?>
						<div id="tabcontent_<?php echo $key + 2?>" data-category="<?php echo $category;?>" data-type="<?php echo $productGroup['type'];?>" data-count="<?php echo $productGroup['count'];?>" class="carousel slide hide">
							<div class="carousel-inner">
								<div class="item active <?php echo $category != ProductsController::getTag() ? 'sub' : '' ?>" data-itemId="1">
									<?php foreach($productGroup['model'] as $product): ?>
									<div class="tab-product <?php echo $product->cartPattern; ?>">
										<div class="pd-qty-block"><input hidden value="1"></div>
										<div class="code pd-code" style="display: none;">Код товара: <span><?php echo $product->id;?></span></div>
										<div class="thumb">
											<a href="<?php echo $product->link;?>">
												<span style="background-image: url('<?php echo $product->image_url;?>')" class="img-vert">
                                                    <img style="display:none" src="<?php echo $product->image_url;?>" alt="">
                                                </span>
											</a>
										</div>
										<div class="name"><a href="<?php echo $product->link;?>"><?php echo $product->brand0->name.' '.$product->model;?></a></div>
										<div class="price"><?php echo CurrencySys::exchange($product->price).' '.CurrencySys::getLabel(); ?></div>
                                        <?php if ($product->quantity != null):?>
                                            <?php if ($product->quantity > 0):?>
                                                <div class="btn btn-buy-<?php echo $product->id; ?> enabled" style="<?php echo $product->inCart ? 'display: none;' : ''?>">
                                                    <a data-is_complect="0"  href="#" data-id="pd<?php echo $product->id; ?>" class="btn-buy-buy index-buy-but">Купить</a>
                                                </div>
                                                <div class="btn-buy-<?php echo $product->id; ?> disabled"   style="<?php echo $product->inCart ? '' : 'display: none;'?>">
                                                    <a data-is_complect="0" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
                                                </div>
                                            <?php else:?>
                                                <a class="catalog btn-buy-order" data-is_complect="0" href="#"  style="font-size: 20px; line-height: 30px; margin-left: 28px;">Заказать</a>
                                            <?php endif;?>
                                        <?php else:?>
                                            <a class="catalog btn-buy-soon" data-is_complect="0" href="#"  style="font-size: 15px; padding: 5px; line-height:23px; margin-left: 28px;">Уведомить о наличии</a>
                                        <?php endif;?>
									</div>
									<?php endforeach; ?>
								</div>
							</div>
                            <a class="carousel-control left" style="margin-top: 0;" href="#" data-slide="prev">
                                <img alt="" src="/images/pageListLeft.png" height="24" width="15">
                            </a>
                            <a class="carousel-control right" style="margin-top: 0;" href="#" data-slide="next">
                                <img alt="" src="/images/pageListRight.png" height="24" width="15">
                            </a>
						</div>
						<?php endforeach; ?>
						<script>
							    $('.carousel').each(function(){
									$(this).carousel({
										interval: false
									});
								});
						</script>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>