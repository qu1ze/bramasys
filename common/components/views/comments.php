<?php
Yii::app()->clientScript->registerScript("commentShow","
	 $('.comment-show-link').on('click', '.comment-show', function () {
		$(this).toggleClass('comment-hide');
		$(this).toggleClass('comment-show');
		$(this).text('Скрыть')
		commentId = $(this).attr('comment-id');
		$('#comment' + commentId + ' .comment').slideDown();
	 });
	 
	 $('.comment-show-link').on('click', '.comment-hide', function () {
		$(this).toggleClass('comment-hide');
		$(this).toggleClass('comment-show');
		$(this).text('Показать все комментарии')
		commentId = $(this).attr('comment-id');
		$('#comment' + commentId + ' .comment:gt(2)').slideUp();
	 });
	 
	  $('.add-comment').on('click', function () {
		$('.comment-form-show').click();
		$('body').animate({
			scrollTop: $('#comment #post_comment:not(.response_form)').offset().top
		}, 200);
	 });
	", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript("commentResponse","
	 $('#comment').on('click', '.comment-response a', function () {
		if($(this).parent().children('.response_form').length == 0)
		{
			var response_form = $('#post_comment.response_form').clone();
			$(response_form).attr('id', $(this).attr('comment-id'));
			$(response_form).attr('comment-id', $(this).attr('comment-id'));
			$(this).parent().prepend($(response_form));
			$(response_form).slideDown();
			$(this).text('Скрыть');
                        
		}
		else
		{
			var response_form = $(this).parent().children('.response_form');
			$(response_form).slideUp('slow', function(){
				$(response_form).parent().children('a').text('Ответить');
				$(response_form).remove();
			});
                       
		}
	 });
	", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("usefulCount","
	 $('.useful-btn').on('click', function () {
		if($(this).hasClass('disabled'))
		    return false;
		$(this).addClass('disabled')
		var usefulBlock = $(this).parent();
		$.ajax(
		{
			url: '/comment/changeUsefulCount',
			data: {commentId: $(this).attr('comment-id')},
			dataType: 'json',
			type: 'post',
			success: function(data){
				if(typeof(data) == 'object')
				{
					if(data.title == 'Success')
					{
						var usefulCount = $(usefulBlock).children('div').children('div');
						$(usefulCount).slideUp('slow', function(){
							$(usefulCount).text(data.count);
							$(usefulCount).slideDown();
						});
					}
				}
			}
		});
	 });
	", CClientScript::POS_READY);
$commentsCount = $comments->getCount();
?>
<div class="comment-title"> 
	<h3>Отзывы <?php echo $this->title; ?></h3><span>(<?php echo Comment::model()->countByAttributes(array( "{$type}_id" => $contentId, 'parent_id' => null, 'status' => 1));; ?>)</span>
    <span class="sort">
        Сортировать:
        <?php if(isset($_GET['sort']) && $_GET['sort'] == 'useful_comments'): ?>
        <a href="<?php echo $this->getUrl(); ?>#comment">
                по дате
            </a>
            | по полезности
        <?php else:?>
            по дате |
            <a href="<?php echo $this->getUrl(); ?>?sort=useful_comments#comment">
                по полезности
            </a>
        <?php endif;?>
    </span>
</div>
<?php if($commentsCount > 0): ?>
<div class="add-comment">
    <a href="javasctipt://" style="font: 15px 'PTSN Bold';" onclick="">Добавить отзыв</a>
</div>
<?php endif; ?>
<?php $this->render('_comment_pattern', array('model' => $model, 'contentId' => $contentId, 'type' => $type));?>
<ul class="comments">
<?php
$ic=0;
$ferst=0;
foreach ($comments->getChilds(0) as $rootComment):
$comment = $rootComment['data'];
$criteria = new CDbCriteria();
$criteria->addCondition("comment_id = $comment->id");
$criteria->addCondition("ip_address = '".Yii::app()->request->userHostAddress."'");
if(Yii::app()->user->id !== null && !Yii::app()->user->getState('isAdmin'))
{
	$criteria->addCondition("client_id = ".Yii::app()->user->id);
}
$ic++;
$voteEnabled = CommentVote::model()->find($criteria) === null && !Yii::app()->user->getState('isAdmin');
$voteEnabled &= ($comment->author_id != Yii::app()->user->id || Yii::app()->user->id == null)

?>
<li class="comment  <?php echo $ic>5?'afterfive':'' ?>" id="comment<?php echo $comment->id; ?>">
<?php if ($ferst!==0): ?> 
    <hr>
<?php endif;
$ferst=1; ?>    
    <table>
		<tr class="comment-blocks">
			<td class="comment-container">
				<div class="comment-author-name">
					<?php echo $comment->authorName; ?>,<span class="comment-post-date"><?php echo $comment->post_date; ?></span>
				</div>
				<?php if($comment->rating > 0): ?>
					<div class="comment-rating">
						<font style="float:left;">Оценка товара:</font>
						<?php 
						$starsCount = $comment->rating;
						for($i = 0; $i < 5; $i++): ?>
						<div class="star <?php echo $starsCount > 0 ? 'on' : ''; ?>"><span style="width: 100%;"><?php echo $i + 1; ?></span></div>
						<?php 
						$starsCount--;
						endfor; ?>
					</div>
				<?php endif; ?>
				<div class="comment-content">
					<font style="float:left;">Комментарий:</font>
					<span><?php echo $comment->content; ?></span>
				</div>
                                <?php if($plus_minus): ?>
                                    <?php if(!empty($comment->pros)): ?>
                                    <div class="comment-pros">
                                            <font style="float:left;">Плюсы:</font>
                                            <span><?php echo $comment->pros; ?></span>
                                    </div>
                                    <?php endif; ?>
                                    <?php if(!empty($comment->cons)): ?>
                                    <div class="comment-cons">
                                            <font style="float:left;">Минусы: </font>
                                            <span><?php echo $comment->cons; ?></span>
                                    </div>
                                    <?php endif; ?>
                                <?php endif; ?>
				<?php if(!$comment->status): ?>
				<div style="
					position: absolute;
					width: 100%;
					height: 100%;
					top: 0;
					margin: 0;
					background-color: rgba(220,220,250,0.7);
				">
					<table style="height: 100%;">
						<tr>
							<td style="text-align: center; font: 18px 'PTSN Bold';">
								Ваш отзыв не подтвержден администрацией
							</td>
						</tr>
					</table>
				</div>
				<?php endif; ?>
				<div class="comment-response">
					<a class="respone-link<?php echo $comment->id; ?>" comment-id="<?php echo $comment->id; ?>">Ответить</a>
				</div>
			</td>
			<td class="useful-block">
                <div style="height: 23px; overflow-y: hidden;"><div  style="display: inline-block"><?php echo $comment->useful_count; ?></div> голосов</div>
				<div class="useful-btn<?php echo $voteEnabled ? '' : ' disabled'?>" comment-id="<?php echo $comment->id; ?>">
					<a>Отзыв полезен</a>
				</div>
			</td>
		</tr>
	</table>
        
	<?php 
	$this->render('_comment', array(
			'comments' => $comments,
			'parentId' => $rootComment['id'],
			'level' => 1,
			)
		);
	?>
	<?php if($this->commentCount > 3): ?>
	<div class="comment-show-link">
		<a class="comment-show" comment-id="<?php echo $comment->id; ?>">Показать все комментарии</a>
	</div>
	<?php endif;
	$this->commentCount = 0;
	?>
	
</li>
<?php endforeach;
    echo '<div style="display:none" id="comment_count" count='.$ic.'></div>'
?>
</ul>
<div id="up_hr" class="up-to">
            <a href="#" class="up">Вверх</a>
            <a href="#"><img src="/images/upArrow.png"></a>
        </div>
<br>