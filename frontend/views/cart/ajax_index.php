<div id="cart-box">
<div class="cart-header">
	<h1>Корзина</h1>
	<a class="close hover" id="closeCart" href="#"></a>
</div>
<div class="cart-content">
<table class="cart-table">
<thead>
	<tr class="header">
        <td class="product-img-cart" style="text-align: center;">Товар</td>
        <td class="product-price-cart">Стоимость</td>
        <td class="input-cart">Кол-во</td>
        <td class="checkbox-cart">Монтаж</td>
        <td class="product-price-cart">Сумма</td>
    </tr>
</thead>
</table>
<div class="cart-scroll">
<table class="cart-table">
<tbody>
	<?php
	$priceSum = 0;
	foreach ($cart as $id => $prod)
	{
		if($id === 'complect')
		{
			foreach ($prod as $id => $complectProd)
			{
				$product = Complect::model()->findByPk($id);
				$checked = $complectProd['mounting'];
				$amount = $complectProd['price'] * $complectProd['count'];
				$priceSum += $amount;
				$this->renderPartial('_view', array(
					'product' => $product,
					'amount' => $amount,
					'checked' => $checked,
					'isComplect' => true,
					'price' => $complectProd['price'],
					'count' => $complectProd['count'],
					'id' => $id,
				));
			}
		}
		else
		{
			$product = Products::model()->findByPk($id);
			$checked = $prod['mounting'];
			$amount = $prod['price'] * $prod['count'];
			$priceSum += $amount;
			$this->renderPartial('_view', array(
				'product' => $product,
				'amount' => $amount,
				'checked' => $checked,
				'isComplect' => false,
				'price' => $prod['price'],
				'count' => $prod['count'],
				'id' => $id,
			));
		}
	}
	?>
</tbody>
</table>
</div>
</div>
<table class="cart-table" style="padding-right: 10px;">
    <tr>
        <td colspan="6">
                <a class="cart-btn" href="<?php echo app()->getBaseUrl(true); ?>/order/placeorder">Oформить заказ</a>

                <a class="link-cart"  id="closeCart" href="#">Продолжить покупки</a>

            <div class="total-cart">
                <div class="total-price-text">
                    <p>Итого:</p>
                    <p>(без учета доставки,
                        без учета монтажа)</p>
                </div>

                <div class="total-price" >
					<p id="firstPrice">
						<?php echo CurrencySys::exchange($priceSum).' '.CurrencySys::getLabel(); ?>
					</p>
					<?php if (!CurrencySys::isDefaultInCookie()):?>
						<p id="lastPrice"><?php echo $priceSum.' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY)?></p>
					<?php endif; ?>
                </div>
            </div>
        </td>
    </tr>
</table>
</div>