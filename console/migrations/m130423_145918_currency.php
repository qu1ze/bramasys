<?php

class m130423_145918_currency extends CDbMigration
{
	public function up()
	{
			$this->execute("
				CREATE TABLE IF NOT EXISTS `currency` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `name` varchar(255) NOT NULL,
				  `koef` float NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;
		");		
	}

	public function down()
	{
		$this->dropTable("currency");
	}

}