<?php
/* @var $this ProductsController */
/* @var $model Products */
$scrollToPost = 0;
$scrollToComent =0;
if($scroll == 'post_comment')
{
	$scrollToPost = 1;
} elseif ($scroll == 'comment-title')
{
    $scrollToComent =1;
}

Yii::app()->clientScript->registerScript("scrollTo","
    function scrolltocom(){
		if($scrollToPost || window.location.hash=='#scroll=post_comment')
		{
			$('.comment-form-show').click();
			$('body').animate({
				scrollTop: $('#comment #post_comment:not(.response_form)').offset().top
			}, 200);
		}
        if($scrollToComent || window.location.hash=='#scroll=comment-title')
		{
			$('body').animate({
				scrollTop: $('.comment-title').offset().top
			}, 200);
		}
        }
        scrolltocom();
        $('.scrolltocom').on('click', function(){
        window.location=$(this).attr('href');
        scrolltocom();
        
        });
", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript("changeTab","		
		function changeTab(tab)
		{
			$('.productTab').removeClass('active');
			$('.productTab').addClass('nonactive');
			$(tab).removeClass('nonactive');
			$(tab).addClass('active');
			var tabId = $(tab).attr('tab-id');
			$('.tabcontent').removeClass('show');
			$('.tabcontent').addClass('hide');
			if(tabId == 1)
			{
				$('#tabcontent_1').removeClass('hide');
				$('#tabcontent_1').addClass('show');
				$('#tabcontent_2').removeClass('hide');
				$('#tabcontent_2').addClass('show');
				$('#tabcontent_3').removeClass('hide');
				$('#tabcontent_3').addClass('show');
                if($('#comment_count').attr('count')>6)
                    {
                    $('.comment-show').show();
                    $('.afterfive').hide();
                    }
			}
			else
			{
				$('#tabcontent_' + tabId).removeClass('hide');
				$('#tabcontent_' + tabId).addClass('show');
        if (tabId == 3) 
          {
          $('.afterfive').show();
          $('.comment-show').hide();
          }
			}
		}

		if(window.location.hash == '#tab=features')
		{
			changeTab($('.productTab')[1]);
		} else if (window.location.hash == '#tab=customer-reviews') {
      
			changeTab($('.productTab')[2]);
		} else if (window.location.hash == '#tab=photo-and-video-review') {
			changeTab($('.productTab')[3]);
		} else if (window.location.hash == '#tab=instruction') {
			changeTab($('.productTab')[4]);
		}
	
		$('.productTab').on('click', function(){
			changeTab(this);
		});
", CClientScript::POS_READY);

Yii::app()->getClientScript()->registerCssFile('/css/product.css');

$this->breadcrumbs = CMap::mergeArray(
    app()->categoryController->getCategoryBcs($model->category0->name, true),
    array(
        $model->name,
    ));
$categoryController = Yii::app()->categoryController;
$categoryTitle = '';
foreach ($categoryController->addParentsIds($model->category) as $categoryId)
{
	$categoryTitle .= ' '.$categoryController->getTitle($categoryId).'.';
}
$this->categoryTitle = "$model->name. $categoryTitle";
?>
<div id="container" class="show">
    
    <?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
    ?>
	<div class="pd-list-block pd<?php echo $model->id; ?>">
		<div class="pd-title">
			<h1><?php echo !empty($model->h1) ? $model->h1 : $model->name ?></h1>
		</div>
		<div class="pd-code" >
			Код товара: <?php echo $model->actualId;?><span><?php echo $model->id; ?></span>
		</div>
		<div class="pd-detail">
			<div class="pd-image-block">
				<div class="pd-image-large">
					<div class="image-container">
						<img src="<?php echo CHtml::encode($model->image_url); ?>"  title="<?php echo $model->photosTitleAndAlt[0]['title']; ?>" alt="<?php echo $model->photosTitleAndAlt[0]['alt']; ?>">
					</div>
					<div class="labes">
						<ul>
						<?php if($model->novelty): ?><li><span class="labes-new">НОВИНКА</span></li><?php endif; ?>
						<?php if($model->top_seller): ?><li><span class="labes-sail-leader">ЛИДЕР ПРОДАЖ</span></li><?php endif; ?>
						<?php if($model->price_of_the_week): ?><li><span class="labes-price-of-the-week">ЦЕНА НЕДЕЛИ</span></li><?php endif; ?>
						</ul>
					</div>
				</div>
                <div class="image-container" style="margin-top: 10px;">
                    <?php foreach($model->photoArray as $key => $image_url):?>
                        <div class="small-image-container small-img" title="<?php echo $model->photosTitleAndAlt[$key]['title']; ?>" alt="<?php echo $model->photosTitleAndAlt[$key]['alt']; ?>" img-indx="<?php echo $key; ?>" style="background-image: url('<?php echo str_replace('\\','/', $image_url); ?>')"></div>
                    <?php endforeach; ?>
                </div>
			</div>
			<div class="pd-info-block">
				<div class="pd-price-block">
					<?php if(!empty($model->old_price)): ?>
					<div class="pd-old-price"><?php echo CurrencySys::exchange($model->old_price).' '.CurrencySys::getLabel(); ?></div>
					<?php endif; ?>
					<div class="pd-new-price"><?php echo CurrencySys::exchange($model->price).' '.CurrencySys::getLabel(); ?></div>
					<?php if (!CurrencySys::isDefaultInCookie()):?>
						<div class="pd-usd-price"><?php echo CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY).' '.$model->price; ?></div>
					<?php endif; ?>
					
				</div>
				<div class="pd-qty-block">
					<?php if ($model->quantity != null):?>
						<?php if ($model->quantity > 0):?>
						<span class="isset"><span class="tick"></span>Есть в наличии</span>
						<?php else:?>
						<span class="isset" style="color:#04ce34; padding-left: 30px;">Под заказ</span>
						<?php endif;?>
					<?php else:?>
					<span class="isset" style="color:#e411c9;">Скоро в продаже</span>
					<?php endif;?>
					<p>Количество: <input type="text" size="5" value="1"></p>
				</div>
				<div class="pd-buy-block">
					<?php if ($model->quantity != null):?>
						<?php if ($model->quantity > 0):?>
						<div class="btn-buy-<?php echo $model->id; ?> enabled" style="<?php echo $model->inCart ? 'display: none;' : ''?>">
							<a class="catalog btn-buy-buy" data-is_complect="0" href="#" data-id="pd<?php echo $model->id; ?>">Купить</a>
							<p><a class="catalog href-buy-buy" data-is_complect="0" href="#" data-id="pd<?php echo $model->id; ?>">Купить и заказать монтаж</a></p>
						</div>
						<div class="btn-buy-<?php echo $model->id; ?> disabled" data-id="pd<?php echo $model->id; ?>"  style="<?php echo $model->inCart ? '' : 'display: none;'?>">
							<a data-is_complect="0" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
						</div>
						<?php else:?>
						<a class="catalog btn-buy-order" data-is_complect="0" href="#" data-id="pd<?php echo $model->id; ?>">Заказать</a>
						<?php endif;?>
					<?php else:?>
					<a class="catalog btn-buy-soon" data-is_complect="0" href="#" data-id="pd<?php echo $model->id; ?>" style="font-size: 18px; line-height:23px;">Уведомить о наличии</a>
					<?php endif;?>
				</div>

				<div class="pd-action">
					<ul>
						<li>
							<img src="/images/compare.png" alt="" title="">
							<a class="add-to-compared" data-productId="<?php echo $model->id; ?>" inCompare="<?php echo $model->inCompare;?>" href="<?php echo $model->inCompare ? app()->getBaseUrl(true).'/compare' : '#';?>">
								<?php echo $model->inCompare ? 'К сравнению' : 'Сравнить';?>
							</a>
						</li>
						<?php if(Yii::app()->user->id != null && !Yii::app()->user->getState('isAdmin')): ?><li>
							<img src="/images/bookmark.png" alt="" title="">
							 <a class="add-bookmark" data-isComplect="0" mark-id="<?php echo $model->id;?>" <?php echo $model->inBookmarks ? 'href="'.app()->getBaseUrl(true).'/user/profile/bookmarks">Уже в закладках' : '>В закладки';  ?></a> 
						</li><?php endif; ?>
						<li>
							<img src="/images/print.png" alt="" title="">
							<a href="" target="_blank">Печатная версия</a>
						</li>
					</ul>
				</div>

				<div class="pd-short-text">
					<?php echo CHtml::encode($model->description); ?>
				</div>

				<div class="pd-add-btn-block">
					<ul>
						<li pageName="minioplata">
							<p><img src="/images/payment.png" alt="" title=""></p>
							<p><a class="nohover-pd-a">Оплата</a></p>
							<p class="desc"><a href="#">Гибкие условия</a></p>
						</li>
						<li pageName="minidostavka">
							<p><img src="/images/delivery.png" alt="" title=""></p>
							<p><a class="nohover-pd-a">Доставка</a></p>
							<p class="desc"><a href="#">По всей Украине</a></p>
						</li>
						<li pageName="minivosvrat">
							<p><img src="/images/return.png" alt="" title=""></p>
							<p><a class="nohover-pd-a">Возврат</a></p>
							<p class="desc"><a href="#">В течении 14-ти дней</a></p>
						</li>
						<li pageName="minigarantia">
							<p><img src="/images/warranty.png" alt="" title=""></p>
							<p><a class="nohover-pd-a">Гарантия</a></p>
							<p class="desc"><a href="#">От 12-ти месяцев</a></p>
						</li>
						<li pageName="minimontazh" class="clear-bgrd">
							<p><img src="/images/installation.png" alt="" title=""></p>
							<p><a class="nohover-pd-a">Монтаж</a></p>
							<p class="desc"><a href="#">Киев, Запорожье, Днепр.</a></p>
						</li>
					</ul>
				</div>
                 <div class="rate-count-prod">
                        <div class="vote-wrap">
                            <div class="comment-rating">
                                <?php $this->widget('common.components.RatingStars', array('rating' => $model->rating)); ?> <?php echo $model->votedCount;?> оц.
                            </div>
                        </div>
                  <div class="pd-reviews-block"> 
                       <?php if(!$model->hasComments):?>
                            <a class="scrolltocom" href="<?php echo $model->link."#scroll=post_comment"; ?>">
                            Оставить отзыв
                            <?php else:?>
                            <a class="scrolltocom" href="<?php echo $model->link."#scroll=comment-title"; ?>">
                            <?php echo $model->commentsCount;?> отзывов
                            <?php endif;?>
                            </a>
                            </div>
                    </div>
                <div style="float:left; margin-left: 30px"> <?php $this->widget('common.components.SocButtons'); ?></div>
			</div>
			<div class="tabbs">
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<td class="name" style="border-bottom: 1px solid #CCD6D5">
							<div tab-id="1" class="productTab active">
								<p><a href="#">Основное</a></p>
							</div>
							<div tab-id="2" class="productTab nonactive">
								<p><a href="#tab=features">Характеристики</a></p>
							</div>
							<div tab-id="3" class="productTab nonactive">
                                                            <p><a href="#tab=customer-reviews">Отзывы <span class="count">(<?php echo $model->commentsCount ?>)</span></a></p>
							</div>
							<div tab-id="4" class="productTab nonactive">
								<p><a href="#tab=photo-and-video-review">
										Фото
										<span class="count">(<?php echo $model->photoCount; ?>)</span>
										и видео
										<span class="count">(<?php echo ProductVideo::model()->countByAttributes(array('product_id' => $model->id)); ?>)</span></a></p>
							</div>
							<div tab-id="5" class="productTab nonactive">
								<p><a href="#tab=instruction">Тех. документация</a></p>
							</div>
						</td>
					</tr>
					<tr>
					<td class="content">
						<div id="tabcontent_1" class="tabcontent show">
							<h2>Подробное описание <?php echo $model->name;?></h2>
							<p><?php echo $model->full_description; ?></p>
							<div class="up-to">
								<a href="#" class="up">Вверх</a>
								<a href="#"><img src="/images/upArrow.png"></a>
							</div>
						</div>
						<div id="tabcontent_2" class="tabcontent show">
							<div class="compare_characteristic">
                                <h2>Характеристики <?php echo $model->name;?></h2>
								<?php $this->renderPartial('_specs', array('specifications' => $specifications)) ?>
							</div>
							<div class="up-to">
								<a href="#" class="up">Вверх</a>
								<a href="#"><img src="/images/upArrow.png"></a>
							</div>
						</div>
						<div id="tabcontent_3" class="tabcontent show">
                                                        
							<div id="comment">
								<?php $this->widget('common.components.CommentTree', array('contentId' => $model->id, 'contentType' => 'product', 'title' => "к $model->name")); ?>
							</div>
                                                  
							
						</div>
						<div id="tabcontent_4" class="tabcontent hide">
							<h2>Фотообзор <?php echo $model->name; ?></h2>
							<div class="image-container">
								<?php foreach($model->photoArray as $key => $image_url):?>
                                <div class="small-image-container small-img" title="<?php echo $model->photosTitleAndAlt[$key]['title']; ?>" alt="<?php echo $model->photosTitleAndAlt[$key]['alt']; ?>" style="width: 200px; height: 200px; background-image: url('<?php echo str_replace('\\','/', $image_url); ?>');" img-indx="<?php echo $key; ?>"></div>
								<?php endforeach; ?>
							</div>
							<h2>Видеообзор <?php echo $model->name; ?></h2>
							<?php $this->widget('common.components.EmbedVideos', array('videos' => CHtml::listData(ProductVideo::model()->findAllByAttributes(array('product_id' => $model->id)), 'id', 'link'))); ?>
							<div class="up-to">
								<a href="#" class="up">Вверх</a>
								<a href="#"><img src="/images/upArrow.png"></a>
							</div>
						</div>
						<div id="tabcontent_5" class="tabcontent hide">
                                                     	<?php $this->widget('common.components.SupportFiles', array('productId' => $model->id)); ?>
							<div class="up-to">
                                                            <a href="#" class="up">Вверх</a>
                                                        <a href="#"><img src="/images/upArrow.png"></a>
                                                        </div>
						</div>
					</td>
				</tr>
			</table>
			</div>
		</div>
</div>
    </div>

<?php $this->renderPartial('_gallery', array('model' => $model, 'inCart' => $model->inCart)) ?>