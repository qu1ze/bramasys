<?php

class m130615_185547_add_is_complect_to_order extends CDbMigration
{
	public function up()
	{
		$this->execute("
                ALTER TABLE  `order_product` ADD  `is_complect` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `product_id`
            ");
	}

	public function down()
	{
		$this->execute("
				ALTER TABLE `order_product` DROP `is_complect`
                ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}