<?php

class m130515_160749_create_product_spec_value_table extends CDbMigration
{
	public function up()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `product_spec_value` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `product_id` int(11) NOT NULL,
                  `spec_id` int(11) NOT NULL,
                  `value` varchar(255),
                  PRIMARY KEY (`id`),
                  KEY `product_id` (`product_id`),
                  KEY `spec_id` (`spec_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
            $this->execute("
                ALTER TABLE `product_spec_value`
                  ADD CONSTRAINT `product_spec_value_ibfk_2` FOREIGN KEY (`spec_id`) REFERENCES `specification` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                  ADD CONSTRAINT `product_spec_value_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
              ");
	}

	public function down()
	{
            $this->dropTable("product_spec_value");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}