<?php

class NewsController extends Controller
{		
	public function actionCreate()
	{
		$model = new News; 
		
		$this->performAjaxValidation($model);
		
		if (isset($_POST['News']))
		{	
			$transaction = Yii::app()->db->beginTransaction();
			try
			{
				$model->attributes = $_POST['News'];			
				if (!$model->save())
					throw new Exception('Bad News');
				
				$transaction->commit();
				$this->redirect(array('/admin/news'));
			} catch (Exception $e) {			
				$transaction->rollback();
				var_dump($model->getErrors());
			}
		}
		
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new News();	

		if (isset($_POST['News']) && Yii::app()->request->isAjaxRequest)
		{
			$transaction = Yii::app()->db->beginTransaction();
			try
			{
				$model->attributes = $_POST['News'];								
				if (!$model->save())	
					throw new Exception('Не возможно создать новость');
				
				$transaction->commit();
				echo json_encode(array('title'=>'Success!', 'msg'=>'Новость успешно создана'));							
				app()->end();
			} catch (Exception $e) {			
				$transaction->rollback();
				echo json_encode(array('title' => 'Ошибка!', 'msg' => $e->getMessage()));		
			}
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new News('search');
		
		if (isset($_GET['News']))
			$model->attributes = $_GET['News'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = News::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/news'));				
				
		if (isset($_POST['News']))
		{	
			$transaction = Yii::app()->db->beginTransaction();
			try
			{
				$model->attributes = $_POST['News'];
				if (!$model->save())
					throw new Exception('Не удалость обновить новость/акцию');
					
				$transaction->commit();
				$this->redirect(array('/admin/news'));
			} catch (Exception $e) {			
				$transaction->rollback();
				var_dump($e->getMessage());		
			}
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('News');	
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			$es->update();		
			$transaction->commit();
			die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Новость успешно отредактирована')));			
		} catch (Exception $e) {			
			$transaction->rollback();
			die(json_encode(array('Error' => true, 'title'=>'Error', 'msg'=> 'Ошибка')));			
		}			
	}	
	
	public function actionAjaxDelete($id)
	{		
		if (($model = News::model()->findByPk($id)) !== null)
		{			
			if (!$model->newsProducts && !$model->relatedNews)
				if ($model->delete())												
					echo json_encode(array('title' => 'Success', 'msg' => 'Новость успешно удалена'));				
				else
					echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить новость'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Существуют связи с новостью/продуктом'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Новость не найдена'));
	}
	
}