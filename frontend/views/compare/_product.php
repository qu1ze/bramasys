<?php $inCart = isset(Yii::app()->session['cart'][$product->id]); ?>
<li class="compareProduct compareCarouselItem pd<?php echo $product->id; ?>">
	<table>
		<tr>
			<td id="productTitle">
				<img class="removeProduct" data-productId="<?php echo $product->id; ?>" style="padding-top:9px;" src="/images/cross.gif" width="10">
				<a class="removeProduct" data-productId="<?php echo $product->id; ?>" href="#">удалить</a>
				<a class="imgUrl" href="<?php echo $product->link; ?>">
					<span style="background-image: url('<?php echo $product->image_url; ?>')" class="img-vert">
						<img style="display:none" src="<?php echo $product->image_url; ?>" alt="">
					</span>
				</a>
			</td>
		</tr>
		<tr>
			<td id="productRating">
				<div class="name">
                    <a style="border-bottom: 1px solid #9FA4A6;" href="<?php echo $product->link;?>">
						<?php echo $product->name;?>
					</a>
				</div>
				<div class="rate-count" style="display: inline-block;">
					<div class="vote-wrap">
						<?php  ?>
						<div class="comment-rating">
                            <?php $this->widget('common.components.RatingStars', array('rating' => $product->rating)); ?>
						</div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td id="productPrice">
				<div class="pd-qty-block" style="display: none;"><input hidden value="1"></div>
				<div class="code pd-code" style="display: none;">Код товара: <span><?php echo $product->id;?></span></div>
				<div class="price"><?php echo CurrencySys::exchange($product->price).' '.CurrencySys::getLabel(); ?></div>
				<?php if (!CurrencySys::isDefaultInCookie()):?>
					<div class="pd-usd-price"><?php echo CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY).' '.$product->price; ?></div>
				<?php endif; ?>
				<?php if ($product->quantity != null):?>
					<?php if ($product->quantity > 0):?>
					<div class="btn-buy-<?php echo $product->id; ?> enabled" style="<?php echo $inCart ? 'display: none;' : ''?>">
						<a class="catalog btn-buy-buy" data-is_complect="0" href="#" id="pd<?php echo $product->id; ?>">Купить</a>
					</div>
					<div class="btn-buy-<?php echo $product->id; ?> disabled" id="pd<?php echo $product->id; ?>"  style="<?php echo $inCart ? '' : 'display: none;'?>">
						<a data-is_complect="0" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
					</div>
					<?php else:?>
					<a class="catalog btn-buy-order" data-is_complect="0" href="#" id="pd<?php echo $product->id; ?>">Заказать</a>
					<?php endif;?>
				<?php else:?>
				<a class="catalog btn-buy-soon" data-is_complect="0" href="#" id="pd<?php echo $product->id; ?>" style="font-size: 18px; line-height:23px;">Уведомить о наличии</a>
				<?php endif;?>
			</td>
		</tr>
	</table>
</li>