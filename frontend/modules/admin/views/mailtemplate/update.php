<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldRow($model, 'name');?>
    <?php echo $form->textFieldRow($model, 'from');?>
    <?php echo $form->textFieldRow($model, 'to');?>
    <?php echo $form->textFieldRow($model, 'subject');?>
	<?php echo $form->ckEditorRow($model, 'content', array('class'=>'span4', 'rows'=>5)); ?>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	
<?php $this->endWidget(); ?>