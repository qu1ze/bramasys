<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->datepickerRow($model, 'create_time'); ?>
	<?php echo $form->dropDownListRow($model , 'status', $model->getOrderStatuses());?>

	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'type' => 'danger',
			'size' => 'normal',
			'label' => 'Удалить',
			'url' => Yii::app()->createUrl('/admin/products/delete', array('id' => $model->id))
		)); 
	?>
	
	
<?php $this->endWidget(); ?>

