<?php

class SupportController extends Controller
{
	public function actionUpload()
	{
		$model = new SupportFile; 
				
		if (isset($_POST['SupportFile']) && isset($_FILES['SupportFile']))
		{			
			$model->attributes = $_POST['SupportFile'];
			$model->file = $_FILES['SupportFile'];
			if ($model->save())
				$this->redirect(array('/admin/support'));
		}
				
		$this->render('upload', array(
			'model' => $model, 			
		));
	}
	
	public function getCategories($emptyCategory = false)
	{
		$data = CHtml::listData(Category::model()->findAllByAttributes(array('tag' => CategoryTag::model()->findByAttributes(array('name' => 'catalog'))->id)), 'id', 'title');
		return $emptyCategory ? array('' => '') + $data : $data;	
	}
	
	public function getSubCategories($parentCategoryId, $emptyCategory = false)
	{
		$categoryIds = Yii::app()->categoryController->getChildrenIds($parentCategoryId);
		$data = CHtml::listData(Category::model()->findAllByPk($categoryIds), 'id', 'title');		
		return $emptyCategory ? array('' => '') + $data : $data;	
	}
	
	public function actionIndex()
	{		
		$model = new SupportFile('search');
		
		if (isset($_GET['SupportFile']))
			$model->attributes = $_GET['SupportFile'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = SupportFile::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/support'));				
				
		if (isset($_POST['SupportFile']))
		{			
			$model->attributes = $_POST['SupportFile'];
			if ($model->save())
				$this->redirect(array('/admin/support'));
		}
				
		$this->render('update', array(
			'model' => $model,
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('SupportFile');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Файл успешно отредактирован')));			
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = SupportFile::model()->findByPk($id)) !== null)
		{
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Файл успешно удален'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить файл'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Файл не найден'));
	}
}