<?php
/* @var $this TermController */
/* @var $model Term */
?>
<div id="container">
	<?php
	$this->breadcrumbs = CMap::mergeArray(
		app()->categoryController->getCategoryBcs($this->categoryName, true),
		array(
			$model->title,
		));

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
	?>
	<div class="content_term" itemscope itemtype="http://webmaster.yandex.ru/vocabularies/enc-article.xml">
		<h3 itemprop="title"><?php echo $model->title; ?></h3>
		<div itemprop="content" style="display: inline-block;">
			<?php if(!empty($model->photo)): ?><img itemprop="photo" style="margin: 10px; width: 200px; float: left;" src="<?php echo $model->photo; ?>"></img><?php endif; ?>
			<?php echo $model->content; ?>
		</div> 
		<?php if(!empty($model->references)): ?><br><span>Список литературы:</span><br><pre style="background: none; border: none; margin: 0; padding-left: 0;" itemprop="references"><?php echo $model->references; ?></pre><?php endif; ?> 
		<div>
			<?php if(!empty($model->author)): ?><span itemprop="author"><?php echo $model->author; ?></span><br><?php endif; ?>
			<?php if(!empty($model->source)): ?><span itemprop="source"><?php echo $model->source; ?></span><br><?php endif; ?>
			<?php if(!empty($model->source_date)): ?><span itemprop="source-date"><?php echo $model->source_date; ?></span><br><?php endif; ?>
		</div>
	</div>
</div>