<?php

class m130514_102532_create_specification_table extends CDbMigration
{
	public function up()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `specification` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
              ");
	}

	public function down()
	{
            $this->dropTable("specification");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}