<?php
class CurrencySys
{
	const k = 8;
	
	const USD = 0;
	const UAH = 1;
	const DEFAULT_CURRENCY = self::USD;
	
	private static $labels = array(
		self::UAH => 'грн.',
		self::USD => '$',
	);
    
    public static function getKof()
    {
        if (Yii::app()->request->cookies['currency']->value==self::UAH)
        {
            return 1;
        }
        else{
            if(($input = Currency::model()->findByPK(self::UAH)) === null)
						return self::k;
					return $input->koef;
        }
            
    }
	
	public static function exchange($value = 0, $currency = -1, $fromThis = null)
	{
		if(!isset(Yii::app()->request->cookies['currency']->value))
		{
			$defCurrency = Currency::model()->findByAttributes(array('name' => array('Гривна', 'гривна', 'UAH', 'uah', 'грн.', 'грн')));
			if($defCurrency !== null)
				Yii::app()->request->cookies['currency'] = new CHttpCookie('currency', $defCurrency->id);
			else
				Yii::app()->request->cookies['currency'] = new CHttpCookie('currency', self::DEFAULT_CURRENCY);
		}
		if($currency == -1)
			$currency = Yii::app()->request->cookies['currency']->value;
		if(empty($fromThis))
		{
				if($currency != Yii::app()->request->cookies['currency']->value)
				{
					if(($input = Currency::model()->findByPK(Yii::app()->request->cookies['currency']->value)) === null)
						return (round(100 * $value * self::k)/100);
					return (round(100 * $value * $input->koef)/100);
				}
			
			if(($input = Currency::model()->findByPK($currency)) === null)
				return $value;
			else
				return (round(100 * $value * $input->koef)/100);
		}
		else
		{
			if ($fromThis == $currency) return $value;
			if(($input = Currency::model()->findByPK($fromThis)) !== null)
				$value = $value / $input->koef;
			if(($input = Currency::model()->findByPK($currency)) !== null)
				$value = $value * $input->koef;
			return (round(100*$value)/100);
		}
		return $value;
	}
	
	public static function getLabel($currency = -1)
	{
		if(empty(Yii::app()->request->cookies['currency']->value))
			Yii::app()->request->cookies['currency'] = new CHttpCookie('currency', self::DEFAULT_CURRENCY);
		if($currency == -1)
			$currency = Yii::app()->request->cookies['currency']->value;
		if($currency == self::DEFAULT_CURRENCY)
			return self::$labels[$currency];
		$curModel = Currency::model()->findByPk($currency);
		if($curModel == null)
			return '';
		return $curModel->label;
	}
	
	public static function isDefaultInCookie()
	{
		if(empty(Yii::app()->request->cookies['currency']->value))
			Yii::app()->request->cookies['currency'] = new CHttpCookie('currency', self::DEFAULT_CURRENCY);
		return Yii::app()->request->cookies['currency']->value == self::DEFAULT_CURRENCY;
	}
}
?>