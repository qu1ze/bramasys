<?php

class SlidesController extends Controller
{		
	public function actionCreate()
	{
		$model = new Slides(); 
				
		if (isset($_POST['Slides']))
		{			
			$model->attributes = $_POST['Slides'];
			if ($model->save())
				$this->redirect(array('/admin/slides'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new Slides();	

		if (isset($_POST['Slides']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['Slides'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Слайд упешно создан'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать слайд'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new Slides('search');
		
		if (isset($_GET['Slides']))
			$model->attributes = $_GET['Slides'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = Slides::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/slides'));				
				
		if (isset($_POST['Slides']))
		{			
			$model->attributes = $_POST['Slides'];
			if ($model->save())
				$this->redirect(array('/admin/slides'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Slides');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Слайд упешно отредактирован')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
                    Slides::model()->deleteByPk( $id );					
                    Yii::app()->user->setFlash('success', "Слайд упешно удален");
		}
		
		$this->redirect(array('/admin/slides'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = Slides::model()->findByPk($id)) !== null)
		{
                    if ($model->delete())
                            echo json_encode(array('title' => 'Success', 'msg' => 'Слайд упешно удален'));
                    else
                            echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить слайд'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Слайд не найден'));
	}
	
}