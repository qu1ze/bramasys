<?php

class PartnersController extends StaticController
{
	public function actionIndex()
	{
		$categories = app()->categoryController->getChilds(ProductsController::getTag());
		$partnersCategories = array();
		foreach ($categories as $category)
		{
			$categoriesIds = Yii::app()->categoryController->getChildrenIds($category['id']);
			$brandsCriteria = new CDbCriteria();
			$brandsCriteria->with = array('products'=>array('alias'=>'products',));
			$brandsCriteria->addInCondition('`products`.`category`', $categoriesIds);
			$partnersCategories[$category['data']['title']] = Brand::model()->findAll($brandsCriteria);
		}
		$this->render('index', array(
			'partnersCategories' => $partnersCategories,
		));
	}
}
