<?php
/**
 * Controller.php
 *
 * @author: Oleg Meleshko <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:55 AM
 */
class Controller extends CController
{

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

	public $currentUserModel = null;

	/**
	 * @return array list of action filters (See CController::filter)
	 */
	public function filters()
	{
		return array('accessControl');
	}
	/**
	 * @return array rules for the "accessControl" filter.
	 */
	public function accessRules()
	{
		return array(
			// not logged in users should be able to login and view captcha images as well as errors
			array('allow', 'actions' => array('registration','ajaxphonevalidation')),
			// logged in users can do whatever they want to
			array('allow', 'users' => array('@')),
			// not logged in users can't do anything except above
			array('deny'),
		);
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === $model->formId)
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	protected function checkFlashes()
	{
		foreach(Yii::app()->user->getFlashes() as $key => $message)
			echo '<div class="alert alert-' . $key . '">' . trim(ucfirst($key)) . '! ' . $message . "</div>\n";
	}

	protected function beforeAction($action)
	{
		if(Yii::app()->user->getState('isAdmin') || isset($_GET['id']))
		{
            if($action->id !== 'error')
			    throw new CHttpException(404);
		}
		$_GET['id'] = Yii::app()->user->id;
        return parent::beforeAction($action);
	}
}
