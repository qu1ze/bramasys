<?php
	Yii::app()->clientScript->registerScript("auto_desc", "
        $('.full_desc_field').on('change', function(){
            var full_desc_field = $(this);
            var desc_field = $('.desc_field');
            if (full_desc_field.val())
            {
                var desc = full_desc_field.val().substring(0, 255);
                if (desc_field.val() != desc)
                {
                    if (!desc_field.val())
                        desc_field.val(desc);
                    else
                    {
                        $('.desc').val(desc);
                        $('#confirmReplace').dialog('open');
                    }
                }
            }
        });
    ", CClientScript::POS_READY);
	
	Yii::app()->clientScript->registerScript("show_sub_categories", "
		$('#SupportFile_category_id').on('change', function(){
			$('.sub-category select').val('');
			$('.sub-category select').attr('name', '');
			$('.sub-category').css('display', 'none');
			$('#cat' + $('#SupportFile_category_id').val()).fadeIn();
			$('#cat' + $('#SupportFile_category_id').val() + ' select').attr('name', 'SupportFile[for_category_id]');
		});
	", CClientScript::POS_READY);
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'horizontal',
	'htmlOptions' => array('enctype' => 'multipart/form-data', 'style' => 'margin: 0px'),
));
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'options'=>array(
                        'title'=>'Заменить описание?',
                        'modal'=>true,
                        'autoOpen'=>false,
                        'buttons'=>array(
                            'Yes'=>'js:function(){$(".desc_field").val($(".desc").val()); $(this).dialog("close")}',
                            'No'=>'js:function(){$(this).dialog("close")}'
                            )
                    ),
                    'id'=>'confirmReplace',
                    
            )
        );

        echo CHtml::textArea('desc', '', array('class' => 'desc'));

        $this->endWidget('zii.widgets.jui.CJuiDialog');
?>
	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->textFieldRow($model, 'name');?>
    <?php echo $form->ckEditorRow($model, 'full_description', array('class'=>'full_desc_field', 'rows'=>5)); ?>
	<?php echo $form->textAreaRow($model , 'description', array('class' => 'desc_field'));?>
	<?php echo $form->dropDownListRow($model, 'category_id', $this->getCategories()); ?>
	<?php 
	$display = 'style="display: none;"';
	$name = '';
	foreach ($this->getCategories() as $key => $catTitle):
	$subCategoriesList = $this->getSubCategories($key, true);
	foreach ($subCategoriesList as $subkey => $catTitle)
	{
		if($model->for_category_id == $subkey)
		{
			$display = '';
			$name = 'SupportFile[for_category_id]';
		}
	}
	?>
	<div class="sub-category" id="cat<?php echo $key; ?>" <?php echo $display; ?>>
		<?php 
		echo $form->dropDownListRow($model, 'for_category_id', $subCategoriesList, array('name' => $name));
		$display = 'style="display: none;"';
		$name = '';
		?>
	</div>
	<?php endforeach; ?>
	<?php echo $form->dropDownListRow($model, 'brand_id', Brand::getListData()); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
<?php $this->endWidget();?>