<?php

class m130806_161721_add_middle_name_and_dob_in_client extends CDbMigration
{
	public function up()
	{
		$this->execute("
			ALTER TABLE `client`
				ADD `middle_name` varchar(50) DEFAULT NULL,
				ADD `date_of_birth` int(11) DEFAULT NULL;
				");
	}

	public function down()
	{
		$this->execute("
			ALTER TABLE `client`
				DROP `middle_name`,
				DROP `date_of_birth`;
				");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}