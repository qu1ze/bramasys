<?php
/* @var $phone Phone */
$key = isset($key) && $phone->id != null ? $key : 'new';
$additional = $key === 'new' ? '[]' : '';
?>

<div class="control-group" phone-id="<?php echo $key === 'new' ? 'new' : $phone->id; ?>" <?php echo ($phone->id == null && empty($phone->value)) ? 'style="display: none;"' : ''; ?>>
	<?php echo CHtml::dropDownList("Phones[$key][type]".$additional, $phone->type, $phone->getPhoneTypes(), array('class' => 'control-label', 'style' => 'width: 145px;'))?>
	<div class="controls">
		<div class="fieldWithSubLabel" style="float: left; margin-right: 10px;">
            <input type="text" name="Phones<?php echo "[$key]"; ?>[value]<?php echo $additional; ?>" id="phone" style="width: 150px;" value="<?php echo $phone->value; ?>">
			<label class="subLabelNoLeft">В формате (096)631-63-63</label><br>
            <span class="help-inline error" id="Phone_em_" style="display: none;"></span> 
			<?php echo CHtml::error($phone, 'value'); ?>
		</div>
	</div>
	<?php if($count > 1 || $key === 'new'): ?>
		<img class="drop" style="padding-top: 15px; width: 10px;" src="/images/cross.gif"/>
	<?php endif; ?>
</div>