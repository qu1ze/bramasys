<?php
class ProductFilter extends CWidget
{
	public $substractConditions = array();
	public $excludeConditions = array();
	public $priceCriteria = null;
	public $searchCriteria = null;
	
	public $counts = array();
	public $specifications = array();
	public $brands = array();
	
	public $offers = array();
	public $specs = array();
	public $price = array();
	public $getBrands = array();
	
	private function getConditionArrays(array $columns, & $conditionArray, & $inConditionArray)
	{
		foreach ($columns as $column => $value)
		{
			if(!is_numeric($column))
			{
				$conditionArray['values'][$column] = $value > 0 ? 1 : 0;
				$conditionArray['operators'][$column] = 'OR';
			}
			else if (is_array($value))
			{
				$specValCount = 1;
				foreach ($value as $specValue => $checked)
				{
					if($specValCount != 1)
						$operator = 'OR';
					else
						$operator = 'AND';
					$conditionArray[$column]['values'][$specValue] = 1;
					$conditionArray[$column]['operators'][$specValue] = 'OR';
					$specValCount++;
				}
			}
			else
			{
				$inConditionArray = $columns;
				break(1);
			}
		}
	}
	
	private function appendSubstractCondition($label, array $array)
	{
		$substractConditionArray = array();
		$substractInConditionArray = array();
		$criteria = new CDbCriteria();
		
		$this->getConditionArrays($array, $substractConditionArray, $substractInConditionArray);
		if(!empty($substractConditionArray) && $criteria !== null)
		{
			if(isset($substractConditionArray['values']))
			{
				foreach ($substractConditionArray['values'] as $column => $value)
					$criteria->addCondition($column." != '$value'", $substractConditionArray['operators'][$column]);
				$this->substractConditions[$label] = " AND ($criteria->condition) ";
			}
			return true;
		}
		return false;
	}
	
	private function appendExcludeCondition($label, array $array)
	{
		$excludeConditionArray = array();
		$excludeInConditionArray = array();
		$criteria = new CDbCriteria();
		
		$this->getConditionArrays($array, $excludeConditionArray, $excludeInConditionArray);
		if(!empty($excludeConditionArray) && $criteria !== null)
		{
			if(isset($excludeConditionArray['values']))
			{
				foreach ($excludeConditionArray['values'] as $column => $value)
					$criteria->addCondition($column." = '$value'", $excludeConditionArray['operators'][$column]);
				$this->excludeConditions[$label] = " AND $criteria->condition ";
			}
			else
			{
				foreach ($excludeConditionArray as $specId => $conditionArray)
				{
					foreach ($conditionArray['values'] as $column => $value)
						$criteria->addCondition("spec_id = '$specId' AND value = '$column'", $conditionArray['operators'][$column]);
					$this->excludeConditions[$label][$specId] = "$criteria->condition";
				}
			}
			return true;
		}
		else if(!empty($excludeInConditionArray) && $criteria !== null)
		{
			$values = '';
			foreach ($excludeInConditionArray as $value)
			{
				$values .= $value.', ';
			}
			$values = rtrim($values, ', ');
			$this->excludeConditions[$label] = " AND `brand` IN ($values) ";
			return true;
		}
		return false;
	}

	private function generateConditions(array $attrArrays)
	{
		foreach ($attrArrays as $label => $array)
		{
			switch ($label)
			{
				case 'search':
					$this->searchCriteria = $array;
					break;
				case 'price':
					$this->priceCriteria = new CDbCriteria();
					$this->priceCriteria->addBetweenCondition('price', isset($array['min']) ? $array['min'] : Products::MIN_PRODUCT_PRICE, isset($array['max']) ? $array['max'] : Products::MAX_PRODUCT_PRICE);
					break;
				case 'offers':
					$this->appendSubstractCondition($label, $array);
				default:
					if(!Yii::app()->controller->isSearch || $label == 'offers')
						$this->appendExcludeCondition($label, $array);
					break;
			}
		}
	}
	
	private function getSubstractCondition($substractLabel, $id = null)
	{
		if($id === null)
			return isset($this->substractConditions[$substractLabel])  && $substractLabel == 'offers' ? $this->substractConditions[$substractLabel] : '';
		return isset($this->substractConditions[$substractLabel][$id]) ? $this->substractConditions[$substractLabel][$id] : '';
	}
	
	private function getExcludeCondition($excludeLabel, $id = null)
	{
		$excludeCondition = '';
		foreach ($this->excludeConditions as $label => $value)
		{
			if($label != $excludeLabel || $id !== null)
			{
				if(!is_array($value))
				{
					$excludeCondition .= $value;
				}
				else
				{
					foreach ($value as $specId => $specValue)
					{
						if($id !== null && $specId == $id)
							continue(1);
						if($excludeLabel == 'offers')
							$excludeCondition .=  " AND ($specValue)";
						else
							$excludeCondition .=  " AND (SELECT `product_id` FROM `product_spec_value` `t2` WHERE $specValue AND `t2`.`product_id` = `products`.`id` LIMIT 1)";
					}
				}
			}
		}
		return $excludeCondition;
	}
	
	private function getOfferCount($categoryIds, $offerLabel)
	{
		$offerCount = 0;
		if(!empty($offerLabel) && !empty($categoryIds))
		{
			$criteria = new CDbCriteria();
			$criteria->alias = 'products';
			$criteria->select = "count(DISTINCT `products`.`id`) as product_count";
			$criteria->addCondition($offerLabel.' = 1');
			$criteria->addInCondition('category', $categoryIds);
			$criteria->mergeWith($this->priceCriteria);
			$criteria->mergeWith($this->searchCriteria);
			$criteria->condition .= $this->getExcludeCondition('offers');
			$criteria->condition .= $this->getSubstractCondition('offers');
			$criteria->with = array(
				'productSpecValues'=>array('alias'=>'product_spec_value', 'together' => true),
				'brand0'=>array('alias'=>'brand', 'together' => false),
			);
			$offerCount = Products::model()->count($criteria);
			if(!empty($this->offers))
			{
				$offerCount = '+'.$offerCount;
			}
		}
		return $offerCount;
	}
	
	private function getSpecsValueCount($categoryIds)
	{
		$resultArray = array();
		if(!empty($categoryIds))
		{
			/*Geting ids for specs names*/
			$specsIds = array();
			foreach (CategorySpec::model()->findAllByAttributes(array('category_id' => $categoryIds, 'in_filter' => 1), array('order' => '`order`')) as $categorySpec)
			{
				array_push($specsIds, $categorySpec->spec_id);
			}
			/*Geting specs names*/
			foreach ($specsIds as $specsId)
			{
                $specification = Specification::model()->findByPk($specsId);
				$resultArray[$specification->id] = array('name' => $specification->name, 'values' => array());
			}
			/*Geting specs values*/
			$specCriteria = new CDbCriteria;
			$specCriteria->select = '`t`.`id`, `t`.`spec_id`, `t`.`value`';
			$specCriteria->addInCondition('`t`.`spec_id`', $specsIds);
			$specCriteria->addCondition('`t`.`value` IS NOT NULL');
			$specCriteria->addInCondition('`products`.`category`', $categoryIds);
			$specCriteria->with = array(
					'product'=>array('alias'=>'products', 'together' => true),
				);
			$specCriteria->group = 'value';
			$dataProvider=new CActiveDataProvider('ProductSpecValue');
			$dataProvider->setCriteria($specCriteria);
			$dataProvider->pagination = false;
			foreach ($dataProvider->getData() as $specValue)
			{
				$specCriteria = new CDbCriteria;
				$criteria = new CDbCriteria();
				$criteria->addInCondition('category', $categoryIds);
				$criteria->condition .= $this->getExcludeCondition('specs', $specValue->spec_id);
				$criteria->mergeWith($this->priceCriteria);
				$criteria->mergeWith($this->searchCriteria);
				$specCriteria->with = array(
					'product'=>array('alias'=>'products', 'condition' => $criteria->condition, 'params' => $criteria->params, 'together' => true),
					'product.brand0'=>array('alias'=>'brand', 'together' => false),
				);
				$specCriteria->select = 'count(`t`.`product_id`) as product_count';
				$specCriteria->addCondition("`t`.`spec_id` = '".$specValue->spec_id."'");
				$specCriteria->addCondition("`t`.`value` = '".$specValue->value."'");
				$specValue->product_count = ProductSpecValue::model()->count($specCriteria);
				if(!empty($this->specs) && isset($this->specs[$specValue->spec_id]) && $specValue->product_count != 0)
				{
					$specValue->product_count = '+'.$specValue->product_count;
				}
				array_push($resultArray[$specValue->spec_id]['values'], $specValue);
			}
		}
		return $resultArray;
	}
	
	private function getBrandsCount($categoryIds)
	{
		$resultArray = array();
		if(!empty($categoryIds))
		{
			$criteria = new CDbCriteria;
			$criteria->alias = 'products';
			$criteria->select = '`brand`.`id` as `brand_id`, `brand`.`name` as `brand_name`, count(`products`.`id`) as `product_count`';
			$criteria->join = 'JOIN brand ON brand.id = brand';
			$criteria->addInCondition('category', $categoryIds);
			$criteria->group = '`brand`.`name`';
			$dataProvider=new CActiveDataProvider('Products');
			$dataProvider->setCriteria($criteria);
			$dataProvider->pagination = false;
			foreach ($dataProvider->getData() as $product)
			{
				$brandCriteria = new CDbCriteria;
				$brandCriteria->addInCondition('category', $categoryIds);
				$brandCriteria->alias = 'products';
				$brandCriteria->addCondition('brand = '.$product->brand_id);
				$brandCriteria->condition .= $this->getExcludeCondition('brands');
				$brandCriteria->mergeWith($this->priceCriteria);
				$brandCriteria->mergeWith($this->searchCriteria);
				$brandCriteria->with = array('brand0'=>array('alias'=>'brand', 'together' => false,));
				$pdCount = Products::model()->count($brandCriteria);
				if(!empty($this->getBrands) && $pdCount != 0)
				{
					$pdCount = '+'.$pdCount;
				}
				$brand = array('name' => $product->brand_name, 'product_count' => $pdCount);
				$resultArray[$product->brand_id] = $brand;
			}
		}
		return $resultArray;
	}
	
	public function init()
	{
		if(isset($_GET['top_seller']))
			$this->offers['top_seller'] = $_GET['top_seller'];
		if(isset($_GET['novelty']))
			$this->offers['novelty'] = $_GET['novelty'];
		if(isset($_GET['price_of_the_week']))
			$this->offers['price_of_the_week'] = $_GET['price_of_the_week'];
		if(isset($_GET['specs']))
			$this->specs = $_GET['specs'];
		if(isset($_GET['min_price']))
			$this->price['min'] = $_GET['min_price'];
		if(isset($_GET['max_price']))
			$this->price['max'] = $_GET['max_price'];
		if(isset($_GET['brand']))
			$this->getBrands = $_GET['brand'];
		
		$categoryIds = Yii::app()->controller->categoryIds;
        $controller = Yii::app()->controller->id.'Controller';
        $search = array();
        if(isset($_GET['search']) && $controller::getSearchCriteria($_GET['search']) !== null)
            $search = $controller::getSearchCriteria($_GET['search']);
		$this->generateConditions(array(
			'offers' => $this->offers,
			'specs' => $this->specs,
			'brands' => $this->getBrands,
			'price' => $this->price,
			'search' => $search,
			)
		);

        if(Yii::app()->controller->id !== 'complect')
        {
            $this->counts['topSellers'] = $this->getOfferCount($categoryIds, 'top_seller');
            $this->counts['novelties'] = $this->getOfferCount($categoryIds, 'novelty');
            $this->counts['priceOfTheWeek'] = $this->getOfferCount($categoryIds, 'price_of_the_week');
        }
		if(!Yii::app()->controller->isSearch && Yii::app()->controller->id !== 'complect')
		{
			$this->brands = $this->getBrandsCount($categoryIds);
			$this->specifications = $this->getSpecsValueCount(app()->categoryController->addParentsIds(end($categoryIds)));
		}
	}
	
    public function run()
    {
		if(isset($_GET['orig_min_price']))
		{
			$this->price['min'] = $_GET['orig_min_price'];
		}
		if(isset($_GET['orig_max_price']))
		{
			$this->price['max'] = $_GET['orig_max_price'];
		}
		$this->render("product_filter", array(
			'counts' => $this->counts,
			'specifications' => $this->specifications,
			'brands' => $this->brands,
			'offers' => $this->offers,
			'specs' => $this->specs,
			'getBrands' => $this->getBrands,
			'price' => $this->price,
			)
		);
    }
}
?>
