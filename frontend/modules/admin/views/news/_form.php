<?php
	Yii::app()->clientScript->registerScript("load_image", "
        $('.image_url_field').on('change', function(){
            var image_url_field = $('.image_url_field');
            var prod_image = $('.prod_image');
            if (image_url_field.val())
            {
                prod_image.fadeOut('slow', function(){
                    prod_image.attr('src', image_url_field.val());
                    prod_image.fadeIn();
                });
            }
			else
				prod_image.hide();
        });
    ", CClientScript::POS_READY);
?>
<?php echo $form->textFieldRow($model, 'name'); ?>
<?php echo $form->textFieldRow($model, 'title'); ?>
<?php echo $form->textFieldRow($model, 'thumb', array('class' => 'image_url_field')); ?>
<?php echo CHtml::image($model->thumb, '', array('class' => 'prod_image', 'style' => 'margin-bottom:10px; margin-left:180px;'));?>
<?php //echo $form->textFieldRow($model, 'keywords', array('hint' => 'Вводить через ","')); ?>
<?php echo $form->ckEditorRow($model, 'text', array('options'=>array('fullpage'=>'js:true', 'width'=>'640', 'resize_maxWidth'=>'640','resize_minWidth'=>'320'))); ?>
<?php echo $form->ckEditorRow($model, 'short_description', array('options'=>array('fullpage'=>'js:true', 'width'=>'640', 'resize_maxWidth'=>'640','resize_minWidth'=>'320'))); ?>

