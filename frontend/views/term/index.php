<div id="container">
<?php
$this->breadcrumbs = app()->categoryController->getCategoryBcs($this->categoryName);

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
?>
<h3>Словарь терминов охранных систем</h3>
<?php 
$alphabets = array(
	'ru' => array(
		'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я',
		),
	'en' => array(
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		),
	);
?>
<?php foreach ($alphabets as $alphabet):?>
<div class="alphabel">
	<?php foreach ($alphabet as $char):?>
		<a href="<?php echo app()->getBaseUrl(true); ?>/<?php echo Yii::app()->request->pathInfo.'/?search_term='.$char; ?>"><?php echo $char; ?></a>
	<?php endforeach; ?>
</div>
<?php endforeach; ?>
<?php
$summaryText = 'Всего статей - '.$totalCount.' шт.';
if(!empty($search))
	$summaryText .= ', статей на '.$search.' - '.$count.' шт.';
	
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'summaryText' => $summaryText,
)); ?>
</div>
