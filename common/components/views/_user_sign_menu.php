<div class="sign-in">
	<?php if(Yii::app()->user->id == null): ?>
		<div class="sign-btn">
			<a id="user-auth-registration" href="<?php echo app()->getBaseUrl(true); ?>/user/profile/registration">Зарегистрироваться</a>
		</div>
	<?php else: ?>
		<?php if(!Yii::app()->user->getState('isAdmin')): ?>
			<div class="user-btn">
				<a id="user-auth-registration" href="<?php echo app()->getBaseUrl(true); ?>/user"><?php echo Yii::app()->user->getState('name') ?> <?php echo Yii::app()->user->getState('lastName') ?></a>
			</div>
		<?php else: ?>
			<div class="user-btn">
				<a id="user-auth-registration" href="<?php echo app()->getBaseUrl(true); ?>/admin">Админ панель</a>
			</div>
		<?php endif; ?>
	<?php endif; ?>
    
	<?php if(Yii::app()->user->id == null): ?>
<div class="form-overlay" style="display: none;"></div>
    <?php $this->widget('common.components.LoginFormWidget', array('model' => $model, 'type' => LoginForm::LOGIN_FORM_TYPE_AJAX)); ?>
    <?php $this->widget('common.components.NewPassword'); ?>
	<?php endif; ?>
    <div id="login_btn" class="ajax-login-btn" <?php echo Yii::app()->user->id == null ? '' : 'style="display:none;"'?>>
        <img src="/images/user-icon.png" alt="">
        <a href="#">Войти</a>
    </div>
	<div id="logout_btn" <?php echo Yii::app()->user->id == null ? 'style="display:none;"' : ''?>>
		<img src="/images/exitimeg.png" alt="" style="margin-bottom: 3px;" />
		<a href="#" onclick="window.location='<?php echo app()->getBaseUrl(true); ?>/site/logout';">Выйти</a>
	</div>
</div>