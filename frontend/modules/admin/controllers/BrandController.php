<?php

class BrandController extends Controller
{		
	public function actionCreate()
	{
		$model = new Brand; 
				
		if (isset($_POST['Brand']))
		{			
			$model->attributes = $_POST['Brand'];
			if ($model->save())
				$this->redirect(array('/admin/brand'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new Brand();	

		if (isset($_POST['Brand']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['Brand'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Продукт успешно создан'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать бренд'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new Brand('search');
		
		if (isset($_GET['Brand']))
			$model->attributes = $_GET['Brand'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = Brand::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/brand'));				
				
		if (isset($_POST['Brand']))
		{			
			$model->attributes = $_POST['Brand'];
			if ($model->save())
				$this->redirect(array('/admin/brand'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Brand');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Бренд успешно отредактирован')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
			if ((Products::model()->findByAttributes(array('brand' => $id))) === null &&
				(SupportFile::model()->findByAttributes(array('category_id' => $id))) === null)
			{
				Brand::model()->deleteByPk( $id );					
				Yii::app()->user->setFlash('success', "Бренд успешно удален");
			}
			else
				Yii::app()->user->setFlash('error', "Существует связь с продуктом/файлом");
		}
		
		$this->redirect(array('/admin/brand'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = Brand::model()->findByPk($id)) !== null)
		{
			if ((Products::model()->findByAttributes(array('brand' => $id))) === null &&
				(SupportFile::model()->findByAttributes(array('category_id' => $id))) === null)
				if ($model->delete())
					echo json_encode(array('title' => 'Success', 'msg' => 'Бренд успешно удален'));
				else
					echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить бренд'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с продуктом/файлом'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Бренд не найден'));
	}
	
	public function actionToggle($id, $attribute)
    {
		if (($model = Brand::model()->findByPk($id)) === null)
                app()->end();
		$model->setAttribute($attribute, $model->getAttribute($attribute) ? 0 : 1);
		$model->save();
		echo json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> "Значение $attribute изменено"));
    }
}