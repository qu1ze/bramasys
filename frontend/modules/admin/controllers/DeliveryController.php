<?php

class DeliveryController extends Controller
{		
	public function actionCreate()
	{
		$model = new DeliveryType; 
				
		if (isset($_POST['DeliveryType']))
		{			
			$model->attributes = $_POST['DeliveryType'];
			if ($model->save())
				$this->redirect(array('/admin/delivery'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new DeliveryType();	

		if (isset($_POST['DeliveryType']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['DeliveryType'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Тип доставки успешно создан'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать тип доставки'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new DeliveryType('search');
		
		if (isset($_GET['DeliveryType']))
			$model->attributes = $_GET['DeliveryType'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = DeliveryType::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/delivery'));				
				
		if (isset($_POST['DeliveryType']))
		{			
			$model->attributes = $_POST['DeliveryType'];
			if ($model->save())
				$this->redirect(array('/admin/delivery'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('DeliveryType');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Тип доставки успешно отредактирован')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
			if ((Order::model()->findByAttributes(array('delivery_type_id' => $id))) === null)
			{
				DeliveryType::model()->deleteByPk( $id );					
				Yii::app()->user->setFlash('success', "Тип доставки успешно удален");
			}
			else
				Yii::app()->user->setFlash('error', "Существует связь с заказом");
		}
		
		$this->redirect(array('/admin/delivery'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = DeliveryType::model()->findByPk($id)) !== null)
		{
			if ((Order::model()->findByAttributes(array('delivery_type_id' => $id))) === null)
				if ($model->delete())
					echo json_encode(array('title' => 'Success', 'msg' => 'Тип доставки успешно удален'));
				else
					echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить тип доставки'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с заказом'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Тип доставки не найден'));
	}
	
	public function actionToggle($id, $attribute)
    {
		if (($model = DeliveryType::model()->findByPk($id)) === null)
                app()->end();
		$model->setAttribute($attribute, $model->getAttribute($attribute) ? 0 : 1);
		$model->save();
		echo json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> "Значение $attribute изменено"));
    }
}