<?php


/**
 * This is the model class for excel files.
 *
 * The followings are the model variables:
 * @property string $file
 */
class ExcelFile extends CModel
{
	public $file;
	public $extensionName;
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file', 'file', 'types'=>'xls, xlsx'),
			
		);
	}

	public function attributeNames()
	{
		return array(
			'file',
		);
	}
}