<?php

class ProductsController extends Controller
{
	public function getCategories()
	{
		$criteria = new CDbCriteria();
		$criteria->addSearchCondition('name', '%_complects', false, 'AND', 'NOT LIKE');
		$criteria->addCondition('`in_url` != 0');
		$categories = array();
		foreach (Category::model()->findAll($criteria) as $category)
		{
			if(app()->categoryController->isCatalog($category->id) && $category->parent !== null)
			{
                
				$children = app()->categoryController->getChilds($category->id);
				if(empty($children))
				{
					$categories[$category->id] = $category->title;
				}
			}
		}
		return $categories;
	}
    
    /**
     * Дерево категорий для DropDown
     * @return array() - array('tree'=>array() - лист
     * , 'options'=> $category_option) - htmlOptions 
     */
        public function getCategoriesTree()
	{
		$criteria = new CDbCriteria();
		$criteria->addSearchCondition('name', '%_complects', false, 'AND', 'NOT LIKE');
		$criteria->addCondition('`in_url` != 0');
		$categories = Category::model()->findAll($criteria);
        $category_option=array('encode'=>false);
         foreach($categories as $value)
         {
            if(app()->categoryController->isCatalog($value->id) )
            {
                $tree_categories[$value->parent ? $value->parent : 0][$value->id] = $value;
                $children = app()->categoryController->getChilds($value->id);
                if($value->parent == null || !empty($children)){
                    $category_option['options'][$value->id]=array("style" => "background-color: rgb(189, 189, 189);color: white;", 'disabled'=>true);
                }
            }
         }
         $tree=array();
         
         Category::buildlistDataTree($tree,$tree_categories);
         return array('tree'=>$tree, 'options'=> $category_option);
        
//        $categories = Category::listDataTree(Category::model()->findAll($criteria));
//		
//        
//		return $categories;
	}
	
    public function actionIndex()
    {
        $model = new Products('search');

        if (isset($_GET['Products']))
            $model->attributes = $_GET['Products'];
        
        $brands = CHtml::listData(Brand::model()->findAll(), 'id', 'name');
        $categories = $this->getCategories();
        $this->render('index', array(
                'model' => $model,
                'brands' => $brands,
                'categories' => $categories,
                )
        );
    }

    public function actionCreate()
    {
        $model = new Products; 

        if (isset($_POST['Products']))
        {
			$_POST['Products']['description'] = substr($_POST['Products']['description'], 0, 255);
            $model->attributes = $_POST['Products'];
            if ($model->save())
			{
                $this->redirect(array('/admin/products'));
			}
        }
        $brands = CHtml::listData(Brand::model()->findAll(), 'id', 'name');
        $categories = $this->getCategoriesTree();
        $this->getCategoriesTree();
        $this->render('create', array(
                'model' => $model,
                'brands' => $brands,
                'categories' => $categories['tree'],
                'categories_options' => $categories['options'],
            
                )
        );
    }
    
    public function actionSetSpec($id, $page = "")
    {
        $page = !is_numeric($page) ? "" : "/index/Products_page/$page";
        if(($model = ProductSpecValue::model()->findAllByAttributes(array('product_id' => $id))) === null)
            $this->redirect(array('/admin/products'));
        
        if (isset($_POST['ProductSpecValue']))
        {
            $result = true;
            foreach ($_POST['ProductSpecValue'] as $id => $productSpecValue)
            {
                $model = ProductSpecValue::model()->findByPk($id);
                if(empty($productSpecValue['value']))
                    $productSpecValue['value'] = null;
                $model->attributes = $productSpecValue;
                $result &= $model->save();
            }
            if($result)
                $this->redirect(array("/admin/products$page"));
        }
        
        $this->render('set_spec', array(
                'model' => $model,
			)
        );
    }

    public function actionAjaxDelete($id)
    {		
        if (($model = Products::model()->findByPk($id)) !== null)
        {
			if (($complectProduct = ComplectProduct::model()->findByAttributes(array('product_id' => $id))) === null)
			{
				if (($orderProduct = OrderProduct::model()->findByAttributes(array('product_id' => $id))) === null)
				{
					if ($model->delete())
						echo json_encode(array('title' => 'Success', 'msg' => 'Продукт успешно удален'));
				}
				else
					echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с заказами'));
			}
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с комплектами'));
        } else
                echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить: Продукт не найден'));
    }

    public function actionUpdate($id, $page = "")
    {
        $page = !is_numeric($page) ? "" : "/index/Products_page/$page";
        if (($model = Products::model()->findByPk($id)) === null)
                $this->redirect(array('/admin/products'));

        if (isset($_POST['Products']))
        {			
                $model->attributes = $_POST['Products'];
                if ($model->save())
                        $this->redirect(array("/admin/products$page"));
        }

        $brands = CHtml::listData(Brand::model()->findAll(), 'id', 'name');
        $categories = $this->getCategoriesTree();
        $this->render('update', array(
                'model' => $model,
                'brands' => $brands,
                'categories' => $categories['tree'],
                'categories_options' => $categories['options'],
                )
        );
    }

    public function actionAjaxUpdate()
    {
        Yii::import('bootstrap.widgets.TbEditableSaver', true);
        $es = new TbEditableSaver('Products');

        $es->update();

        die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Продукт успешно отредактирован')));			
    }
	
	public function actionToggle($id, $attribute)
    {
		if (($model = Products::model()->findByPk($id)) === null)
                app()->end();
		$model->setAttribute($attribute, $model->getAttribute($attribute) ? 0 : 1);
		$model->save();
		echo json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> "Значение $attribute изменено"));
    }
	
	public function actionGetProductBrands($term)
	{
		$autocompliteArray = array();
		
		$criteria = new CDbCriteria();
		$criteria->group = '`brand`.`name`';
		$criteria->with = array('brand0' => array('alias' => 'brand', 'together' => true,));
		$criteria->addSearchCondition('`brand`.`name`', $term);
		$products = Products::model()->findAll($criteria);
		foreach ($products as $product)
		{
			array_push($autocompliteArray, $product->brand0->name);
		}
		
		echo json_encode($autocompliteArray);
	}
	
	public function actionGetProductModels($term, $brandName)
	{
		$autocompliteArray = array();
		
		$brand = Brand::model()->findByAttributes(array('name' => $brandName));
		if($brand !== null)
		{
			$criteria = new CDbCriteria();
			$criteria->addCondition('brand = '.$brand->id);
			$criteria->addSearchCondition('model', $term);
            $criteria->order = 'category';
			$products = Products::model()->findAll($criteria);
			foreach ($products as $product)
			{
				array_push($autocompliteArray, array(
                    'label' => $product->model,
                    'category' => $product->category0->title,
                    'parentCategory' => $product->category0->parent_category->title,
                    'parentId' => $product->category0->parent,
                    ));
			}
		}
		
		echo json_encode($autocompliteArray);
	}
	
	public function actionGetProductSupport($term, $categoryId)
	{
		$autocompliteArray = array();
		
		$criteria = new CDbCriteria();
		$criteria->addCondition('for_category_id = '.$categoryId);
		$criteria->addSearchCondition('name', $term);
		$supportFiles = SupportFile::model()->findAll($criteria);
		foreach ($supportFiles as $supportFile)
		{
			array_push($autocompliteArray, $supportFile->name);
		}
		
		echo json_encode($autocompliteArray);
	}
	
	public function actionSetSupport($id, $page = "")
	{
		if(($model = ProductSupport::model()->findAllByAttributes(array('product_id' => $id))) === null)
			$this->redirect(array('/admin/products'));
		$categoryId = Products::model()->findByPk($id)->category;
        if(isset($_POST['Products_page']))
        {
            $page = !is_numeric($_POST['Products_page']) ? "" : "/index/Products_page/{$_POST['Products_page']}";
            $this->redirect(array("/admin/products$page"));
        }
		echo $this->render('set_support', array(
			'model' => $model,
			'currentProduct' => $id,
			'categoryId' => $categoryId,
            'page' => $page,
		));	
	}
	
	public function actionAjaxAddSupport()
	{	
            $productSupport = new ProductSupport();
            if (isset($_POST['Product']) && Yii::app()->request->isAjaxRequest)
            {
				$supportFile = SupportFile::model()->findByAttributes(array('name' => $_POST['Product']['support']));
                $product = Products::model()->findByPk($_POST['Product']['id']);
				if($supportFile !== null && $product !== null)
				{
					$productSupport->support_file_id = $supportFile->id;
					$productSupport->product_id = $product->id;
                    $categories = app()->categoryController->addParentsIds($product->category);
                    if(in_array($supportFile->for_category_id, $categories))
                    {
                        if ($productSupport->save())
                        {
                            echo json_encode(array(
                                'title'=>'Success!',
                                'msg'=>'Файл успешно добавлен',
                                'html'=>$this->renderPartial('_support', array(
                                    'model' => $productSupport,
                                ), true),
                                'id'=>$productSupport->id,
                                ));
                            app()->end();
                        }
                        else
                        {
                            echo json_encode(array('title'=>'Oops!', 'msg'=>'Файл уже был добавлен'));
                            app()->end();
                        }
                    }
				}
            }
			echo json_encode(array('title'=>'Oops!', 'msg'=>'Файл не найден'));
            app()->end();
	}
	
	public function actionAjaxRemoveSupport($supportId)
	{		
		if (($model = ProductSupport::model()->findByPk($supportId)) !== null)
		{
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Связь с файлом удалена'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить связь с файлом'));
		} 
		else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить, связь не найдена'));
	}
	
	public function actionAttachVideo($id, $page = "")
	{
        $page = !is_numeric($page) ? "" : "/index/Products_page/$page";
		if(($model = ProductVideo::model()->findAllByAttributes(array('product_id' => $id))) === null)
			$this->redirect(array('/admin/products'));
		if (isset($_POST['ProductVideo']['link']))
        {
			foreach ($model as $productVideo)
			{
				$productVideo->delete();
			}
			if(!empty($_POST['ProductVideo']['link']))
				foreach ($_POST['ProductVideo']['link'] as $link)
				{
					if(!empty($link))
					{
						$productVideo = new ProductVideo();
						$productVideo->product_id = $id;
						$productVideo->link = $link;
						$productVideo->save();
					}
				}
            $this->redirect(array("/admin/products$page"));
		}
		echo $this->render('attach_video', array(
			'videos' => $model,
		));	
	}

    public function actionAjaxDeleteRows(array $ids)
    {
        if(!empty($ids))
        {
            $count = 0;
            $msg = '';
            foreach ($ids as $id)
            {
                if (($model = Products::model()->findByPk($id)) !== null)
                {
                    if (($complectProduct = ComplectProduct::model()->findByAttributes(array('product_id' => $id))) === null)
                    {
                        if (($orderProduct = OrderProduct::model()->findByAttributes(array('product_id' => $id))) === null)
                        {
                            $model->delete();
                            if($model->hasErrors())
                            {
                                $msg .= "$id. ";
                                foreach ($model->errors as $error)
                                    $msg .= "$error<br>";
                            }
                            else
                                $count++;
                        }
                        else
                            $msg .= "$id. У продукта существует связь с заказами<br>";
                    }
                    else
                        $msg .= "$id. У продукта существует связь с комплектами<br>";
                } else
                    $msg .= "$id. Продукт не найден<br>";
            }
            $hasErrors = false;
            if(!empty($msg))
            {
                $hasErrors = true;
                $msg = "Не возможно удалить продукты:<br>".$msg;
            }
            $msg = "Удалено $count продуктов<br><br>".$msg;
            echo json_encode(array('title'=> $hasErrors ? 'Возникли ощибки!' : "Удаленно.", 'msg'=>$msg, 'sticky' => $count < sizeof($ids) ? true : false));
        }
        app()->end();
    }

    public function actionAjaxSetLabel(array $ids, $labelName, $value = null)
    {
        $value = isset($value) && $value == 0 ? 0 : 1;
        if(!empty($ids))
        {
            $count = 0;
            $msg = '';
            foreach ($ids as $id)
            {
                if (($model = Products::model()->findByPk($id)) !== null)
                {
                    $model->setAttribute($labelName, $value);
                    $model->save();
                    if($model->hasErrors())
                    {
                        $msg .= "$id. ";
                        foreach ($model->errors as $error)
                            $msg .= "$error<br>";
                    }
                    else
                        $count++;
                } else
                    $msg .= "$id. Продукт не найден<br>";
            }
            $hasErrors = false;
            if(!empty($msg))
            {
                $hasErrors = true;
                $msg = "Не возможно изменить значение:<br>".$msg;
            }
            $msg = "Изменено $count продуктов<br><br>".$msg;
            echo json_encode(array('title'=> $hasErrors ? 'Возникли ощибки!' : "Изменено.", 'msg'=>$msg, 'sticky' => $count < sizeof($ids) ? true : false));
        }
        app()->end();
    }
}
