<?php

class m130530_145058_change_client_fields_defaults extends CDbMigration
{
	public function up()
	{
		$this->execute("
			ALTER TABLE  `cart` DROP FOREIGN KEY  `cart_ibfk_1` ;
			TRUNCATE `client`;
			ALTER TABLE  `cart` ADD FOREIGN KEY (  `client_id` ) REFERENCES  `client` (
				`id`
				) ON DELETE RESTRICT ON UPDATE RESTRICT ;
				");
		
	
		$this->execute("
			ALTER TABLE  `client` CHANGE  `address`  `address` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
				CHANGE  `delivery_address`  `delivery_address` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            ");
	}

	public function down()
	{
		$this->execute("
			ALTER TABLE  `client`
				CHANGE  `delivery_address`  `delivery_address` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
				CHANGE  `address`  `address` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}