<?php

class m130522_133218_create_cart_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `cart` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`client_id` int(11) NOT NULL,
				`order_id` int(11) NOT NULL,
				PRIMARY KEY (`id`),
				KEY `client_id` (`client_id`),
				KEY `order_id` (`order_id`)
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			");
		$this->execute("
			ALTER TABLE `cart`
				ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`),
				ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);
			");
	}
	
	public function down()
	{
		$this->dropTable("cart");
	}
	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}