<table>
	<?php
	$isTransporent = true;
	$count = 0;
	foreach ($specsValues as $specValue):
		$classes = !$isTransporent ? 'table_color' : '';
		$classes .= " spec$count";
	?>
	<tr <?php echo "class='$classes'"; ?>><td><?php echo $specValue; ?></td></tr>
	<?php
	$count++;
	$isTransporent = !$isTransporent;
	endforeach;
	?>
</table>