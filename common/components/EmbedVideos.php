<?php
class EmbedVideos extends CWidget
{
	public $videos;
	
	public function getYouTubeCode($link)
	{
        $matches = preg_split('#/#', $link);
        $dirtyVideoId = end($matches).'&';
		preg_match('#(.*v=)?([^=]*)[?&]+#', $dirtyVideoId, $matches);
		if(!empty($matches[2]))
		{
            $link =  $matches[2];
		}
		return $link;
	}
	
	public function run()
    {
        $this->render("embed_videos", array('videos' => $this->videos));
    }
}
?>
