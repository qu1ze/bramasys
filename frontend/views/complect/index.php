<?php
/* @var $this ProductsController */
/* @var $dataProvider CActiveDataProvider */

$isMainCategory = false;
$categoryTitle = $this->categoryTitle;
if($isSearch)
{
    $this->breadcrumbs= array($this->categoryTitle);
	$categoryTitle .= ' «<font style="color:#27489B;">'.(strlen($_GET['search']) < 31 ? $_GET['search'] : substr($_GET['search'], 0, 29).'...').'</font>»';
}
else
{
    $this->breadcrumbs = app()->categoryController->getCategoryBcs($this->categoryName);
	$categoryId = app()->categoryController->getCategoryId($this->categoryName);
	$categoryVars = app()->categoryController->getCategory($categoryId);
	$isMainCategory = $categoryVars['data']['isMain'];
	if($isMainCategory)
	{
		
		$categories = app()->categoryController->getChilds($categoryId);
		$extendedExtra = $categoryVars['data']['extendedExtra'];
		$mainCategory = array();
		$extraCategories = array();
		foreach ($categories as $key => $category)
		{
			if($category['data']['in_url'] > 0)
			{
				$catTitle = $category['data']['title'];
				$categoryLink = app()->categoryController->getCategoryAddress($category['data']['name']);
				$filter = $category['data']['filter'];

				$subCategories = app()->categoryController->getChilds($category['id']);
				$subCategoriesData = array();
				if(!empty($subCategories))
				{
					foreach ($subCategories as $key => $subCategory)
					{
						if($subCategory['data']['in_url'] > 0)
							array_push($subCategoriesData, array(
									'title' => $subCategory['data']['title'],
									'link' => app()->categoryController->getCategoryAddress($subCategory['data']['name']),
								)
							);
					}
					array_push($mainCategory, array(
							'title' => $catTitle,
							'link' => $categoryLink,
							'image' => '/images/layout/noimg.png',
							'filter' => $filter,
							'subCategory' => $subCategoriesData,
						)
					);
				}
				else
				{
					if(empty($extendedExtra))
					{
						array_push($extraCategories, array(
								'title' => '- '.$catTitle,
								'link' => $categoryLink,
							)
						);
					}
					else
					{
						//TODO:pull extra categories array from model and reverse it
					}
				}
			}
		}
		if(!empty($extraCategories))
		{
			if(empty($extendedExtra))
			{
				$catTitle = "Дополнительные категории";
				$categoryLink = null;
				array_push($mainCategory, array(
						'title' => $catTitle,
						'link' => $categoryLink,
						'image' => null,
						'filter' => null,
						'subCategory' => $extraCategories,
					)
				);
			}
			else
			{
				//TODO:make array_unshift to main category for each one
			}
		}
		
		$mainCategoriesCount = sizeof($mainCategory);
		
		$emptyBlock = true;
		if($mainCategoriesCount%2 !== 0)
		{
			$emptyBlock = false;
		}
	}
}
?>
<div id="container" <?php echo !$isMainCategory ? 'style="float:left; width: 550px;"' : ''; ?>>
<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));

    $pathInfo = explode("/page/", Yii::app()->request->pathInfo);
    $metadata = CategoryMetadata::model()->findByAttributes(array('url' => "/".$pathInfo[0]));
?>
<div class="pd-title">
    <h1><?php echo !empty($metadata->H1) ? $metadata->H1 : $categoryTitle?></h1>
</div>
<?php if($isMainCategory): ?>
	<div id="sub-categories-block-row">
	<?php 
	$count = 0;
	foreach ($mainCategory as $key => $category):?>
		<?php $count++;?>
		<?php if($count != 1 && $count%2 !== 0): ?>
	</div><div id="sub-categories-block-row">
		<?php endif;?>
		<div class="category-item-sub">
			<?php if(!empty($category['link'])): ?>
			<div class="category-name">
				<a href="<?php echo $category['link'];?>">
					<?php echo $category['title'];?>
				</a>
			</div>
			<?php else:?>
				<div class="extra-category-name">
					<?php echo $category['title'];?>
				</div>
			<?php endif;?>
			<?php if(!empty($category['image'])): ?><img src="<?php echo $category['image'];?>" alt="" title=""><?php endif;?>
			<ul class="<?php echo empty($category['link']) ? 'extra-' : '';?>sub-category">
				<?php foreach ($category['subCategory'] as $key => $subCategory):?>
				<li>
					<a href="<?php echo $subCategory['link'];?>"><?php echo $subCategory['title'];?></a>
				</li>
				<?php endforeach; ?>
				<?php if(!empty($category['link'])): ?>
				<li>
					<a href="<?php echo $category['link'];?>" class="all">Все товары</a>
				</li>
				<?php endif;?>
			</ul>
			<?php if(!empty($category['filter'])): ?>
				<?php //TODO: filter layout?>
			<?php endif;?>
		</div>
	<?php endforeach; ?>	
	<?php if($emptyBlock): ?>
		<?php $count++;?>
		<?php if($count != 1 && $count%2 !== 0): ?>
	</div><div id="sub-categories-block-row">
	<?php endif;?>
		<!--[if lte IE 7]>
		<style type="text/css">
			.category-item-sub-empty
			{
				height: 350px;
			}
		</style>
		<![endif]-->
		<div class="category-item-sub-empty">
		</div>
	<?php endif; ?>
		<div class="category-item-sub">
			<div style="background: #E5EEF0; overflow: hidden;height: 180px;">
            <div style="font: 18px 'PTSN Regular';color: #000;margin: 0 0 0 5px;">Публикации:</div>
			<ul class="sub-category" style="margin: 0 0 0 5px; font-size: 15px;">
				<li><a href="#"></a></li>
				
			</ul>
			</div>
		</div>
	</div>
	<?php $this->widget('common.components.UsefulTabs', array('category' => $categoryId));?>
<?php else:
Yii::app()->getClientScript()->registerCssFile('/css/filter.css');
?>
<div class="sorting-block">
    <p>Сортировать :
        <span>
			<?php if(isset($_GET['sort'])): ?>
			<a href="<?php echo $this->createUrl('complect/index', array('categoryNames' => $this->pathCategories, 'sort' => null));?>">по популярности</a>
			<?php else: ?>
			по популярности
			<?php endif; ?>
		</span>
		<span>
			<?php if(!isset($_GET['sort']) || $_GET['sort'] != 'low_price'): ?>
			<a class="ajaxbutton" href="<?php echo $this->createUrl('complect/index', array('categoryNames' => $this->pathCategories, 'sort' => 'low_price'));?>">сначала дешевые</a>
			<?php else: ?>
			сначала дешевые
			<?php endif; ?>
		</span>
        <span>
			<?php if(!isset($_GET['sort']) || $_GET['sort'] != 'high_price'): ?>
			<a class="ajaxbutton" href="<?php echo $this->createUrl('complect/index', array('categoryNames' => $this->pathCategories, 'sort' => 'high_price'));?>">сначала дорогие</a>
			<?php else: ?>
			сначала дорогие
			<?php endif; ?>
		</span>
		<?php ?>
    </p>
    <p>Товаров в списке:
        <span>
			<?php if(isset($_GET['limit'])): ?>
			<a class="ajaxbutton" href="<?php echo $this->createUrl('complect/index', array('categoryNames' => $this->pathCategories, 'limit' => null));?>">20</a>
			<?php else: ?>
			20
			<?php endif; ?>
		</span>
        <span>
			<?php if(!isset($_GET['limit']) || $_GET['limit'] !== '30'): ?>
			<a class="ajaxbutton" href="<?php echo $this->createUrl('complect/index', array('categoryNames' => $this->pathCategories, 'limit' => 30));?>">30</a>
			<?php else: ?>
			30
			<?php endif; ?>
		</span>
        <span>
			<?php if(!isset($_GET['limit']) || $_GET['limit'] !== '49'): ?>
			<a class="ajaxbutton" href="<?php echo $this->createUrl('complect/index', array('categoryNames' => $this->pathCategories, 'limit' => 49));?>">49</a>
			<?php else: ?>
			49
			<?php endif; ?>
		</span>
    </p>
</div>
<div id="products_content">
    <!-- Product list 'default' and 'existence in cart' -->
    <?php
    $dataProvider->setTotalItemCount('');
    ?>
    <?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
        'ajaxUpdate'=>false,
		'cssFile'=>'/css/filter.css',
        'template'=>'{items}{pager}',
        'pager' => array(
            'header' => false,
            'class' => 'LinkPager',
            'prevPageLabel' => '<img src="/images/pageListLeft.png" height="24" width="15">',
            'nextPageLabel' => '<img src="/images/pageListRight.png" height="24" width="15">',
            'internalPageCssClass' => 'internalPages',
            'nextPageCssClass' => 'nextPage',
            'previousPageCssClass' => 'previousPage',
        ),
        'emptyText'=>'',
		'summaryText' => 'Отображенно '. $dataProvider->itemCount .' товаров из '.$dataProvider->totalItemCount.' найденых',
		)); ?>
</div>
<?php endif; ?>
</div><!-- #container-->
<?php
if(isset($_GET['Complect_page']) && ( $dataProvider->getPagination()->getCurrentPage()+1)!= $_GET['Complect_page']){
    Yii::app()->request->redirect($this->createUrl('products/index', array('categoryNames' => $this->pathCategories, 're_page'=>$dataProvider->getPagination()->getCurrentPage()+1)));
}