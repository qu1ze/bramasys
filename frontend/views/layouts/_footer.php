
<div id="footer-middle">
    <div id="footer-middle-top" class="footer-columns">
        <div id="social_and_rss" class="footer-column">
            <p class="title">Будьте в курсе обновлений:</p>

            <div class="social-block" >
                <a href="http://www.facebook.com/BramaSys" target="_blank"><img src="/images/facebookIcon.png" alt="" title=""></a>
                <a href="http://vk.com/bramasys" target="_blank"><img src="/images/vkontakteIcon.png" alt="" title=""></a>
                <a href="http://www.youtube.com/bramaua" target="_blank"><img src="/images/youtubeIcon.png" alt="" title=""></a>
                <a href="https://plus.google.com/+BramasysUa" target="_blank"><img src="/images/googleIcon.png" alt="" title=""></a>
                <a href="http://www.twitter.com/BramaSys" target="_blank"><img src="/images/twitter_icon-mini.png" alt="" title=""></a>
            </div>

            <div class="rss_block">
                <a href="<?php echo app()->getBaseUrl(true); ?>/rss/news"><img src="/images/rssIcon.png" alt="" title=""></a>
                <p><a href="<?php echo app()->getBaseUrl(true); ?>/company/news">Новости и акции</a></p>
            </div>

            <div class="rss_block">
                <a href="<?php echo app()->getBaseUrl(true); ?>/rss/articles"><img src="/images/rssIcon.png" alt="" title=""></a>
                <p><a href="<?php echo app()->getBaseUrl(true); ?>/support/stati-publikatcii-obzory">Публикации и обзоры</a></p>
            </div>
        </div>

        <div id="bot_menu_block" class="footer-column">
            <p><a href="<?php echo app()->getBaseUrl(true); ?>/company">О Компании</a> | <a href="<?php echo app()->getBaseUrl(true); ?>/kontakty">Контакты</a></p>
            <p><a href="<?php echo app()->getBaseUrl(true); ?>/company/nashi-vakansii">Вакансии</a></p>
            <p><a href="<?php echo app()->getBaseUrl(true); ?>/sotrudnichestvo">Сотрудничество с нами</a></p>
        </div>

        <div id="shedule_block" class="footer-column">
            <p class="title">График работы</p>
            <p>Пн.-Пт. 9.00-18.00</p>
            <p>Суббота 10.00-17.00</p>
            <p>Воскресенье 11.00-17.00</p>
        </div>

        <div id="contacts_block" class="footer-column">
            <p class="title">Контакты:</p>
            <p>тел. +38 096 631-63-63,</p>
            <p><a class="callback-bnt" href="#">заказать обратный звонок</a></p>
            <p>info@bramasys.com.ua</p>
        </div>

        <div id="faq_block" class="footer-column">
            <p class="title">Помощь:</p>
            <p><a href="<?php echo app()->getBaseUrl(true); ?>/support/faq">Вопросы и ответы</a></p>
            <p><a href="<?php echo app()->getBaseUrl(true); ?>/oplata_dostavka">Оплата и доставка</a></p>
            <p><a href="<?php echo app()->getBaseUrl(true); ?>/vozvrat_garantiya">Возврат и гарантия</a></p>
            <p><a href="<?php echo app()->getBaseUrl(true); ?>/uslugi">Монтаж</a> | <a href="<?php echo app()->getBaseUrl(true); ?>/support">Сервис</a></p>
        </div>        

		<div id="creator_block" class="footer-column">
			<p>Разработка сайта - <a href="mailto:qu1ze34@gmail.com"><strong>Мелешко Олег</strong></a></p>
			<p>Дизайн сайта - <a href="http://dominido.com"><strong>Dominido</strong></a></p>									
		</div>
    </div>

    <div id="footer-middle-bottom">
        «© 2011-<?php echo date('Y'); ?>
        <?php echo $_SERVER["REQUEST_URI"] != '/' ? '<a href="'.app()->getBaseUrl(true).'/">BramaSys</a>' : 'BramaSys' ?>.
         –  компания охранных систем и видеонаблюдения»
         <br><br><br>
    </div>
</div>
