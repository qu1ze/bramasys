<?php

class m130729_143605_add_currency_label extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE `currency` ADD `label` varchar(10) NOT NULL;");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `currency` DROP `label`;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}