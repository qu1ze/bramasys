<?php
/* @var $this ProductsController */
/* @var $data Products */

//Yii::app()->getClientScript()->registerCssFile('/css/product.css');
Yii::app()->getClientScript()->registerCssFile('/css/rating.css');
?>
<div class="show">
    <div class="pd-list-block <?php echo $data->cartPattern; ?>">
        <div class="pd-left">
            <div class="pd-list-image-block">
                <div class="image-container">
                    <img src="<?php echo CHtml::encode($data->image_url); ?>" alt="<?php echo $data->name; ?>">
                </div>
                <?php if($data->hasLabels): ?>
                    <div class="labes">
                        <ul>
                            <?php if($data->novelty): ?><li><span class="labes-new">НОВИНКА</span></li><?php endif; ?>
                            <?php if($data->top_seller): ?><li><span class="labes-sail-leader">ЛИДЕР ПРОДАЖ</span></li><?php endif; ?>
                            <?php if($data->price_of_the_week): ?><li><span class="labes-price-of-the-week">ЦЕНА НЕДЕЛИ</span></li><?php endif; ?>
                        </ul>
                    </div>
                <?php endif;?>
            </div>
            <div class="pd-options">
                <div class="pd-reviews-block">
                        <?php if(!$data->hasComments):?>
                        <a href="<?php echo $data->link."#scroll=post_comment"; ?>">
                        Оставить отзыв
                        <?php else:?>
                        <a href="<?php echo $data->link."#scroll=comment-title"; ?>">
                        <?php echo $data->commentsCount;?> отзывов
                        <?php endif;?>
                    </a>
                    <div class="rate-count">
                        <div class="vote-wrap">
                            <div class="comment-rating">
                                <?php $this->widget('common.components.RatingStars', array('rating' => $data->rating)); ?> (<?php echo $data->votedCount;?> оц.)
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(Yii::app()->user->id != null && !Yii::app()->user->getState('isAdmin')): ?> <ul>
                    <li>
                        <img src="/images/bookmark.png" alt="" title="">
                        <a class="add-bookmark" data-isComplect="1" mark-id="<?php echo $data->id;?>" <?php echo $data->inBookmarks ? 'href="'.app()->getBaseUrl(true).'/user/profile/bookmarks">Уже в закладках' : '>В закладки';  ?></a>
                    </li>
                </ul><?php endif; ?>
            </div>
        </div>
        <div class="pd-right">
            <div class="pd-list-text-block">
                <div class="title">
                    <a href="<?php echo $data->link; ?>"><?php echo $data->name; ?></a>
                </div>
                <div class="code hide pd-code" >Код товара:  <?php echo $data->actualId;?><span><?php echo $data->id;?></span></div>
                <div class="text"><?php echo $data->description; ?></div>
            </div>
            <div style="
                float: left;
                position: relative;
                height: 100px;
                width: 100px;
            ">
                <div class="pd-price-block">
                    <?php if(!empty($data->old_price)): ?>
                        <div class="pd-old-price"><?php echo CurrencySys::exchange($data->old_price).' '.CurrencySys::getLabel(); ?></div>
                    <?php endif; ?>
                    <div class="pd-new-price"><?php echo CurrencySys::exchange($data->price).' '.CurrencySys::getLabel(); ?></div>
                    <?php if (!CurrencySys::isDefaultInCookie()):?>
                        <div class="pd-usd-price"><?php echo CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY).' '.$data->price ?></div>
                    <?php endif; ?>
                </div>
                <div class="pd-qty-block">
                    <?php if ($data->quantity != null):?>
                        <?php if ($data->quantity > 0):?>
                            <span class="isset"><span class="tick"></span>Есть в наличии</span>
                        <?php else:?>
                            <span class="isset" style="color:#04ce34;">Под заказ</span>
                        <?php endif;?>
                    <?php else:?>
                        <span class="isset" style="color:#e411c9;">Скоро в продаже</span>
                    <?php endif;?>
                    <p>Количество: <input type="text" size="5" value="1"></p>
                </div>
                <div class="pd-buy-block">
                    <?php if ($data->quantity != null):?>
                        <?php if ($data->quantity > 0):?>
                            <div class="btn-buy-<?php echo $data->id; ?> enabled" style="margin-top: 20%; <?php echo $data->inCart ? 'display: none;' : ''?>">
                                <a class="catalog btn-buy-buy" data-is_complect="<?php echo $data->isComplect; ?>" href="#" data-id="<?php echo $data->cartPattern; ?>">Купить</a>
                                <p><a class="catalog href-buy-buy" data-is_complect="<?php echo $data->isComplect; ?>" href="#" data-id="<?php echo $data->cartPattern; ?>">Купить и заказать монтаж</a></p>
                            </div>
                            <div class="btn-buy-<?php echo $data->id; ?> disabled" id="cartPattern" style="margin-top: 30%; <?php echo $data->inCart ? '' : 'display: none;'?>">
                                <a data-is_complect="<?php echo $data->isComplect; ?>" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
                            </div>
                        <?php else:?>
                            <a class="catalog btn-buy-order" data-is_complect="<?php echo $data->isComplect; ?>" href="#" style="margin-top: 30%;" id="<?php echo $data->cartPattern; ?>">Заказать</a>
                        <?php endif;?>
                    <?php else:?>
                        <a class="catalog btn-buy-soon" data-is_complect="<?php echo $data->isComplect; ?>" href="#" id="<?php echo $data->cartPattern; ?>" style="margin-top: 25%; font-size: 18px; line-height:23px;">Уведомить о наличии</a>
                    <?php endif;?>

                </div>
            </div>
        </div>
    </div>
</div>
<hr class="line">