<div class="specContainer" style="display: none;" cat-spec-id="<?php echo $specification->id; ?>">
    <div class="control-group" id="dropSpec<?php echo $specification->spec_id; ?>">
        <label class="control-label"><?php echo $specification->spec->name; ?></label>
        <div class="controls">
            <label class="checkbox" style="float: left; padding-right: 10px;">
                <input name="in-filter" id="inFilter" value="1" <?php echo $specification->in_filter ? 'checked="checked"' : ''; ?> type="checkbox">
                <?php echo $specification->getAttributeLabel('in_filter'); ?>
            </label>
             <input type="text" class="orderNumber" id="orderNumber<?php echo $specification->id; ?>" value="0">
            <img class="dropSpec" value="<?php echo $specification->spec_id; ?>" style="padding-top:10px;" src="/images/cross.gif" width="10"/>
        </div>
    </div>
</div>