<?php

class m130502_174534_brand_table_fix extends CDbMigration
{
	public function up()
	{
            $this->execute("ALTER TABLE  `brand` CHANGE  `brand`  `name` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL");
	}

	public function down()
	{
            $this->execute("ALTER TABLE  `brand` CHANGE  `name`  `brand` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}