<h2>Медиа центр</h2>
<?php
if ($model === null) {
    echo '<p>Галерея не найдена</p>';
} else {
    $this->widget('UrlGalleryManager', array(
        'gallery' => $model,
        'controllerRoute' => '/admin/urlgallery',
    ));
}
?>