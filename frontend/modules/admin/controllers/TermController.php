<?php

class TermController extends Controller
{		
	public function actionCreate()
	{
		$model = new Term(); 
				
		if (isset($_POST['Term']))
		{			
			$model->attributes = $_POST['Term'];
			if ($model->save())
				$this->redirect(array('/admin/term'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}

	public function actionIndex()
	{		
		$model = new Term('search');
		
		if (isset($_GET['Term']))
			$model->attributes = $_GET['Term'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = Term::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/term'));				
				
		if (isset($_POST['Term']))
		{			
			$model->attributes = $_POST['Term'];
			if ($model->save())
				$this->redirect(array('/admin/term'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Term');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Термин успешно отредактирован')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
			Term::model()->deleteByPk( $id );					
			Yii::app()->user->setFlash('success', "Термин успешно удалены");
		}
		
		$this->redirect(array('/admin/term'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = Term::model()->findByPk($id)) !== null)
		{
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Термин успешно удален'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить термин'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Термин не найден'));
	}
	
}