<?php

/**
 * This is the model class for table "support_file".
 *
 * The followings are the available columns in table 'support_file':
 * @property string $path
 */
class Robots extends CModel
{
	const ROBOTS_RULE_ALLOW = 1;
	const ROBOTS_RULE_DISALLOW = 0;
	
	private $ruleTypes = array(
		self::ROBOTS_RULE_ALLOW => 'Allow:',
		self::ROBOTS_RULE_DISALLOW => 'Disallow:',
	);
	
	private $ruleNames = array(
		self::ROBOTS_RULE_ALLOW => 'Разрешить',
		self::ROBOTS_RULE_DISALLOW => 'Запретить',
	);
	
	public function getRuleTypes($type = null)
	{
		return is_null($type) ? $this->ruleTypes : $this->ruleTypes[$type];
	}
	
	public function getRuleNames($name = null)
	{
		return is_null($name) ? $this->ruleNames : $this->ruleNames[$name];
	}
	
	private $path = '';
	public $mainRule = 1;
	public $rules = array();
	public $cleanParams = array();
	public $crawlDelay = 0;
	private $content = '';

	public function setPath($value = null)
	{
		if(empty($value))
			$this->path = Yii::app()->basePath.DIRECTORY_SEPARATOR.'www';
		else
			$this->path = $value;
	}
	
	public function getPath()
	{
		if(empty($this->path))
		{
			$this->setPath();
		}
		return $this->path;
	}
	
	public function getContent()
	{
		if(empty($this->content))
		{
			$this->content = file_get_contents("{$this->getPath()}".DS."robots.txt");
		}
		return $this->content;
	}
	
	protected function afterConstruct()
	{
		parent::afterConstruct();
		$this->setPath();
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mainRule, crawlDelay', 'numerical', 'integerOnly'=>true, 'allowEmpty'=>false),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'path' => 'Путь',
			'mainRule' => 'Основное правило',
			'rules' => 'Правило',
			'cleanParams' => 'Очищать параметр',
			'crawlDelay' => 'Задержка при индексации (сек.)',
			'content' => 'Текст'
		);
	}

	public function attributeNames()
	{
		return array(
			'path',
			'mainRule',
			'rules',
			'cleanParams',
			'crawlDelay',
			'content',
		);
	}
	
	public function save($text = null)
	{
		if(empty($text))
		{
			if(!$this->validate())
			{
				return false;
			}
			$text = "\r\nUser-agent: *\r\n\r\n";
			$text .= "Crawl-delay: $this->crawlDelay\r\n\r\n";
			foreach ($this->rules as $key => $rule)
			{
				if(!empty($rule))
					$text .= "{$this->getRuleTypes(self::ROBOTS_RULE_DISALLOW)} $rule*\r\n\r\n";
				else
					unset($this->rules[$key]);
			}
			foreach ($this->cleanParams as $key => $cleanParam)
			{
				if(!empty($cleanParam))
					$text .= "Clean-param: $cleanParam*\r\n\r\n";
				else
					unset($this->cleanParams[$key]);
			}
			$text .= "Sitemap: ".app()->getBaseUrl(true)."/sitemap.xml\r\n\r\n";
			$text .= "Host: ".app()->getBaseUrl(true);
		}
		file_put_contents("{$this->getPath()}".DS."robots.txt", $text);
		return true;
	}
	
	public function load($justLoadText = false)
	{
		if(!file_exists("{$this->getPath()}".DS."robots.txt"))
		{
			return false;
		}
		$text = file_get_contents("{$this->getPath()}".DS."robots.txt");
		$this->content = $text;
		if(!$justLoadText)
		{
			$rules = explode("{$this->getRuleTypes(self::ROBOTS_RULE_DISALLOW)} ", $text);
			if(sizeof($rules) > 1)
			{
				for ($i = 1; $i < sizeof($rules); $i++)
				{
					preg_match('/(.*)\*{0,1}[\s\r\n]*/', $rules[$i], $matches);
					if(isset($matches[1]))
						array_push($this->rules, $matches[1]);
				}
			}
			$cleanParams = explode("Clean-param: ", $text);
			if(sizeof($cleanParams) > 1)
			{
				for ($i = 1; $i < sizeof($cleanParams); $i++)
				{
					preg_match('/(.*)\*[\r\n]*/', $cleanParams[$i], $matches);
					if(isset($matches[1]))
						array_push($this->cleanParams, $matches[1]);
				}
			}
			preg_match('/Crawl-delay: (\d*)[\r\n]*/', $rules[0], $matches);
			if(isset($matches[1]) && is_numeric($matches[1]))
				$this->crawlDelay = $matches[1];
		}
		return true;
	}
}