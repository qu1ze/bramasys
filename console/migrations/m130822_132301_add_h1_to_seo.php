<?php

class m130822_132301_add_h1_to_seo extends CDbMigration
{
	public function up()
	{
		$this->execute("
                ALTER TABLE `category_metadata`  
					CHANGE `title` `title` VARCHAR(150) DEFAULT NULL,
					CHANGE `description` `description` VARCHAR(255) DEFAULT NULL,
					CHANGE `keywords` `keywords` VARCHAR(255) DEFAULT NULL,
					ADD `H1` VARCHAR(255) DEFAULT NULL;
            ");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `category_metadata`
                CHANGE `title` `title` VARCHAR(150) NOT NULL,
				CHANGE `description` `description` VARCHAR(255) NOT NULL,
				CHANGE `keywords` `keywords` VARCHAR(255) NOT NULL,
				DROP `H1`;
                ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}