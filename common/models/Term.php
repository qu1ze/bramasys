<?php

/**
 * This is the model class for table "term".
 *
 * The followings are the available columns in table 'term':
 * @property integer $id
 * @property string $title
 * @property string $definition
 * @property string $content
 * @property string $references
 * @property string $photo
 * @property string $author
 * @property string $source
 * @property string $source_date
 * @property integer $update_time
 * @property integer $create_time
 */
class Term extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Term the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'term';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, definition, content, update_time, create_time', 'required'),
            array('title, definition, content, references, photo, author, source, source_date', 'filter', 'filter'=>'trim'),
			array('title', 'unique'),
			array('update_time, create_time', 'numerical', 'integerOnly'=>true),
			array('title, source_date', 'length', 'max'=>50),
			array('photo, author, source', 'length', 'max'=>255),
			array('references', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, definition, content, references, photo, author, source, source_date, update_time, create_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Термин',
			'definition' => 'Определение',
			'content' => 'Контент',
			'references' => 'Список литературы',
			'photo' => 'Фото',
			'author' => 'Автор',
			'source' => 'Источник',
			'source_date' => 'Дата публикации (в источнике)',
			'update_time' => 'Update Time',
			'create_time' => 'Create Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('definition',$this->definition,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('references',$this->references,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('source_date',$this->source_date,true);
		$criteria->compare('update_time',$this->update_time);
		$criteria->compare('create_time',$this->create_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getUrl()
	{
		return app()->categoryController->getCategoryAddress('terms').'/'.$this->title;
	}

	protected function beforeValidate()
	{
		if (!$this->create_time)
			$this->create_time = time();
		$this->update_time = time();
		return parent::beforeValidate();
	}
}