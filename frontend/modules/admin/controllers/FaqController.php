<?php

class FaqController extends Controller
{
	public function actionIndex()
	{		
		$model = new Faq('search');
		
		if (isset($_GET['Faq']))
			$model->attributes = $_GET['Faq'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionCreate()
	{
		$model = new Faq; 
				
		if (isset($_POST['Faq']))
		{			
			$model->attributes = $_POST['Faq'];		
			if ($model->save())
				$this->redirect(array('/admin/faq'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = Faq::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/faq'));				
				
		if (isset($_POST['Faq']))
		{			
			$model->attributes = $_POST['Faq'];
			if ($model->save())
				$this->redirect(array('/admin/faq'));
		}
				
		$this->render('update', array(
			'model' => $model,
		));
	}
	
	public function getCategories($emptyCategory = false)
	{
		$data = CHtml::listData(Category::model()->findAllByAttributes(array('tag' => CategoryTag::model()->findByAttributes(array('name' => 'catalog'))->id)), 'id', 'title');
		return $emptyCategory ? array('' => '') + $data : $data;	
	}
	
	public function getSubCategories($parentCategoryId, $emptyCategory = false)
	{
		$categoryIds = Yii::app()->categoryController->getChildrenIds($parentCategoryId);
		$data = CHtml::listData(Category::model()->findAllByPk($categoryIds), 'id', 'title');		
		return $emptyCategory ? array('' => '') + $data : $data;	
	}
			
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Faq');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Вопрос с ответом отредактированы')));			
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = Faq::model()->findByPk($id)) !== null)
		{
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Вопрос с ответом успешно удалены'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить вопрос с ответом'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: вопрос с ответом не найден'));
	}
}