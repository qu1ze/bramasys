 <div class="header-my-order">
                <h1 class="h3">Мой заказ</h1>
                <div class="edit-my-order cart">
                    <a href="#"><img src="/images/orderedit.jpg"></a>
                    <a class="order-edit" href="#">Редактировать</a>
                </div>
            </div>
            <div id="my-order-prod">
            <?php
            foreach ($cart as $id => $prod)
            {
                if($id !== 'complect')
                    $view = '_product';
                else
                    $view = '_complect';
                $this->renderPartial($view, array(
                    'id' => $id,
                    'prod' => $prod,
                    'product' => Products::model()->findByPk($id),
                    'withPrice' => true,
                ));
            }
            ?>
            </div>
            <div class="header-my-order delivery-my-order ">
                <table><tr><td>
                <h1 class="h3">Доставка:</h1>
                </td><td><div class="edit-my-order price-delivery">
                    <p><?php echo $delivery ?></p>
                </div>
               </td></tr></table>
            </div>
             <div class="header-my-order commission-my-order ">
                <table><tr><td>
                 <h1 class="h3">Комиссия:</h1>
                 </td><td>
                <div class="edit-my-order price-commission">
                    <p><?php echo $commissia ?></p>
                </div>
               </td></tr></table>
            </div>
            <div class="header-my-order total-my-order">
                <h1 class="h3">Итого к оплате:</h1>
                <div class="edit-my-order total-price">
                    <p id="firstPrice"><?php echo $sum.' '.CurrencySys::getLabel(); ?></p>
                </div>
            </div>