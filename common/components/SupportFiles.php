<?php
class SupportFiles extends CWidget
{
	public $productId;
	
	public function run()
    {
		$productSupports = ProductSupport::model()->findAllByAttributes(array('product_id' => $this->productId));
		$supportIds = array();
		foreach ($productSupports as $productSupport)
		{
			array_push($supportIds, $productSupport->support_file_id);
		}
		$supportFiles = SupportFile::model()->findAllByPk($supportIds);
        $this->render("support_files", array('supportFiles' => $supportFiles));
    }
}
?>
