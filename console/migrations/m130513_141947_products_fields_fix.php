<?php

class m130513_141947_products_fields_fix extends CDbMigration
{
	public function up()
	{
            $this->execute("ALTER TABLE `products`
                ADD `full_description` TEXT NOT NULL AFTER `description`,
                ADD `image_url` VARCHAR(255) NOT NULL DEFAULT '/images/layout/noimg.png' AFTER `full_description`");
	}

	public function down()
	{
            $this->execute("ALTER TABLE `products` DROP `full_description`");
            $this->execute("ALTER TABLE `products` DROP `image_url`");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}