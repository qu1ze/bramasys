<?php
/* @var $product Products */
if(!empty($products)):
?>
<div class="top-product-block">
	<div class="title-block">Топ продуктов:</div>
	<?php 
	$count = 1;
	foreach ($products as $product):?>
	<div class="item">
		<div class="item-number"><?php echo $count; ?></div>
		<div class="item-detail">
			<div class="img">
				<a href="<?php echo $product->link; ?>"><img src="<?php echo $product->image_url; ?>" alt=""></a>
			</div>
			<div class="name">
				<a href="<?php echo $product->link; ?>"><?php echo $product->name; ?></a>
			</div>
			<div class="price"><?php echo CurrencySys::exchange($product->price).' '.CurrencySys::getLabel(); ?></div>
		</div>
	</div>
	<?php 
	$count++;
	endforeach; ?>
</div>
<?php endif;?>