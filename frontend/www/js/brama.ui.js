/**
 * Created with JetBrains PhpStorm.
 * User: antonio
 * Date: 11/3/12
 * Time: 10:02 PM
 * To change this template use File | Settings | File Templates.
 */
var brama = {}; // namespace
brama.ui = (function($){
    return {
        notify: function(title, msg, sticky){
            $.gritter.add({
                title: title || '',
                text: msg,
                sticky: sticky,
                time: 3000 // hang on the screen for...
            });
        },
        scrollTop: function() {
            $('html, body').animate({
                scrollTop: $('#header').offset().top
            }, 1000);
        },
        updateViews: function(update)
        {
            if(!update){return false;}
            update = update.indexOf("|") ? update.split("|") : [update];
            $.each(update, function(){
                var item = this.split(':'); // format: g:ID <- update a grid, ID is the grid
                if(item[0]=='g') // is a gridview that we have to update
                {
                    $('#'+item[1]).yiiGridView('update');
                }
                if(item[0]=='l') // is a listView
                {
                    $.fn.yiiListView.update(item[1],{});
                }
            });
        }
    };
})(jQuery);

$(document).ready(function () {

    // === Tooltips === //
    $('.tip').tooltip();
    $('.tip-left').tooltip({ placement:'left' });
    $('.tip-right').tooltip({ placement:'right' });
    $('.tip-top').tooltip({ placement:'top' });
    $('.tip-bottom').tooltip({ placement:'bottom' });


    // === Add new objects with pop-over === //
    $(document).on('click', '.btn-new', function(){
        var that = $(this);
        var url = that.attr('data-href');

        $.colorbox({href:url, title:that.attr('data-title'), height:that.attr('data-height')});
    })
        // === Close colorbox === //
        .on('click', '.btn-colorbox-close', function(){$.colorbox.close();})

        // === Switch buttons === //
        .on('click', '.control-switch', function(){
            var that = $(this);
            $('.'+ that.attr('data-panels')).hide();
            $('#'+ that.attr('data-target')).show();
            return false;
        })
        // === Toggle visibility buttons === //
        .on('click', '.control-toggle', function(){
            var that = $(this);
            if(that.hasClass('active'))
            {
                that.removeClass('active');
                $('#'+that.attr('data-target')).hide();
            }
            else
            {
                that.addClass('active');
                $('#'+that.attr('data-target')).show();
            }
            return false;
        })
        .on('click', '.control-url', function(){
            var url = $(this).attr('data-href');
            document.location.href = url;
            return true;
        })
        .on('click','.control-linkupdate', function(){
            var that = $(this);
            var url = that.prop('href') || that.attr('data-href');
            var info = $('#'+that.attr('data-info'));
            var update = that.attr('data-update');
            var confirmMsg = that.attr('data-confirm');
            var fn = function() {
                info.spin({ lines: 13,length: 4, width: 2, radius: 5,corners: 0});

                $.ajax({url:url, type: 'post',
                    success: function(d){
                        info.spin(false).html(d);
                        brama.ui.updateViews(update);
                    },
                    error: function(){that.spin(false);brama.ui.notify('error','Unexpected error on test!')}});
            }
            if(confirmMsg)
            {
                bootbox.confirm(confirmMsg, 'No', 'Yes', function(result){
                    if(result){fn();}});
            } else {
                fn();
            }
            return false;
        })
        // === Delete buttons === //
        .on('click', '.control-delete', function() {
            var that = $(this);
            var confirmation = that.attr('data-confirmation');
            var update = that.attr('data-update');
            var info = $(that.attr('data-info'));
            if(confirmation)
            {
                bootbox.confirm(confirmation, 'No', 'Yes', function(result){
                    if(result){
                        info.spin();
                        $.ajax({
                            url: that.prop('href'),
                            data: that.attr('data-params'),
                            type: 'post',
                            dataType: 'json',
                            success: function(d) {
                                info.spin(false);
                                if(d){

                                    brama.ui.notify('success', 'Successfully removed');
                                    brama.ui.updateViews(update);
                                }else{
                                    brama.ui.notify('error', 'Unexpected error occurred!');
                                }

                            },
                            error: function(){
                                info.spin(false);brama.ui.notify('error','Unexpected error occurred!');
                            }
                        });
                    }
                });
            }
            return false;
        })
        .on('click', '.control-update', function(){
            var that = $(this);

            if(that.hasClass('disabled'))
            {
                return false;
            }
            var frm = $(that.attr('data-form'));
            var data = frm.serialize();
            that.addClass('disabled');
            var info = $(that.attr('data-info'));
            var url = that.attr('data-url');
            var update = that.attr('data-update');
            that.spin({ lines: 13,length: 4, width: 2, radius: 5,corners: 0});
            $.ajax({url:url, data: data, type: 'post',
                success: function(d){
                    that.removeClass('disabled').spin(false); info.html(d);
                    brama.ui.updateViews(update);
                },
                error: function(){that.removeClass('disabled').spin(false);brama.ui.notify('error','Unexpected error on test!')}});
            return false;
        })
    ;
});
