<?php
	

	//app()->bootstrap->registerAssetJs('spin.js');
	
	Yii::app()->clientScript->registerScript("changeCount", "
        $('body').on('keyup', '.input-cart', function() {
			var isComplect = $(this).children(0).attr('data-isComplect');
			var id = $(this).children(0).attr('rel');
			var count = $(this).children(0).val();
			if(!isNaN(parseFloat(count)) && isFinite(count) && count < 1)
			{
				count = 1;
			}
			if(id)
			{
				var url = '/cart/ajaxChangeCount';
				$.ajax(
				{
					url: url,
					data: {Product:{id: id, count: count}, isComplect: isComplect},
					dataType: 'json',
					type: 'post',
					success: function(data){
						if(typeof(data) == 'object' && data.title == 'Success')
						{
							var product = 0;
							if(isComplect > 0)
							{
								product = $('.complect_'+id);
								$(product.selector + ' .cart-prod-count').text('x' + $('.input-cart input').val() + ' шт.');
							}
							else
							{
								product = $('.product_'+id);
								$(product.selector + ' .cart-prod-count').text('x' + $('.input-cart input').val() + ' шт.');
							}
							$(product.selector + ' .product-price-cart #firstPrice').text(data.amount);
							if($(product.selector + ' .product-price-cart #lastPrice'))
								$(product.selector + ' .product-price-cart #lastPrice').text(data.amountLast);
							$('.total-price #firstPrice').text(data.sum);
							if($('.total-price #lastPrice'))
								$('.total-price #lastPrice').text(data.sumLast);
						}
					}
				});
			}
		});
    ", CClientScript::POS_READY);
	
	Yii::app()->clientScript->registerScript("putProduct", "
		$('body').on('click', '.btn-buy-buy', function(){
			var isComplect = 0;
			if($(this).attr('data-is_complect') == '1')
				isComplect = 1;
			var id = $($('.show .'+ $(this).attr('data-id') +' .pd-code span')[0]).text();
			count = $('.show .'+ $(this).attr('data-id') +' .pd-qty-block input').val();
			var url = '/cart/ajaxPutProduct';
			$.ajax(
			{
				url: url,
				data: {Product:{id: id, count: count, mounting: 0}, isComplect: isComplect},
				dataType: 'json',
				type: 'post',
				success: function(data){
					if(typeof(data) == 'object')
					{
						if(data.title == 'Success')
						{
							oldCount = parseInt($('.prod_number span').text());
							if(oldCount < 1)
							{
								$('.cart_block').fadeOut('slow', function(){
										$('#cart_btn_order').fadeIn()
									});
							}
							if(data.count != oldCount)
								$('.prod_number span').slideUp('slow', function(){
									$('.prod_number span').text(data.count);
									$('.prod_number span').slideDown();
								});
							var that = 0;
							if(isComplect > 0)
								that = 'complPd' + id;
							else
								that = 'pd' + id;
							$('.'+ that + ' .btn-buy-'+ id +'.enabled').fadeOut('slow', function(){
										$('.'+ that + ' .btn-buy-'+ id +'.enabled').hide();
										$('.'+ that + ' .btn-buy-'+ id +'.disabled').fadeIn();
									});
								
						}
					}
				}
			});
			return false;
		});
	", CClientScript::POS_READY);
	
	Yii::app()->clientScript->registerScript("changeMounting", "
        $('body').on('change', '.checkbox-cart input', function() {
			var isComplect = $(this).attr('data-isComplect');
			var checked = this.checked;
			var id = $(this).attr('rel');
			if(id)
			{
				var url = '/cart/ajaxChangeMounting';
				$.ajax(
				{
					url: url,
					data: {Product:{id: id, mounting: checked}, isComplect: isComplect},
					dataType: 'json',
					type: 'post',
					success: function(data){
//						if(typeof(data) == 'object')
//						{
//						}
					}
				});
			}
		});
    ", CClientScript::POS_READY);
	
	Yii::app()->clientScript->registerScript("putProductWithMounting", "
		$('body').on('click', '.href-buy-buy', function(){
			var isComplect = 0;
			if($(this).attr('data-is_complect') == '1')
				isComplect = 1;
			var id = $($('.show .'+ $(this).attr('data-id') +' .pd-code span')[0]).text();
			var count = $('.show .'+ $(this).attr('data-id') +' .pd-qty-block input').val();
			var url = '/cart/ajaxPutProduct';
			$.ajax(
			{
				url: url,
				data: {Product:{id: id, count: count, mounting: 1}, isComplect: isComplect},
				dataType: 'json',
				type: 'post',
				success: function(data){
					if(typeof(data) == 'object')
					{
						if(data.title == 'Success')
						{
							oldCount = parseInt($('.prod_number span').text());
							if(oldCount < 1)
							{
								$('.cart_block').fadeOut('slow', function(){
										$('#cart_btn_order').fadeIn()
									});
							}
							if(data.count != oldCount)
								$('.prod_number span').slideUp('slow', function(){
									$('.prod_number span').text(data.count);
									$('.prod_number span').slideDown();
								});
							var that = 0;
							if(isComplect > 0)
								that = 'complPd' + id;
							else
								that = 'pd' + id;
							$('.'+ that + ' .btn-buy-'+ id +'.enabled').fadeOut('slow', function(){
										$('.'+ that + ' .btn-buy-'+ id +'.enabled').hide();
										$('.'+ that + ' .btn-buy-'+ id +'.disabled').fadeIn();
									});
						}
					}
				}
			});
			return false;
		});
	", CClientScript::POS_READY);
	
	Yii::app()->clientScript->registerScript("cart", "
        function loadCart()
		{
			$.colorbox({href:'/cart/ajaxIndex'}, function(){
			$.colorbox.resize();});
		}

		$('body').on('click', '.cart', function(){
			loadCart();
			return false;
        });

		$('body').on('click', '.btn-buy-isset', function(){
			loadCart();
			return false;
		});

		$('body').on('click', '.delete-cart-item', function(){
			var isComplect = $(this).attr('data-isComplect');
			var id = $(this).attr('rel');
			var url = '/cart/ajaxDropProduct';
			$.ajax(
			{
				url: url,
				data: {id: id, isComplect: isComplect},
				dataType: 'json',
				type: 'post',
				success: function(data){
					if(typeof(data) == 'object')
					{
						var dropedProduct = 0;
						if(isComplect > 0)
							dropedProduct = $('.complect_'+id);
						else
							dropedProduct = $('.product_'+id);
						dropedProduct.fadeOut('slow', function()
						{
							dropedProduct.remove();
							$.colorbox.resize({height:($('body #cart-box').height() + 77)});
							$('.total-price #firstPrice').text(data.sum);
							if($('.total-price #lastPrice'))
								$('.total-price #lastPrice').text(data.sumLast);
							if(data.count < 1)
								$('#cart_btn_order').fadeOut('slow', function(){
										$('.prod_number span').text(0);
										$('.cart_block').fadeIn()
									});
							else
								$('.prod_number span').slideUp('slow', function(){
									$('.prod_number span').text(data.count);
									$('.prod_number span').slideDown();
								});
							if(isComplect > 0)
								that = 'complPd' + id;
							else
								that = 'pd' + id;
							$('.'+ that + ' .btn-buy-'+ id +'.disabled').fadeOut('slow', function(){
									$('.'+ that + ' .btn-buy-'+ id +'.disabled').hide();
									$('.'+ that + ' .btn-buy-'+ id +'.enabled').fadeIn()
								});
						});
					}
				}
			});
			return false;
		});
		
		$('body').on('click', '#closeCart', function(){
			$.colorbox.close();
			return false;
		});
	", CClientScript::POS_READY);
	$pdCount = sizeof(Yii::app()->session['cart']);
	$pdCount += isset(Yii::app()->session['cart']['complect']) ? sizeof(Yii::app()->session['cart']['complect']) - 1: 0;
?>
<div class="cart_block cart" style="<?php echo $pdCount < 1 ? '' : 'display:none;'?>">
	<div id="cart_btn" class="cart_btn">
		<img src="/images/cart-icon.png" alt=""><a href="#">Корзина</a>
	</div>
	<div id="cart_status">пуста</div>
</div>
<div id="cart_btn_order" class="cart_btn_order cart_hide" style="<?php echo $pdCount > 0 ? '' : 'display:none;'?>">
	<div class="cart">
		<img src="/images/cart-icon.png" alt="">
		<a href="#">В корзине</a><br>
		<a class="prod_number" href="#"> <span><?php echo $pdCount; ?></span> товаров</a>
	</div>
    <div class="cart_btn login_reg-btn btn_order"><a href="#" onclick="window.location='<?php echo app()->getBaseUrl(true); ?>/order/placeorder';">Оформить заказ</a></div>
</div>
