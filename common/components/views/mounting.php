<?php
	Yii::app()->clientScript->registerScript("sendMountingForm","
		$('#ajax-mounting-form').on('click', '.mounting-btn', function () {
			var form = $('#ajax-mounting-form');
			$(form).children('.alert').fadeOut();
			var errorList = $(form).children('.alert').children('.error-list');
			var errorLine = $($(errorList).children()[0]).clone();
			$(errorList).children().remove()
			$(errorList).append(errorLine);
			var MountingForm = $(form).serializeArray();
			$.ajax({
				url: '/site/ajaxMounting',
				data: MountingForm,
				dataType: 'json',
				type: 'post',
				success: function(data){
					if(typeof(data) == 'object' && data.title == 'Success')
					{
						$('#mounting-form-wrapper').click();
						$('#mounting-response-wrapper').slideDown(600);
						$('#mounting-response').fadeIn(800);
					}
					else if (typeof(data.message) == 'object')
					{
						for(var field in data.message)
						{
							var newLine = $(errorLine).clone();
							$(errorList).append($(newLine).text(data.message[field][0]));
							$(newLine).fadeIn();
						}
						$(form).children('.alert').fadeIn();
					}
				}
			});
		});
   ", CClientScript::POS_READY);

	Yii::app()->clientScript->registerScript("mounting-show-hide","
		$('body').on('click', '#mounting-btn', function () {
			$('#mounting-form-wrapper').slideUp(0).slideDown(300);
			$('#mounting-form-container').fadeIn(800);
            $('#MountingForm_phone').focus();
			return false;
		});
		$('#mounting-response').on('click', '#close', function () {
			$('#mounting-response-wrapper').click();
			return false;
		});
		$('body').on('click', '#mounting-response-wrapper:not(#mounting-form-container)', function (e) {
			if(e.target.id !== '#mounting-response' && !$(e.target).parents().is('#mounting-response'))
			{
				$('#mounting-response-wrapper').slideUp(600);
				$('#mounting-response').fadeOut(300);
				return false;
			}
		});
		$('#mounting-form-container').on('click', '#close', function () {
			$('#mounting-form-wrapper').click();
			return false;
		});
		$('body').on('click', '#mounting-form-wrapper:not(#mounting-form-container)', function (e) {
			if(e.target.id !== '#mounting-form-container' && !$(e.target).parents().is('#mounting-form-container'))
			{
				$('#mounting-form-wrapper').slideUp(600);
				$('#mounting-form-container').fadeOut(300);
				return false;
			}
		});
   ", CClientScript::POS_READY);
	
	$categories = app()->categoryController->getChilds(ProductsController::getTag());
?>
<table id="mounting-form-wrapper" style="display:none; background-color: rgba(92, 92, 92, 0.6);"><tr><td>
	<div id="mounting-form-container" style="display:none;">
		<div class="mounting-form-header">
			<h3>ЗАКАЗАТЬ МОНТАЖ</h3>
			<a class="close hover" id="close" href="#" style="top: -5px;">
			</a>
		</div>
		<div class="mounting-form-content">
			<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'id' => 'ajax-mounting-form',
				'enableAjaxValidation' => false,
				'enableClientValidation' => true,
				'clientOptions' => array(
				'validateOnSubmit' => true,
				),
			)); ?>

				<div class="alert alert-block alert-error" style="display:none;"><p>Необходимо исправить следующие ошибки:</p>
					<ul class="error-list">
						<li style="display:none;"></li>
					</ul>
				</div>

				<?php echo $form->textFieldRow($mountingForm, 'phone'); ?>
				<label class="subLabel">В формате (093) 777-77-77</label>
				<?php echo $form->textFieldRow($mountingForm, 'name'); ?>
				<?php echo $form->textAreaRow($mountingForm, 'address'); ?>
				<div class="right-block">
					<div class="actions">
						<?php echo CHtml::button('Заказать монтаж', array('class' => 'mounting-btn')); ?>
					</div>
				</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</td></tr></table>

<table id="mounting-response-wrapper" style="display:none; background-color: rgba(92, 92, 92, 0.6);"><tr><td>
	<div id="mounting-response" style="display:none;">
		<a class="close hover" id="close" href="#" style="top: 5px;"></a>
		<p>Спасибо! Мы перезвоним Вам в течение одного часа, при условии,
что ваше обращение было сделано согласно нашего графика роботы:</p>
		<div>
			Пн.-Пт. 9:00-18:00<br/>
            Суббота 10:00-17:00<br/>
            Воскресенье 11:00-16:00
		</div>
		<hr>
		Возможно, вам будет интересен <a href="<?php echo app()->getBaseUrl(true); ?>/company/klienty-kompanii">список клиентов, которые нам доверяют </a>
        <hr>
        Вы можете пока ознакомится с товаром у нас в каталоге:
		<ul>
			<!--<?php foreach ($categories as $category): ?>
			<li><a href="<?php echo app()->categoryController->getCategoryAddress($category['data']['name']);?>"><?php echo $category['data']['title'];?></a></li>
			<?php endforeach; ?>-->
           
            <li><a href="<?php echo app()->getBaseUrl(true); ?>/uslugi/okhrannye-sistemy)">Установка охранных систем</a></li>
            <li><a href="<?php echo app()->getBaseUrl(true); ?>/uslugi/pultovaya-okhrana-zhilya">Охрана жилья</a></li>
            <li><a href="<?php echo app()->getBaseUrl(true); ?>/uslugi/avtomaticheskie-vorota">Установка автоматических ворот</a></li>
            <li><a href="<?php echo app()->getBaseUrl(true); ?>/uslugi/strukturirovannye-kabelnye-sistemy">Монтаж СКС </a></li>
            <li><a href="<?php echo app()->getBaseUrl(true); ?>/uslugi/smart-house">Установка системы &quot;умный дом&quot;</a></li>

		</ul>
	</div>
</td></tr></table>