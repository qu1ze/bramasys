<div id="commentPattern" style="display: none;">
	<div id="commentMainBlock" style="display: none;">
		<li class="comment" id="comment<?php // append comment id here?>" style="position: relative; display: none;">
			<table>
				<tr class="comment-blocks">
					<td class="comment-container">
						<div class="comment-content">
							<font style="float:left;">Комментарий:</font>
							<span></span>
						</div>
					</td>
				</tr>
			</table>
			<ul></ul>
		</li>
	</div>
	<div id="commentTitleBlock" style="display: none;">
		<div class="comment-author-name">
			,<span class="comment-post-date"></span>
		</div>
	</div>
	<div id="commentRatingBlock" style="display: none;">
		<div class="comment-rating">
			<font style="float:left;">Оценка товара:</font>
		</div>
	</div>
	<div id="commentStarBlock" style="display: none;">
		<div class="star"><span style="width: 100%;"></span></div>
	</div>
	<div id="commentProsBlock" style="display: none;">
		<div class="comment-pros">
			<font style="float:left;">Плюсы:</font>
			<span></span>
		</div>
	</div>
	<div id="commentConsBlock" style="display: none;">
		<div class="comment-cons">
			<font style="float:left;">Минусы: </font>
			<span></span>
		</div>
	</div>
	<div id="commentNotApprovedBlock" style="display: none;">
		<div style="
					position: absolute;
					width: 100%;
					height: 100%;
					margin: 0;
          background-color: rgba(237, 245, 244, 0.7);
          box-shadow: 0 0 10px #EDF5F4;
          z-index:1;
				">
			<table style="height: 100%;">
				<tr>
					<td style="text-align: center; font: 18px 'PTSN Bold';">
						Ваш отзыв будет доступен после проверки модератором
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div id="commentResponseBlock" style="display: none;">
		<div class="comment-response">
			<a comment-id="">Ответить</a>
		</div>
	</div>
	<table style="display: none;">
		<tr id="commentUsefulBlock">
			<td class="useful-block">
				<span>0</span> голосов
			</td>
		</tr>
	</table>
</div>
<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'post_comment',
		'type'=>'vertical',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
		'htmlOptions' => array('style' => 'display:none;', 'class' => 'response_form'),
	));
?>
	<div class="alert alert-block alert-error" style="display:none;"><p>Необходимо исправить следующие ошибки:</p>
		<ul class="error-list">
			<li style="display:none;"></li>
		</ul>
	</div>
	<?php if(Yii::app()->user->id == null): ?>
		<?php
		$model->setAttributeLabel('author_name', $model->getAttributeLabel('author_name').', или <a href="'.app()->getBaseUrl(true).'/site/login">авторизируйтесь</a>');
		echo $form->textFieldRow($model, 'author_name', array('value' => 'Гость'));
		?>
	<?php else: ?>
		<?php echo CHtml::label($model->getAttributeLabel('author_name'), 'Comment_author_name')?>
		<?php echo "<p>".Yii::app()->user->getState('name')."</p>"; ?>
	<?php endif; ?>
	<?php $model->setAttributeLabel('email', $model->getAttributeLabel('email').'<span class="subtext"> (для уведомления об ответе)</span>');?>
	<?php echo $form->textFieldRow($model, 'email'); ?>
	<?php echo CHtml::hiddenField("Comment[{$type}_id]", $contentId)?>
	<?php echo $form->textAreaRow($model, 'content'); ?>
	<?php echo CHtml::button('Добавить отзыв', array('class' => 'post-btn')); ?>
<?php $this->endWidget(); ?>