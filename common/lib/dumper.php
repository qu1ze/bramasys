<?php
function create_table_gen($table, &$drop_fk, &$add_fk, $db)
{
$query = "SHOW CREATE TABLE ". $table;
$com = $db->createCommand($query);
$res = $com->query();
$crea_table=$res->read();
$crea_table=current($crea_table);    
$crea_table = preg_replace(
            '/^CREATE TABLE/',
            'CREATE TABLE IF NOT EXISTS',
            $crea_table
        );
$sql_lines = preg_split('/\\r\\n?|\\n/', $crea_table);
$sql_count = count($sql_lines);
for ($i= 0; $i < $sql_count; $i++) {
    if (stripos($sql_lines[$i],'CONSTRAINT')){
       $add_fk .= 'ADD ' . $sql_lines[$i] . '\n\r';
    }
}
 
}

$dump_file = 'dump.sql';
$db = Yii::app()->db;
$transaction=$db->beginTransaction();
try
{
@unlink($dump_file);
$tables = "SHOW TABLES";
$comTables = $db->createCommand($tables);
$res = $comTables->query();
 $fp = fopen( $dump_file, "a" );
fwrite ($fp, "SET AUTOCOMMIT = 0;
SET FOREIGN_KEY_CHECKS=0;");
while( ($table=$res->read())!==false )
{
   
    if ( $fp )
    {
        $query = "TRUNCATE TABLE `".current($table)."`;\n";
        fwrite ($fp, $query);
        $rows = 'SELECT * FROM `'.current($table).'`';
        $comContent = $db->createCommand($rows);
        $resContent = $comContent->query();
        while( ($row=$resContent->read())!==false )
        {
            $query = "";
            foreach ( $row as $field )
            {
                if ( is_null($field) )
                    $field = "NULL";
                else
                    $field = "'".mysql_escape_string( $field )."'";
                if ( $query == "" )
                    $query = $field;
                else
                    $query = $query.', '.$field;
            }
            $query = "INSERT INTO `".current($table)."` VALUES (".$query.");\n";
            fwrite ($fp, $query);
        }
        
    }
}
fwrite ($fp, 'SET FOREIGN_KEY_CHECKS = 1;
                    COMMIT;
                    SET AUTOCOMMIT = 1;');
        fclose ($fp);
if (file_exists($dump_file)) {
    if (ob_get_level()) {
      ob_end_clean();
    }
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($dump_file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: no-cache');
    header('Pragma: public');
    header('Content-Length: ' . filesize($dump_file));
    readfile($dump_file);
        $db->createCommand('SET AUTOCOMMIT = 0;')->execute();
	$db->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
        $comTables = $db->createCommand($tables);
        $res = $comTables->query();
        while( ($table=$res->read())!==false )
        {
	       	$query = "TRUNCATE TABLE `".current($table)."`;\n";
		$db->createCommand($query)->execute();

	}
        $db->createCommand('SET FOREIGN_KEY_CHECKS = 1')->execute();
	$db->createCommand('COMMIT;
                            SET AUTOCOMMIT = 1;')->execute();
	@unlink($dump_file);
       
}
 $transaction->commit();
}
catch(Exception $e) 
{
    echo $e;
    $transaction->rollback();
}
?>