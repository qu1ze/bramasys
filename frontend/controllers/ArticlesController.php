<?php

class ArticlesController extends StaticController
{		
	public $general_category_id;
	
	public static function getTag()
	{
		return 'static';
	}
	
	public function beforeAction($action)
	{
	   $r = parent::beforeAction($action);
       if ($this->categoryName == 'stati-publikatcii-obzory')
			$this->general_category_id = $this->categoryId;
		else
			$this->general_category_id = ($category = Category::model()->findByPk($this->categoryId)) ? $category->parent : $this->categoryId;	
		return $r;
	}
	
	public function actionIndex()
	{
        
		$model = new Article('search');			
		
		$all = new stdClass;
		$all->id = $this->general_category_id;
		$all->name = '';
		$all->title = 'Обо всем';
		
		$sub_categories = array_merge(array($all), Category::model()->findAllByAttributes(array('parent' => $this->general_category_id)));
		
		$criteria = new CDbCriteria;
		$criteria->with = 'category';
		if ($this->general_category_id !== $this->categoryId)
			$criteria->addCondition('category_id = ' . $this->categoryId);
		$dataProvider = new CActiveDataProvider(new Article, array(
			'criteria' => $criteria
		));
				
		// echo '<pre>';	
			// print_r($_GET);				
			// print_r($this);				
		// echo '</pre>';die();
        $this->render('index', array(
			'sub_categories' => $sub_categories,
			'model' => $model,			
			'dataProvider' => $dataProvider,
		));   
	}
    
        
        
	public function actionView($name)
	{
		if (($model = Article::model()->findByAttributes(array('name' => $name))) === null)
			throw new CHttpException(404, 'Oops!');
		
		$model->views += 1;
		if (!$model->save())
			Yii::app()->user->setFlash('error', 'Saving error!');
		
		$related_products = CHtml::listData(ArticleProduct::model()->findAllByAttributes(array('article_id' => $model->id)), 'id', 'product_id');
		$related_articles = RelatedArticle::model()->findAllByAttributes(array('article_id' => $model->id));
		$this->render('view', array(
			'model' => $model,
			'related_products' => $related_products,
			'related_articles' => $related_articles,
		));
	}

	
}
