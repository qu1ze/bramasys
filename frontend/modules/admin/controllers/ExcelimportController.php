<?php

class ExcelimportController extends Controller
{
	private function uploadFile($file)
	{
		$excelFile = new ExcelFile();
		$excelFile->file = $file;
		if(!$excelFile->validate('file', false))
		{
			return $excelFile;
		}
		$uploader = CUploadedFile::getInstance($excelFile, 'file');
		if($uploader != null)
		{
			$path = app()->basePath.DIRECTORY_SEPARATOR.'www'.DIRECTORY_SEPARATOR.md5($uploader->name.time()).'.'.$uploader->extensionName;
			$uploader->saveAs($path);
			$excelFile->file = $path;
			$excelFile->extensionName = $uploader->extensionName;
		}
		else
		{
			$excelFile->addError('file', "Не удалось загрузить файл.");
		}
		return $excelFile;
	}
	
	private function setCategoryTreeChilds(&$worksheet, $caregoryId, $columnNumner, $rowNumner)
	{
		foreach(app()->categoryController->getChilds($caregoryId) as $caregory)
		{
			$worksheet->setCellValueByColumnAndRow($columnNumner, $rowNumner, $caregory['data']['name']);
			$rowNumner++;
			$rowNumner = $this->setCategoryTreeChilds($worksheet, $caregory['id'], $columnNumner, $rowNumner);
		}
		return $rowNumner;
	}
	
	public function saveRecord($recordArray)
	{
		if(!is_numeric($recordArray['A']))
		{
			return 'id должен быть числовым';
		}
		$product['excel_id'] = (int)$recordArray['A'];
		
		$newProduct = Products::model()->findByAttributes(array('excel_id' => $product['excel_id']));
		if($newProduct == null)
		{
			$brand = Brand::model()->findByAttributes(array('name' => $recordArray['B']));
			if($brand === null)
			{
				return 'Бренд не найден или поле пустое';
			}
			$product['brand'] = $brand->id;

			if(empty($recordArray['C']))
			{
				return 'Поле модель пустое';
			}
			$product['model'] = $recordArray['C'];

			$category = Category::model()->findByAttributes(array('name' => $recordArray['G']));
			if($category === null)
			{
				return 'Категория не найден или поле пустое';
			}
			if($category->parent === null)
			{
				return 'Импортировать товары в категорию 1-го уровня запрещено';
			}
			$product['category'] = $category->id;
			
			$newProduct = new Products();
		}
		if(empty($recordArray['D']))
		{
			return 'Поле описания пустое';
		}
		$product['description'] = mb_substr($recordArray['D'], 0, 255, 'UTF-8');
		
		$product['quantity'] = $recordArray['H'];
		
		$price = $recordArray['E'];
		$UAHPrice = $recordArray['F'];
		if(!empty($UAHPrice))
		{
			if(is_numeric($UAHPrice))
				$price = CurrencySys::exchange($UAHPrice, CurrencySys::DEFAULT_CURRENCY, CurrencySys::UAH);
			else
				return 'Поле цена в грн. должно быть цифровым';
		}
		else if(!empty($price))
		{
			if(!is_numeric($price))
				return 'Поле цена в у.е. должно быть цифровым';			
		}
		else
		{
			return 'Оба поля цены не могут быть пустыми или содержать \'0\'. Заполните одно из них.';	
		}
		$product['price'] = $price;
		$product['old_price'] = 0;
		
		$newProduct->attributes = $product;
		
		if($newProduct->save())
			return true;
		return 'Непредвиденная ощибка. Возможно дублирование уникальных полей';
	}
	
	public function actionIndex()
	{
		Yii::import('common.extensions.EZendAutoloader', true);
		EZendAutoloader::$prefixes = array('PHPExcel', 'PHPExcel_Reader', 'PHPExcel_Writer');
		EZendAutoloader::$basePath = Yii::getPathOfAlias('common.extensions.phpexcel') . DS;
		Yii::registerAutoloader(array("EZendAutoloader", "loadClass"), true);
		
		$excelReader = new PHPExcel_Reader_Excel2007();
		$excel = $excelReader->load(app()->basePath.DIRECTORY_SEPARATOR.'www/template.xlsx');
		$worksheet = $excel->getActiveSheet();
		$brands = Brand::model()->findAll();
		foreach($brands as $index => $brand)
		{
			$rowNumber = $index + 2;
			$worksheet->setCellValue("J$rowNumber", $brand->name);
		}
		
		$columnNumner = 10;
		foreach(app()->categoryController->getChilds(ProductsController::getTag()) as $caregory)
		{
			$worksheet->setCellValueByColumnAndRow($columnNumner, 2, $caregory['data']['title']);
			$this->setCategoryTreeChilds($worksheet, $caregory['id'], $columnNumner, 3);
			$columnNumner++;
		}
		$excelWriter = new PHPExcel_Writer_Excel2007();
		$excelWriter->setPHPExcel($excel);
		$excelWriter->save(app()->basePath.DIRECTORY_SEPARATOR.'www/newTemplate.xlsx');
		
		if(isset($_FILES['ExcelFile']))
		{
			$excelFile = $this->uploadFile($_FILES['ExcelFile']);
			if(!$excelFile->hasErrors())
			{
				if($excelFile->extensionName == 'xls')
				{
					$excelReader = new PHPExcel_Reader_Excel5();
				}
				$excel = $excelReader->load($excelFile->file);
				$worksheet = $excel->getActiveSheet();
				$data = array();
				foreach ($worksheet->getRowIterator() as $row)
				{
					$cellIterator = $row->getCellIterator();
					$cellIterator->setIterateOnlyExistingCells(false);
					foreach ($cellIterator as $cell)
					{
						if($cell->getRow() != 1 && in_array($cell->getColumn(), array('A','B','C','D','E','F','G', 'H')))
							$data[$cell->getRow()][$cell->getColumn()] = $cell->getValue();
					}
				}
				$transaction = Yii::app()->db->begintransaction();
				try
				{
					foreach ($data as $key => $rows)
					{
						$isNull = true;
						foreach ($rows as $cell)
						{
							if($cell !== null)
							{
								$isNull = false;
							}
						}
						if($isNull)
						{
							unset($data[$key]);
						}
						else
						{
							$result = $this->saveRecord($data[$key]);
							if(!is_bool($result))
							{
								throw new Exception("строка $key: $result");
							}
						}
					}
					$transaction->commit();
				}
				catch (Exception $e)
				{
				   $transaction->rollback();
				   $excelFile->addError('file', $e->getMessage());
				}
			}
			@unlink($excelFile->file);
		}
		else
		{
			$excelFile = new ExcelFile();
		}
		
		$this->render('index', array(
			'excelFile' => $excelFile,
		));
	}
}