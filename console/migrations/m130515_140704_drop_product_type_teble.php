<?php

class m130515_140704_drop_product_type_teble extends CDbMigration
{
	public function up()
	{
            $this->dropTable("product_type");
	}

	public function down()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `product_type` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `name` varchar(255) NOT NULL,
                    PRIMARY KEY (`id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");	
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}