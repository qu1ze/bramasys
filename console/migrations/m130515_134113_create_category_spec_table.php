<?php

class m130515_134113_create_category_spec_table extends CDbMigration
{
	public function up()
	{
            $this->execute("CREATE TABLE IF NOT EXISTS `category_spec` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `category_id` int(11) NOT NULL,
              `spec_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `category_id` (`category_id`),
              KEY `spec_id` (`spec_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
            $this->execute("ALTER TABLE `category_spec`
			  ADD CONSTRAINT `FK_spec_category_spec` FOREIGN KEY (`spec_id`) REFERENCES `specification` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              ADD CONSTRAINT `FK_category_category_spec` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
              ");
	}

	public function down()
	{
            $this->dropTable("category_spec");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}