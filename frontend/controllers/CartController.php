<?php

class CartController extends Controller
{
	private $cart;
	private $isComplect;
	
	private function getCount()
	{
		$count = sizeof(Yii::app()->session['cart']);
		if(isset(Yii::app()->session['cart']['complect']))
		{
			$count--;
			$count += isset(Yii::app()->session['cart']['complect']) ? sizeof(Yii::app()->session['cart']['complect']) : 0;
		}
		return $count;
	}
	
	private function getSum()
	{
		$sum = 0;
		foreach ($this->cart as $key => $product) {
		if($key !== 'complect')
			$sum += $product['price'] * $product['count'];
		}
		if($this->isComplect)
			$cart = Yii::app()->session['cart'];
		else
			$cart = isset(Yii::app()->session['cart']['complect']) ? Yii::app()->session['cart']['complect'] : array();
		foreach ($cart as $key => $product) {
			if($key !== 'complect')
				$sum += $product['price'] * $product['count'];
		}
		return $sum;
	}
	
	protected function beforeAction($action)
    {
		$this->isComplect = 0;
		if(isset($_POST['isComplect']) && $_POST['isComplect'] > 0)
				$this->isComplect = 1;
		if(isset(Yii::app()->session['cart']))
		{
			if(!$this->isComplect)
				$this->cart = Yii::app()->session['cart'];
			else
				$this->cart = isset(Yii::app()->session['cart']['complect']) ? Yii::app()->session['cart']['complect'] : array();
		}
		else
		{
			$this->cart = array();
		}
		return true;
	}
	
	public function actionAjaxIndex()
	{
        cs()->reset();
        cs()->corePackages=array();
		$this->renderPartial('ajax_index', array('cart' => isset(Yii::app()->session['cart']) ? Yii::app()->session['cart'] : array()), false, true);
	}
	
	public function actionAjaxChangeCount()
	{
		if (isset($_POST['Product']) && Yii::app()->request->isAjaxRequest)
		{
			$productId = $_POST['Product']['id'];
			if(!empty($this->cart[$productId]) && $_POST['Product']['count'] > 0)
			{
				$this->cart[$productId]['count'] = $_POST['Product']['count'];
				$amount = $this->cart[$productId]['count'] * $this->cart[$productId]['price'];
				$sum = $this->getSum();
				echo json_encode(array(
					'title'=>'Success',
					'msg'=>'Count changed',
					'amount'=>CurrencySys::exchange($amount).' '.CurrencySys::getLabel(),
					'amountLast'=>$amount.' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY),
					'sum'=>CurrencySys::exchange($sum).' '.CurrencySys::getLabel(),
					'sumLast'=>$sum.' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY),
					));
			}
		}
	}
	
	public function actionAjaxChangeMounting()
	{
		if (isset($_POST['Product']) && Yii::app()->request->isAjaxRequest)
		{
			$_POST['Product']['mounting'] = $_POST['Product']['mounting'] == 'true' ? 1 : 0;
			$productId = $_POST['Product']['id'];
			if(!empty($this->cart[$productId]) && $_POST['Product']['mounting'] != $this->cart[$productId]['mounting'])
			{
				$this->cart[$productId]['mounting'] = $_POST['Product']['mounting'];
				echo json_encode(array(
					'title'=>'Success',
					'msg'=>'Mounting changed',
					));
			}
		}
	}
	
	public function actionAjaxDropProduct()
	{
		if (isset($_POST['id']) && Yii::app()->request->isAjaxRequest)
		{
			unset($this->cart[$_POST['id']]);
			$count = $this->getCount() - 1;
			$sum = $this->getSum();
			echo json_encode(array(
				'title'=>'Success',
				'sum'=>CurrencySys::exchange($sum).' '.CurrencySys::getLabel(),
				'sumLast'=>$sum.' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY),
				'count'=>$count,
				));
		}
	}
	
	public function actionAjaxPutProduct()
	{
		if (isset($_POST['Product']) && Yii::app()->request->isAjaxRequest)
		{
			$productId = $_POST['Product']['id'];
			if(!$this->isComplect)
				$product = Products::model()->findByPk($productId);
			else
				$product = Complect::model()->findByPk($productId);
			
			if($product === null)
			{
				echo json_encode(array('title'=>'Error', 'msg'=>'Product not found'));
				app()->end();
			}
			$count = $_POST['Product']['count'];
			if($count > 0 && !empty($product->quantity))
			{
				$pdCount = $this->getCount();
				if(empty($this->cart[$productId]))
				{
					$this->cart[$productId] = array(
						'count' => $count,
						'price' => $product->price,
						'mounting'=> $_POST['Product']['mounting'],
					);
					$pdCount++;
				}
				else
					$this->cart[$productId]['count'] += $count;
				echo json_encode(array('title'=>'Success', 'msg'=>'Product puted in cart', 'count'=>$pdCount));
			}
			else
				echo json_encode(array('title'=>'Error', 'msg'=>'Product count must be bigger than 0'));
		}
	}
	
	protected function afterAction($action)
    {
		if($this->isComplect)
		{
			$someArray['complect'] = $this->cart;
			$this->cart = Yii::app()->session['cart'];
			$this->cart['complect'] = $someArray['complect'];
		}
		Yii::app()->session['cart'] = $this->cart;
	}
}
