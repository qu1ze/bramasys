<?php
Yii::app()->getClientScript()->registerCssFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/themes/ui-lightness/jquery-ui.css');
Yii::app()->getClientScript()->registerCssFile('/css/slider.css');

Yii::app()->clientScript->registerScript("Filter", "
function initSlider(){

var maxPriceVal = ".(Products::MAX_PRODUCT_PRICE).";
var minPriceVal = ".Products::MIN_PRODUCT_PRICE.";
var maxCostVal = ".Products::MAX_PRODUCT_PRICE.";
var minCostVal = ".Products::MIN_PRODUCT_PRICE.";
if($('input#maxCost').val())
	maxCostVal = $('input#maxCost').val();
if($('input#minCost').val())
	minCostVal = $('input#minCost').val();
jQuery('#slider').slider('destroy');
jQuery('#slider').slider({
min: minPriceVal,
max: maxPriceVal,
values: [minCostVal,maxCostVal],
range: true,
animate: 'fast',
stop: function(event, ui) {
	
	jQuery('input#minCost').val(jQuery('#slider').slider('values',0));
	jQuery('input#maxCost').val(jQuery('#slider').slider('values',1));
	jQuery('#minCost').change();
	jQuery('#maxCost').change();
        if(jQuery('#slider').slider('option', 'max') < ".Products::MAX_PRODUCT_PRICE." && (jQuery('#slider').slider('values',1) + 500) >= jQuery('#slider').slider('option', 'max'))
	{
                var end_animate = jQuery('#slider').slider('values',1);
                var start_animate = ".Products::MAX_PRODUCT_PRICE.";
                jQuery('#slider').slider('option', 'max', ".Products::MAX_PRODUCT_PRICE.");
                jQuery('#slider').slider('option', 'animate', false);
                jQuery('#slider').slider('values',1, ".Products::MAX_PRODUCT_PRICE.");
                jQuery('#maxCostLable').animate({
                            opacity: 0,
                            right: '150'}, 500)
                            .delay().queue( function(next){
                            $(this).css({'opacity': '1','right': '0'});
                            $(this).text('".Products::MAX_PRODUCT_PRICE."');
                            $(this).fadeIn(100);
                            next();
                            });
                setTimeout(function(){
                    jQuery('#slider').slider('option', 'animate', 'slow');
                    jQuery('#slider').slider('values',1, end_animate);
                    
                },50);
                
                    
	}

},
slide: function(event, ui){
	jQuery('input#minCost').val(jQuery('#slider').slider('values',0));
	jQuery('input#maxCost').val(jQuery('#slider').slider('values',1));
}

});
}

initSlider();
jQuery('body').on('change','input#minCost', function(){

	var value1=jQuery('input#minCost').val();
	var value2=jQuery('input#maxCost').val();

	if(parseInt(value1) > parseInt(value2)){
		value1 = value2;
		jQuery('input#minCost').val(value1);
	}
	jQuery('#slider').slider('values',0,value1);
});

jQuery('body').on('change','input#maxCost',function(){

	var value1=jQuery('input#minCost').val();
	var value2=jQuery('input#maxCost').val();

	if (value2 > ".Products::MAX_PRODUCT_PRICE.") { value2 = ".Products::MAX_PRODUCT_PRICE."; jQuery('input#maxCost').val(".Products::MAX_PRODUCT_PRICE.")}
jQuery('#slider').slider('values',1,value2);
	if(parseInt(value1) > parseInt(value2)){
		value2 = value1;
		jQuery('input#maxCost').val(value2);
	}
	if(parseInt(value2) > jQuery('#slider').slider('option', 'max')){
		jQuery('#slider').slider('option', 'max', parseInt(value2));
	}
	else if ((parseInt(value2) < ".(Products::MAX_PRODUCT_PRICE/10).")&& (jQuery('#slider').slider('option', 'max')!=".(Products::MAX_PRODUCT_PRICE/10).") ){
             var end_animate = jQuery('#slider').slider('values',1);
                var start_animate = Math.round(end_animate/".Products::MAX_PRODUCT_PRICE."*".(Products::MAX_PRODUCT_PRICE/10).");
		jQuery('#slider').slider('option', 'max', ".(Products::MAX_PRODUCT_PRICE/10).");
                jQuery('#slider').slider('option', 'animate', false);
                jQuery('#slider').slider('values',1, start_animate);
                jQuery('#maxCostLable').animate({
                            opacity: 1,
                            right: '-10'}, 20);
                jQuery('#maxCostLable').fadeOut(100)
                            .delay().queue(function(next){
                            $(this).css({'opacity': '0','right': '150px','display':'block'});
                            $(this).text('".(Products::MAX_PRODUCT_PRICE/10)."');
                            next();
                            });
                 jQuery('#maxCostLable').animate({
                            opacity: 1,
                            right: '0'}, 500);
                                    
                setTimeout(function(){
                    jQuery('#slider').slider('option', 'animate', 'slow');
                    jQuery('#slider').slider('values',1, end_animate);

                 },50);
		
	}
	
});

function RemoveCheckFilter(checkBox)
{
	$(checkBox).removeAttr('checked');
	SetFilter();
}

function RemoveTextFilter(textBox)
{
	$(textBox).removeAttr('value');
	SetFilter();
}

function clearSearch(pathname)
{
	if(!pathname)
		pathname = window.location.pathname;
	var getRequest = $('#filters').serialize();
	var parts = getRequest.split('&');
	getRequest = '';
	parts.forEach(function (value, key) {
		if(value.charAt(value.length - 1) != '=')
			getRequest += parts[key] + '&';
	})
	getRequest = getRequest.substr(0, getRequest.length - 1);
	window.location = pathname + '#:' + decodeURI(getRequest);
        
}

$('body').on('change', '#filters input:not(.price)', function() {
	SetFilter();
});
	
$('body').on('click', '#setPriceButton', function() {
	SetFilter();
});

$('body').on('click', '#chosen_filters li', function() {
	var forElement = $(this).children('.cross').attr('for');
	if(forElement)
	{
		if(forElement != 'search')
		{
			if($('#' + forElement).attr('type') == 'text')
			{
				RemoveTextFilter('#' + forElement);
			}
			else
			{
				RemoveCheckFilter('#' + forElement);
			}
		}
		else
		{
			clearSearch();
		}
	}
});
$('body').on('click', '.spec_select', function() {
    var inputid = $(this).attr('inputid');
    if ($('#'+inputid).prop('checked')) 
        $('#'+inputid).prop('checked', false);
        else 
        $('#'+inputid).prop('checked', true);
        $('#'+inputid).change();
    });
", CClientScript::POS_END);
?>

<div id="product_filter">
	<div id="chosen_filters">
		<?php $filtersCount=0;
        if (!empty($_GET['search'])) : ?>
		<h3>Поисковый запрос</h3>
		<ul>
			<li><a class="cross" for="search">x</a><a> <?php echo strlen($_GET['search']) < 31 ? $_GET['search'] : substr($_GET['search'], 0, 29).'...'?></a></li>
		</ul>
		<?php endif; 
        ob_start();
        ?>
        <h3>Выбранные фильтры</h3>
		<ul>
			<?php if (!empty($price)) :
                $filtersCount++;?>
            <li>
                Цена:
                <ul>
					<?php if(isset($price['min'])) : ?>
					<li><a class="cross" for="minCost">x</a><a> от <?php echo $price['min']?></a></li>
					<?php endif;?>

					<?php if(isset($price['max'])) : ?>
					<li><a class="cross" for="maxCost">x</a><a> до <?php echo $price['max']?></a></li>
					<?php endif;?>
                </ul>
            </li>
            <?php endif;?>
			<?php if (!empty($offers)) : 
                $filtersCount++;?>
            <li>
                Выгодные предложения
                <ul>
                    <?php
                        $offersLabels = array(
                            'top_seller' => 'Лидер продаж',
                            'novelty' => 'Новинка',
                            'price_of_the_week' => 'Снижение цены',
                        );
                    ?>
                    <?php foreach ($offersLabels as $offerName => $label) : ?>
                        <?php if (!empty($offers[$offerName])) : ?>
                        <li><a class="cross" for="<?php echo $offerName?>">x</a><a> <?php echo $label?></a></li>
                        <?php endif?>
                    <?php endforeach;?>
                </ul>
            </li>
            <?php endif;?>
			<?php if (!empty($getBrands)) : 
                $filtersCount++;?>
            <li>
                Производители
                <ul>
                    <?php foreach ($getBrands as $brandId) : ?>
                        <li><a class="cross" for="<?php echo "brnd".$brandId?>">x</a><a> <?php echo $brands[$brandId]['name']?></a></li>
                    <?php endforeach;?>
                </ul>
            </li>
            <?php endif;?>
			<?php if (!empty($specs)) : 
                $filtersCount++;?>
			<?php foreach ($specs as $specId => $specValues) : ?>
            <li>
                <?php  echo $specifications[$specId]['name']; ?>
                <ul>
                    <?php foreach ($specValues as $specValue => $id) : ?>
                    <li><a class="cross" for="<?php echo "val".$id?>">x</a><a> <?php echo urldecode($specValue)?></a></li>
                    <?php endforeach;?>
                </ul>
            </li>
			<?php endforeach;?>
            <?php endif;?>
		</ul>
        <?php $filterBlock = ob_get_contents();
        ob_end_clean();
        if ($filtersCount>0) echo $filterBlock; ?>
        <?php if($filtersCount>0): ?>
		<div class="filterClear">
            <a href="<?php echo app()->getBaseUrl(true); ?><?php echo "/".Yii::app()->request->getPathInfo(); ?><?php echo !empty($_GET['search']) ? "?search=".$_GET['search'] : ''; ?>">
                <img src="/images/filterClear.png" alt="" />
                Сбросить все фильтры
            </a>
        </div>
        <?php endif; 
        ?>
    </div>
	<form id="filters">
		<h3>Выбор по фильтрам</h3>
		<ul>
            <?php if(Yii::app()->controller->id !== 'complect'): ?>
			<li>
                <div class="title_spec" id="filter_by_offers">Выгодные предложения</div>
                <ul>
                    <li>
                        <?php
                            $checked = isset($offers['top_seller']) ? ' checked="checked"' : '';
							$disabled = $counts['topSellers'][0] !== '+' && $counts['topSellers'] == 0 ? 'disabled' : '';
                        ?>
                        <input <?php echo $disabled ?> id="top_seller" type="checkbox"<?php echo $checked?> name="top_seller" value="1">
						<span <?php echo !empty($disabled) ? 'style="color:gray;"' : '' ?>>
                        <?php if (empty($disabled)): ?>
                            <a href="#" inputid="top_seller" class="spec_select" >Лидер продаж</a>
                        <?php  else : ?>
                            Лидер продаж
                        <?php endif; ?>
                            </span>
						<?php if(empty($checked)):?>
						<span class="count">(<?php echo $counts['topSellers']?>)</span>
						<?php endif;?>
                    </li>
                    <li>
                        <?php
							$checked = isset($offers['novelty']) ? ' checked="checked"' : '';
							$disabled = $counts['novelties'][0] !== '+' && $counts['novelties'] == 0 ? 'disabled' : '';
                        ?>
                        <input <?php echo $disabled ?> id="novelty" type="checkbox"<?php echo $checked?> name="novelty" value="1">
						<span <?php echo !empty($disabled) ? 'style="color:gray;"' : '' ?>>
                        <?php if (empty($disabled)): ?>
                            <a href="#" inputid="novelty" class="spec_select" >Новинки</a>
                        <?php  else : ?>
                            Новинки
                        <?php endif; ?>
                        </span>
						<?php if(empty($checked)):?>
						<span class="count">(<?php echo $counts['novelties']?>)</span>
						<?php endif;?>
                    </li>
                    <li>
                        <?php
							$checked = isset($offers['price_of_the_week']) ? ' checked="checked"' : '';
							$disabled = $counts['priceOfTheWeek'][0] !== '+' && $counts['priceOfTheWeek'] == 0 ? 'disabled' : '';
                        ?>
                        <input <?php echo $disabled ?> id="price_of_the_week" type="checkbox"<?php echo $checked?>  name="price_of_the_week" value="1">
						<span <?php echo !empty($disabled) ? 'style="color:gray;"' : '' ?>>
                        <?php if (empty($disabled)): ?>
                            <a href="#" inputid="price_of_the_week" class="spec_select" >Цена недели</a>
                        <?php  else : ?>
                            Цена недели
                        <?php endif; ?>
                        </span>
						<?php if(empty($checked)):?>
						<span class="count">(<?php echo $counts['priceOfTheWeek']?>)</span>
						<?php endif;?>
					</li>
                </ul>
			</li>
            <?php endif;?>
			<li>
				Цена:
                <div class="formCost">
                    <?php
                    $min_price_val = isset($price['min']) ? $price['min'] : '';
                    $max_price_val = isset($price['max']) ? $price['max'] : '';
                    ?>
                    <label for="minCost">от</label> <input class="price" type="text" name="min_price" id="minCost" value="<?php echo $min_price_val?>"/>
                    <label for="maxCost">до</label> <input class="price" type="text" name="max_price" id="maxCost" value="<?php echo $max_price_val?>"/>
					<input type="button" name="setPrice" id="setPriceButton" value="Ok"/>
                </div>
                <div class="sliderCont">
                    <div id="slider"></div>
					<div id="minCostLable" style="float:left;font-size: 10px">0</div>
                    <div id="maxCostLable" style="position: relative;float:right;font-size: 10px; opacity: 1;"><?php echo Products::MAX_PRODUCT_PRICE; ?></div>
                </div>
            </li>
			<?php if(!empty($brands)):?>
			<li>
                <span class="title_spec" id="filter_by_brand">Производитель</span>
                <ul class="brand_list">
                    <?php foreach($brands as $id => $brand) : ?>
					<li>
						<?php
						$getBrands = isset($getBrands) ? $getBrands : array();
						$brandChecked = in_array($id, $getBrands) ? 'checked="checked"' : '';
						$disabled = $brand['product_count'] == 0 ? 'disabled' : '';
						?>
						<input <?php echo $disabled ?> id="<?php echo "brnd".$id?>" brand-name="<?php echo $brand['name']?>" type="checkbox" name="brand[]" value="<?php echo $id?>"<?php echo $brandChecked?> />
                        <span <?php echo !empty($disabled) ? 'style="color:gray;"' : '' ?>>
                        <?php if (empty($disabled)): ?>
                            <a href="#" inputid="<?php echo "brnd".$id?>" class="spec_select" ><?php echo $brand['name']?></a>
                        <?php  else : ?>
                            <?php echo $brand['name']?>
                        <?php endif; ?>
						<?php if(empty($brandChecked)):?>
						<span class="count">(<?php echo $brand['product_count']; ?>)</span>
						<?php endif;?>
					</li>
					<?php endforeach;?>
                </ul>
            </li>
			<?php endif;?>
			<?php foreach($specifications as $specId => $specValues) : ?>
			<li class="hint">
                <span class="title_spec" ><?php echo $specValues['name']; ?></span><a class="question" spec-id="<?php echo $specId; ?>" href="#"></a>
                <ul>
					<?php foreach($specValues['values'] as $specValue) :?>
                    <li>
                        <?php
						$specChecked = (isset($specs[$specValue->spec_id])&&(array_search($specValue->id, $specs[$specValue->spec_id])!=false)) ? 'checked="checked"' : '';
                        $disabled = $specValue->product_count == 0 ? 'disabled' : '';
						?>
                        <input <?php echo $disabled ?> id="<?php echo "val".$specValue->id?>" type="checkbox" name="<?php echo "specs[$specValue->spec_id][".urlencode($specValue->value)."]";?>" <?php echo $specChecked?> value="<?php echo $specValue->id?>" />
                        <span <?php echo !empty($disabled) ? 'style="color:gray;"' : '' ?>>
                        <?php if (empty($disabled)): ?>
                            <a href="#" inputid="<?php echo "val".$specValue->id?>" class="spec_select" ><?php echo $specValue->value; ?></a>
                        <?php  else : ?>
                            <?php echo $specValue->value; ?>
                        <?php endif; ?>    
                        </span>
						<?php if(empty($specChecked)):?>
						<span class="count">(<?php echo $specValue->product_count; ?>)</span>
						<?php endif;?>
                    </li>
                    <?php endforeach;?>
                </ul>
            </li>
			<?php endforeach;?>
		</ul>
	</form>
</div>