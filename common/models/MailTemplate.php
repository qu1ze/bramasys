<?php

/**
 * This is the model class for table "mail_template".
 *
 * The followings are the available columns in table 'mail_template':
 * @property integer $id
 * @property string $name
 * @property string $from
 * @property string $to
 * @property string $subject
 * @property string $content
 */
class MailTemplate extends ActiveRecord
{
    private $systemTemplates = array(
        'mounting',
        'callback',
        'registration',
        'order',
        'newpassword',
    );

    private $origAttrs = array();

    protected function afterFind()
    {
        $this->origAttrs['name'] = $this->name;
        return parent::afterFind();
    }

    public function isSystemTemplate()
    {
        if(isset($this->origAttrs['name']))
        {
            if(in_array($this->origAttrs['name'], $this->systemTemplates) && MailTemplate::model()->countByAttributes(array('name' => $this->origAttrs['name'])) == 1)
            {
                return true;
            }
        }
        return false;
    }

    public function getOrig($attribute)
    {
        return isset($this->origAttrs[$attribute]) ? $this->origAttrs[$attribute] : null;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MailTemplate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mail_template';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, from, to, subject, content', 'required'),
            array('name, from, to, subject, content', 'filter', 'filter'=>'trim'),
            array('name', 'validateIsSystem'),
			array('name, from, to, subject', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, from, to, subject, content', 'safe', 'on'=>'search'),
		);
	}

    public function validateIsSystem($attribute)
    {
        if($this->isSystemTemplate())
        {
            if($this->getAttribute($attribute) != $this->getOrig($attribute))
                $this->addError($attribute, 'В этом шаблоне поле '.$this->getAttributeLabel($attribute).' не редактируемое');
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'from' => 'From',
			'to' => 'To',
			'subject' => 'Subject',
			'content' => 'Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('from',$this->from,true);
		$criteria->compare('to',$this->to,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
/*
 * params 
 *  file - NULL or array('name'=>'','file'=>'')
 */
    public function send(array $args, $file=NULL)
    {
        $from = $this->from;
        $body = "$this->content";
        $to = $this->to;
        $subject = $this->subject;
        foreach($args as $template => $arg)
        {
            $body = str_replace($template, $arg, $body);
            $to = str_replace($template, $arg, $to);
            $from = str_replace($template, $arg, $from);
            $subject = str_replace($template, $arg, $subject);
        }
        
        $header = "From: $from \r\nContent-type:text/html; charset = utf-8";
        if($file!==NULL && is_array($file)){
            $message = $body;
            $boundary = "---=bramasys=--";
            $header = "From: $from \r\nContent-Type: multipart/mixed; boundary=\"$boundary\"";
            $body = "--$boundary\n";
            $body .= "Content-type: text/html; charset='utf-8'\n";
            $body .= "Content-Transfer-Encoding: quoted-printablenn";
            $body .= "Content-Disposition: attachment; filename=\"=?utf-8?B?".base64_encode('body')."?=\"\n\n";
            $body .= $message."\n";
            $body .= "--$boundary\n";
            $text = $file['file'];
            $body .= "Content-Disposition: attachment; filename=\"=?utf-8?B?".base64_encode($file['name'])."?=\"\n";
            $body .= "Content-Transfer-Encoding: base64\n";
            $body .= "Content-Type: application/octet-stream; name=\"=?utf-8?B?".base64_encode($file['name'])."?=\"\n\n"; 
            $body .= chunk_split(base64_encode($text))."\n";
            $body .= "--".$boundary ."--\n"; 
        }
        mail($to, "=?utf-8?B?".base64_encode($subject)."?=", $body, $header);
        return true;
    }
}