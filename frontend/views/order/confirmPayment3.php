<?php $client = Cart::model()->findByAttributes(array('order_id' => $order->id))->client; ?>
<p>Спасибо!</p>
<div>
	Мы оповещены об оформленном заказе и свяжемся с вами в ближайшее время.</div>
<div>
	Тем временем, товар будет зарезервирован.&nbsp;</div>
<div>
	На электронный адрес &laquo;<a href="mailto:<?php echo $client->email ?>" target="_blank"><?php echo $client->email ?></a>&raquo; отправлен счет на оплату в кассе банка.</div>
<div>
	&nbsp;</div>
<div>
    <a href="/order/invoice?id=<?php echo $order->id;?>">Посмотреть и распечатать счет в отдельном окне</a></div>


