<?php

class NewsproductsController extends Controller
{		
	public function actionCreate()
	{
		$model = new NewsProduct; 
		
		$this->performAjaxValidation($model);
		
		if (isset($_POST['NewsProduct']))
		{			
			if (isset($_POST['NewsProduct']['product_id']) && is_array($_POST['NewsProduct']['product_id']))
                if($model->saveProducts($_POST['NewsProduct']['product_id'],$_POST['NewsProduct']['news_id']))
                	$this->redirect(array('/admin/newsproducts'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new NewsProduct();	

		if (isset($_POST['NewsProduct']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['NewsProduct'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Связь новость-продукт успешно создана'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Невозможно создать связь'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new NewsProduct('search');
		
		if (isset($_GET['NewsProduct']))
			$model->attributes = $_GET['NewsProduct'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = NewsProduct::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/newsproducts'));				
				
		if (isset($_POST['NewsProduct']))
		{			
			if (isset($_POST['NewsProduct']['product_id']) && is_array($_POST['NewsProduct']['product_id']))
                if($model->saveProducts($_POST['NewsProduct']['product_id'],$_POST['NewsProduct']['news_id'],true))
                    $this->redirect(array('/admin/newsproducts'));
		}
		$mass = NewsProduct::model()->findAllByAttributes(array('news_id' => $model->news_id)); 
        foreach ($mass as $value)
            $model->product_id_s[]=$value->product_id;			
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('NewsProduct');		
		$es->update();		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Связь новость-продукт успешно отредактирована')));			
	}	
	
	public function actionAjaxDelete($id)
	{		
		if (($model = NewsProduct::model()->findByPk($id)) !== null)
		{			
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Связь успешно удалена'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить связь'));
			
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить: Связь новость-продукт не найдена'));
	}
	
}