<?php
echo $this->renderPartial('index',array(
				'dataProvider'=>$dataProvider,
				'isSearch' => $this->isSearch,
                ));
?>
<?php
			$isMainCategory = false;
            if((Yii::app()->controller->id == 'products' || Yii::app()->controller->id == 'complect') && Yii::app()->controller->action->id == 'index'):
			if(!Yii::app()->controller->isSearch):
			$categoryVars = app()->categoryController->getCategory(Yii::app()->controller->categoryIds[0]);
			$isMainCategory = $categoryVars['data']['isMain'];
			endif;
			if(!$isMainCategory):
			?>
			<aside id="sideRight">
				<?php $this->widget('common.components.ProductFilter'); ?>
				<?php $this->widget('common.components.TopProducts', array('categoriesIds' => Yii::app()->controller->categoryIds)); ?>
			</aside>
			<?php endif; endif; ?>
