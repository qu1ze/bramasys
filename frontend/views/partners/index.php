<div id="container">
<?php
Yii::app()->clientScript->registerScript("partnersShow","
	 $('body').on('click', '.show-partners', function () {
		$(this).toggleClass('hide-partners');
		$(this).toggleClass('show-partners');
		var ul = $(this).parent().children('ul');
		$(ul).children('li').slideDown();
		$(this).text('Скрыть');
	 });
	 
	 $('body').on('click', '.hide-partners', function () {
		$(this).toggleClass('hide-partners');
		$(this).toggleClass('show-partners');
		var ul = $(this).parent().children('ul');
		$(ul).children('li:gt(2)').slideUp();
		$(this).text('Развернуть');
	 });
	", CClientScript::POS_READY);

$this->breadcrumbs = app()->categoryController->getCategoryBcs($this->categoryName);

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
?>
	<div class="partners">
		<h3><?php echo app()->categoryController->getTitle($this->categoryId); ?></h3>
		<?php foreach ($partnersCategories as $title => $partners) : ?>
		<div class="content_partners">
			<div class="partners-group">
				<p><?php echo $title; ?></p>
				<ul>
					<?php 
					$partnersCount = 0;
					foreach ($partners as $partner) :
					if($partner->isAnnounceable):
					?>
					<li <?php echo $partnersCount > 2 ? 'style="display:none;"' : '';?>>
						<table>
							<tr>
								<td width="150">
									<a class="imgUrl" href="<?php echo $partner->pageUrl; ?>"><img width="150" src="<?php echo $partner->img_url; ?>"></a>
								</td>
								<td>
									<p>
										<a href="<?php echo $partner->pageUrl; ?>"><?php echo $partner->name; ?></a><br>
										<?php echo $partner->short_description; ?>
									</p>
								</td>
							</tr>
						</table>
					</li>
					<?php 
					$partnersCount++;
					endif;
					endforeach; ?>
				</ul>
				<?php if($partnersCount > 2):?>
				<a class="show-partners">Развернуть</a>
				<?php endif;?>
			</div>

			<div class="arrow-up">
				<a href="#">Вверх<img src="/images/arrowup.jpg"></a>
			</div>
		</div>
		<?php endforeach;?>
	</div>
</div>