<?php

/**
 * This is the model class for table "category_spec".
 *
 * The followings are the available columns in table 'category_spec':
 * @property integer $id
 * @property integer $category_id
 * @property integer $spec_id
 * @property integer $order
 * @property boolean $in_filter
 *
 * The followings are the available model relations:
 * @property Specification $spec
 * @property Category $category
 */
class CategorySpec extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CategorySpec the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category_spec';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, spec_id', 'required'),
			array('category_id', 'uniqueWithSpec_id'),
			array('category_id, spec_id, order', 'numerical', 'integerOnly'=>true),
            array('in_filter', 'boolean'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_id, spec_id, order, in_filter', 'safe', 'on'=>'search'),
		);
	}

	public function uniqueWithSpec_id()
	{
		if(($model = CategorySpec::model()->findByAttributes(array('category_id' => $this->category_id, 'spec_id' => $this->spec_id))) !== null)
		{
			if($model->id != $this->id)
				$this->addError('category_id', 'Поля категория-характеристика должны быть уникальными');
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'spec' => array(self::BELONGS_TO, 'Specification', 'spec_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Категория',
			'spec_id' => 'Характеристика',
            'order' => 'Порядок',
            'in_filter' => 'Отображать в фильтре',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('spec_id',$this->spec_id);
        $criteria->compare('order',$this->order);
        $criteria->compare('in_filter',$this->in_filter);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    protected function beforeDelete()
    {
        if(CategorySpec::model()->countByAttributes(array('category_id' => app()->categoryController->AddParentsIds($this->category_id), 'spec_id' => $this->spec_id)) < 2)
        {
            $products = Products::model()->findAllByAttributes(array('category' => $this->category_id));
            foreach ($products as $product)
            {
                $specValue = ProductSpecValue::model()->findByAttributes(array('product_id' => $product->id, 'spec_id' => $this->spec_id));
                $specValue->delete();
            }
        }
        return parent::beforeDelete();
    }

    protected function beforeSave()
    {
        $categoryIds = app()->categoryController->GetChildrenIds($this->category_id);
        array_push($categoryIds, $this->category_id);
        $products = Products::model()->findAllByAttributes(array('category' => $categoryIds));
        foreach ($products as $product)
        {
            if(ProductSpecValue::model()->findByAttributes(array('product_id' => $product->id, 'spec_id' => $this->spec_id)) === null)
            {
                $model = new ProductSpecValue();
                $model->attributes = array('product_id' => $product->id, 'spec_id' => $this->spec_id);
                $model->save();
            }
        }
        return parent::beforeSave();
    }
}