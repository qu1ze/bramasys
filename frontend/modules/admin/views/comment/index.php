<?php
$grid_id = 'products-table';

$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $grid_id,
	'type' => 'bordered striped',
	'dataProvider' => $model->search(),
	'filter' => $model,	
	'afterAjaxUpdate' => 'js:function(){
		jQuery(".tooltip").remove();
		jQuery("a[rel=tooltip]").tooltip();				
	}',
	'template' => "{items}\n{pager}",
	'columns' => array(
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'content',
			'editable' => array(
				'type' => 'text',
				'url' => app()->createUrl('/admin/comment/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
                                                brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			),
			'htmlOptions' => array('style' => '
				overflow: hidden;
				max-width: 150px;
				width: 150px;
				text-overflow: ellipsis;
				white-space: nowrap;
			'),
		),	
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'pros',
			'editable' => array(
				'type' => 'text',
				'url' => app()->createUrl('/admin/comment/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
                                                brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			),
			'htmlOptions' => array('style' => '
				overflow: hidden;
				max-width: 150px;
				width: 150px;
				text-overflow: ellipsis;
				white-space: nowrap;
			'),
		),
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'cons',
			'editable' => array(
				'type' => 'text',
				'url' => app()->createUrl('/admin/comment/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
                                                brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			),
			'htmlOptions' => array('style' => '
				overflow: hidden;
				max-width: 150px;
				width: 150px;
				text-overflow: ellipsis;
				white-space: nowrap;
			'),
		),
		array(
			'class'=>'bootstrap.widgets.TbToggleColumn',
			'toggleAction'=>'/admin/comment/toggle',
			'checkedButtonLabel' => 'Подтвержден',     
			'uncheckedButtonLabel' => 'Не подтвержден',
			'filter' => $model->getOrderStatuses(),
			'name' => 'status',
			'htmlOptions' => array('style' => '
				width: 70px;
				max-width: 70px;
			'),
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('nowrap' => 'nowrap'),			
			'template' => '{delete} {update}',
			'updateButtonUrl'=>'Yii::app()->controller->createUrl("update", array("id" => $data->primaryKey))',
			'deleteButtonUrl'=>'Yii::app()->controller->createUrl("ajaxDelete", array("id" => $data->primaryKey))',
			'deleteConfirmation' => 'Вы уверенны что хотите удалить?',
			'afterDelete' => 'function(link,success,data){
				if(success){data = $.parseJSON(data);brama.ui.notify(data.title, data.msg, false);}
				else{brama.ui.notify("Oops!", data.responseText )};
			}',
		)
	)
));

?>