<?php
/* @var $robotsModel Robots */

Yii::app()->clientScript->registerScript("setFileDesc", "
	function setFileDesc()
	{
		var fileDesc = 'Файл robots.txt разрешает индексировать все страницы, кроме тех, которые начинаются с:<br>';
		var rules = $('#rules input');
		var cleanParams = $('#cleanParams input');
		if ($(rules).length > 0)
		{
			$(rules).each(function(key, value)
			{
				if($(value).val())
					fileDesc += $(value).val() + ';<br>';
			});
		}
		if ($(cleanParams).length > 0)
		{
			fileDesc += 'Очищаются параметры:<br>';
			$(cleanParams).each(function(key, value)
			{
				if($(value).val())
					fileDesc += $(value).val() + ';<br>';
			});
		}
		fileDesc += '<br>Задержка при индексации ' + $('#Robots_crawlDelay').val() + ' сек.';
		$('#fileDesc').html(fileDesc);
	}
	
	$('.form-horizontal').on('input', 'input', function(){
		setFileDesc();
	});
	
	$('.form-horizontal').on('change', 'select', function(){
		setFileDesc();
	});
", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript("addRule", "
	$('#add-rule').on('click', function(){
		var input = $('#rule-pattern input').clone();
		var count = $('#rules input').length;
		$(input).attr('name', $(input).attr('name') + '[' + count + ']');
		$('#rules').append(input);
		$(input).slideDown(300, function()
		{
			$(this).css('display', 'block');
		});
	});
", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript("addCleanParam", "
	$('#add-cleanParam').on('click', function(){
		var input = $('#cleanParams-pattern input').clone();
		var count = $('#cleanParams input').length;
		$(input).attr('name', $(input).attr('name') + '[' + count + ']');
		$('#cleanParams').append(input);
		$(input).slideDown(300, function()
		{
			$(this).css('display', 'block');
		});
	});
", CClientScript::POS_READY);

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'horizontal',
));?>
	<?php echo $form->errorSummary($robotsModel); ?>

	<?php echo $form->textFieldRow($robotsModel, 'crawlDelay'); ?>
	
	<div class="control-group ">
		<label class="control-label" for="">Запретить индексировать пути</label>
		<div id="rule-pattern" class="hide"><input style="display: none; margin-bottom: 15px;" name="Robots[rules]" type="text" value=""></div>
		<div class="controls">
			<div style="display: inline-block; width: 100%;">
				<div id="rules" style="float: left;">
				<?php if(!empty($robotsModel->rules)):?>
				<?php 
				$count = 0;
				foreach ($robotsModel->rules as $rule): ?>
					<input style="display: block; margin-bottom: 15px;" name="Robots[rules][<?php echo $count; ?>]" type="text" value="<?php echo $rule; ?>">
				<?php 
				$count++;
				endforeach; ?>
				<?php endif;?>
				</div>
				<div class="help" style="padding-left: 20px; float: left;">
					Если пред введеным адресом, есть категории или страницы, поставте перед адрессом "*".<br>
					Например: "/nashi-vakansii" - такое правило запрещает "host.com/nashi-vakansii",<br>
					но не "host.com/company/nashi-vakansii". Правильно так - "*/nashi-vakansii".<br>
					Также в конце адреса знак "*" ставится автоматически.
				</div>
			</div>
			<a id="add-rule" style="float:none; margin-top: 5px; display: inline-block; cursor: pointer;">Добавить</a>
		</div>
	</div>

	<div class="control-group ">
		<label class="control-label" for="">Очищать параметры</label>
		<div id="cleanParams-pattern" class="hide"><input style="display: none; margin-bottom: 15px;" name="Robots[cleanParams]" type="text" value=""></div>
		<div class="controls">
			<div style="display: inline-block; width: 100%;">
				<div id="cleanParams" style="float: left;">
				<?php if(!empty($robotsModel->cleanParams)):?>
				<?php 
				$count = 0;
				foreach ($robotsModel->cleanParams as $cleanParam): ?>
					<input style="display: block; margin-bottom: 15px;" name="Robots[cleanParams][<?php echo $count; ?>]" type="text" value="<?php echo $cleanParam; ?>">
				<?php 
				$count++;
				endforeach; ?>
				<?php endif;?>
				</div>
				<div class="help" style="padding-left: 20px; float: left;">
					Эти поля заполняются так: XXX YYY, где XXX - название параметра (sort) или нескольких<br>
					параметров (sort&tab), а YYY адрес. Адресс "/" означает все страницы. Также в конце адреса<br>
					знак "*" ставится автоматически.
				</div>
			</div>
			<a id="add-cleanParam" style="float:none; margin-top: 5px; display: inline-block; cursor: pointer;">Добавить</a>
		</div>
	</div>

	<div id="fileDesc">
		Файл robots.txt разрешает индексировать все страницы, кроме тех, которые начинаются с:<br>
		<?php if(!empty($robotsModel->rules)):?>
		<?php 
			foreach ($robotsModel->rules as $rule)
			{
				echo $rule.';<br>';
			}
		?>
		<?php endif;?>
		<?php if(!empty($robotsModel->cleanParams)):?>
		Очищаются параметры:<br>
		<?php 
			foreach ($robotsModel->cleanParams as $cleanParam)
			{
				echo $cleanParam.';<br>';
			}
		?>
		<?php endif;?>
		<br>Задержка при индексации <?php echo $robotsModel->crawlDelay; ?> сек.
	</div>
	<br>
	<div id="fileSource">
		
	</div>
	<br>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
<?php $this->endWidget();?>