<?php

class m130710_013225_add_excel_id_to_products extends CDbMigration
{
	public function up()
	{
		$this->execute("
                ALTER TABLE  `products` ADD  `excel_id` INT(11) DEFAULT NULL AFTER  `id`
            ");
	}

	public function down()
	{
		$this->execute("
				ALTER TABLE `products` DROP `excel_id`
                ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}