<?php

class m130919_042023_add_fields_to_complect extends CDbMigration
{
    public function up()
    {
        $this->execute("
			ALTER TABLE `complect`
				ADD `actual_id` int(11) DEFAULT NULL,
				ADD `full_description` TEXT DEFAULT NULL,
				ADD `discount` float DEFAULT 0;
				");
    }

    public function down()
    {
        $this->execute("
			ALTER TABLE `complect`
				DROP `actual_id`,
				DROP `full_description`,
				DROP `discount`;
				");
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}