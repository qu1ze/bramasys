<?php

class m130914_120710_create_mail_template extends CDbMigration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `mail_template` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                `from` varchar(255) NOT NULL,
                `to` varchar(255) NOT NULL,
                `subject` varchar(255) NOT NULL,
                `content` text NOT NULL,
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");

        $mails = array(
            array(
                'name' => 'mounting',
                'from' => 'noreply@bramasys.com.ua',
                'to' => 'sales@bramasys.com.ua',
                'subject' => 'Монтаж',
                'content' => 'Имя: $name<br>Номер: $phone<br>Адресс: $address',
            ),
            array(
                'name' => 'callback',
                'from' => 'noreply@bramasys.com.ua',
                'to' => 'sales@bramasys.com.ua',
                'subject' => 'Обратный звонок',
                'content' => 'Имя: $name<br>Номер: $phone<br>Тема вопроса: $subject',
            ),
            array(
                'name' => 'registration',
                'from' => 'noreply@bramasys.com.ua',
                'to' => '$email',
                'subject' => 'Регистрация на сайте http://bramasys.com.ua',
                'content' => '
                    <p><strong>Уважаемый(ая),<em> $name</em>!</strong></p>
                    <p>Спасибо, что вы зарегистрировались на сайте &laquo;БРАМА ИС&raquo; &ndash; интернет магазине охранных систем и видеонаблюдения!</p>
                    <p>Ваши данные для авторизации на сайте <a href="http://bramasys.com.ua/">http://bramasys.com.ua</a>:</p>
                    <p>E-mail $email</p>
                    <p>Пароль $password</p>
                    <p>Для изменения личной информации и/или пароля перейдите, пожалуйста, на страницу личного профиля: <a href="http://bramasys.com.ua/user">http://bramasys.com.ua/user</a></p>
                    <p>Если вы не осуществляли регистрацию на сайте <a href="http://bramasys.com.ua/">http://bramasys.com.ua</a>, проигнорируйте это письмо.</p>
                    <p><strong>С уважением,</strong></p>
                    <div>
                    <p><strong>администрация интернет-магазина охранных систем и видеонаблюдения &laquo;БРАМА ИС&raquo;</strong><strong>?</strong></p>
                    <hr style="border-color: #000" />
                    <p>Если у вас возникли сложности в работе с сайтом, изложите суть вопроса в письме и направьте его по адресу: info@bramasys.com.ua.&nbsp;</p>
                    </div>
                    <p>&nbsp;</p>',
            ),
            array(
                'name' => 'order',
                'from' => 'noreply@bramasys.com.ua',
                'to' => '$email',
                'subject' => 'http://bramasys.com.ua. Ваш заказ оформлен!',
                'content' => '
                    <p><strong>Уважаемый(ая),<em> Имя пользователя</em>!</strong></p>
                    <p>Спасибо, что вы совершили покупку в&nbsp; интернет-магазине охранных систем и видеонаблюдения &laquo;БРАМА ИС&raquo;!</p>
                    <p>В ближайшее время наш менеджер свяжется с вами для дальнейшей координации заказа.</p>
                    <p>Просмотреть данные оформленного заказа вы можете, перейдя по ссылке: <a href="http://bramasys.com.ua/user/profile/myorders">http://bramasys.com.ua/user/profile/myorders</a></p>
                    <p>Пользование приобретёнными товарами будет максимально приятным и лишенным сложностей, если вы ознакомитесь с полезной информацией, размещенной в разделе <a href="http://bramasys.com.ua/support/stati-publikatcii-obzory">Статьи, публикации, обзоры</a>, а также с условиями <a href="http://bramasys.com.ua/garantia">Гарантии</a><u>,</u> предоставляемой нашей компанией.</p>
                    <p>Также следите за новостями и акционными предложениями в разделе <a href="http://bramasys.com.ua/company/news">Новости и акции</a> и в социальных сетях:</p>
                    <p>Facebook: <a href="https://www.facebook.com/BramaUA">https://www.facebook.com/BramaUA</a>,</p>
                    <p>Twitter: <a href="https://twitter.com/BramaUA">https://twitter.com/BramaUA</a></p>
                    <p>Вконтакте: <a href="http://vk.com/BramaU">http://vk.com/BramaU</a>A,</p>
                    <p>Google+: <a href="https://plus.google.com/102956489542905048139/posts">https://plus.google.com/102956489542905048139/posts</a>,</p>
                    <p>а также на YouTube: <a href="http://www.youtube.com/BramaUA">http://www.youtube.com/BramaUA</a>.</p>
                    <p><strong>Удачных покупок!</strong></p>
                    <p><strong>С уважением,</strong></p>
                    <div>
                    <p><strong>администрация интернет-магазина охранных систем и видеонаблюдения &laquo;БРАМА ИС&raquo;</strong><strong>​</strong></p>
                    <hr style="border-color: #000" />
                    <p>Если у вас возникли сложности в работе с сайтом, изложите суть вопроса в письме и направьте его по адресу: info@bramasys.com.ua.&nbsp;</p>
                    </div>
                    <p>&nbsp;</p>',
            ),
            array(
                'name' => 'newpassword',
                'from' => 'noreply@bramasys.com.ua',
                'to' => '$email',
                'subject' => 'http://bramasys.com.ua. Восстановление пароля',
                'content' => '
                    <p><strong>Уважаемый(ая),<em> Имя пользователя</em>!</strong></p>
                    <p>Спасибо, что вы воспользовались услугами &laquo;БРАМА ИС&raquo; &ndash; интернет магазина охранных систем и видеонаблюдения!</p>
                    <p>Это письмо было направлено автоматически в ответ на запрос о восстановлении пароля на сайте: <a href="http://bramasys.com.ua/">http://bramasys.com.ua</a>.</p>
                    <p>Для изменения пароля перейдите, пожалуйста, по ссылке: $link</p>
                    <p>Если изменение пароля уже не актуально или вы не пытались осуществить его замену, проигнорируйте это письмо.</p>
                    <p><strong>С уважением,</strong></p>
                    <div>
                    <p><strong>администрация интернет-магазина охранных систем и видеонаблюдения &laquo;БРАМА ИС&raquo;</strong><strong>​</strong></p>
                    <hr style="border-color: #000" />
                    <p>Если у вас возникли сложности в работе с сайтом, изложите суть вопроса в письме и направьте его по адресу: info@bramasys.com.ua.&nbsp;</p>
                    </div>
                    <p>&nbsp;</p>',
            ),
        );

        foreach($mails as $mail)
        {
            $demoUser = new MailTemplate();
            $demoUser->attributes = $mail;
            $demoUser->save();
        }
    }

    public function down()
    {
        $this->dropTable('mail_template');
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}