<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property integer $parent
 * @property integer $tag
 * @property integer $order_number
 * @property boolean $in_url
 * @property string $image_url
 * @property string $product_title
 * @property string $product_description
 * @property string $product_keywords
 * @property string $H1
 * 
 * The followings are the available model relations:
 * @property Category $parent_category
 * @property Category[] $categories
 * @property CategoryTag $related_tag
 * @property CategorySpec[] $categorySpecs
 * @property Complect[] $complects
 * @property Products[] $products
 * @property StaticPage[] $staticPages
 */
class Category extends ActiveRecord
{
	private $systemCategories = array(
		'company',
		'partners',
		'news',
		'faq',
		'terms',
		'support',
		'instruction_and_software',
		'publication',
		'videonablyudenie',
		'ohrannaya-signalizaciya',
		'domofony',
		'sistemy-kontrolya-dostupa',
		'sputnikovoe-televidenie',
		'dopolnitelnye-materialy',
        'stati-publikatcii-obzory',
        'minioplata',
        'minidostavka',
        'minivosvrat',
        'minigarantia',
        'minimontazh',
        
	);
	
	private $origAttrs = array();
	
	protected function afterFind()
	{
		$this->origAttrs['name'] = $this->name;
		$this->origAttrs['tag'] = $this->tag;
		$this->origAttrs['parent'] = $this->parent;
		$this->origAttrs['in_url'] = $this->in_url;
		
		if($this->image_url == null)
			$this->image_url = '/images/layout/noimg.png';
		
		return parent::afterFind();
	}

	public function isSystemCategory()
	{
		if(isset($this->origAttrs['name']))
		{
			if(in_array($this->origAttrs['name'], $this->systemCategories))
			{
				return true;
			}
		}
		return false;
	}
	
	public function getOrig($attribute)
	{
		return isset($this->origAttrs[$attribute]) ? $this->origAttrs[$attribute] : null;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, title', 'required'),
            array('name, title, product_title, product_description, product_keywords, H1, image_url', 'filter', 'filter'=>'trim'),
			array('name, tag, parent, in_url', 'validateIsSystem'),
			array('name', 'unique'),
			array('order_number', 'numerical', 'integerOnly'=>true),
			array('in_url', 'boolean'),
			array('tag, parent', 'default', 'value' => NULL),
			array('tag, parent', 'safe'),
			array('tag, parent', 'validateEmpty'),
            array('name, title, image_url, product_title, product_description, product_keywords, H1', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, title, parent, order_number, tag', 'safe', 'on'=>'search'),
		);
	}
	
	public function validateIsSystem($attribute)
	{
		if($this->isSystemCategory())
		{
			if($this->getAttribute($attribute) != $this->getOrig($attribute))
				$this->addError($attribute, 'В этой категории поле '.$this->getAttributeLabel($attribute).' не редактируемое');
		}
	}
    public function validateEmpty($attribute)
	{
        
		if($this->tag == null && $this->parent == null)
				$this->addError($attribute, 'Тег и родительская категория не должны быть пустыми одновременно');
		
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent_category' => array(self::BELONGS_TO, 'Category', 'parent'),
			'related_tag' => array(self::BELONGS_TO, 'CategoryTag', 'tag'),
			'categories' => array(self::HAS_MANY, 'Category', 'parent'),
			'categorySpecs' => array(self::HAS_MANY, 'CategorySpec', 'category_id'),
			'complects' => array(self::HAS_MANY, 'Complect', 'category_id'),
			'products' => array(self::HAS_MANY, 'Products', 'category'),
			'staticPages' => array(self::HAS_MANY, 'StaticPage', 'category_id'),
		);
	}
    
    public function getReadableName()
    {
       $depth = $this->countDepth();
       $newName = str_repeat(' - ', $depth).$this->title;
       return $newName;
    }
    
    public function countDepth()
    {
        $depth=0;
        $categoy = $this;
        while (!empty($categoy->parent_category))
        {
            $depth++;
            $categoy = $categoy->parent_category;
        }
        return $depth;
    }

	
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Машинное имя',
			'title' => 'Наименование',
			'parent' => 'Радительская категория',
			'order_number' => 'Порядковый номер',
			'in_url' => 'Отображать в ссылках',
			'tag' => 'Тег',
			'image_url' => 'Изображение категории',
			'product_title' => 'Title продуктов',
			'product_description' => 'Description продуктов',
			'product_keywords' => 'Keywords продуктов',
			'H1' => 'H1 продуктов',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
        $criteria->compare('title',$this->title,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('order_number',$this->order_number);
		$criteria->compare('tag',$this->tag);
		$criteria->compare('in_url',$this->in_url);
		$criteria->compare('image_url',$this->image_url,true);
		$criteria->compare('product_title',$this->product_title,true);
		$criteria->compare('product_description',$this->product_description,true);
		$criteria->compare('product_keywords',$this->product_keywords,true);
		$criteria->compare('H1',$this->H1,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getListData($first = false)
	{		
		$data = CHtml::listData(self::model()->findAll(array('index' => 'id')), 'id', 'title');		
		return $first ? array('' => '') + $data : $data;		
	}	
	
	protected function beforeValidate()
	{
		$r = parent::beforeValidate();
		
		if($this->tag && $this->parent)					
			$this->addError(isset($_POST['name']) ? $_POST['name'] : '', 'You can not set both tag and parent values');										
		
		return $r;
	}
        
    public static function listDataTree($categories)
    {
        
    foreach($categories as $value)
                    $tree_categories[$value->parent ? $value->parent : 0][$value->id] = $value;	
            $tree=array();
            Category::buildlistDataTree($tree,$tree_categories);
            return $tree;
    }
    
    public static function buildlistDataTree(&$tree,&$categories, $parent_id = 0,$depth=0)
	{
 		if(is_array($categories) && isset($categories[$parent_id]))
		{
			foreach($categories[$parent_id] as $category)
			{
					$tree[$category->id] = str_repeat(' - &nbsp;&nbsp;', $depth).$category->title;
                    $depth++;
                    Category::buildlistDataTree($tree,$categories, $category->id,$depth);
                    $depth--;        
			}    
		}    
	}
}