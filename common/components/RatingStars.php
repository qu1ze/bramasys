<?php
class RatingStars extends CWidget
{
    public $starSize = 20;
    public $rating;

    public function run()
    {
        $this->render("rating_stars");
    }
}
