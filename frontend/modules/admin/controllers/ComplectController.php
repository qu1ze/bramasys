<?php

class ComplectController extends Controller
{
	private $complectProductArray = array();
	public $parentId = null;
	public function getCategories()
	{
        Yii::app()->getComponent('categoryController');
		$criteria = new CDbCriteria();
		$criteria->addSearchCondition('name', '%_complects', false);
		$criteria->addCondition('`in_url` != 0');
		$categories = array();
		foreach (Category::model()->findAll($criteria) as $category)
		{
			if(app()->categoryController->isCatalog($category->id) && $category->parent !== null)
			{
				$children = app()->categoryController->getChilds($category->id);
				if(empty($children))
				{
					$categories[$category->id] = $category->title;
				}
			}
		}
		return $categories;
	}
	
	public function getComplectProducts($id)
	{
		if(!isset($this->complectProductArray[$id]))
			$this->complectProductArray[$id] = Complect::model()->findByPk($id)->getComplectProductList();
		return $this->complectProductArray[$id];
	}
	
	public function actionIndex()
    {
		$complect = new Complect('search');
		
		if (isset($_GET['Complect']))
		{
            $complect->attributes = $_GET['Complect'];
            $complect->complectProductsField = $_GET['Complect']['complectProductsField'];
		}
		
        $this->render('index', array(
			'complect' => $complect,
			)
        );
    }
	
	public function actionCreate()
	{		
		$model = new Complect; 
				
		if (isset($_POST['Complect']))
		{						
			$model->attributes = $_POST['Complect'];
			if ($model->save())
				$this->redirect(array('/admin/complect'));
		}
		
		$this->render('create', array(
			'model' => $model,		
		));
	}
	
	public function actionSetItems($id)
	{
		if(($model = ComplectProduct::model()->findAllByAttributes(array('complect_id' => $id), array('order' => '`order` ASC'))) === null)
			$this->redirect(array('/admin/complect'));
		$this->parentId = Complect::model()->findByPk($id)->category->parent;
		echo $this->render('set_items', array(
			'model' => $model,
			'currentComplect' => $id,
		));	
	}
	
	public function actionAjaxCreate()
	{	
		$model = new Complect();	

		if (isset($_POST['Complect']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['Complect'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Комплект успешно создан'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать комплект'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}
	     
	public function actionAjaxChangeCount()
	{
		if (isset($_POST['Complect']) && Yii::app()->request->isAjaxRequest)
		{
			$complectId = $_POST['Complect']['complectId'];
			$productId = $_POST['Complect']['productId'];
			$complectProduct = ComplectProduct::model()->findByAttributes(array('complect_id' => $complectId, 'product_id' => $productId));
			if($complectProduct !== null && $_POST['Complect']['count'] > 0)
			{
				$complectProduct->count = $_POST['Complect']['count'];
				if ($complectProduct->save())
				{
					echo json_encode(array(
						'title'=>'Success',
						'msg'=>'Количество измененно',
						));
					app()->end();
				}
			}
		}
		echo json_encode(array(
			'title'=>'Error',
			'msg'=>'Не удалось изменить количество',
			));
	}
        
	public function actionAjaxAddProduct()
	{	
            $complectProduct = new ComplectProduct();
			if (isset($_POST['Product']['brand']) && isset($_POST['Product']['model']) && Yii::app()->request->isAjaxRequest)
			{
				$criteria = new CDbCriteria();
				$criteria->addSearchCondition('brand.name', $_POST['Product']['brand']);
				$criteria->with = array('brand0' => array('alias' => 'brand', 'together' => true,));
				$criteria->addSearchCondition('model', $_POST['Product']['model']);
				$product = Products::model()->find($criteria);
				if($product !== null)
					$_POST['Product']['id'] = $product->id;
			}
            if (isset($_POST['Product']['id']) && Yii::app()->request->isAjaxRequest)
            {
				$complectProduct->complect_id = $_POST['Product']['complectId'];
                $complectProduct->product_id = $_POST['Product']['id'];
				$complectProduct->count = $_POST['Product']['count'];
                if ($complectProduct->save())
                {
                    echo json_encode(array(
                        'title'=>'Success!',
                        'msg'=>'Продукт успешно добавлен',
						'html'=>$this->renderPartial('_product', array(
                            'model' => $product,
							'count' => $_POST['Product']['count'],
                        ), true),
						'id'=>$_POST['Product']['id'],
                        ));
                    app()->end();
                }
                else
                {
                    echo json_encode(array('title'=>'Oops!', 'msg'=>'Продукт уже был добавлен'));
                    app()->end();
                }
            }
            echo json_encode(array('title'=>'Oops!', 'msg'=>'Продукт не найден'));
            app()->end();
	}
	
	public function actionUpdate($id)
	{		
		if (($model = Complect::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/complect'));		
		
		if (isset($_POST['Complect']))
		{
			$model->attributes = $_POST['Complect'];
			if ($model->save())
				$this->redirect(array('/admin/complect'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Complect');
		$es->update();
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Комплект успешно отредактирован')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
			if (($complectProduct = ComplectProduct::model()->findAllByAttributes(array('complect_id' => $id))) !== null)
			{
				$complectProduct->deleteAll();				
				
			}
			Complect::model()->deleteByPk( $id );
			Yii::app()->user->setFlash('success', "Комплект успешно удален");
		}
		$this->redirect(array('/admin/complect'));
	}
	
	public function actionAjaxDelete($id)
	{
		if (($complect = Complect::model()->findByPk($id)) !== null)
		{
			if (($complectProduct = ComplectProduct::model()->findAllByAttributes(array('complect_id' => $id))) !== null)
			{
				foreach ($complectProduct as $complectProductModel)
				{
					$complectProductModel->delete();
				}
			}
			if ($complect->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Комплект успешно удален'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить комплект'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Комплект не найден'));
	}
        
    public function actionAjaxRemoveProduct($complectId, $productId)
	{		
		if (($model = ComplectProduct::model()->findByAttributes(array('complect_id' => $complectId, 'product_id' => $productId))) !== null)
		{
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Продукт успешно изъят из комплекта'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно изъять продукт'));
		} 
		else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно изъять, продукт не найден'));
	}

    public function actionChangeProdOrder(array $list)
    {
        foreach($list as $value)
        {
            if(!is_array($value))
            {
                echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно изменить порядок'));
                app()->end();
            }
            $model = ComplectProduct::model()->findByPk($value[0]);
            if($model === null)
            {
                echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно изменить порядок'));
                app()->end();
            }
            $model->order = $value[1];
            $model->save();
        }
        echo json_encode(array('title'=>'Success!', 'msg'=>'Порядок изменен'));
    }
}