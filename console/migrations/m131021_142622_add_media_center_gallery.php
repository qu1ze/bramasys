<?php

class m131021_142622_add_media_center_gallery extends CDbMigration
{
    public function up()
    {
        $gallery = new Gallery();
        $gallery->id = 1;
        $gallery->name = true;
        $gallery->description = true;
        $gallery->versions = array(
            'small' => array(
                'centeredpreview' => array(98, 98),
            ),
            'medium' => array(
                'resize' => array(800, null),
            )
        );
        $gallery->save();
    }

    public function down()
    {
        Gallery::model()->deleteByPk(1);
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}