<?php

class CurrencyController extends Controller
{
	
	public function actionIndex()
	{
		$model = new Currency('search');
		
		if (isset($_GET['Currency']))
			$model->attributes = $_GET['Currency'];
		
		$this->render('index', array(
			'model' => $model,
			)
		);
	}
	
	public function actionCreate()
	{
		$model = new Currency; 
				
		if (isset($_POST['Currency']))
		{			
			$model->attributes = $_POST['Currency'];
			if ($model->save())
				$this->redirect(array('/admin/currency'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new Currency();	

		if (isset($_POST['Currency']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['Currency'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Валюта успешно создана'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать валюту'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = Currency::model()->findByPk($id)) !== null)
		{
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Валюта успешно удалена'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить валюту'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: валюта не найдена'));
	}
	
	public function actionUpdate($id)
	{
		if (($model = Currency::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/currency'));				
				
		if (isset($_POST['Currency']))
		{			
			$model->attributes = $_POST['Currency'];
			if ($model->save())
				$this->redirect(array('/admin/currency'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Currency');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Валюта успешно отредактирована')));			
	}
	
//	public function actionTest()
//	{
//		throw new CHttpException(404, 'Message');
//	}
}