<?php
?>
<a href="/sitemap.xml">Скачать индекс</a>
<br><br>
<div id="fileDesc">
	Последние обновление sitemap было: <?php echo $lastMod; ?> <a href="/admin/sitemap/update">Обновить дату</a>
</div>
<br>
<?php

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'horizontal',
	'action'=>'/admin/sitemap/generate',
	'method'=>'get',
));?>
	<h5>Приоритет</h5>
	<div class="control-group ">
		<label class="control-label required" for="">Главная</label>
		<div class="controls">
			<?php echo CHtml::textField('homePagePriority', SitemapController::PRIORITY_HOME_PAGE);?>
		</div>
	</div>
	<div class="control-group ">
		<label class="control-label required" for="">Главные категории каталога</label>
		<div class="controls">
			<?php echo CHtml::textField('mainCategoriesPriority', SitemapController::PRIORITY_MAIN_CATEGORY);?>
		</div>
	</div>
	<div class="control-group ">
		<label class="control-label required" for="">Категории каталога</label>
		<div class="controls">
			<?php echo CHtml::textField('categoriesPriority', SitemapController::PRIORITY_CATEGORY);?>
		</div>
	</div>
	<div class="control-group ">
		<label class="control-label required" for="">Остальные категории</label>
		<div class="controls">
			<?php echo CHtml::textField('staticPriority', SitemapController::PRIORITY_STATIC);?>
		</div>
	</div>
	<div class="control-group ">
		<label class="control-label required" for="">Продукты</label>
		<div class="controls">
			<?php echo CHtml::textField('productsPriority', SitemapController::PRIORITY_PRODUCT);?>
		</div>
	</div>
	<div class="control-group ">
		<label class="control-label required" for="">Комплекты</label>
		<div class="controls">
			<?php echo CHtml::textField('complectsPriority', SitemapController::PRIORITY_COMPLECT);?>
		</div>
	</div>
        <div class="control-group ">
		<label class="control-label required" for="">Статьи</label>
		<div class="controls">
			<?php echo CHtml::textField('articlePriority', SitemapController::PRIORITY_ARTICLE);?>
		</div>
	</div>
        <div class="control-group ">
		<label class="control-label required" for="">Новасти</label>
		<div class="controls">
			<?php echo CHtml::textField('newsPriority', SitemapController::PRIORITY_NEWS);?>
		</div>
	</div>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Генерировать новый')); ?>
<?php $this->endWidget();?>