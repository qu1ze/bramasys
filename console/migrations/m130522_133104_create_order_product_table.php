<?php

class m130522_133104_create_order_product_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `order_product` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`product_id` int(11) NOT NULL,
				`count` int(11) NOT NULL,
				`price` float NOT NULL,
				`order_id` int(11) NOT NULL,
				PRIMARY KEY (`id`),
				KEY `product_id` (`product_id`),
				CONSTRAINT `FK_order` FOREIGN KEY (`order_id`)
						  REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			");
	}

	public function down()
	{
		$this->dropTable("order_product");
	}
	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}