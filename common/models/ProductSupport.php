<?php

/**
 * This is the model class for table "product_support".
 *
 * The followings are the available columns in table 'product_support':
 * @property integer $id
 * @property integer $product_id
 * @property integer $support_file_id
 *
 * The followings are the available model relations:
 * @property SupportFile $supportFile
 * @property Products $product
 */
class ProductSupport extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProductSupport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_support';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, support_file_id', 'required'),
			array('product_id, support_file_id', 'numerical', 'integerOnly'=>true),
            array('product_id', 'uniqueWithSupportFile'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, product_id, support_file_id', 'safe', 'on'=>'search'),
		);
	}

    public function uniqueWithSupportFile()
    {
        if(($model = ProductSupport::model()->findByAttributes(array('product_id' => $this->product_id, 'support_file_id' => $this->support_file_id))) !== null)
        {
            if($model->id != $this->id)
                $this->addError('support_file_id', 'Файл уже добавлен');
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'supportFile' => array(self::BELONGS_TO, 'SupportFile', 'support_file_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'support_file_id' => 'Support File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('support_file_id',$this->support_file_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}