<?php
	Yii::app()->clientScript->registerScript("sendCallbackForm","
		$('#ajax-callback-form').on('click', '.callback-btn', function () {
			var form = $('#ajax-callback-form');
			$(form).children('.alert').fadeOut();
			var errorList = $(form).children('.alert').children('.error-list');
			var errorLine = $($(errorList).children()[0]).clone();
			$(errorList).children().remove()
			$(errorList).append(errorLine);
			var CallbackForm = $(form).serializeArray();
			$.ajax({
				url: '/site/ajaxCallback',
				data: CallbackForm,
				dataType: 'json',
				type: 'post',
				success: function(data){
					if(typeof(data) == 'object' && data.title == 'Success')
					{
						$('#callback-form-wrapper').click();
						$('#callback-response-wrapper').slideDown(600);
						$('#callback-response').fadeIn(800);
					}
					else if (typeof(data.message) == 'object')
					{
						for(var field in data.message)
						{
							var newLine = $(errorLine).clone();
							$(errorList).append($(newLine).text(data.message[field][0]));
							$(newLine).fadeIn();
						}
						$(form).children('.alert').fadeIn();
					}
				}
			});
		});
   ", CClientScript::POS_READY);

	Yii::app()->clientScript->registerScript("callback-show-hide","
		$('body').on('click', '.callback-bnt', function () {
			$('#callback-form-wrapper').fadeIn(600);
			$('#callback-form-container').slideDown(600).fadeIn(800);
            $('#CallbackForm_phone').focus();
			return false;
		});
		$('#callback-response').on('click', '#close3', function () {
			$('#callback-response-wrapper').click();
			return false;
		});
		$('body').on('click', '#callback-response-wrapper:not(#callback-form-container)', function (e) {
			if(e.target.id !== '#callback-response' && !$(e.target).parents().is('#callback-response'))
			{
				$('#callback-response-wrapper').fadeOut(600);
				$('#callback-response').slideUp(600).fadeOut(300);
				return false;
			}
		});
		$('#callback-form-container').on('click', '#close3', function () {
			$('#callback-form-wrapper').click();
			return false;
		});
		$('body').on('click', '#callback-form-wrapper:not(#callback-form-container)', function (e) {
			if(e.target.id !== '#callback-form-container' && !$(e.target).parents().is('#callback-form-container'))
			{
				$('#callback-form-wrapper').fadeOut(600);
				$('#callback-form-container').slideUp(600).fadeOut(300);
				return false;
			}
		});
   ", CClientScript::POS_READY);
		
	$categories = app()->categoryController->getChilds(ProductsController::getTag());
?>
<table id="callback-form-wrapper" style="display:none;"><tr><td>
	<div id="callback-form-container" style="display:none;">
		<div class="callback-form-header">
			<h3>ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</h3>
			<a class="close hover" id="close3" href="#" style="top: -5px;">
			</a>
		</div>
		<div class="callback-form-content">
			<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'id' => 'ajax-callback-form',
				'enableAjaxValidation' => false,
				'enableClientValidation' => true,
				'clientOptions' => array(
				'validateOnSubmit' => true,
				),
			)); ?>

				<div class="alert alert-block alert-error" style="display:none;"><p>Необходимо исправить следующие ошибки:</p>
					<ul class="error-list">
						<li style="display:none;"></li>
					</ul>
				</div>

				<div class="fieldWithSubLabel">
					<?php echo $form->textFieldRow($callbackForm, 'phone'); ?>
					<label class="subLabel">В формате (093) 777-77-77</label>
				</div>
				<?php echo $form->textFieldRow($callbackForm, 'name'); ?>
				<div class="fieldWithSubLabel">
					<?php echo $form->textFieldRow($callbackForm, 'subject'); ?>
					<label class="subLabel">Например, выбор домофона</label>
				</div>
				<div class="right-block">
					<div class="actions">
						<?php echo CHtml::button('Заказать звонок', array('class' => 'callback-btn')); ?>
					</div>
				</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</td></tr></table>

<table id="callback-response-wrapper" style="display:none;"><tr><td>
	<div id="callback-response" style="display:none;">
		<a class="close hover" id="close" href="#" style="top: 5px;"></a>
		<p>Спасибо! Мы перезвоним Вам в течение одного часа, при условии,<br>
		что звонок был заказан согласно нашего графика роботы:</p>
		<div>
			Пн.-Пт. 8:00-21:00<br>
			Суббота 9:00-20:00<br>
			Воскресенье 10:00-19:00
		</div>
		<hr>
		Возможно ответ на итересующий Вас вопрос,<br>
		вы найдете в разделе <ul style="display: inline-block;"><li><a href="<?php echo app()->getBaseUrl(true); ?>/support/faq">«Вопросы и ответы»</a></li></ul>
		<hr>
		Ознакомьтесь с услугами, которые оказывает наша компания:
		<ul>
			<?php foreach ($categories as $category): ?>
			<li><a href="<?php echo app()->categoryController->getCategoryAddress($category['data']['name']);?>"><?php echo $category['data']['title'];?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
</td></tr></table>