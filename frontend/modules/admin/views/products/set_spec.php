<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
    <?php foreach ($model as $productSpec):?>
    <div class="control-group">
        <?php echo CHtml::hiddenField("ProductSpecValue[$productSpec->id][product_id]", $productSpec->product_id); ?>
        <?php echo CHtml::hiddenField("ProductSpecValue[$productSpec->id][spec_id]", $productSpec->spec_id); ?>
        <label class="control-label"><?php echo $productSpec->spec->name; ?></label>
        <div class="controls">
        <?php echo CHtml::textField("ProductSpecValue[$productSpec->id][value]", $productSpec->value); ?>
        </div>
    </div>
    <?php endforeach; ?>

    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	
	
<?php $this->endWidget(); ?>