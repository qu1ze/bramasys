<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property integer $payment_type_id
 * @property integer $delivery_type_id
 * @property string $comment
 * @property integer $create_time
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Cart[] $carts
 * @property OrderProduct[] $orderProducts
 * @property DeliveryType $deliveryType
 * @property PaymentType $paymentType
 */
class Order extends ActiveRecord
{
    
    	
    public $paystatvar = array(
                "notpay"=>'Оплата еще не выполнена',
                "success"=>"Покупка совершена <a href='https://liqpay.com/'>проверьте личный кабинет</a>",
                "failure"=>"Покупка отклонена",
                "wait_secure"=>"Платеж находится на проверке", );
    
	const FREEDELIVERY=2500;
	
	const ORDER_STATUS_PENDING = 1;
	const ORDER_STATUS_APPROVED = 2;
	const ORDER_STATUS_REJECTED = 3;
	
	private $orderStatuses = array(
		self::ORDER_STATUS_PENDING => 'Ожидает обработки',
		self::ORDER_STATUS_APPROVED => 'Подтвержден',
		self::ORDER_STATUS_REJECTED => 'Откланен',
	);
	
	
	public function getFreedelivery()
	{
		return self::FREEDELIVERY;
	}
	
	
	public $afterDate = null;
	public $beforeDate = null;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('payment_type_id, delivery_type_id, delivery_address, create_time', 'required'),
            array('comment, delivery_address', 'filter', 'filter'=>'trim'),
			array('payment_type_id, delivery_type_id, create_time, status', 'numerical', 'integerOnly'=>true),
			array('status', 'in', 'range'=>  array_keys($this->orderStatuses)),
			array('comment, delivery_address', 'length', 'max'=>255),
			array('company_name', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, payment_type_id, delivery_type_id, comment, create_time, status, afterDate, beforeDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'carts' => array(self::HAS_MANY, 'Cart', 'order_id'),
			'deliveryType' => array(self::BELONGS_TO, 'DeliveryType', 'delivery_type_id'),
			'paymentType' => array(self::BELONGS_TO, 'PaymentType', 'payment_type_id'),
			'orderProducts' => array(self::HAS_MANY, 'OrderProduct', 'order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'payment_type_id' => 'Способ оплаты',
			'delivery_type_id' => 'Способ доставки',
			'delivery_address' => 'Адресс доставки',
			'comment' => 'Комментарий',
			'create_time' => 'Дата заказа',
			'status' => 'Статус',
			'company_name' => 'Плательщик',
		);
	}
	public function getSumPrice()
	{
		$client = Cart::model()->findByAttributes(array('order_id' => $this->id))->client;
		
		$orderProducts = OrderProduct::model()->findAllByAttributes(array('order_id' => $this->id));
		
		$priceSum = 0;
	foreach ($orderProducts as $key => $prod)
	{
		if($prod->is_complect)
		{
			$complect = Complect::model()->findByPk($prod->product_id);
			$complectProducts = ComplectProduct::model()->findAllByAttributes(array('complect_id' => $complect->id));
			foreach ($complectProducts as $complectProd)
			{
				$product = Products::model()->findByPk($complectProd->product_id);
				$complectProdArray = array(
					'product_id' => $complectProd->product_id,
					'price' => round(100 * $product->price / 100 * (100 - $complect->discount)) / 100,
					'count' => $complectProd->count * $prod->count,
					'mounting' => $prod->mounting,
					'is_complect' => 0,
				);
				$amount = $complectProdArray['price'] * $complectProdArray['count'];
				$priceSum += $amount;
				$this->renderPartial('_product', array(
							'product' => $product,
							'amount' => $amount,
							'prod' => $complectProdArray,
							'inComplect' => $complect->name,
						));
			}
			unset($orderProducts[$key]);
		}
	}
	foreach ($orderProducts as $prod)
	{
		$product = Products::model()->findByPk($prod['product_id']);
		$amount = $prod['price'] * $prod['count'];
		$priceSum += $amount;
		
	}
		return $priceSum;
	}
	
	
	public function getProdPriceNewOrder()
	{
		$sum = 0;
		$count = 0;
		$cart = Yii::app()->session['cart'];
		if (empty($cart)) return 0;  
		
		$sum = 0;
		foreach ($cart as $id => $product)
		{
			if($id === 'complect')
		    {
				foreach ($product as $id => $complectProd)
		        {
					$sum += $complectProd['count'] * $complectProd['price'];
		            $count++;// += $complectProd['count'];
		        }
		    }
			else
		    {
				$sum += $product['count'] * $product['price'];
		        $count++;// += $product['count'];
		    }
		}
		return $sum;
	}



	public function getTotalpriceNewOrder($delivery_id, $payment_id)
	{
		$delivery = 0;
		if($delivery_id>0)
		{
			$delivery = DeliveryType::model()->findByPk($delivery_id)->description;
			$delivery = CurrencySys::exchange($delivery,-1,1);
		}
		
		$commissia = 0;
		if($payment_id>0)
		{
			$commissia = PaymentType::model()->findByPk($payment_id)->commission;
		}
		
		$freedelivery = CurrencySys::exchange(self::FREEDELIVERY,-1,1);
		$price = CurrencySys::exchange($this->getProdPriceNewOrder());
		if (($price>$freedelivery) && ($delivery_id == DeliverySystemAddress::model()->NOVAYA_POCHTA_ID))
			{
				$delivery = 0;
			} 
			
		$commissia = (($price+$delivery)*($commissia/100));
		if($payment_id == 3){ 
               $commissia=0;
           }
        if($payment_id == 4){ 
               $commissia=$commissia+CurrencySys::exchange(17,-1,1);
           }
			
		return round(100*($price+$delivery))/100+round(100*$commissia)/100;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$this->afterDate = !empty($this->afterDate) ? $this->dateToTime($this->afterDate) : null;
		$this->beforeDate = !empty($this->beforeDate) ? $this->dateToTime($this->beforeDate) : null;

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->addBetweenCondition('create_time', !empty($this->afterDate) ? $this->afterDate : 0, !empty($this->beforeDate) ? $this->beforeDate : time());
		if(!empty($this->status))
			$criteria->compare('status',$this->status);
		$criteria->compare('payment_type_id',$this->payment_type_id);
		$criteria->compare('delivery_type_id',$this->delivery_type_id);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getOrderStatuses($status = null)
	{
		return is_null($status) ? $this->orderStatuses : $this->orderStatuses[$status];
	}
	
	private function timeToDate($time = null)
	{
        if(empty($time))
			$this->create_time = date('d/m/Y', $this->create_time);
		else
			return date('d/m/Y', $time);
	}
	
	public function dateToTime($date = null)
	{
		if(empty($date))
		{
			if(!is_numeric($this->create_time) && !empty($this->create_time))
			{
				$timeArray = explode("/", $this->create_time);
				$this->create_time = sizeof($timeArray) > 2 ? mktime(0, 0, 0, $timeArray[1], $timeArray[0], $timeArray[2]) : 0;
			}
		}
		else
		{
			$timeArray = explode("/", $date);
			return sizeof($timeArray) > 2 ? mktime(0, 0, 0, $timeArray[0], $timeArray[1], $timeArray[2]) : 0;
		}
	}

	protected function afterConstruct()
	{
		parent::afterConstruct();
		$this->status = null;
	}

    protected function afterFind()
    {
        $this->timeToDate();
        return parent::afterFind();
    }
	
	protected function beforeValidate()
	{
		if($this->status == null)
			$this->status = 1;
		$this->dateToTime();
		return parent::beforeValidate();
    }

    protected function afterSave()
    {
        parent::afterSave();
        $this->timeToDate();
    }
}