<?php

class m130830_151206_add_description_to_specification extends CDbMigration
{
	public function up()
	{
		$this->execute("
                ALTER TABLE `specification`
					ADD `description` TEXT DEFAULT NULL;
				");
	}

	public function down()
	{
		$this->execute("
				ALTER TABLE `specification`
					DROP `description`;
                ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}