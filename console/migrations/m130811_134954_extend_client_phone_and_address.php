<?php

class m130811_134954_extend_client_phone_and_address extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `phone` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`type` tinyint(4) NOT NULL,
				`value` varchar(14) NOT NULL,
				`client_id` int(11) NOT NULL,
				PRIMARY KEY (`id`),
				CONSTRAINT `FK_phone_client` FOREIGN KEY (`client_id`)
						  REFERENCES `client` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			  
			CREATE TABLE IF NOT EXISTS `delivery_address` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`town` varchar(100) NOT NULL,
				`street` varchar(100) NOT NULL,
				`house` varchar(10) NOT NULL,
				`flat` varchar(10) NOT NULL,
				`additional_info` varchar(100) DEFAULT NULL,
				`client_id` int(11) NOT NULL,
				PRIMARY KEY (`id`),
				CONSTRAINT `FK_delivery_address_client` FOREIGN KEY (`client_id`)
						  REFERENCES `client` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
		
		$this->execute("
			ALTER TABLE `client`
				DROP `phone`,
				DROP `delivery_address`;
				");
	}

	public function down()
	{
		$this->dropTable('phone');
		$this->dropTable('delivery_address');
		$this->execute("
			ALTER TABLE `client`
				ADD `phone` varchar(12) NOT NULL,
				ADD `delivery_address` varchar(255) NOT NULL;
				");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}