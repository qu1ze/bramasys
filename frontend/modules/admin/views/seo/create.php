<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'url');?>
	<?php echo $form->textFieldRow($model, 'title');?>
	<?php echo $form->textFieldRow($model, 'description');?>
	<?php echo $form->textFieldRow($model, 'keywords');?>
	<?php echo $form->textFieldRow($model, 'H1');?>
	<?php echo $form->ckEditorRow($model, 'seo', array('class'=>'span4', 'rows'=>5)); ?>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Создать')); ?>
	
<?php $this->endWidget(); ?>