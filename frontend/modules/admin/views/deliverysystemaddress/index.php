<?php
// cs()->registerCoreScript('yiiactiveform');

$grid_id = $model->getGridId();
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $grid_id,
	'type' => 'bordered striped',
	'dataProvider' => $model->search(),
	'filter' => $model,	
	'template' => "{items}\n{pager}",
	'columns' => array(
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'town',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/deliverysystemaddress/ajaxUpdate'),
				 'success' => 'js: function(data){					
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),	
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'office',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/deliverysystemaddress/ajaxUpdate'),
				 'success' => 'js: function(data){					
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),	
        array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'address',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/deliverysystemaddress/ajaxUpdate'),
				 'success' => 'js: function(data){					
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
        array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'delivery_type_id',
			'editable' => array(
				'type' => 'select', 
				'source' => array('') + CHtml::listData(DeliveryType::model()->findAll(), 'id', 'title'),
				'url' => app()->createUrl('/admin/deliverysystemaddress/ajaxUpdate'),
				 'success' => 'js: function(data){					
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),		
        
		
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('nowrap' => 'nowrap'),			
			'template' => '{delete} ',	
			'deleteButtonUrl'=>'Yii::app()->controller->createUrl("ajaxDelete", array("id" => $data->primaryKey))',
			// 'header' => EHtml::addGridButton($this->createAbsoluteUrl('/admin/articles/ajaxCreate'), 'Добавить', $height=350, 'icon-question-sign'),
			'deleteConfirmation' => 'Вы уверенны что хотите удалить?',
			'afterDelete' => 'function(link,success,data){
				if(success){data = $.parseJSON(data);brama.ui.notify(data.title, data.msg, false);}
				else{brama.ui.notify("Oops!", data.responseText )};
			}',
		)
	)
));

