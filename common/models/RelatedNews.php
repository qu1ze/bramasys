<?php

/**
 * This is the model class for table "related_news".
 *
 * The followings are the available columns in table 'related_news':
 * @property integer $id
 * @property integer $news_id
 * @property integer $related_news_id
 */
class RelatedNews extends ActiveRecord
{
    public $related_news_id_s = array(); 
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RelatedNews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'related_news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('news_id, related_news_id', 'required'),
			array('news_id, related_news_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, news_id, related_news_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'relatedNews' => array(self::BELONGS_TO, 'News', 'related_news_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'news_id' => 'News',
			'related_news_id' => 'Related News',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('news_id',$this->news_id);
		$criteria->compare('related_news_id',$this->related_news_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
     public function saveRrelateds($related_news, $news_id, $update=false)
    {
            $error=false;            
            $transaction = Yii::app()->db->beginTransaction(); // Transaction begin
            if($update) RelatedNews::model()->deleteAllByAttributes(array('news_id' => $news_id));
            try{
                foreach ($related_news as $value)
                {
                    $model = new RelatedNews();
                    $model->news_id = $news_id;
                    $model->related_news_id = $value;
                    if (0==RelatedNews::model()->countByAttributes(array('news_id'=>$news_id, 'related_news_id' => $value))){
                        $error=  !$model->save() | $error;  
                    }
                }
                if (!$error){
                    $transaction->commit();
                    return true;
                }
            }
            catch (Exception $e){
                            $transaction->rollBack();
            }
            return false;
    }
}