<?php

class m131207_190616_terms_page extends CDbMigration
{
	/*public function up()
	{
         $category = new Category();
        $category->name = 'agreement';
        $category->title = 'agreement';
        $category->tag = CategoryTag::model()->findByAttributes(array('name'=>'hidden'))->id;
        if ($category->save()) {
            $page = new StaticPage();
            $page->category_id = $category->id;
            $page->content = '<h2><span style="color: rgb(0, 0, 0); font-size: 14px; line-height: 19px;">Пользовательское соглашение</span></h2><p>Соглашение пользователя можно изменить в статических страницах &quot;agreement&quot;</p>';
            if ($page->save()) return false;            
        }
        else return false;
	}

	public function down()
	{
		echo "m131207_190616_terms_page does not support migration down.\n";
		return false;
	}*/

	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        
         $category = new Category();
        $category->name = 'agreement';
        $category->title = 'agreement';
        $category->parent = Category::model()->findByAttributes(array('name'=>'hidden_container'))->id;
        if ($category->save()) {
            $page = new StaticPage();
            $page->category_id = $category->id;
            $page->content = '<h2><span style="color: rgb(0, 0, 0); font-size: 14px; line-height: 19px;">Пользовательское соглашение</span></h2><p>Соглашение пользователя можно изменить в статических страницах &quot;agreement&quot;</p>';
            if (!$page->save()) {
                echo 'faaa!2--'.$page->content;
                return false;            
                
            }
        }
        else {
            echo 'faaa!';
            return false;
            
        }
        
     return true;
     }
	

	public function safeDown()
	{
       $category = Category::model()->findByAttributes(array('name' => 'agreement'));
       if (!$category->staticPages[0]->delete()) {
                           return false;
                }
       if(!$category->delete()) return false;
	
     return true;
     }
	
}