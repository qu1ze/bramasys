<?php
/* @var $this TermController */
/* @var $data Term */
?>
<div class="content_term" itemscope itemtype="http://webmaster.yandex.ru/vocabularies/term-def.xml">
	<span itemprop="term"><a href="<?php echo $data->url; ?>"><?php echo $data->title; ?></a></span>
	<div itemprop="definition"><?php echo $data->definition; ?></div>
	<?php if(!empty($data->author)): ?><span itemprop="author"><?php echo $data->author; ?></span><?php endif; ?>
    <?php if(!empty($data->source)): ?><span itemprop="source"><?php echo $data->source; ?></span><?php endif; ?>
    <?php if(!empty($data->source_date)): ?><span itemprop="source-date"><?php echo $data->source_date; ?></span><?php endif; ?>
</div>