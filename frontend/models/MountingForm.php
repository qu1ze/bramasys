<?php
/**
 * CallbackForm.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 8:37 PM
 */

class MountingForm extends CFormModel
{
	const MOUNTING_MAIL = 'sales@bramasys.com.ua';
	
	public $phone = null;
	public $name = null;
	public $address = null;
	
	/**
	 * Model rules
	 * @return array
	 */
	public function rules() {
		return array(
			array('phone, name, address', 'required'),
			array('name', 'length', 'max' => 255),
		);
	}

	/**
	 * Returns attribute labels
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'phone' => Yii::t('labels', 'Номер телефона'),
			'name' => Yii::t('labels', 'Ваше имя'),
			'address' => 'Ваша задача (вопрос)',
		);
	}
	
	public function send()
	{
		$mails = MailTemplate::model()->findAllByAttributes(array('name' => 'mounting'));
        foreach($mails as $mail)
        {
            $mail->send(array(
                '$name' => $this->name,
                '$phone' => $this->phone,
                '$address' => $this->address,
            ));
        }
		return true;
	}
}
