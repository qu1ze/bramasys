<?php

class m130613_190307_create_complect_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
                CREATE TABLE IF NOT EXISTS `complect` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`name` varchar(255) NOT NULL,
					PRIMARY KEY (`id`)
				  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
	}

	public function down()
	{
		$this->dropTable('complect');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}