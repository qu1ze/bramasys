<?php
$this->breadcrumbs=array(
	'Новый пароль',
);
?>
<div id="container">
	<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
	?>
	<h3>Новый пароль</h3>
	<div class="profile-content">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id'=>'horizontalForm',
			'type'=>'horizontal',
			'htmlOptions' => array('enctype' => 'multipart/form-data', 'style' => 'width: 600px; margin: 0px'),
		));?>
	
	<?php echo $form->errorSummary($client); ?>

	<?php echo $form->passwordFieldRow($client, 'newPassword'); ?>
	<?php echo $form->passwordFieldRow($client, 'passwordConfirm'); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить', 'htmlOptions' => array('class' => 'blue-button'))); ?>
<?php $this->endWidget();?>
	</div>
</div>
