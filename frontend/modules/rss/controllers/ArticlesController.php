<?php

class ArticlesController extends Controller
{
	public function actionIndex()
	{
		$seo = CategoryMetadata::model()->findByAttributes(array('url'=>'/'));
        header('Content-type: application/rss+xml; charset=utf-8');
        $criteria_articles = new CDbCriteria();
		$criteria_articles->order = 'create_time DESC';
		$criteria_articles->limit = 20;
		$last_articles = Article::model()->findAll($criteria_articles);
        
        $this->renderPartial('rss', array(
			'seo' => $seo,
			'model' => $last_articles,
		));   
		
	}
	
}