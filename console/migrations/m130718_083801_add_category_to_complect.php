<?php

class m130718_083801_add_category_to_complect extends CDbMigration
{
	public function safeUp()
	{
		$this->execute("
                ALTER TABLE  `complect`
					ADD  `category_id` INT(11) NOT NULL AFTER  `id`/*,
					ADD INDEX  `category_id` (`category_id`)*/;
			");
		$this->execute("
				ALTER TABLE  `complect`
					ADD CONSTRAINT `FK_complect_category`
					FOREIGN KEY (`category_id`)
					REFERENCES `category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ");
	}

	public function safeDown()
	{
		$this->execute("
                ALTER TABLE  `complect`
					DROP FOREIGN KEY `FK_complect_category`,
					DROP  `category_id`
			");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}