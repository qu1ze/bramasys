<?php 
Yii::app()->clientScript->registerCoreScript('jquery');
//Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/js/nivo-slider/nivo-slider.css");
//Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/js/nivo-slider/themes/default/default.css");
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/nivo-slider/jquery.nivo.slider.pack.js");

Yii::app()->clientScript->registerScript("initslider","
         $(window).load (function () {
         $.getScript('".Yii::app()->baseUrl."/js/nivo-slider/jquery.nivo.slider.pack.js',  function(){
            $('#slider').nivoSlider ({
				animSpeed:500,
				pauseTime:4000,
                controlNavThumbs:false,
                controlNavThumbsFromRel:true
            });});
            
            $(\"head link[rel='stylesheet']\").last().after(\"<link rel='stylesheet' href='".Yii::app()->baseUrl."/js/nivo-slider/nivo-slider.css' type='text/css' media='screen'>\");
            $(\"head link[rel='stylesheet']\").last().after(\"<link rel='stylesheet' href='".Yii::app()->baseUrl."/js/nivo-slider/themes/default/default.css' type='text/css' media='screen'>\");
      
         });
        ");
?>
<div class="slider-wrapper theme-default" style="height: 203px;overflow: hidden;">
    <div id= "slider" class= "nivoSlider">
        <?php foreach ($slides as $slide):?>
        <a href="<?php echo $slide->url; ?>" ><img alt="" src="<?php echo $slide->image_url; ?>"></a>
        <?php endforeach; ?>
    </div>
    <div id= "htmlcaption" class= "nivo-html-caption">
        <?php //текст ?>
    </div>
</div>