<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $text
 * @property integer $views
 * @property string $thumb
 * @property integer $create_time
 * @property integer $update_time
 * @property string $short_description
 * @property string $keywords
 */
class News extends ActiveRecord
{
	public $rating;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, title, text, create_time, update_time', 'required'),
            array('name, title, text, thumb, short_description, keywords', 'filter', 'filter'=>'trim'),
			array('views, create_time, update_time', 'numerical', 'integerOnly'=>true),
			array('name, title, thumb, keywords, short_description', 'length', 'max'=>255),			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, title, text, views, thumb, create_time, update_time, short_description, keywords', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'newsProducts' => array(self::HAS_MANY, 'NewsProduct', 'news_id'),
			'relatedNews' => array(self::HAS_MANY, 'RelatedNews', 'related_news_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'title' => 'Title',
			'text' => 'Text',
			'views' => 'Views',
			'thumb' => 'Thumb',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'short_description' => 'Short Description',
			'keywords' => 'Keywords',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('views',$this->views);
		$criteria->compare('thumb',$this->thumb,true);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('update_time',$this->update_time);
		$criteria->compare('short_description',$this->short_description,true);
		$criteria->compare('keywords',$this->keywords,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeValidate()
	{
		$r = parent::beforeValidate();
		
		if (!$this->create_time)
			$this->create_time = time();
		
		$this->update_time = time();
		
		return $r;
	}
	
	public function afterSave()
	{
		parent::afterSave();
		
		Keyword::model()->deleteAllByAttributes(array('object_id' => $this->id, 'object_type' => Keyword::OBJECT_TYPE_NEWS));
		if ($this->keywords)
		{			
			$keywords = explode(',', $this->keywords);
			foreach ($keywords as $keyword)
			{
				$new_keyword = new Keyword();
				$new_keyword->keyword = $keyword;
				$new_keyword->object_type = Keyword::OBJECT_TYPE_NEWS;
				$new_keyword->object_id = $this->id;
				$new_keyword->save();					
			}
		}				
	}
	public function getLink() {
		return  app()->getBaseUrl(true). Yii::app()->createUrl('/company/news/' . $this->name);
	}
	public function afterFind()
	{
		parent::afterFind();
		
		$keywords = Keyword::model()->findAllByAttributes(array('object_id' => $this->id, 'object_type' => Keyword::OBJECT_TYPE_NEWS));
		if ($keywords)
			$this->keywords = implode(', ', array_values(CHtml::listData($keywords, 'id', 'keyword')));
	}
	
	public function afterDelete()
	{
		parent::afterDelete();		
		Keyword::model()->deleteAllByAttributes(array('object_id' => $this->id, 'object_type' => Keyword::OBJECT_TYPE_NEWS));
	}
	
	 public function getRating() {
		if($this->rating == null)
		{
			$rating = 0;
			$comments = Comment::model()->findAll("`news_id` = $this->id AND rating != 0 AND status = 1");
			if(!empty($comments))
			{
				foreach ($comments as $comment)
				{
					$rating += $comment->rating;
				}
				$rating = round($rating / sizeof($comments), 1);

			}
			$this->rating = $rating;
		}
		return $this->rating;
	}
}