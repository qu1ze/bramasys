<?php

class m130902_055215_create_product_video_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `product_video` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `product_id` int(11) NOT NULL,
			  `link` varchar(255) NOT NULL,
			  PRIMARY KEY (`id`),
			  CONSTRAINT `FK_product_video_product` FOREIGN KEY (`product_id`)
					REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->dropTable('product_video');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}