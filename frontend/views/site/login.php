<?php
/**
 * login.php
 *
 * Example page <given as is, not even checked styles>
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:27 AM
 */
$this->pageTitle = 'Авторизация';
$this->breadcrumbs = array(
	'Авторизация',
);
?>
<h3 style="margin-bottom: 30px;">Авторизация</h3>

<?php $this->widget('common.components.LoginFormWidget', array('model' => $model, 'type' => LoginForm::LOGIN_FORM_TYPE_SUBMIT)); ?>