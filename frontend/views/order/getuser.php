<?php
/*
 *  <div class="sub_checkbox">
                        <label>
                            <input type="checkbox"> Хочу получать на электронную почту информацию о технических публикациях и обзорах
                        </label>
                    </div>
 */  
/* @var $client Client */
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/css/nivo-slider/nivo-slider.css");
	Yii::app()->clientScript->registerScript("delivery", "
		$('#Order_delivery_type_id').on('change', function() {
        	var select = parseInt($('#Order_delivery_type_id').val());
        	 if (!isNaN(select)){		
	        	if(select==2)
	            {
	                
	                $('#nova_poshta').show();
	                $('#address').hide();
	                $('#sam').hide();
	            } 
	            else if(select==3 || select==4)
	            {
	                $('#nova_poshta').hide();
	                $('#address').show();
	                $('#sam').hide();
	            }
	            else
	            {
	                
	                $('#nova_poshta').hide();
	                $('#address').hide();
	                $('#sam').show();
	            }
            }
		});
        $('#NP_town').on('change', function() {
        	 $('select#NP_id').hide(); 
            $.ajax({
		        url: '/order/getDeliverySysyemAddress',
		        data: {town: $(this).val()},
                dataType: 'json',
                type: 'get',
                success: function(data){
                    if(data.title === 'success')
                    {
                    $('select#NP_id').html('');
                        $.each(data.office, function(key, value){
                            $('select#NP_id').append($('<option>')
                                .val(key)
                                .html(value)
                            );
                            
                        })
                        $('select#NP_id').show();
                       
                    }
		        }
		    });
		});
        $('#Order_delivery_type_id').change();
	", CClientScript::POS_READY);
 
 
	Yii::app()->clientScript->registerScript("comment", "
		$('#comment-order-link').on('click', function() {
			if ($('#comment').css('display') == 'none')
			{
				$('#comment').fadeIn();
			}
			else
			{
				$('#comment').fadeOut();
			}
			return false
		});
	", CClientScript::POS_READY);

    Yii::app()->clientScript->registerScript("deliveryPayment", "
		$('#Order_delivery_type_id').on('change', function() {
            $.ajax({
		        url: '/order/getDeliveryDescription',
		        data: {id: $(this).val()},
                dataType: 'json',
                type: 'get',
                success: function(data){
                    if(data.title === 'success')
                    {
                        var lable = $('#priceorder').attr('data-lable');
                        var kof = parseFloat($('#priceorder').attr('data-kurs'));
                        if (isNaN(Math.round(100*data.value/kof)/100))
                            $('.price-delivery p').text(data.value);
                        else
                            $('.price-delivery p').text(data.value + lable);
                        $('select#Order_payment_type_id').html('');
                        $.each(data.payments, function(key, value){
                            $('select#Order_payment_type_id').append($('<option>')
                                .val(key)
                                .html(value)
                            );
                        })
                        $('#Order_payment_type_id').val('');
                        $('select#Order_payment_type_id').change();
                       
                    }
		        }

        });
        });
        
		$('select#Order_payment_type_id').bind('change focus', function() {
		    if($('#Order_payment_type_id').val()==5)
			{
				$('#company_name_div').show();
				reGetCart(2, $('#Order_delivery_type_id').val(), $('#Order_payment_type_id').val());
                
					
			}
			else {
				$('#company_name_div').hide();
				var curent = $('#priceorder').attr('data-curent');
				if (curent==2)	{
                    $('#priceorder').attr('data-lable','грн.');
					reGetCart(1, $('#Order_delivery_type_id').val(), $('#Order_payment_type_id').val());
				}
				else {
                    $('#priceorder').attr('data-lable', $('#priceorder').attr('data-lable2'));
					reGetCart(curent, $('#Order_delivery_type_id').val(), $('#Order_payment_type_id').val());
				}
				
			}	
		    
			
		});
        function reGetCart(currency, delivery, payment){
        	 $.ajax({
		        url: '/order/getCard',
		        data: {currency: currency, payment_id:  payment, delivery_id: delivery},
                dataType: 'json',
                type: 'get',
                success: function(data){
                    if(data.title === 'success')
                    {
                    	$('.my-order').html(data.value);
						$('#upprice').text(data.price);
                        
                    }
		        }
		    });
        	
		}
		
        $('body').on('click', '#agreement', function(){
			$.getJSON('/products/ajaxGetMesage?pageName=agreement', function(data) {
                $('#form_template #form-container .form-content').html(data.desc);
                var text =$('#form_template').html(); 
                $.colorbox({html:text}, function(){
                    $.colorbox.resize();});
            });
            return false;
		});
        
       $('body').on('click', '#close_',  function(){
        $.colorbox.close();
        });
    $('#Client_email').focus();
	", CClientScript::POS_READY);
?>
<?php
$this->pageTitle = 'Оформление заказа';
$this->breadcrumbs = array(
	'Оформление заказа',
);

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));

$sum = 0;
$count = 0;
foreach ($cart as $id => $product)
{
	if($id === 'complect')
    {
		foreach ($product as $id => $complectProd)
        {
			$sum += $complectProd['count'] * $complectProd['price'];
            $count++;// += $complectProd['count'];
        }
    }
	else
    {
		$sum += $product['count'] * $product['price'];
        $count++;// += $product['count'];
    }
}
?>
<div id="ordering">
    <div class="header-text">
        <h1 class="h3">Оформление заказа</h1>
        <p><?php echo $count ?> товара на сумму <span id="upprice"><?php echo CurrencySys::exchange($sum).' '.CurrencySys::getLabel(); ?></span></p>
        <div style="display: none;" id="priceorder"  data-curent="<?php echo Yii::app()->request->cookies['currency']->value ?>"  data-kurs="<?php echo CurrencySys::getKof(); ?>" data-lable=" <?php echo CurrencySys::getLabel(); ?>"><?php echo CurrencySys::exchange($sum) ?></div>
    </div>
    <?php if(!isset($confirm)): ?>
    <div tab-id="1" class="regTab active">
        <a>Новый клиент</a>
    </div> или <a class="ajax-login-btn" href="#">постоянный покупатель</a>
    <?php endif; ?>
    <div id="tabcontent">
    <?php if(!isset($confirm)): ?>
        <div id="new-client" class="tabcontent show" style="border: none;">
            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id' => 'registration-form',
                'type' => 'horizontal',
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'clientOptions' => array(
                'validateOnSubmit' => true,
                ),
            )); ?>
                <?php echo $form->errorSummary($client); ?>
                <?php echo $form->textFieldRow($client, 'email'); ?>
             <div class="controls"><div class="fieldWithSubLabel"> <label class="subLabelNoLeft">не менее 6-ти символов</label></div></div>
                <?php echo $form->passwordFieldRow($client, 'newPassword',array('style'=>'width: 170px;')); ?>
             <div class="controls"><div class="fieldWithSubLabel"> <label class="subLabelNoLeft">не менее 6-ти символов</label></div></div>
		        <?php echo $form->passwordFieldRow($client, 'passwordConfirm',array('style'=>'width: 170px;')); ?>
                <?php echo $form->textFieldRow($client, 'name'); ?>
                <?php echo $form->textFieldRow($client, 'lastname'); ?>
                <?php echo $form->textFieldRow($client, 'middle_name'); ?>
                <label class="control-label required" style="float: none;">Номер телефона <span class="required">*</span></label>
                <div class="control-group">
                    <?php echo CHtml::dropDownList("Phone[type]", $phone->type, $phone->getPhoneTypes(), array('class' => 'control-label', 'style' => 'width: 145px;'))?>
                    <div class="controls">
                        <div class="fieldWithSubLabel">
                            <input type="text" name="Phone[value]" id="phone" style='width: 170px;' value="<?php echo $phone->value; ?>">
                            <label class="subLabelNoLeft">В формате (067)676-76-76</label>
                            <?php echo CHtml::error($phone, 'value'); ?>
                        </div>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($order, 'delivery_type_id', DeliveryType::getList(true)); ?>
                <div class="control-group" id="address" style="display: none">
                    <label class="control-label required">Адрес доставки: <span class="required">*</span></label>
                    <div class="controls">
                        <div class="fieldWithSubLabel">
                            <input type="text" name="DeliveryAddress[town]" id="city" value="<?php echo $deliveryAddress->town; ?>">
                            <label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('town'); ?></label>
                        </div>
                        <div class="fieldWithSubLabel sub" style="width:180px;">
                            <input type="text" name="DeliveryAddress[street]" id="street" value="<?php echo $deliveryAddress->street; ?>">
                            <label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('street'); ?></label>
                        </div>
                        <div class="fieldWithSubLabel sub" style="width:72px;">
                            <input type="text" name="DeliveryAddress[house]" id="dom" value="<?php echo $deliveryAddress->house; ?>">
                            <label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('house'); ?></label>
                        </div>
                        <div class="fieldWithSubLabel sub" style="width:92px; margin-right:0px;">
                            <input type="text" name="DeliveryAddress[flat]" id="flat" value="<?php echo $deliveryAddress->flat; ?>">
                            <label class="subLabelNoLeft" style="padding-top: 3px;line-height: 12px;"><?php echo $deliveryAddress->getAttributeLabel('flat'); ?></label>
                        </div>
                        <div class="fieldWithSubLabel sub" style="width: 100%;">
                            <input type="text" name="DeliveryAddress[additional_info]" id="city" value="<?php echo $deliveryAddress->additional_info; ?>">
                            <label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('additional_info'); ?></label>
                        </div>
                    </div>
                </div>
                <div class="control-group" id="nova_poshta" style="display: none">
                    <label class="control-label required">Отделение новой почты  <span class="required">*</span></label>
                    <div class="controls">
                        <div class="fieldWithSubLabel sub">
                            <?php echo CHtml::dropDownList('NP[town]', isset($NP['town'])?$NP['town']:0, CHtml::listData(DeliverySystemAddress::model()->findAll(array('order'=>'town')), 'town', 'town'), array('empty'=>'')); ?>
                             <label class="subLabelNoLeft"><?php echo DeliverySystemAddress::model()->getAttributeLabel('town'); ?></label>
                            </div>
                        <div class="fieldWithSubLabel sub">
                            <?php if (isset($NP['town'])) { $mas = CHtml::listData(DeliverySystemAddress::model()->findAllByAttributes(array('town'=>$NP['town'])));}
                                else { $mas=array();}
                            echo CHtml::dropDownList('NP[id]', isset($NP['id'])?$NP['id']:0, $mas, array()); ?>
                             <label class="subLabelNoLeft"><?php echo DeliverySystemAddress::model()->getAttributeLabel('office'); ?></label>
                            </div>
                     </div>
                </div>
                 <div class="control-group" id="sam" style="display: none">
                <div class="fieldWithSubLabel" style="border: solid 1px #B9B9B9;margin-left: 160px;">  <font color="#010101"><span style="font-size:11pt"><b>Адрес:</b></span></font> 
                    <span style="font-size:11pt">г. Запорожье, пр. Ленина 158, оф. 302</span>
                    <div align="left" style="line-height: 115%;">
                            <span style="font-size:11pt">Схема проезда в разделе «</span>
                            <span style="font-size:11pt"><a href="/kontakty"> Контакты</a></span>
                            <span style="font-size:11pt">»</span></div>
                    <div align="left" style="line-height: 115%;"><span style="font-size:11pt"><b>График работы офиса:</b></span> 
                        <span style="font-size:11pt">Пн.-Пт. 9.00-18.00</span></div>
                    <div align="left" style="line-height: 115%;"><span style="font-size:11pt"><b>Контактный телефон:</b></span> <span style="font-size:11pt">(061) 701-19-92</span>
                    </div>
                    </div>
                </div>
                
                <?php echo $form->dropDownListRow($order, 'payment_type_id', array('' => 'Выберите способ доставки')); ?>
                <div id="company_name_div" style='display:none'><div class="controls"><div class="fieldWithSubLabel"> <label class="subLabelNoLeft">ООО «Рога и копыта»</label></div></div>
                	<?php echo $form->textFieldRow($order, 'company_name'); ?></div>
                
                <div class="complete_order">
                    <div class="comment_link" id="comment-order-link"><a>Добавить комментарий к заказу</a></div>
                    <textarea name="Order[comment]" id="comment" cols="42" rows="10" style="display: none"><?php echo $order->comment; ?></textarea>
                    <div class="actions">
                        <?php echo CHtml::submitButton('Подтвердить заказ', array('class' => 'cart-btn')); ?>
                    </div>
                    <p>
                        <span style="color: red">*</span>Поля отмеченные звездочкой, обязательны для заполнения
                    </p>
                    <div class="sub_checkbox">
                        <label>
                            <input type="checkbox" checked > Подтверждая заказ, я соглашаюсь с
                            <a id="agreement" href="#">пользовательским соглашением</a>
                        </label>
                    </div>
                   
                </div>
            <?php $this->endWidget(); ?>
        </div>
    <?php else: ?>
        <p style="font: 24px 'PTSN Regular'">Укажите данные для оформления</p>
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'confirm-form',
            'type' => 'horizontal',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        )); ?>
            <?php echo $form->errorSummary($order); ?>

            <input name="confirm" value="1" type="hidden">
            <?php echo $form->textFieldRow($client, 'name'); ?>
            <?php echo $form->textFieldRow($client, 'lastname'); ?>
            <?php echo $form->textFieldRow($client, 'middle_name'); ?>
            <?php if(empty($client->phone)): ?>
            <label class="control-label required" style="float: none;">Номер телефона <span class="required">*</span></label>
            <div class="control-group">
                <?php echo CHtml::dropDownList("Phone[type]", $phone->type, $phone->getPhoneTypes(), array('class' => 'control-label', 'style' => 'width: 145px;'))?>
                <div class="controls">
                    <div class="fieldWithSubLabel">
                        <input type="text" name="Phone[value]" id="phone" style='width: 170px;' value="<?php echo $phone->value; ?>">
                        <label class="subLabelNoLeft">В формате (067) 676-76-76</label>
                        <?php echo CHtml::error($phone, 'value'); ?>
                    </div>
                </div>
             </div>
            <?php endif; ?>
            
            <?php echo $form->dropDownListRow($order, 'delivery_type_id', DeliveryType::getList(true)); ?>
            
            <div class="control-group" id="nova_poshta" style="display: none">
                    <label class="control-label required">Отделение новой почты  <span class="required">*</span></label>
                    <div class="controls">
                        <div class="fieldWithSubLabel sub">
                            <?php echo CHtml::dropDownList('NP[town]', isset($NP['town'])?$NP['town']:0, CHtml::listData(DeliverySystemAddress::model()->findAll(array('order'=>'town')), 'town', 'town'), array('empty'=>'')); ?>
                             <label class="subLabelNoLeft"><?php echo DeliverySystemAddress::model()->getAttributeLabel('town'); ?></label>
                            </div>
                        <div class="fieldWithSubLabel sub">
                            <?php if (isset($NP['town'])) $mas = CHtml::listData(DeliverySystemAddress::model()->findAllByAttributes(array('town'=>$NP['town'])));
                                else $mas=array();
                            echo CHtml::dropDownList('NP[id]', isset($NP['id'])?$NP['id']:0, $mas, array()); ?>
                             <label class="subLabelNoLeft"><?php echo DeliverySystemAddress::model()->getAttributeLabel('office'); ?></label>
                            </div>
                     </div>
                </div>
            <div class="control-group" id="sam" style="display: none">
                <div class="fieldWithSubLabel" style="border: solid 1px #B9B9B9;margin-left: 160px;">  <font color="#010101"><span style="font-size:11pt"><b>Адрес:</b></span></font> 
                    <span style="font-size:11pt">г. Запорожье, пр. Ленина 158, оф. 302</span>
                    <div align="left" style="line-height: 115%;">
                            <span style="font-size:11pt">Схема проезда в разделе «</span>
                            <span style="font-size:11pt"><a href="/kontakty"> Контакты</a></span>
                            <span style="font-size:11pt">»</span></div>
                    <div align="left" style="line-height: 115%;"><span style="font-size:11pt"><b>График работы офиса:</b></span> 
                        <span style="font-size:11pt">Пн.-Пт. 9.00-18.00</span></div>
                    <div align="left" style="line-height: 115%;"><span style="font-size:11pt"><b>Контактный телефон:</b></span> <span style="font-size:11pt">(061) 701-19-92</span>
                    </div>
                    </div>
                </div>
            <div id="address" style="display: none">
            <?php if(!empty($client->delivery_address)): ?>
                <?php echo $form->dropDownListRow($order, 'delivery_address', $client->getDeliveryAddresses(true)); ?>
            <?php else: ?>
            <div class="control-group" >
                <label class="control-label required">Адрес доставки <span class="required">*</span></label>
                <div class="controls">
                    <div class="fieldWithSubLabel">
                        <input type="text" name="DeliveryAddress[town]" id="city" value="<?php echo $deliveryAddress->town; ?>">
                        <label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('town'); ?></label>
                    </div>
                    <div class="fieldWithSubLabel sub" style="width:180px;">
                        <input type="text" name="DeliveryAddress[street]" id="street" value="<?php echo $deliveryAddress->street; ?>">
                        <label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('street'); ?></label>
                    </div>
                    <div class="fieldWithSubLabel sub" style="width:72px;">
                        <input type="text" name="DeliveryAddress[house]" id="dom" value="<?php echo $deliveryAddress->house; ?>">
                        <label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('house'); ?></label>
                    </div>
                    <div class="fieldWithSubLabel sub" style="width:92px; margin-right:0px;">
                        <input type="text" name="DeliveryAddress[flat]"   id="flat" value="<?php echo $deliveryAddress->flat; ?>">
                        <label  class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('flat'); ?></label>
                    </div>
                    <div class="fieldWithSubLabel sub" style="width: 100%;">
                        <input type="text" name="DeliveryAddress[additional_info]" id="city" value="<?php echo $deliveryAddress->additional_info; ?>">
                        <label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('additional_info'); ?></label>
                    </div>
                </div>
            </div>
           
            <?php endif; ?>
            </div>
            <?php echo $form->dropDownListRow($order, 'payment_type_id', array('' => 'Выберите способ доставки')); ?>
            <div id="company_name_div" style='display:none'><div class="controls"><div class="fieldWithSubLabel"> <label class="subLabelNoLeft">ООО «Рога и копыта»</label></div></div>
            <?php echo $form->textFieldRow($order, 'company_name'); ?> </div>
            <div class="complete_order">
                <div class="comment_link" id="comment-order-link"><a>Добавить комментарий к заказу</a></div>
                <textarea name="Order[comment]" id="comment" cols="42" rows="10" style="display: none"><?php echo $order->comment; ?></textarea>
                <div class="actions">
                    <?php echo CHtml::submitButton('Подтвердить заказ', array('class' => 'cart-btn')); ?>
                </div>
                <p>
                    <span style="color: red">*</span>Поля отмеченные звездочкой, обязательны для заполнения
                </p>
            </div>
        <?php $this->endWidget(); ?>
    <?php endif; ?>
        <div class="my-order">
            <div class="header-my-order">
                <h1 class="h3">Мой заказ</h1>
                <div class="edit-my-order cart">
                    <a href="#"><img src="/images/orderedit.jpg"></a>
                    <a class="order-edit" href="#">Редактировать</a>
                </div>
            </div>
            <div id="my-order-prod">
            <?php
            foreach ($cart as $id => $prod)
            {
                if($id !== 'complect')
                    $view = '_product';
                else
                    $view = '_complect';
                $this->renderPartial($view, array(
                    'id' => $id,
                    'prod' => $prod,
                    'product' => Products::model()->findByPk($id),
                    'withPrice' => true,
                ));
            }
            ?>
            </div>
            <div class="header-my-order delivery-my-order ">
                <h1 class="h3">Доставка:</h1>
                <div class="edit-my-order price-delivery">
                    <p>Способ доставки не выбран.</p>
                </div>
            </div>
             <div class="header-my-order commission-my-order ">
                <h1 class="h3">Комиссия:</h1>
                <div class="edit-my-order price-commission">
                    <p>0</p>
                </div>
            </div>
            <div class="header-my-order total-my-order">
                <h1 class="h3">Итого к оплате:</h1>
                <div class="edit-my-order total-price">
                    <p id="firstPrice"><?php echo CurrencySys::exchange($sum).' '.CurrencySys::getLabel(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="form_template" style="display: none">
<div id="form-container" >
		<div class="form-header">
			<h3>&nbsp;</h3>
			<a class="close hover" id="close_" href="#" style="top: -5px;">
			</a>
		</div>
		<div class="form-content">
		</div>
	</div>
</div>
