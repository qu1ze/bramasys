<?php

/**
 * This is the model class for table "comment".
 *
 * The followings are the available columns in table 'comment':
 * @property integer $id
 * @property integer $author_id
 * @property string $author_name
 * @property string $email
 * @property integer $rating
 * @property string $content
 * @property string $pros
 * @property string $cons
 * @property integer $useful_count
 * @property integer $post_date
 * @property integer $product_id
 * @property integer $complect_id
 * @property integer $article_id
 * @property integer $parent_id
 * @property integer $status
 * The followings are the available model relations:
 * @property Client $author
 * @property Products $product
 * @property Comment $parent
 * @property Comment[] $comments
 * @property CommentVote[] $commentVotes
 */
class Comment extends ActiveRecord
{
	private $authorName = null;
	
	private $attributeLabels = array();

	private $blockedNames = array(
		'admin',
		'administrator',
		'administration',
		'админ',
		'администратор',
		'администрация',
	);
	
	const COMMENT_STATUS_PENDING = 0;
	const COMMENT_STATUS_APPROVED = 1;
	
	private $commentStatuses = array(
		self::COMMENT_STATUS_PENDING => 'Ожидает подтверждения',
		self::COMMENT_STATUS_APPROVED => 'Подтвержден',
	);
	
	public function getOrderStatuses($status = null)
	{
		return is_null($status) ? $this->commentStatuses : $this->commentStatuses[$status];
	}
	
	public function getAuthorName()
	{
		if(empty($this->authorName))
		{
			if(!empty($this->author_name))
			{
				$this->authorName = $this->author_name;
			}
			else
			{
				$this->authorName = $this->author->name;
			}
		}
		return $this->authorName;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content, post_date', 'required'),
            array('author_name, email, content, pros, cons', 'filter', 'filter'=>'trim'),
			array('product_id', 'required', 'on' => 'product', 'except' => 'complect, article'),
			array('complect_id', 'required', 'on' => 'complect', 'except' => 'article, product'),
			array('article_id', 'required', 'on' => 'article', 'except' => 'complect, product'),
			array('status', 'in', 'range'=>array_keys($this->commentStatuses)),
			array('author_name', 'required', 'on' => 'fromGuest', 'message'=>'Введите ваше имя или авторизируйтесь.'),
			array('author_id', 'required', 'on' => 'insert', 'message'=>'Сбой авторизации'),
			array('author_name, author_id', 'validateClientOrGuest'),
			array('author_id, rating, useful_count, post_date, product_id, complect_id, article_id, parent_id, status, news_id', 'numerical', 'integerOnly'=>true),
			array('email', 'email'),
			array('author_name', 'length', 'max'=>50),
			array('email', 'length', 'max'=>255),
			array('pros, cons', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, author_id, author_name, email, rating, content, pros, cons, useful_count, post_date, product_id, complect_id, article_id, parent_id, status, news_id', 'safe', 'on'=>'search'),
		);
	}

	private function isBlockedName($name)
	{
		return in_array(mb_strtolower($name, 'UTF-8'), $this->blockedNames);
	}

	public function validateClientOrGuest($attribute, $params)
	{
		if(Yii::app()->user->id == null)
		{
			if($attribute == 'author_name')
			{
				if($this->author_name == null)
					$this->addError($attribute, 'Введите ваше имя или авторизируйтесь');
				else if($this->isBlockedName($this->author_name) && $this->scenario == 'fromGuest')
					$this->addError($attribute, 'Пожалуйста, подпишитесь другим именем');
			}
		}
		else if($this->scenario == 'insert')
		{
			if($attribute == 'author_id' && $this->author_id == null)
				$this->addError($attribute, 'Сбой авторизации');
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'Client', 'author_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
			'parent' => array(self::BELONGS_TO, 'Comment', 'parent_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'parent_id'),
			'commentVotes' => array(self::HAS_MANY, 'CommentVote', 'comment_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'author_id' => 'Автор',
			'author_name' => 'Ваше имя',
			'email' => 'Электронная почта',
			'rating' => 'Оценка товара',
			'content' => 'Комментарий',
			'pros' => 'Плюсы',
			'cons' => 'Минусы',
			'useful_count' => 'Количество голосов',
			'post_date' => 'Дата публикации',
			'product_id' => 'Продукт',
			'complect_id' => 'Комплект',
			'article_id' => 'Статья',
			'parent_id' => 'Родительский отзыв',
			'status' => 'Статус',
		);
	}

	public function setAttributeLabel($attribute, $value)
	{
		$this->attributeLabels[$attribute] = $value;
	}
	
	public function getAttributeLabel($attribute, $dontClear = false)
	{
		$label = isset($this->attributeLabels[$attribute]) ? $this->attributeLabels[$attribute] : parent::getAttributeLabel($attribute);
		if(!$dontClear)
		{
			unset($this->attributeLabels[$attribute]);
		}
		return $label;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('author_name',$this->author_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('pros',$this->pros,true);
		$criteria->compare('cons',$this->cons,true);
		$criteria->compare('useful_count',$this->useful_count);
		$criteria->compare('post_date',$this->post_date);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('complect_id',$this->complect_id);
		$criteria->compare('article_id',$this->article_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	private function timeToDate($time = null)
	{
		if(empty($time))
			$this->post_date = Yii::app()->dateFormatter->format('d MMMM yyyy', $this->post_date);
		else
			return Yii::app()->dateFormatter->format($time, 'd MMMM yyyy', null);
	}
	
	private function dateToTime($date = null)
	{
		if(empty($date))
		{
			if(!is_numeric($this->post_date) && !empty($this->post_date))
			{
				/* костыль Валера */
				Yii::app()->setLanguage('ru_RU');
				$monthNames = Yii::app()->locale->monthNames;
				Yii::app()->setLanguage('en_US');
				$date = str_replace($monthNames, Yii::app()->locale->monthNames, $this->post_date);
				$this->post_date = strtotime($date);
				Yii::app()->setLanguage('ru_RU');
			}
		}
		else
		{
			return CDateTimeParser::parse($date, 'd MMMM yyyy');
		}
	}
		
	protected function afterSave()
	{
		parent::afterSave();
		$this->timeToDate();
        if ($this->parent_id != null)
        {
            $parent = Comment::model()->findByPk($this->parent_id);
            if ($parent && !empty($parent->email) && $this->status==1)
            {
                $mail = MailTemplate::model()->findByAttributes(array('name' => 'customer-reviews'));
                
                $linck = '<a href='.$parent->product->getLink().'>'.$parent->product->getLink().'</a>';
                $comentlinck = '<a href='.$parent->product->getLink().'#comment'.$parent->id.'>'.$parent->product->getLink().'#comment'.$parent->id.'</a>';
                $sendArg = array(
                    '$email' => $parent->email,
                    '$name' => $parent->author_name,
                    '$brenr' => $parent->product->brand0->name,
                    '$model' => $parent->product->model,
                    '$linck' => $linck, 
                    '$commentlinck' => $comentlinck,
                );
//                ob_start();
//                var_dump($sendArg);
//                $tr =  ob_get_clean();
//                ob_end_flush();
//                throw new Exception($tr);
                $mail->send($sendArg);
            }
        }
	}
	
	protected function afterFind()
	{
		parent::afterFind();
		$this->timeToDate();
	}
	
	protected function afterConstruct()
	{
		parent::afterConstruct();
		$this->rating = null;
		$this->useful_count = null;
		$this->status = null;
	}
	
	protected function beforeValidate()
	{
		if($this->rating == null)
			$this->rating = 0;
		if($this->useful_count == null)
			$this->useful_count = 0;
		if($this->status == null)
			$this->status = 0;
		$this->dateToTime();
		return parent::beforeValidate();
	}
}