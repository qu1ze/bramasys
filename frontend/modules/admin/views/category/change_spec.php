<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js"></script>
<?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/jquery-ui.min.js");
    Yii::app()->clientScript->registerScript("dropSpecs", "
        $('.form-horizontal').on('click', '.dropSpec', function(){
            var id = $(this).attr('value');
            var value = $('#dropSpec' + id + ' .control-label').text();
            if (id)
            {
                var url = '/admin/category/ajaxDeleteSpec';
                $.ajax(
                {
                    url: url,
                    data: {specId: id, currentId: $currentCategory},
                    dataType: 'json',
                    type: 'get',
                    success: function(data){
                        $('#CategorySpec_spec_id').append('<option value=\"' + id + '\" >' + value + '</option>');
                        $('#dropSpec'+id).fadeOut('slow', function()
                        {
                            $('#dropSpec'+id).parent().remove();
                        });
                        brama.ui.notify(data.title, data.msg);
                    }
                });
            }
        });
    ", CClientScript::POS_READY);
        
    Yii::app()->clientScript->registerScript("addSpecs", "
        $('.form-horizontal').on('click', '#buttonAdd', function(){
            var specId = $('#CategorySpec_spec_id').val();
            if (specId)
            {
                var url = '/admin/category/ajaxAddSpec';
                $.ajax(
                {
                    url: url,
                    data: {CategorySpec:{spec_id: specId, category_id: $currentCategory}},
                    dataType: 'json',
                    type: 'post',
                    success: function(data){
                        if(typeof(data) == 'object')
                        {
                            if(data.html)
                            {
                                $('#spec-group #sortable').append(data.html);
                                $('#CategorySpec_spec_id option[value=' + specId + ']').remove();
                                $('#dropSpec'+specId).parent().fadeIn();
                                $('#sortable').sortable('refresh');
                                $('#dropSpec'+specId).parent().disableSelection();
                            }
                            brama.ui.notify(data.title, data.msg);
                        }
                    }
                });
            }
        });
    ", CClientScript::POS_READY);

    Yii::app()->clientScript->registerScript("setInFilter", "
        $('.form-horizontal').on('change', '#inFilter', function(){
             var id = $(this).parent().parent().parent().parent().attr('cat-spec-id');
             id = id ? id : 0;
             var value = $(this).attr('checked') ? 1 : 0;
             $.ajax(
             {
                url: '/admin/category/changeSpecInFilter',
                data: {id: id, value: value},
                dataType: 'json',
                type: 'get',
                success: function(data){
                    if(typeof(data) == 'object')
                    {
                        brama.ui.notify(data.title, data.msg);
                    }
                }
             });
        });
    ", CClientScript::POS_READY);

    Yii::app()->clientScript->registerScript("setOrder", "
        $('.form-horizontal').on('change', '.orderNumber', function(){
             var id = $(this).parent().parent().parent().attr('cat-spec-id');
             id = id ? id : 0;
             var value = $(this).val() ? $(this).val() : -1;
             $.ajax(
             {
                url: '/admin/category/setSpecOrder',
                data: {id: id, value: value},
                dataType: 'json',
                type: 'get',
                success: function(data){
                    if(typeof(data) == 'object')
                    {
                        brama.ui.notify(data.title, data.msg);
                    }
                }
             });
        });
    ", CClientScript::POS_READY);
?>
<div class="form-horizontal">
    <div id="spec-group">
        <div class="control-group">
            <label class="control-label">Scecifications:</label>
        </div>
        <script>
            $(function() {
                $( "#sortable" ).sortable({
                    revert: true,
                    stop: function() {
                        var list = [];
                        $('.specContainer').each(function(){
                            var id = $(this).attr('cat-spec-id');
                            value = $('#orderNumber' + id).val();
                            value = value ? value : 0;
                            list.push([id, value]);
                        });
                        $.ajax(
                        {
                            url: '/admin/category/changeSpecOrder',
                            data: {list: list},
                            dataType: 'json',
                            type: 'get',
                            success: function(data){
                                if(typeof(data) == 'object')
                                {
                                    brama.ui.notify(data.title, data.msg);
                                    if(data.title == 'Success!')
                                    {
                                        $key = 0;
                                        $('.orderNumber').each(function(){
                                            $(this).val(data.orders[$key]);
                                            $key++;
                                        });
                                    }
                                }
                            }
                        });
                    }
                });
                $( ".specContainer" ).disableSelection();
            });
        </script>
        <div id="sortable">
            <?php foreach ($model as $specification):?>
            <div class="specContainer" cat-spec-id="<?php echo $specification->id; ?>">
                <div class="control-group" id="dropSpec<?php echo $specification->spec_id; ?>">
                    <label class="control-label"><?php echo $specifications[$specification->spec_id]; ?></label>
                    <div class="controls">
                        <label class="checkbox" style="float: left; padding-right: 10px;">
                            <input name="in-filter" id="inFilter" value="1" <?php echo $specification->in_filter ? 'checked="checked"' : ''; ?> type="checkbox">
                            <?php echo $specification->getAttributeLabel('in_filter'); ?>
                        </label>
                        <input type="text" class="orderNumber" id="orderNumber<?php echo $specification->id; ?>" value="<?php echo $specification->order; ?>">
                        <img class="dropSpec" value="<?php echo $specification->spec_id; ?>" style="padding-top:10px;" src="/images/cross.gif" width="10"/>
                    </div>
                </div>
            </div>
            <?php
                unset($specifications[$specification->spec_id]);
            ?>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label"><?php echo 'Добавить новую хар-ку'; ?></label>
        <div class="controls">
            <?php echo CHtml::dropDownList('CategorySpec[spec_id]', key($specifications), $specifications); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'type' => 'success',
                'label' => 'Добавить',
                'htmlOptions'=> array('id'=>'buttonAdd'),
                ));
            ?>
        </div>
    </div>
</div>