<?php

class CategoryController extends Controller
{
    
     
	public function actionCreate()
	{		
		$model = new Category; 
				
		if (isset($_POST['Category']))
		{						
			$model->attributes = $_POST['Category'];
			if ($model->save())
				$this->redirect(array('/admin/category'));
		}
		$criteria_articles = new CDbCriteria();
                $criteria_articles->order = 'order_number ASC';
		$categories = Category::model()->findAll($criteria_articles);
        //var_dump($this->listDataTree($categories));
		$list = Category::listDataTree($categories);
		$this->render('create', array(
			'model' => $model, 
			'list' => $list,			
		));
	}
	
	public function buildTree($categories, $parent_id = 0)
	{
		if(is_array($categories) && isset($categories[$parent_id]))
		{
			$tree = '<ul>';

			foreach($categories[$parent_id] as $category)
			{
				$tree .= '<li>';
					$tree .= '<a href="' . Yii::app()->createUrl('admin/category/update/' . $category->id) . '" >' . $category->title . '</a> ';
                                if (is_object($category->related_tag))
                                        $tree .= '(<a href="' . Yii::app()->createUrl('admin/categorytag/update/' . $category->related_tag->id) . '">' . $category->related_tag->name . '</a>) - ';
					$tree .= '<a href="' . Yii::app()->createUrl('admin/category/delete/', array('id' => $category->id) ) . '" ><img src="/images/cross.gif" width="8"/></a>';
					$tree .=  $this->buildtree($categories, $category->id);
				$tree .= '</li>';         
			}

			$tree .= '</ul>';
			
			return $tree;    
		} 		 	  
		    
	}
    
    
    
    
	public function actionChangeSpec($id)
	{
		if(($model = CategorySpec::model()->findAllByAttributes(array('category_id' => $id), array('order' => '`order` ASC'))) === null)
			$this->redirect(array('/admin/category'));
        $excludeIds = array();
        $parent = app()->categoryController->getCategory($id);
        $parents = app()->categoryController->AddParentsIds($parent['parent']);
        foreach(CategorySpec::model()->findAllByAttributes(array('category_id' => $parents)) as $categorySpec)
        {
            array_push($excludeIds, $categorySpec->spec_id);
        }
        $criteria = new CDbCriteria();
        $criteria->addNotInCondition('id', $excludeIds);
		$specifications = CHtml::listData(Specification::model()->findAll($criteria), 'id', 'name');
		echo $this->render('change_spec', array(
				'model' => $model,
				'specifications' => $specifications,
				'currentCategory' => $id,
		));	
	}

    public function actionSetSpecOrder($id, $value)
    {
        if($value != -1)
        {
            $model = CategorySpec::model()->findByPk($id);
            if($model === null)
            {
                echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно изменить порядок'));
                app()->end();
            }
            $model->order = $value;
            $model->save();
            echo json_encode(array('title'=>'Success!', 'msg'=>'Порядок изменен'));
        }
    }

    public function actionChangeSpecOrder(array $list)
    {
        $orders = array();
        foreach($list as $value)
        {
            if(!is_array($value))
            {
                echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно изменить порядок'));
                app()->end();
            }
            array_push($orders, $value[1]);
        }
        sort($orders);
        foreach($list as $key => $value)
        {
            $id = $value[0];
            $model = CategorySpec::model()->findByPk($id);
            if($model === null)
            {
                echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно изменить порядок'));
                app()->end();
            }
            $model->order = $orders[$key];
            $model->save();
        }
        echo json_encode(array('title'=>'Success!', 'msg'=>'Порядок изменен', 'orders'=>$orders));
    }

    public function actionChangeSpecInFilter($id, $value)
    {
        $model = CategorySpec::model()->findByPk($id);
        if($model !== null)
        {
            $model->in_filter = $value;
            $model->save();
            echo json_encode(array('title'=>'Success!', 'msg'=>'Значение изменено'));
            app()->end();
        }
        echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно изменить значение'));
    }
        
    public function actionAjaxAddSpec()
	{	
		$model = new CategorySpec();

		if (isset($_POST['CategorySpec']) && Yii::app()->request->isAjaxRequest)
		{
            $parent = app()->categoryController->getCategory($_POST['CategorySpec']['category_id']);
            $parents = app()->categoryController->AddParentsIds($parent['parent']);
            if(($category = CategorySpec::model()->findByAttributes(array('category_id' => $parents, 'spec_id' => $_POST['CategorySpec']['spec_id']))) !== null)
            {
                echo json_encode(array('title'=>'Oops!', 'msg'=>"Хар-ка уже добавлена в родительской категории ($category->title)"));
                app()->end();
            }
			$model->attributes = $_POST['CategorySpec'];
			if ($model->save())
			{
                $categorySpec = CategorySpec::model()->findByAttributes(array('category_id' => $_POST['CategorySpec']['category_id'], 'spec_id' => $_POST['CategorySpec']['spec_id']));
				echo json_encode(array(
					'title'=>'Success!',
					'msg'=>'Хар-ка успешно добавлена',
					'html'=>$this->renderPartial('_spec', array(
						'specification' => $categorySpec,
					), true)
					));
                app()->end();
            }
		}
        echo json_encode(array('title'=>'Oops!', 'msg'=>'Не удалось добавить хар-ку'));
	}
        
	public function actionIndex($mode = 'tree')
	{		
		if ($mode == 'table')
		{
			$model = new Category('search');
			if (isset($_GET['Category']))
				$model->attributes = $_GET['Category'];
				
			$options['model'] = $model;							
		} else {	
            $criteria_articles = new CDbCriteria();
            $criteria_articles->order = 'order_number ASC';
			$categories = Category::model()->findAll($criteria_articles);
			
			foreach($categories as $value)		
				$tree_categories[$value->parent ? $value->parent : 0][$value->id] = $value;	
				
			$tree = $this->buildTree($tree_categories);
			$options['tree'] = $tree;								
		}
		$options['list'] = '';
		$options['mode'] = $mode;
		$this->render('index', $options);
	}
	
	public function actionUpdate($id)
	{		
		if (($model = Category::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/category'));		
		
		if (isset($_POST['Category']))
		{				
			$model->attributes = $_POST['Category'];
			if ($model->save())
				$this->redirect(array('/admin/category'));
		}
		$criteria_articles = new CDbCriteria();
        $criteria_articles->order = 'order_number ASC';
		$categories = Category::model()->findAll($criteria_articles);
        //var_dump($this->listDataTree($categories));
		$list = Category::listDataTree($categories);
		//$list = CHtml::listData(Category::model()->findAll(), 'id', 'title');		
		$this->render('update', array(
			'model' => $model,
			'list' => $list,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Category');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Категория успешно отредактирована')));			
	}
	
	public function actionDelete($id)
	{
		$category = Category::model()->findByPk($id);
		if(!$category->isSystemCategory())
		{
			if ($id)
			{
				$flag = true;
				if ((Category::model()->findByAttributes(array('parent' => $id))) !== null)
				{
					$flag = false;
					Yii::app()->user->setFlash('error', "Существует связь с категорией");
				}
				if ((Products::model()->findByAttributes(array('category' => $id))) !== null)
				{
					$flag = false;
					Yii::app()->user->setFlash('error', "Существует связь с продуктом");
				}
				if((SupportFile::model()->findByAttributes(array('category_id' => $id))) !== null)
				{
					$flag = false;
					Yii::app()->user->setFlash('error', "Существует связь с файлом");
				}
				if((StaticPage::model()->findByAttributes(array('category_id' => $id))) !== null)
				{
					$flag = false;
					Yii::app()->user->setFlash('error', "Существует связь со статической страницей");
				}
				if($flag)
				{
					$category->delete();					
					Yii::app()->user->setFlash('success', "Категория успешно удалена");
				}
			}
		}
		else
		{
			Yii::app()->user->setFlash('error', "Невозможно удалить системную категорию.");
		}
		$this->redirect(array('/admin/category'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = Category::model()->findByPk($id)) !== null)
		{
			if(!$model->isSystemCategory())
			{
				$flag = true;
				if ((Category::model()->findByAttributes(array('parent' => $id))) !== null)
				{
					$flag = false;
					echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с категорией'));
				}
				if ((Products::model()->findByAttributes(array('category' => $id))) !== null)
				{
					$flag = false;
					echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с продуктом'));
				}
				if((SupportFile::model()->findByAttributes(array('category_id' => $id))) !== null)
				{
					$flag = false;
					echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с файлом'));
				}
				if((StaticPage::model()->findByAttributes(array('category_id' => $id))) !== null)
				{
					$flag = false;
					echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь со статической страницей'));
				}
				if($flag)
				{
					if ($model->delete())
						echo json_encode(array('title' => 'Success', 'msg' => 'Категория успешно удалена'));
					else
						echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить категорию'));
				}
			}
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить системную категорию'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Категория не найдена'));
	}

    public function actionAjaxDeleteSpec($specId, $currentId)
	{		
		if (($model = CategorySpec::model()->findByAttributes(array('category_id' => $currentId, 'spec_id' => $specId))) !== null)
		{
            if ($model->delete())
                echo json_encode(array('title' => 'Success', 'msg' => 'Хар-ка успешно удалена'));
            else
                echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить хар-ку'));
		} 
        else
            echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Хар-ка не найдена'));
	}
	
}