<?php

class ArticleproductsController extends Controller
{		
	public function actionCreate()
	{
		$model = new ArticleProduct; 
		
		//$this->performAjaxValidation($model);
		
        
        $error=false;
		if (isset($_POST['ArticleProduct']))
		{	
             if (isset($_POST['ArticleProduct']['product_id']) && is_array($_POST['ArticleProduct']['product_id']))
                if($model->saveProducts($_POST['ArticleProduct']['product_id'],$_POST['ArticleProduct']['article_id']))
                    $this->redirect(array('/admin/articleproducts'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionIndex()
	{		
		$model = new ArticleProduct('search');
		
		if (isset($_GET['ArticleProduct']))
			$model->attributes = $_GET['ArticleProduct'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = ArticleProduct::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/articleproducts'));				
				
		if (isset($_POST['ArticleProduct']))
		{	
            if (isset($_POST['ArticleProduct']['product_id']) && is_array($_POST['ArticleProduct']['product_id']))
                if($model->saveProducts($_POST['ArticleProduct']['product_id'],$_POST['ArticleProduct']['article_id'],true))
                    $this->redirect(array('/admin/articleproducts'));
		}
        
        
		$mass = ArticleProduct::model()->findAllByAttributes(array('article_id' => $model->article_id)); 
        foreach ($mass as $value)
            $model->product_id_s[]=$value->product_id;	
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('ArticleProduct');		
		$es->update();		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Связь статья-продукт успешно отредактирована')));			
	}	
	
	public function actionAjaxDelete($id)
	{		
		if (($model = ArticleProduct::model()->findByPk($id)) !== null)
		{			
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Связь успешно удалена'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить связь'));
			
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить: Связь статья-продукт не найдена'));
	}
	
    public function actionAutoComplete($term) {
    $query = Products::model()->findAllByAttributes(array(),
        $condition  = 'model LIKE :term',
        $params     = array(':term' => $term.'%' ));
 
    foreach($query as $q){
        $data['value'] = $q['id'];
        $data['label'] = $q['model'];

        $list[] = $data;
        unset($data);
    }
    echo json_encode($list);
    }
}