<?php

class SitemapController extends Controller
{
	private $path = '';
	private $sitemaps = array();
	
	const PRIORITY_HOME_PAGE = '1.0';
	const PRIORITY_MAIN_CATEGORY = '0.9';
	const PRIORITY_CATEGORY = '0.8';
	const PRIORITY_PRODUCT = '0.7';
	const PRIORITY_COMPLECT = '0.7';
	const PRIORITY_STATIC = '0.6';
	const PRIORITY_ARTICLE = '0.7';
	const PRIORITY_NEWS = '0.7';
        
	public function setPath($value = null)
	{
		if(empty($value))
			$this->path = Yii::app()->basePath.DIRECTORY_SEPARATOR.'www';
		else
			$this->path = $value;
	}
	
	public function getPath()
	{
		if(empty($this->path))
		{
			$this->setPath();
		}
		return $this->path;
	}
	
	public function getIndexLasMod()
	{
		$lastmod = 'Никогда';
		
		foreach ($this->sitemaps as $key => $sitemap)
		{
			if($sitemap['lastmod'] > 0)
			{
				if($lastmod === 'Никогда')
					$lastmod = $sitemap['lastmod'];
				else
					if($lastmod < $sitemap['lastmod'])
						$lastmod = $sitemap['lastmod'];
			}
		}
		
		return $lastmod === 'Никогда' ? $lastmod : date('d.m.Y в H:i', $lastmod);
	}
	
	public function getTimeArray($source)
	{
		$timeArray = array();
		$matches = explode("T", $source);
		$dateSource = null;
		$timeAndZoneSource = null;
		$timeSource = null;
		$zoneSource = null;
		if(sizeof($matches) > 1)
		{
			$timeAndZoneSource = $matches[1];
		}
		$dateSource = $matches[0];
		if(!empty($timeAndZoneSource))
		{
			preg_match("#(.*)([-+].*)#", $timeAndZoneSource, $matches);
			if(sizeof($matches) > 2)
			{
				$zoneSource = $matches[2];
			}
			$timeSource = $matches[1];
			
			if(!empty($timeSource))
			{
				$matches = preg_split("#[:.]#", $timeSource);
				$timeArray['hour'] = isset($matches[0]) ? (int)$matches[0] : null;
				$timeArray['minute'] = isset($matches[1]) ? (int)$matches[1] : null;
				$timeArray['second'] = isset($matches[2]) ? (int)$matches[2] : null;
				
				if(!empty($zoneSource))
				{
					preg_match("#([+-])(\d{2}):(\d{2})#", $zoneSource, $matches);
					if(sizeof($matches) == 4)
					{
						if($timeArray['hour'] !== null)
						{
							$timeArray['hour'] = $matches[1] === '+' ? $timeArray['hour'] - $matches[2] : $timeArray['hour'] + $matches[2];
							if($timeArray['minute'] !== null)
							{
								$timeArray['minute'] = $matches[1] === '+' ? $timeArray['minute'] - $matches[3] : $timeArray['minute'] + $matches[3];
							}
						}
					}
				}
			}
		}
		if(!empty($dateSource))
		{
			$matches = explode("-", $dateSource);
			$timeArray['year'] = isset($matches[0]) ? (int)$matches[0] : null;
			$timeArray['month'] = isset($matches[1]) ? (int)$matches[1] : null;
			$timeArray['day'] = isset($matches[2]) ? (int)$matches[2] : null;
		}
		return $timeArray;
	}
	
	public function findArray($source, $tagName)
	{
		$matches = array();
		preg_match_all("#<$tagName>([.<:+>\w\s-/)]+?)</$tagName>#", $source, $matches);
		return isset($matches[1]) ? $matches[1] : array();
	}
	
	public function findValue($source, $tagName)
	{
		$matches = array();
		preg_match("#<$tagName>([.<:+>\w\s-/)]+?)</$tagName>#", $source, $matches);
		return isset($matches[1]) ? $matches[1] : null;
	}
	
	public function getTimestamp($timeArray)
	{
		$timestamp = 0;
		if(!empty($timeArray['year']))
			$timestamp = mktime(
					!empty($timeArray['hour']) ? $timeArray['hour'] : 0, //hour
					!empty($timeArray['minute']) ? $timeArray['minute'] : 0, //minute
					!empty($timeArray['second']) ? $timeArray['second'] : 0, //second
					!empty($timeArray['month']) ? $timeArray['month'] : 0, //month
					!empty($timeArray['day']) ? $timeArray['day'] : 0, //day
					$timeArray['year'] //year
					);
		return $timestamp;
	}
	
	public function getSitemaps()
	{
		$sitemaps = array();
		$path = "{$this->getPath()}".DS."sitemap.xml";
		if(file_exists($path))
		{
			$fileContent = file_get_contents($path);
			$sitemapsArray = $this->findArray($fileContent, 'sitemap');
			foreach ($sitemapsArray as $sitemap)
			{
				$loc = $this->findValue($sitemap, 'loc');
				$timeArray = $this->getTimeArray($this->findValue($sitemap, 'lastmod'));
				$lastmod = $this->getTimestamp($timeArray);
				if(!empty($loc))
					array_push($sitemaps, array(
						'loc' => $loc,
						'timeArray' => $timeArray,
						'lastmod' => $lastmod,
					));
			}
		}
		return $sitemaps;
	}
	
	protected function beforeAction($action)
	{
		$this->sitemaps = $this->getSitemaps();
		return parent::beforeAction($action);
	}
	
	public function actionIndex()
	{
		$this->render('index', array(
			'lastMod' => $this->getIndexLasMod(),
		));
	}
	
	public function getLoc($url)
	{
		return "\t\t<loc>$url</loc>\r\n";
	}
	
	public function getPriority($date)
	{
		return "\t\t<priority>$date</priority>\r\n";
	}
	
	public function getUrl($loc, $priority)
	{
		return "\t<url>\r\n{$loc}{$priority}\t</url>\r\n";
	}
	
	public function getUrlSetHeader()
	{
		$urlSetHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";
		$urlSetHeader .= "<urlset\r\n";
		$urlSetHeader .= "\txmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\r\n";
		$urlSetHeader .= "\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n";
		$urlSetHeader .= "\txsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\r\n";
		return $urlSetHeader;
	}
	
	public function getUrlSets($urls)
	{
		$urlSetArray = array();
		$urlSet = $this->getUrlSetHeader();
		$urlCount = 0;
		foreach ($urls as $url)
		{
			$urlSet .= $url;
			$urlCount++;
			if(strlen($urlSet) > 4718592 || $urlCount > 49999) // size bigger than 9MB in UTF8 or count is 50000
			{
				array_push($urlSetArray, $urlSet."</urlset>\r\n");
				$urlSet = $this->getUrlSetHeader();
				$urlCount = 0;
			}
		}
		array_push($urlSetArray, $urlSet."</urlset>\r\n");
		return $urlSetArray;
	}
	
	public function getChildrenUrls($id, $priority = self::PRIORITY_CATEGORY, $toPriority = null)
	{
		$urls = array();
		if($toPriority == null)
		{
			$toPriority = $priority;
		}
		$categoryController = app()->categoryController;
		$categories = $categoryController->getChilds($id);
		foreach ($categories as $category)
		{
			if($categoryController->isInUrl($category['id']))
				array_push($urls, $this->getUrl($this->getLoc($categoryController->getCategoryAddressById($category['id'])), $this->getPriority($priority)));
			$urls = CMap::mergeArray($urls, $this->getChildrenUrls($category['id'], $toPriority));
		}
		return $urls;
	}
	
	public function getSiteindexHeader()
	{
		$siteindexHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";
		$siteindexHeader .= "<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\r\n";
		return $siteindexHeader;
	}
	
	public function getSiteindex($count)
	{
		$siteindex = $this->getSiteindexHeader();
		for($i = 1; $i < $count; $i++)
		{
			$siteindex .= "\t<sitemap>\r\n";
			$siteindex .= "\t\t<loc>".app()->getBaseUrl(true)."/sitemap$i.xml</loc>\r\n";
			$date = date('Y-m-d');
			$time = date('H:i:s');
			$siteindex .= "\t\t<lastmod>{$date}T{$time}+00:00</lastmod>\r\n";
			$siteindex .= "\t</sitemap>\r\n";
		}
		$siteindex .= "</sitemapindex>\r\n";
		return $siteindex;
	}
	
	public function actionGenerate($homePagePriority = self::PRIORITY_HOME_PAGE, $mainCategoriesPriority = self::PRIORITY_MAIN_CATEGORY, $categoriesPriority = self::PRIORITY_CATEGORY, $productsPriority = self::PRIORITY_PRODUCT, $complectsPriority = self::PRIORITY_COMPLECT, $staticPriority = self::PRIORITY_STATIC, $articlePriority = self::PRIORITY_ARTICLE, $newsPriority = self::PRIORITY_NEWS)
	{
		for ($i = 1; $i <= sizeof($this->sitemaps); $i++)
		{
			@unlink("{$this->getPath()}".DS."sitemap$i.xml");
		}
		$categoryController = app()->categoryController;
		$urls = array();
		
		array_push($urls, $this->getUrl($this->getLoc(app()->getBaseUrl(true).'/'), $this->getPriority($homePagePriority)));
		
		/*Catalog categories*/
		$mainCategories = $categoryController->getChilds('catalog');
		foreach ($mainCategories as $mainCategory)
		{
			$priority = $categoriesPriority;
			if($categoryController->isInUrl($mainCategory['id']))
				array_push($urls, $this->getUrl($this->getLoc($categoryController->getCategoryAddressById($mainCategory['id'])), $this->getPriority($mainCategoriesPriority)));
			else
				$priority = $mainCategoriesPriority;
			$urls = CMap::mergeArray($urls, $this->getChildrenUrls($mainCategory['id'], $priority, $categoriesPriority));
		}
		
		/*Other categories*/
		$categories = $categoryController->getChilds(-1);
		foreach ($categories as $category)
		{
			if($category['id'] !== 'catalog')
				$urls = CMap::mergeArray($urls, $this->getChildrenUrls($category['id'], $staticPriority));
		}
		
		/*Products*/
		$products = Products::model()->findAll('hide != 1');
		foreach ($products as $product)
			array_push($urls, $this->getUrl($this->getLoc($product->link), $this->getPriority($productsPriority)));
		
		/*Complects*/
		$complects = Complect::model()->findAll();
		foreach ($complects as $complect)
			array_push($urls, $this->getUrl($this->getLoc($complect->link), $this->getPriority($complectsPriority)));
		/*Articles*/
		$articles = Article::model()->findAll();
		foreach ($articles as $article)
			array_push($urls, $this->getUrl($this->getLoc($article->link), $this->getPriority($articlePriority)));
		
		/*News*/
		$news = News::model()->findAll();
		foreach ($news as $new)
			array_push($urls, $this->getUrl($this->getLoc($new->link), $this->getPriority($newsPriority)));
		
		$urlSetArray = $this->getUrlSets($urls);
		$count = 1;
		foreach ($urlSetArray as $urlSet)
		{
			file_put_contents("{$this->getPath()}".DS."sitemap$count.xml", $urlSet);
			$count++;
		}
		file_put_contents("{$this->getPath()}".DS."sitemap.xml", $this->getSiteindex($count));
		
		$this->redirect('/admin/sitemap');
	}
	
	public function actionUpdate()
	{
		file_put_contents("{$this->getPath()}".DS."sitemap.xml", $this->getSiteindex(sizeof($this->sitemaps) + 1));
		$this->redirect('/admin/sitemap');
	}
}