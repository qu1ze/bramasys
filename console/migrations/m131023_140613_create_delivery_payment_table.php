<?php

class m131023_140613_create_delivery_payment_table extends CDbMigration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `delivery_payment` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `delivery_type_id` int(11) NOT NULL,
                `payment_type_id` int(11) NOT NULL,
                PRIMARY KEY (`id`),
                CONSTRAINT `delivery_payment_delivery_type` FOREIGN KEY (`delivery_type_id`) REFERENCES `delivery_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `delivery_payment_payment_type` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
    }

    public function down()
    {
        $this->dropTable('delivery_payment');
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}