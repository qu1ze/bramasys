<tr class="<?php echo $isComplect ? "complect_$id" : "product_$id"; ?>">
	<td class="product-img-cart">
		<a href="#" class="delete-cart-item" style="margin-top: 25px;" rel="<?php echo $id?>" id="delete_<?php echo $id?>" data-isComplect="<?php echo $isComplect; ?>">
			<img src="/images/cross.gif" height="13" width="12">
		</a>
		<a href="<?php $product->link; ?>" style="height: 72px;">
			<img style="width: 72px;" src="<?php echo $product->image_url;?>">
		</a>
		<div class="product-name-cart">
			<a href="<?php echo $product->link; ?>"><?php echo $product->name; ?></a>
		</div>
	</td>
	<td class="product-price-cart">
		<p><?php echo CurrencySys::exchange($price).' '.CurrencySys::getLabel(); ?></p>
		<?php if (!CurrencySys::isDefaultInCookie()):?>
			<p><?php echo $price.' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY)?></p>
		<?php endif; ?>

	</td>
	<td class="input-cart">
		<input type="text" data-isComplect="<?php echo $isComplect ? '1' : '0'?>" rel="<?php echo $id?>" value="<?php echo $count?>" style="margin-bottom: 0px;">шт.
	</td>
	<td class="checkbox-cart">
		<?php $checked = $checked > 0 ? "checked" : '';?>
		<input type="checkbox" data-isComplect="<?php echo $isComplect ? '1' : '0'?>" rel="<?php echo $id?>" <?php echo $checked;?> style="margin-top: 0px; margin-left: 20px;">
	</td>
	<td class="product-price-cart">
		<p id="firstPrice">
			<?php echo CurrencySys::exchange($amount).' '.CurrencySys::getLabel(); ?>
		</p>
		<?php if (!CurrencySys::isDefaultInCookie()):?>
			<p id="lastPrice"><?php echo $amount.' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY)?></p>
		<?php endif; ?>
	</td>
</tr>