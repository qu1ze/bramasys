<?php
Yii::app()->clientScript->registerScript("load_image", "
    $('.image_url_field').on('change', function(){
        var image_url_field = $('.image_url_field');
        var prod_image = $('.prod_image');
        if (image_url_field.val())
        {
            prod_image.fadeOut('slow', function(){
                prod_image.attr('src', image_url_field.val());
                prod_image.fadeIn();
            });
        }
    });
", CClientScript::POS_READY);

	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'name'); ?>
    <?php echo $form->textFieldRow($model, 'img_url', array('class' => 'image_url_field')); ?>
    <?php echo CHtml::image($model->img_url, '', array('class' => 'prod_image', 'style' => 'margin-bottom:10px; margin-left:180px;'));?>
	<?php echo $form->dropDownListRow($model, 'category_id', $this->getCategories()); ?>
    <?php echo $form->textFieldRow($model, 'actual_id');?>
    <?php echo $form->ckEditorRow($model, 'full_description', array('class'=>'full_desc_field', 'rows'=>5)); ?>
    <?php echo $form->textFieldRow($model, 'discount');?>

	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	
<?php $this->endWidget(); ?>