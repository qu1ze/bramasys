<?php
/**
 * ActiveRecord.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 11/15/12
 * Time: 6:11 PM
 */
class ActiveRecord extends CActiveRecord
{
	private $_gridId;
	private $_formId;
	
	/**
	 * default form ID for the current model. Defaults to get_class()+'-form'
	 */		
	public function setFormId($value)
	{
		$this->_formId = $value;
	}
	
	public function getFormId()
	{
		if(null!==$this->_formId)
			return $this->_formId;
		else {
			$this->_formId = strtolower(get_class($this)).'-form';
			return $this->_formId;
		}
	}

	/**
	 * default grid ID for the current model. Defaults to get_class()+'-grid'
	 */
		
	public function setGridId($value)
	{
		$this->_gridId = $value;
	}
	
	public function getGridId()
	{
		if(null !== $this->_gridId)
			return $this->_gridId;
		else {
			$this->_gridId = strtolower(get_class($this)).'-grid';
			return $this->_gridId;
		}
	}

	
	
}