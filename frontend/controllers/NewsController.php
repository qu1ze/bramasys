<?php

class NewsController extends StaticController
{		
	public static function getTag()
	{
		return 'static';
	}
	
	public function actionIndex()
	{
		$model = new News('search');
		
		$list = News::model()->findAll(array('order' => 'id DESC', 'limit' => 10));
		
		$criteria = new CDbCriteria;		
		$dataProvider = new CActiveDataProvider(new News, array(
			'criteria' => $criteria
		));
				
        $this->render('index', array(
			'model' => $model,
			'list' => $list,
			'dataProvider' => $dataProvider,
		));   
		
	}
	
	public function actionView($name)
	{
		if (($model = News::model()->findByAttributes(array('name' => $name))) === null)
			throw new CHttpException(404, 'Oops!');
		
		$model->views += 1;
		if (!$model->save())
			Yii::app()->user->setFlash('error', 'Saving error!');
		
		$related_products = CHtml::listData(NewsProduct::model()->findAllByAttributes(array('news_id' => $model->id)), 'id', 'product_id');
		$related_news = RelatedNews::model()->findAllByAttributes(array('news_id' => $model->id));
		$this->render('view', array(
			'model' => $model,
			'related_products' => $related_products,
			'related_news' => $related_news,
		));
	}		
}
