
<div id="header_middle_center">
    <div id="logo">
		<?php if ($_SERVER["REQUEST_URI"] != "/"): ?>
			<a href='<?php echo app()->getBaseUrl(true); ?>'></a>
		<?php endif; ?>
    </div>

    <div id="quick_contacts">
		<div class="city-phones">
			<p>+380 (56) 785-69-44</p><p class="black"> – Днепропетровск</p>
			<p>+380 (61) 701-19-92</p><p class="black"> – Запорожье</p>
			<p>+380 (44) 355-00-90</p><p class="black"> – Киев</p>
		</div>
		<div class="city-phones">
			<p>+380 (50) 940-05-09</p><p class="black"> – MTC<!--<img src="<?php /*echo app()->getBaseUrl(true); */?>/images/phoneLogos/mts_logo_lte-org-ua.png">--></p>
			<p>+380 (93) 098-89-08</p><p class="black"> – Life:)<!--<img src="<?php /*echo app()->getBaseUrl(true); */?>/images/phoneLogos/logo-life.png">--></p>
			<p>+380 (96) 631-63-63</p><p class="black"> – Киевстар<!--<img src="<?php /*echo app()->getBaseUrl(true); */?>/images/phoneLogos/logo-kievstar.png">--></p>
		</div>
        <div id="call_order">
            или <a class="callback-bnt" href="#">заказать обратный звонок</a>
			<?php $this->widget('common.components.Callback'); ?>
        </div>
    </div>

    <div id="calculator"></div>

    <div id="cart_and_curency">
		<select name="curr" class="curency">
		<?php $select = empty(Yii::app()->request->cookies['currency']->value) ? 0 : Yii::app()->request->cookies['currency']->value; ?>
			<option <?php if($select == 0) echo 'selected="selected"'; ?> value="0">USD</option>
		<?php 
		Yii::app()->clientScript->registerScript("changeCur", "
			$('body').on('change', '.curency', function() {
				var url = '/site/setCurrency';
				$.ajax(
				{
					url: url,
					data: {id: $('.curency').val()},
					dataType: 'json',
					type: 'get',
					success: function(data){
						if(typeof(data) == 'object' && data.title == 'Success')
							window.location.reload();
					}
				});
			});
		", CClientScript::POS_READY);
		$currencies = Currency::model()->findAll();
		foreach($currencies as $currency):?>
			<option <?php if($select == $currency->id) echo 'selected="selected"'; ?> value="<?php echo $currency->id; ?>"><?php echo $currency->name; ?></option>
		<?php endforeach; ?>
		</select>
		<?php $this->widget('common.components.CartWidget'); ?>
    </div>
</div>

<div id="header_middle_bottom">

	<?php if ($_SERVER["REQUEST_URI"] == "/"): ?>
	    <div id="catalog">
	        <div class="red_arrow"></div>
	        <p>Каталог товаров</p>
                <div id="catalog-categories">
                    <?php app()->categoryController->renderCatalogTree();?>
                </div>
	    </div>
	<?php else: ?>
	    <div id="catalog">
	        <div class="red_arrow"></div>
	        <p>Каталог товаров</p>
                <div id="catalog-categories">
                    <?php app()->categoryController->renderCatalogTree();?>
                </div>
	    </div>
	<?php endif; ?>
<ul class="middle-menu">
    <li>
        <a title="О компании" class="first" href="<?php echo app()->getBaseUrl(true); ?>/company">О компании</a>
        <?php
		app()->categoryController->renderMenu(Yii::app()->categoryController->getCategoryId('company'));
		?>
    </li>
    <li>
        <a title="Услуги" class="second" href="<?php echo app()->getBaseUrl(true); ?>/uslugi">Услуги</a>
        <?php
		app()->categoryController->renderMenu(Yii::app()->categoryController->getCategoryId('uslugi'));
		?>
    </li>
    <li>
        <a title="Техподдержка" class="third" href="<?php echo app()->getBaseUrl(true); ?>/support">Техподдержка</a>
        <?php
		app()->categoryController->renderMenu(Yii::app()->categoryController->getCategoryId('support'));
		?>
    </li>
    <li>
        <a title="Контакты" class="single" href="<?php echo app()->getBaseUrl(true); ?>/kontakty">Контакты</a>
    </li>
</ul>

<?php 

Yii::app()->clientScript->registerScript("ajax-button", "
		$('body').on('click', 'a.ajaxbutton', function() {
			var fullhref = $(this).attr('href');
			var button = $(this);
			var href = fullhref.split('#:',2);
                        if(href[0].indexOf('?')>-1)
                           var url = href[0] + '&' + href[1] + '&ajax=true';
                        else
                           var url = href[0] + '?' + href[1] + '&ajax=true';
                        ajaxreqest(url,href[1]);
                        return false;
		});
	", CClientScript::POS_READY);



	Yii::app()->clientScript->registerScript("addToBookmarks", "
		$('.add-bookmark').on('click', function() {
			var url = '';
			var button = $(this);
			if($(button).attr('data-isComplect') == 1)
				url = '/complect/ajaxAddToBookmarks';
			else
				url = '/products/ajaxAddToBookmarks';
			$.get(url + '?id=' + $(button).attr('mark-id'), function(data) {
				if(data == 'Success')
				{
					$(button).text('Уже в закладках');
					$(button).attr('href', '".app()->getBaseUrl(true)."/user/profile/bookmarks');
				}
			  });
		});
	", CClientScript::POS_READY);

	Yii::app()->clientScript->registerScript("setFilter", "
		function SetFilter(pathname)
		{
                       
                        var search = $('#search').serialize();
			var getRequest = '';
			var filterGetRequest = $('#filters').serialize();
			if(filterGetRequest && !pathname)
			{
				getRequest = filterGetRequest;
			}
			if(!pathname)
				pathname = window.location.pathname;
			var parts = getRequest.split('&');

			getRequest = '';
			var brands = [];
			parts.forEach(function (value, key) {
				if(value.charAt(value.length - 1) != '=')
				{
					if(value.substring(0, 5) == 'brand')
						brands.push(parts[key]);
					getRequest += parts[key] + '&';
				}
			});
			if(window.location.hash.length > 2)
			{
				var getQuery = window.location.hash;
				getQuery = getQuery.substr(2, getQuery.length);
				parts = getQuery.split('&');
				parts.forEach(function (value, key) {
				if(value.charAt(value.length - 1) != '=')
				{
					subPart = value.split('=');
					if(subPart[0] == 'sort' || subPart[0] == 'limit')
						getRequest += parts[key] + '&';
				}
			});
			}
                        
			parts = pathname.split('/');
			var endPart = parts[parts.length - 1];
			if(brands.length == 0 && $('input:[brand-name=\"' + endPart + '\"]').length > 0)
			{
				pathname = '';
				parts.forEach(function (value, key) {
					if(value != endPart && value != '')
					{
						pathname += '/' + value;
					}
				});
			}
            
                                var brandname=[];
                                $('.brand_list li input').each(function (kay, value) {
                                  brandname.push($(value).attr('brand-name'));
                                });
				pathname = '';
				parts.forEach(function (value, key) {
					if(value != '' && brandname.indexOf(value) == -1)
					{
						pathname += '/' + value;
                                               
                                                
					}
				});
			
                        if(brands.length == 1)
                        {
                            var id = brands[0].split('=');
                            pathname += '/' + $('#brnd'+id[1]).attr('brand-name');
                            var start = getRequest.indexOf(brands[0]);
                            var end = start + brands[0].length+1;
                            
                            getRequest = getRequest.substr(0,start)+getRequest.substr(end);
			
                        }
			getRequest = getRequest.substr(0, getRequest.length - 1);
			
                        if (search.length>7) {
                          pathname += '?' + search;
                        };
                        var oldpathname = pathname;
                        if(getRequest.length != 0){
                                var url = pathname + '?' + decodeURI(getRequest)+'&ajax=true';
                                pathname += '#:' + decodeURI(getRequest);
                                }
                        else 
                                var url = pathname+'?ajax=true'; 
			if (window.location.pathname+window.location.search != oldpathname){
                           window.location = pathname;
                           }
                        else
                        {
                           ajaxreqest(url,decodeURI(getRequest));
                           }
		}
	", CClientScript::POS_END);

	Yii::app()->clientScript->registerScript("search", "
		$(document).ready(function() {
			$('#search-input').autocomplete({
				source: '/products/getBrandsOrModels',
				minLength: 3,
			});
		});
		$('#btnsearch.searchBtn').on('click', function() {
			SetFilter('/products');
		});
		
		$('#search-input').on('focus', function() {
			window.setTimeout (function(){ 
			   $('#search-input').select(); 
			},100);
		});
	", CClientScript::POS_READY);
?>
    <form id="search" action="/products">
        <input type="text" placeholder="Введите текст" id="search-input" name="search" value="<?php echo isset($_GET['search']) ? $_GET['search'] : ''; ?>">
        <input type="button" value="Найти" class="searchBtn" id="btnsearch">
    </form>

</div>