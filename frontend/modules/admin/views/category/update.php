<?php
	Yii::app()->clientScript->registerScript("parent_tag", "
		$('.parent_field').on('change', function(){
			if ($(this).val())
				$('.tag_container').fadeOut();
			else
				$('.tag_container').fadeIn();
		});
		
		$('.tag_field').on('change', function(){
			if ($(this).val())
				$('.parent_container').fadeOut();
			else
				$('.parent_container').fadeIn();
		});
	", CClientScript::POS_READY);
		
	Yii::app()->clientScript->registerScript("load_image", "
        $('.image_url_field').on('change', function(){
            var image_url_field = $('.image_url_field');
            var prod_image = $('.prod_image');
            if (image_url_field.val())
            {
                prod_image.fadeOut('slow', function(){
                    prod_image.attr('src', image_url_field.val());
                    prod_image.fadeIn();
                });
            }
        });
    ", CClientScript::POS_READY);
?>
<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'name');?>
	<?php echo $form->textFieldRow($model, 'title');?>
	<?php echo $form->textFieldRow($model, 'order_number');?>
	<?php echo $form->checkBoxRow($model, 'in_url'); ?>
	<?php echo $form->textFieldRow($model, 'H1');?>
	<?php echo $form->textFieldRow($model, 'product_title');?>
	<?php echo $form->textFieldRow($model, 'product_description');?>
	<?php echo $form->textFieldRow($model, 'product_keywords');?>
	<?php echo $form->textFieldRow($model, 'image_url', array('class' => 'image_url_field')); ?>
	<?php echo CHtml::image($model->image_url, '', array('class' => 'prod_image', 'style' => 'margin-bottom:10px; margin-left:180px;'));?>
	
	<div class="parent_container" style="display: <?php echo $model->tag ? 'none' : 'block' ?>">
		<?php $htmlOptions['encode'] = false;
        echo $form->dropDownListRow($model , 'parent', $list, array('empty' => '', 'encode'=>false, 'class' => 'parent_field'));?>
	</div>
	
	<div class="tag_container" style="display: <?php echo $model->parent ? 'none' : 'block' ?>">
		<?php echo $form->dropDownListRow($model , 'tag', CategoryTag::getListData(), array('empty' => '', 'class' => 'tag_field'));?>
	</div>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'type' => 'danger',
			'size' => 'normal',
			'label' => 'Удалить',
			'url' => Yii::app()->createUrl('/admin/category/delete', array('id' => $model->id))
		)); 
	?>
	
	
<?php $this->endWidget(); ?>

