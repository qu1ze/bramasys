<?php
Yii::app()->getClientScript()->registerCssFile('/css/filter.css');
Yii::app()->getClientScript()->registerCssFile('/css/rating.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/jquery.jcarousel.js");
?>
<div class="compare">
	<h1 class="title">Сравнение товаров</h1>
	<img src="/images/compare.png" alt="" title="">               
    <?php if(!empty($_COOKIE['lastCompare'])) $catlinck=$_COOKIE['lastCompare']; 
    else 
        $catlinck=Yii::app()->categoryController->getCategoryAddressById($this->categoryId);
    
        ?>
    
	<a class="add-to-compare" href="<?php echo $catlinck ?>" >Назад в каталог</a>	
<?php 
if ($empty): ?> 
    <p>Список сравнения пуст</p>
<?php endif;
foreach ($categorySpecsValues as $categoryId => $productSpecsValues):
Yii::app()->clientScript->registerScript("carouselInit$categoryId", "
$(document).ready(function() {
	$('#compareProductsCarousel$categoryId').jcarousel({
	});
	
	$('.compareCarousel-control$categoryId.left').on('click', function(){
		$('#compareProductsCarousel$categoryId').jcarousel('scroll', '-=1');
		return false;
	});
	
	$('.compareCarousel-control$categoryId.right').on('click', function(){
		$('#compareProductsCarousel$categoryId').jcarousel('scroll', '+=1');
		return false;
	});
});

$(document).ready(function() {
	$('#compareSpecsCarousel$categoryId').jcarousel({
	});
	
	$('body .compareCarousel-control$categoryId.left').on('click', function(){
		$('#compareSpecsCarousel$categoryId').jcarousel('scroll', '-=1');
		return false;
	});
	
	$('body .compareCarousel-control$categoryId.right').on('click', function(){
		$('#compareSpecsCarousel$categoryId').jcarousel('scroll', '+=1');
		return false;
	});
});
", CClientScript::POS_HEAD);

Yii::app()->clientScript->registerScript("specsInit$categoryId", "
	var count = $('#compare$categoryId .leftTable tr').length;
	for(var i = 0; i < count; i++)
	{
		var max = 0;
		$('#compare$categoryId .spec' + i).each(function(key, spec)
		{
			var height = $(spec).css('height').replace(/[^-\d\.]/g, '');;
			if(max < height)
			{
				max = height;
			}
		});
		$('#compare$categoryId .spec' + i).css('height', max);
	}
", CClientScript::POS_LOAD);
?>	
<div>
	<table class="topTable">
		<tr>
			<td>
				<table class="leftTable">
					<tr><td id="productTitle">Товар</td></tr>
					<tr><td id="productRating">Оценка</td></tr>
					<tr><td id="productPrice">Цена</td></tr>
				</table>
			</td>
			<td style="position: relative;">
				<div id="compareProductsCarousel<?php echo $categoryId;?>" catId="<?php echo $categoryId;?>" class="compareCarousel rightTable show">
					<ul>
						<?php
						$count = 0;
						foreach ($productSpecsValues as $productId => $specValues)
						{
							$this->renderPartial('_product', array(
								'product' => Products::model()->findByPk($productId),
								'count' => ++$count,
							));
						}
						?>
					</ul>
					<a class="compareCarousel-control<?php echo $categoryId;?> carousel-control left" href="#" style="position: absolute; top: 50%; right: 0px;">
						<img alt="" src="/images/pageListLeft.png" height="24" width="15">
					</a>
					<a class="compareCarousel-control<?php echo $categoryId;?> carousel-control right" href="#" style="position: absolute; top: 50%; right: 0px;">
						<img alt="" src="/images/pageListRight.png" height="24" width="15">
					</a>
				</div>
			</td>
		</tr>
		<tr class="title">
			<td colspan="4">Характеристики</td>
		</tr>
	</table>

	<table class="bottomTable" id="compare<?php echo $categoryId;?>">
		<tr>
			<td>
				<table class="leftTable">
					<?php
					$isTransporent = true;
					$count = 0;
					foreach (end($productSpecsValues) as $specId => $values):
						$classes = !$isTransporent ? 'table_color' : '';
						$classes .= " spec$count";
					?>
						<tr <?php echo "class='$classes'"; ?>>
							<td>
								<?php echo $specifications[$specId]; ?>
								<a class="question" spec-id="<?php echo $specId; ?>" href="#">
									<img src="/images/questionitem.png">
								</a>
							</td>
						</tr>
					<?php
					$count++;
					$isTransporent = !$isTransporent;
					endforeach;
					?>
				</table>
			</td>
			<td class="rightTable">
				<div id="compareSpecsCarousel<?php echo $categoryId;?>">
					<ul>
						<?php 
						foreach ($productSpecsValues as $productId => $specsValues):?>
						<li class="compareProduct compareCarouselItem pd<?php echo $productId; ?>">
						<?php
							$this->renderPartial('_product_spec', array(
								'specsValues' => $specsValues,
								'productId' => $productId,
							));
						?>
						</li>
						<?php endforeach;?>
					</ul>
				</div>
			</td>
		</tr>
	</table>
</div>
<?php endforeach; ?>	
</div>
