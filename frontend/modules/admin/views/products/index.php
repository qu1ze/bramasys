<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
));
$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'label' => 'Удалить', 'type' => 'danger', 'htmlOptions' => array('id' => 'deleteRows')));
?>
<div class="btnGroup">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'label' => 'Показать', 'type' => 'success', 'htmlOptions' => array('class' => 'clearLabel', 'label-name' => 'hide')));
    $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'label' => 'Скрыть', 'type' => 'danger', 'htmlOptions' => array('class' => 'setLabel', 'label-name' => 'hide')));
    ?>
</div>
<div class="btnGroup">
<?php
    $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'label' => 'Дать мекту Новинка', 'type' => 'success', 'htmlOptions' => array('class' => 'setLabel', 'label-name' => 'novelty')));
    $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'label' => 'Убрать мекту Новинка', 'type' => 'danger', 'htmlOptions' => array('class' => 'clearLabel', 'label-name' => 'novelty')));
?>
</div>
<div class="btnGroup">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'label' => 'Дать мекту Цена недели', 'type' => 'success', 'htmlOptions' => array('class' => 'setLabel', 'label-name' => 'price_of_the_week')));
    $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'label' => 'Убрать мекту Цена недели', 'type' => 'danger', 'htmlOptions' => array('class' => 'clearLabel', 'label-name' => 'price_of_the_week')));
    ?>
</div>
<div class="btnGroup">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'label' => 'Дать мекту Лидер продаж', 'type' => 'success', 'htmlOptions' => array('class' => 'setLabel', 'label-name' => 'top_seller')));
    $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'label' => 'Убрать мекту Лидер продаж', 'type' => 'danger', 'htmlOptions' => array('class' => 'clearLabel', 'label-name' => 'top_seller')));
    ?>
</div>
<?php
$this->endWidget();
Yii::app()->clientScript->registerScript("deleteRows", "
    $('#deleteRows').on('click', function(){
        if($('#products-table .selected').length > 0)
        {
            if(!confirm('Вы уверенны что хотите удалить?')) return false;
            ids = [];
            $('#products-table .selected').each(function(){
                ids.push($($(this).children('td').children('a')[0]).attr('data-pk'));
            });
            $.ajax({
                url: '/admin/products/ajaxDeleteRows',
                dataType: 'json',
                data: {
                    ids: ids
                },
                success: function(data) {
                    if(typeof(data) == 'object')
                    {
                        $.fn.yiiGridView.update('products-table');
                        brama.ui.notify(data.title, data.msg, data.sticky);
                    }
                }
            });
        }
    });
", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript("setLabel", "
    $('.setLabel').on('click', function(){
        if($('#products-table .selected').length > 0)
        {
            ids = [];
            $('#products-table .selected').each(function(){
                ids.push($($(this).children('td').children('a')[0]).attr('data-pk'));
            });
            $.ajax({
                url: '/admin/products/ajaxSetLabel',
                dataType: 'json',
                data: {
                    ids: ids,
                    labelName: $(this).attr('label-name'),
                },
                success: function(data) {
                    if(typeof(data) == 'object')
                    {
                        $.fn.yiiGridView.update('products-table');
                        brama.ui.notify(data.title, data.msg, data.sticky);
                    }
                }
            });
        }
    });
", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript("clearLabel", "
    $('.clearLabel').on('click', function(){
        if($('#products-table .selected').length > 0)
        {
            ids = [];
            $('#products-table .selected').each(function(){
                ids.push($($(this).children('td').children('a')[0]).attr('data-pk'));
            });
            $.ajax({
                url: '/admin/products/ajaxSetLabel',
                dataType: 'json',
                data: {
                    ids: ids,
                    labelName: $(this).attr('label-name'),
                    value: 0,
                },
                success: function(data) {
                    if(typeof(data) == 'object')
                    {
                        $.fn.yiiGridView.update('products-table');
                        brama.ui.notify(data.title, data.msg, data.sticky);
                    }
                }
            });
        }
    });
", CClientScript::POS_READY);

$grid_id = 'products-table';

$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $grid_id,
	'type' => 'bordered striped',
	'dataProvider' => $model->search(),
    'selectableRows' => 2,
	'filter' => $model,	
	'afterAjaxUpdate' => 'js:function(){
		jQuery(".tooltip").remove();
		jQuery("a[rel=tooltip]").tooltip();				
	}',
	'template' => "{items}\n{pager}",
	'columns' => array(
        array(
            'class' => 'bootstrap.widgets.TbDataColumn',
            'name' => 'id',
        ),
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'brand',
			'filter' => Brand::getListData(),
			'editable' => array(
				'type' => 'select',
				'source' => Brand::getListData(true),
				'url' => app()->createUrl('/admin/products/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
                                                brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),	
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'model',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/products/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
        array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'description',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/products/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			),
			'htmlOptions' => array('style' => '
				overflow: hidden;
				max-width: 150px;
				width: 150px;
				text-overflow: ellipsis;
				white-space: nowrap;
			'),
		),
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'old_price',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/products/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
        array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'price',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/products/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'category',
			'filter' => Category::getListData(),
			'editable' => array(
				'type' => 'select', 
				'source' => Category::getListData(true),
				'url' => app()->createUrl('/admin/products/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'quantity',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/products/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
		array(
			'class'=>'bootstrap.widgets.TbToggleColumn',
			'toggleAction'=>'/admin/products/toggle',
			'checkedButtonLabel' => 'Да',     
			'uncheckedButtonLabel' => 'Нет',
			'filter' => CHtml::listData( array(
				array(
					'id' => 0,
					'title' => 'Нет'
				),
				array(
					'id' => 1,
					'title' => 'Да'
				),
			), 'id', 'title' ),
			'name' => 'price_of_the_week',
			'htmlOptions' => array('style' => '
				width: 70px;
				max-width: 70px;
			'),
		),
		array(
			'class'=>'bootstrap.widgets.TbToggleColumn',
			'toggleAction'=>'/admin/products/toggle',
			'checkedButtonLabel' => 'Да',     
			'uncheckedButtonLabel' => 'Нет',
			'filter' => CHtml::listData( array(
				array(
					'id' => 0,
					'title' => 'Нет'
				),
				array(
					'id' => 1,
					'title' => 'Да'
				),
			), 'id', 'title' ),
			'name' => 'top_seller',
			'htmlOptions' => array('style' => '
				width: 70px;
				max-width: 70px;
			'),
		),
		array(
			'class'=>'bootstrap.widgets.TbToggleColumn',
			'toggleAction'=>'/admin/products/toggle',
			'checkedButtonLabel' => 'Да',     
			'uncheckedButtonLabel' => 'Нет',
			'filter' => CHtml::listData( array(
				array(
					'id' => 0,
					'title' => 'Нет'
				),
				array(
					'id' => 1,
					'title' => 'Да'
				),
			), 'id', 'title' ),
			'name' => 'novelty',
			'htmlOptions' => array('style' => '
				width: 70px;
				max-width: 70px;
			'),
		),
		array(
			'class'=>'bootstrap.widgets.TbToggleColumn',
			'toggleAction'=>'/admin/products/toggle',
			'checkedButtonLabel' => 'Да',     
			'uncheckedButtonLabel' => 'Нет',
			'filter' => CHtml::listData( array(
				array(
					'id' => 0,
					'title' => 'Нет'
				),
				array(
					'id' => 1,
					'title' => 'Да'
				),
			), 'id', 'title' ),
			'name' => 'hide',
			'htmlOptions' => array('style' => '
				width: 70px;
				max-width: 70px;
			'),
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('nowrap' => 'nowrap'),			
			'template' => '{delete} {update} {setSpec} {attachVideo} {attachSupport} {productPage}',
			'buttons'=>array
			(
				'setSpec' => array
				(
					'label'=>'Задать хар-ки продукта',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/admin/specs.png',
					'url'=>'Yii::app()->createUrl("admin/products/setSpec", array("id"=>$data->primaryKey, "page" => isset($_GET["Products_page"]) ? $_GET["Products_page"] : "index"))',
				),
				'productPage' => array
				(
					'label'=>'Страница продукта',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/admin/product.png',
					'url'=>'Yii::app()->createUrl("/{$data->brand0->name}/$data->model/$data->primaryKey")',
				),
				'attachVideo' => array
				(
					'label'=>'Прикрепить видео',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/admin/video.png',
					'url'=>'Yii::app()->createUrl("admin/products/attachVideo", array("id"=>$data->primaryKey, "page" => isset($_GET["Products_page"]) ? $_GET["Products_page"] : "index"))',
				),
				'attachSupport' => array
				(
					'label'=>'Прикрепить файл',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/admin/file.png',
					'url'=>'Yii::app()->createUrl("admin/products/setSupport", array("id"=>$data->primaryKey, "page" => isset($_GET["Products_page"]) ? $_GET["Products_page"] : "index"))',
				),
			),
			'updateButtonUrl'=>'Yii::app()->controller->createUrl("update", array("id" => $data->primaryKey, "page" => isset($_GET["Products_page"]) ? $_GET["Products_page"] : "index"))',
			'deleteButtonUrl'=>'Yii::app()->controller->createUrl("ajaxDelete", array("id" => $data->primaryKey))',
			'deleteConfirmation' => 'Вы уверенны что хотите удалить?',
			'afterDelete' => 'function(link,success,data){
				if(success){data = $.parseJSON(data);brama.ui.notify(data.title, data.msg, false);}
				else{brama.ui.notify("Oops!", data.responseText )};
			}',
		)
	)
));

?>