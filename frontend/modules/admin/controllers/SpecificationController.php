<?php

class SpecificationController extends Controller
{		
	public function actionCreate()
	{
		$model = new Specification; 
				
		if (isset($_POST['Specification']))
		{			
			$model->attributes = $_POST['Specification'];
			if ($model->save())
				$this->redirect(array('/admin/specification'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new Specification();	

		if (isset($_POST['Specification']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['Specification'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Хар-ка успешно создана'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно удалить хар-ку'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new Specification('search');
		
		if (isset($_GET['Specification']))
			$model->attributes = $_GET['Specification'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = Specification::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/specification'));				
				
		if (isset($_POST['Specification']))
		{			
			$model->attributes = $_POST['Specification'];
			if ($model->save())
				$this->redirect(array('/admin/specification'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Specification');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Хар-ка успешно отредактирована')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
			if (($categories = CategorySpec::model()->findByAttributes(array('spec_id' => $id))) === null)
			{
				CategoryTag::model()->deleteByPk( $id );					
				Yii::app()->user->setFlash('success', "Хар-ка успешно удалена");
			}
			else
				Yii::app()->user->setFlash('error', "Существует связь с категорией");
		}
		
		$this->redirect(array('/admin/specification'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = CategoryTag::model()->findByPk($id)) !== null)
		{
			if (($categories = CategorySpec::model()->findByAttributes(array('spec_id' => $id))) === null)
				if ($model->delete())
					echo json_encode(array('title' => 'Success', 'msg' => 'Хар-ка успешно удалена'));
				else
					echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить хар-ку'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с категорией'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Хар-ка не найдена'));
	}
	
}