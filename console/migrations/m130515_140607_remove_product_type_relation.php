<?php

class m130515_140607_remove_product_type_relation extends CDbMigration
{
	public function up()
	{
            $this->execute("ALTER TABLE  `products` DROP FOREIGN KEY  `products_ibfk_3`;
                ALTER TABLE `products` DROP `product_type_id`");
	}

	public function down()
	{
            $this->execute("
                ALTER TABLE  `products` ADD  `product_type_id` INT NOT NULL ,
                ADD INDEX (  `product_type_id` )
            ");
            $this->execute("
                ALTER TABLE  `products` ADD CONSTRAINT `products_ibfk_3` FOREIGN KEY (  `product_type_id` ) REFERENCES  `product_type` (
                `id`
                ) ON DELETE RESTRICT ON UPDATE RESTRICT ;
            ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}