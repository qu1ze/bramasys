<?php

class m130501_134611_fix_table_relations extends CDbMigration
{
	// public function up()
	// {
	// }

	// public function down()
	// {
		// echo "m130501_134611_fix_table_relations does not support migration down.\n";
		// return false;
	// }

	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->execute("ALTER TABLE  `category` CHANGE  `parent`  `parent` INT( 11 ) NULL DEFAULT NULL");
		$this->execute("
			ALTER TABLE  `category` ADD FOREIGN KEY (  `parent` ) REFERENCES  `category` (
			`id`
			) ON DELETE RESTRICT ON UPDATE RESTRICT ;

			ALTER TABLE  `category` ADD FOREIGN KEY (  `tag` ) REFERENCES  `category_tag` (
			`id`
			) ON DELETE RESTRICT ON UPDATE RESTRICT ;

		");
	}

	public function safeDown()
	{
		$this->execute("
			ALTER TABLE  `category` DROP FOREIGN KEY  `category_ibfk_1` ;
			ALTER TABLE  `category` DROP FOREIGN KEY  `category_ibfk_2` ;
		");
	}
	
}