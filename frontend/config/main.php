<?php
/**
 * main.php
 *
 * This file holds frontend configuration settings.
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 5:48 PM
 */
defined('YII_DEBUG') or define('YII_DEBUG', false);
$frontendConfigDir = dirname(__FILE__);

$root = $frontendConfigDir . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..';

$params = require_once($frontendConfigDir . DIRECTORY_SEPARATOR . 'params.php');

// Setup some default path aliases. These alias may vary from projects.
Yii::setPathOfAlias('root', $root);
Yii::setPathOfAlias('common', $root . DIRECTORY_SEPARATOR . 'common');
Yii::setPathOfAlias('commonExt', $root . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'extensions');
Yii::setPathOfAlias('frontend', $root . DIRECTORY_SEPARATOR . 'frontend');
Yii::setPathOfAlias('www', $root. DIRECTORY_SEPARATOR . 'frontend' . DIRECTORY_SEPARATOR . 'www');

$mainLocalFile = $frontendConfigDir . DIRECTORY_SEPARATOR . 'main-local.php';
$mainLocalConfiguration = file_exists($mainLocalFile)? require($mainLocalFile): array();

$mainEnvFile = $frontendConfigDir . DIRECTORY_SEPARATOR . 'main-env.php';
$mainEnvConfiguration = file_exists($mainEnvFile) ? require($mainEnvFile) : array();

return CMap::mergeArray(
	array(
		// @see http://www.yiiframework.com/doc/api/1.1/CApplication#basePath-detail
		'basePath' => 'frontend',
		// set parameters
		'params' => $params,
		// preload components required before running applications
		// @see http://www.yiiframework.com/doc/api/1.1/CModule#preload-detail
		'preload' => array('bootstrap', 'log'),
		// @see http://www.yiiframework.com/doc/api/1.1/CApplication#language-detail
		'language' => 'ru',
		// uncomment if a theme is used
		/*'theme' => '',*/
		// setup import paths aliases
		// @see http://www.yiiframework.com/doc/api/1.1/YiiBase#import-detail
		'import' => array(
			'common.components.*',
			'common.extensions.*',
			'common.models.*',
			'frontend.modules.user.components.ClientIdentity',
			// uncomment if behaviors are required
			// you can also import a specific one
			/* 'common.extensions.behaviors.*', */
			// uncomment if validators on common folder are required
			/* 'common.extensions.validators.*', */
			'application.components.*',
			'application.controllers.*',
			'application.models.*',
			'common.extensions.image.*',
			'common.extensions.gallerymanager.*',
			'common.extensions.gallerymanager.models.*',
            'common.extensions.eoauth.*',
            'common.extensions.eoauth.lib.*',
            'common.extensions.lightopenid.*',
            'common.extensions.eauth.services.*',
		),
		/* uncomment and set if required */
		// @see http://www.yiiframework.com/doc/api/1.1/CModule#setModules-detail
		'modules' => array(
			'rss',
			'admin' => array(
				'defaultController' => 'Default'
			),
			'user' => array(
				'defaultController' => 'Profile'
			),
			
			/*'gii' => array(
				'class' => 'system.gii.GiiModule',
				'password' => 'clevertech',
				'ipFilters'=> array('localhost', '81.90.239.126'),
				'generatorPaths' => array(
					'bootstrap.gii'
				)
			)*/	
		),
		'components' => array(
                    'cache.core' => extension_loaded('apc') ?
                        array(
                            'class' => 'CApcCache',
                        ) :
                        array(
                            'class' => 'CDbCache',
                            'connectionID' => 'db',
                            'autoCreateCacheTable' => true,
                            'cacheTableName' => 'cache',
                        ),
                    'cache.content' => array(
                        'class' => 'CDbCache',
                        'connectionID' => 'db',
                        'autoCreateCacheTable' => true,
                        'cacheTableName' => 'cache',
                    ),
                        'widgetFactory' => array(
                           'class' => 'CWidgetFactory',
                           'widgets' => array(
                               'GalleryManager'=>array(
                               'controllerRoute' => '/admin/gallery',
                                ) ,
                           )
                        ),
			'errorHandler' => array(
				// @see http://www.yiiframework.com/doc/api/1.1/CErrorHandler#errorAction-detail
				'errorAction'=>'site/error'
			),
			'db' => array(
				'connectionString' => $params['db.connectionString'],
				'username' => $params['db.username'],
				'password' => $params['db.password'],
				'schemaCachingDuration' => YII_DEBUG ? 0 : 86400000, // 1000 days
				'enableParamLogging' => YII_DEBUG,
				'charset' => 'utf8'
			),
			'urlManager' => array(
				'urlFormat' => 'path',
				'showScriptName' => false,
				'urlSuffix' => '/',
				'rules' => $params['url.rules']
			),
			
			'user' => array(				
				'allowAutoLogin'=>true,
				'returnUrl' => '/admin'
			),
			
			'bootstrap' => array(
				'class' => 'common.extensions.bootstrap.components.Bootstrap',
				'responsiveCss' => true,
			),
			'categoryController'=>array('class'=>'AppCategoryController'),
			/* make sure you have your cache set correctly before uncommenting */
			/* 'cache' => $params['cache.core'], */
			/* 'contentCache' => $params['cache.content'] */
//                        'log'=>array(
//                            'class'=>'CLogRouter',
//                            'routes'=>array(
//                              array(
//                             'class'=>'CWebLogRoute',  'levels'=>'trace, info, error, warning',
//                              ),
//                              array(
//                             'class'=>'CFileLogRoute',  'levels'=>'trace, info, error, warning',
//                              ),
//                            )
//                         ),
                        'image'=>array(
                            'class'=>'common.extensions.image.CImageComponent',
                            // GD or ImageMagick
                            'driver'=>'GD',
//                            // ImageMagick setup path
//                            'params'=>array('directory'=>'D:/Program Files/ImageMagick-6.4.8-Q16'),
                        ),
            'ePdf' => array(
                'class'         => 'common.extensions.yii-pdf.EYiiPdf',
                'params'        => array(
                    'mpdf'     => array(
                        'librarySourcePath' => 'common.extensions.mpdf.*',
                        'constants'         => array(
                            '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                        ),
                        'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
                    ),
                ),
             ),
            'loid' => array(
                'class' => 'commonExt.lightopenid.loid',
            ),
            'eauth' => array(
                'class' => 'commonExt.eauth.EAuth',
                'popup' => true, // Use the popup window instead of redirecting.
                'services' => array( // You can change the providers and their classes.
                    'google_oauth' => array(
                        // register your app here: https://code.google.com/apis/console/
                        'class' => 'CustomGoogleService',
                        'client_id' => '566786702787-u7347edb571skc9f4pcauobqi6ha278p.apps.googleusercontent.com',
                        'client_secret' => 'MPMP2w7f8v-NorkDCix9p0JB',
                        'title' => 'Google',
                    ),
//                    'yandex' => array(
//                        'class' => 'CustomYandexService',
//                        //'realm' => '*.example.org',
//                    ),
                    'facebook' => array(
                        // register your app here: https://developers.facebook.com/apps/
                        'class' => 'CustomFacebookService',
                        'client_id' => '196086027247412',
                        'client_secret' => '415cce9bee434d69e1422e04447c77c2',
                    ),
                    'twitter' => array(
                        // register your app here: https://dev.twitter.com/apps/new
                        'class' => 'CustomTwitterService',
                        'key' => '9mCM8DxgVKVq39E0KZgA',
                        'secret' => 'nuu40XtHRjIOIXgogTIPThFpbfRSZYjlOtm9tZ22sSE',
                    ),
                    'vkontakte' => array(
                        // register your app here: https://vk.com/editapp?act=create&site=1
                        'class' => 'CustomVKontakteService',
                        'client_id' => '4082902',
                        'client_secret' => 'fJ7btcE857S3gJSkH7ML',
                        'title' => 'VK',
                    ),
                    'odnoklassniki' => array(
                        // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
                        // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
                        'class' => 'CustomOdnoklassnikiService',
                        'client_id' => '210374656',
                        'client_public' => 'CBABBLINABABABABA',
                        'client_secret' => '88271971CBD032426BBF584B',
                        'title' => 'Odnokl.',
                    ),
                    'yandex_oauth' => array(
                        // register your app here: https://oauth.yandex.ru/client/my
                        'class' => 'CustomYandexService',
                        'client_id' => '8d9bb9c4802442d6a39dd101598454d0',
                        'client_secret' => '1216063ce0174185b2e688b8d4ea84c1',
                        'title' => 'Яндекс',
                    ),
                    
                ),
            ),
		),
	),
	CMap::mergeArray($mainEnvConfiguration, $mainLocalConfiguration)
);