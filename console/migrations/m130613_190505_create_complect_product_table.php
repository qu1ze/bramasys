<?php

class m130613_190505_create_complect_product_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
                CREATE TABLE IF NOT EXISTS `complect_product` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`complect_id` int(11) NOT NULL,
					`product_id` int(11) NOT NULL,
					`count` int(11) NOT NULL,
					PRIMARY KEY (`id`),
					KEY `complect_id` (`complect_id`),
					KEY `product_id` (`product_id`)
				  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
				  
				  ALTER TABLE `complect_product`
					ADD CONSTRAINT `complect_product_ibfk_3` FOREIGN KEY (`complect_id`) REFERENCES `complect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
					ADD CONSTRAINT `complect_product_ibfk_4` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
            ");
	}

	public function down()
	{
		$this->dropTable('complect_product');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}