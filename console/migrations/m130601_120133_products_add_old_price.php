<?php

class m130601_120133_products_add_old_price extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE  `products` ADD  `old_price` FLOAT NOT NULL AFTER  `price`");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `products` DROP `old_price`;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}