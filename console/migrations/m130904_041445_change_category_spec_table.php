<?php

class m130904_041445_change_category_spec_table extends CDbMigration
{
    public function up()
    {
        $this->execute("
			ALTER TABLE `category_spec`
				ADD `order` int(11) DEFAULT NULL,
				ADD `in_filter` tinyint(1) DEFAULT 1;
				");
    }

    public function down()
    {
        $this->execute("
			ALTER TABLE `category_spec`
				DROP `order`,
				DROP `in_filter`;
				");
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}