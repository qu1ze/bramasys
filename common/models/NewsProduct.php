<?php

/**
 * This is the model class for table "news_product".
 *
 * The followings are the available columns in table 'news_product':
 * @property integer $id
 * @property integer $news_id
 * @property integer $product_id
 */
class NewsProduct extends ActiveRecord
{
    public $product_id_s = array(); 
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NewsProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('news_id, product_id', 'required'),
			array('news_id, product_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, news_id, product_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
			'article' => array(self::BELONGS_TO, 'Article', 'article_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'news_id' => 'News',
			'product_id' => 'Product',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('news_id',$this->news_id, true);
		$criteria->compare('product_id',$this->product_id, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function saveProducts($product_ids, $news_id, $update=false)
    {
            $error=false;            
            $transaction = Yii::app()->db->beginTransaction(); // Transaction begin
            if($update) NewsProduct::model()->deleteAllByAttributes(array('news_id' => $news_id));
            try{
                foreach ($product_ids as $value)
                {
                    $model = new NewsProduct();
                    $model->news_id = $news_id;
                    $model->product_id = $value;
                    if (0==NewsProduct::model()->countByAttributes(array('news_id'=>$news_id, 'product_id' => $value))){
                        $error=  !$model->save() | $error;  
                    }
                    

                }
                if (!$error){
                    $transaction->commit();
                    return true;
                }
            }
            catch (Exception $e){
                            $transaction->rollBack();
            }
            return false;
    }
}