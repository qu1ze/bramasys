<?php
    Yii::app()->clientScript->registerScript("char_count", "
        $('.desc_field').on('keyup', function(){
            if($('#char_count').length == 0)
                $(this).parent().append($('<div>').attr('id', 'char_count'));
            count = 255;
            count -= $(this).val().length;
            $('#char_count').text('Осталось ' + count + ' симв.');
        });
    ", CClientScript::POS_READY);

	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
        
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'options'=>array(
                        'title'=>'Заменить описание?',
                        'modal'=>true,
                        'autoOpen'=>false,
                        'buttons'=>array(
                            'Yes'=>'js:function(){$(".desc_field").val($(".desc").val()); $(this).dialog("close")}',
                            'No'=>'js:function(){$(this).dialog("close")}'
                            )
                    ),
                    'id'=>'confirmReplace',
                    
            )
        );

        echo CHtml::textArea('desc', '', array('class' => 'desc'));

        $this->endWidget('zii.widgets.jui.CJuiDialog');
?>
	<?php echo $form->errorSummary($model); ?>
        
	<?php echo $form->dropDownListRow($model , 'brand', $brands);?>
	<?php echo $form->textFieldRow($model, 'model'); ?>
	<?php echo $form->textFieldRow($model, 'excel_id'); ?>
    <?php echo $form->ckEditorRow($model, 'full_description', array('class'=>'full_desc_field', 'rows'=>5)); ?>
	<?php echo $form->textAreaRow($model , 'description', array('class' => 'desc_field'));?>
	<?php echo $form->textFieldRow($model, 'old_price'); ?>
	<?php echo $form->textFieldRow($model, 'price'); ?>
	<?php echo $form->dropDownListRow($model, 'category', $categories, $categories_options); ?>
	<?php echo $form->textFieldRow($model, 'quantity'); ?>
	<?php echo $form->checkBoxRow($model, 'price_of_the_week'); ?>
	<?php echo $form->checkBoxRow($model, 'top_seller'); ?>
	<?php echo $form->checkBoxRow($model, 'novelty'); ?>
	<?php echo $form->checkBoxRow($model, 'hide'); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Создать')); ?>
	
<?php   $this->endWidget(); ?>