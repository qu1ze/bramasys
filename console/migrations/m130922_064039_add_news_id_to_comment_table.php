<?php

class m130922_064039_add_news_id_to_comment_table extends CDbMigration
{
	public function up()
	{
		$this->addColumn('comment', 'news_id', 'int');
	}

	public function down()
	{
		$this->dropColumn('comment', 'news_id');
	}

}