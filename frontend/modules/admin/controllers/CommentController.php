<?php

class CommentController extends Controller
{
	public function actionIndex()
	{		
		$model = new Comment('search');
		
		if (isset($_GET['Comment']))
			$model->attributes = $_GET['Comment'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = Comment::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/comment'));				
				
		if (isset($_POST['Comment']))
		{			
			$model->attributes = $_POST['Comment'];
			if ($model->save())
				$this->redirect(array('/admin/comment'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Comment');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Отзыв успешно отредактирован')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
			Comment::model()->deleteByPk( $id );					
			Yii::app()->user->setFlash('success', "Отзыв успешно удален");
		}
		
		$this->redirect(array('/admin/comment'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = Comment::model()->findByPk($id)) !== null)
		{
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Отзыв успешно удален'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить отзыв'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Отзыв не найден'));
	}
	
	public function actionToggle($id, $attribute)
    {
		if (($model = Comment::model()->findByPk($id)) === null)
                app()->end();
		$model->setAttribute($attribute, $model->getAttribute($attribute) ? 0 : 1);
		$model->save();
		echo json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> "Value $attribute toggled"));
    }
}