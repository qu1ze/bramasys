<?php
/**
 * EHtml.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 11/4/12
 * Time: 12:27 AM
 */
class EHtml extends CHtml
{
	const TYPE_SUCCESS = 'success';
	const TYPE_WARNING = 'warning';
	const TYPE_IMPORTANT = 'important';
	const TYPE_INFO = 'info';
	const TYPE_INVERSE = 'inverse';
	const TYPE_DANGER = 'danger';
	const TYPE_NONE = '';

	public static function addClassName($className, $htmlOptions)
	{
		if (is_array($className))
			$className = implode(' ', $className);
		if (isset($htmlOptions['class']))
			$htmlOptions['class'] .= ' ' . $className;
		else
			$htmlOptions['class'] = $className;

		return $htmlOptions;
	}

	public static function alert($content, $type=self::TYPE_NONE, $closeText=null, $increasePadding=false)
	{
		$classes = array('alert');
		if($type !== self::TYPE_NONE)
			$classes[] = 'alert-'.$type;
		if($increasePadding)
			$classes[] = 'alert-block';

		$html = '<div class="'.implode(' ', $classes).'">';
		if($closeText !== null)
			$html .= '<button type="button" class="close" data-dismiss="alert">'.$closeText.'</button>';

		return $html . $content.'</div>';
	}

	public static function hint($text, $htmlOptions=array())
	{
		return parent::tag('p', self::addClassName('help-block', $htmlOptions), $text);
	}

	public static function progressBar($type, $content='', $percent = 0, $striped = false, $animated = false, $htmlOptions = array())
	{
		$validTypes = array(self::TYPE_INFO, self::TYPE_SUCCESS, self::TYPE_WARNING, self::TYPE_DANGER );
		$classes = array('progress');
		if(in_array($type, $validTypes))
			$classes[] = 'progress-' . $type;
		if($striped)
			$classes[] = 'progress-striped';
		if($animated)
			$classes[] = 'active';
		if ($percent < 0)
			$percent = 0;
		else if ($percent > 100)
			$percent = 100;

		echo parent::openTag('div', self::addClassName($classes, $htmlOptions));
		echo '<div class="bar" style="width:'.$percent.'%;">'.$content.'</div>';
		echo '</div>';
	}

	public static function bootstrapLabel($content, $type, $encode = true, $htmlOptions = array())
	{
		return self::labelOrBadge('label', $content, $type, $encode, $htmlOptions);
	}

	public static function badge($content, $type, $encode = true, $htmlOptions = array())
	{
		return self::labelOrBadge('badge', $content, $type, $encode, $htmlOptions);
	}

	protected static function labelOrBadge($class, $content, $type, $encode = true, $htmlOptions = array())
	{
		$validTypes = array(self::TYPE_SUCCESS, self::TYPE_WARNING, self::TYPE_IMPORTANT, self::TYPE_INFO, self::TYPE_INVERSE);
		$classes = array($class);
		if (isset($type) && in_array($type, $validTypes))
		{
			$classes[] = $class . '-' . $type;
		}
		$htmlOptions = self::addClassName($classes, $htmlOptions);

		if ($encode)
		{
			$content = parent::encode($content);
		}
		return parent::tag('span', $htmlOptions, $content);
	}

	public static function addGridButton($url, $title, $height=400, $icon='icon-user', $iconColor = 'icon-white', $style = 'btn-new btn-small btn-primary')
	{
		return "<button class='btn {$style}' data-height='{$height}' data-title='{$title}' data-href='{$url}' type='button'>" .
			"<i class='{$icon} {$iconColor}'></i><i class='icon-plus {$iconColor}'></i></button>";

	}
}