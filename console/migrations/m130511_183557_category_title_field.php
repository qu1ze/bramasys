<?php

class m130511_183557_category_title_field extends CDbMigration
{
	public function up()
	{
            $this->execute("ALTER TABLE `category` ADD  `title` varchar(255) NOT NULL AFTER  `name`;");
	}

	public function down()
	{
            $this->execute("ALTER TABLE `category` DROP `title`");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}