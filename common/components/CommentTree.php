<?php
Yii::import('zii.widgets.CPortlet');

class CommentTree extends CPortlet
{
	private $model;
    public $title;
    public $additionalUrlNode;
	public $contentId;
	public $contentType;
	private $comments = null;
	public $commentCount = 0;
    public $plus_minus = true;
	
	public function init()
	{
		Yii::app()->user->id == null || Yii::app()->user->getState('isAdmin') ? $this->model = new Comment('fromGuest, ' + $this->contentType) : $this->model = new Comment('insert, ' + $this->contentType);
		$this->comments = new Tree();
		$criteria = new CDbCriteria();
		$criteria->addCondition("{$this->contentType}_id = $this->contentId");
		$subCriteria = new CDbCriteria();
		$subCriteria->addCondition("status = 1");
		if(Yii::app()->user->id != null)
		{
			$subCriteria->addCondition("author_id = ".Yii::app()->user->id, 'OR');
		}
		$criteria->mergeWith($subCriteria);
		if(isset($_GET['sort']) && $_GET['sort'] == 'useful_comments')
		{
			$criteria->order = 'useful_count DESC';
		} else {
            $criteria->order = 'post_date DESC';
        }
		$comments = Comment::model()->findAll($criteria);
		foreach ($comments as $comment)
		{
			if ($comment->parent_id != null)
                $parentId = $comment->parent_id;
            else
                $parentId = 0;
			$this->comments->addItem($comment->id, $parentId, $comment);
		}
	}
 public function getUrl()
    {
    $url = app()->getBaseUrl(true); 
    $path = Yii::app()->request->getPathInfo();
    foreach (explode('/', $path) as $part )
    {
     $url.='/'.urlencode($part);   
    }
    return $url;
    
    } 
    public function run()
    {
		$this->render('comments', array(
			'comments' => $this->comments,
			'model' => $this->model,
			'contentId' => $this->contentId,
			'type' => $this->contentType,
                        'plus_minus' => $this->plus_minus,
			)
		);
		
		$this->render('comment_form', array(
			'model' => $this->model,
			'contentId' => $this->contentId,
			'type' => $this->contentType,
                        'plus_minus' => $this->plus_minus,
			)
		);
    }
}
?>
