<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => $model->getFormId(),
		'type' => 'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); 
    $htmlOpt = array('multiple'=>'multiple', 'size'=> '6' );
    ?>

	<?php $this->renderPartial('_form', array('form' => $form, 'model' => $model, 'htmlOpt' => $htmlOpt)) ?>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Создать')); ?>
	
<?php $this->endWidget(); ?>