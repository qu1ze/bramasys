<?php
/* @var $this DefaultController */
Yii::app()->getClientScript()->registerCssFile('/css/profile.css');

Yii::app()->clientScript->registerScript("removeAddress","
	 $('body').on('click', '#addresses .drop', function () {
		var address = $(this).parent();
		var id = $(address).attr('address-id');
		var count = $('#addresses ' + '.control-group').not('[address-id=\"new\"]').length;
		if(count < 3 && id !== 'new')
		{
			$('#addresses .control-group').not('[address-id=\"new\"]').children('.drop').remove();
		}
		if(count > 1 || id === 'new')
		{
			$(address).fadeOut('slow', function()
			{
				$(address).remove();
			});
		}
	 });
	", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("removePhone","
	 $('body').on('click', '#phones .drop', function () {
		var phone = $(this).parent();
		var id = $(phone).attr('phone-id');
		var count = $('#phones ' + '.control-group').not('[phone-id=\"new\"]').length;
		if(count < 3 && id !== 'new')
		{
			$('#phones .control-group').not('[phone-id=\"new\"]').children('.drop').remove();
		}
		if(count > 1 || id === 'new')
		{
			$(phone).fadeOut('slow', function()
			{
				$(phone).remove();
			});
		}
	 });
	", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("addAddress","
	 $('#add-address').on('click', function () {
		var address_form = $('#address-form').children('div').clone();
		$('#addresses').append($(address_form));
		$(address_form).slideDown();
	 });
	", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("addPhone","
	 $('#add-phone').on('click', function () {
		var phone_form = $('#phone-form').children('div').clone();
		$('#container #phones').append($(phone_form));
		$(phone_form).slideDown();
	 });
	", CClientScript::POS_END);
Yii::app()->clientScript->registerScript("PhoneValid","
 $('body').on('blur', '#phone',function(){
         var phone = $(this); 
         $.ajax(
			{
				url: '/user/profile/ajaxphonevalidation',
				data: {value: $(phone).val()},
				dataType: 'json',
				type: 'get',
				success: function(data){
					if(data.title == 'error')
					{
					$(phone).parent().children('#Phone_em_').show();
                    $(phone).parent().children('#Phone_em_').text(data.error);
                    $(phone).parent().children('#Phone_em_').parent().parent().parent().removeClass('success').addClass('error');
					}
                    else
                    {
                    $(phone).parent().children('#Phone_em_').parent().parent().parent().removeClass('error').addClass('success');
                    $(phone).parent().children('#Phone_em_').hide();
                    }
				}
			});
        });
", CClientScript::POS_END);
$this->breadcrumbs=array(
	'Редактирование личного профиля',
);
?>
<?php $this->renderPartial('_side'); ?>
<div id="container">
	<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
	?>
	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'id'=>'horizontalForm',
				'type'=>'horizontal',
				'htmlOptions' => array('enctype' => 'multipart/form-data', 'style' => 'margin: 0px'),
			));?>
	<h3>Редактирование личного профиля</h3>
	<div class="profile-content">
		<?php echo $form->errorSummary($client); ?>
		<font style="font: 15px/25px 'PTSN Bold';">Личные данные</font>
        <?php echo $form->textFieldRow($client, 'email'); ?>
		<?php echo $form->textFieldRow($client, 'name', array('value' => $client->originalName)); ?>
		<?php echo $form->textFieldRow($client, 'lastname'); ?>
		<?php echo $form->textFieldRow($client, 'middle_name'); ?>
         <div class="controls"><div class="fieldWithSubLabel"> <label class="subLabelNoLeft">в формате дд.мм.гггг</label></div></div>
		
		<?php echo $form->textFieldRow($client, 'date_of_birth', array('style' => 'width: 150px;')); ?>
        
		<div id="phones">
		<font style="font: 15px/25px 'PTSN Bold';">Номер телефона</font>
			<?php 
			$count = 0;
			foreach ($phones as $key => $phone):?>
			<?php $this->renderPartial('_phone', array(
				'phone' => $phone,
				'key' => $key,
				'count' => $this->countOld($phones),
			)); ?>
			<?php 
			$count++;
			endforeach;?>
		</div>
		
		<div id="phone-form" class="hide">
		<?php $this->renderPartial('_phone', array(
				'phone' => $newPhone,
				'count' => $this->countOld($phones),
			)); ?>
		</div>
         <a class="hover_color" id="add-phone">Добавить номер</a>
		
		<div id="addresses">
		<font style="font: 15px/25px 'PTSN Bold';">Адрес доставки</font>
			<?php 
			$count = 0;
			foreach ($deliveryAddresses as $key => $deliveryAddress):?>
			<?php $this->renderPartial('_delivery_address', array(
				'deliveryAddress' => $deliveryAddress,
				'key' => $key,
				'count' => sizeof($deliveryAddresses),
			)); ?>
			<?php 
			$count++;
			endforeach;?>
		</div>
		
		<div id="address-form" class="hide">
		<?php $this->renderPartial('_delivery_address', array(
				'deliveryAddress' => $newDeliveryAddress,
				'count' => sizeof($deliveryAddresses),
			)); ?>
		</div>
         <a class="hover_color" id="add-address">Добавить адрес</a>
		<br>
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить', 'htmlOptions' => array('class' => 'blue-button'))); ?> <a class="hover_color" href="<?php echo app()->getBaseUrl(true); ?>/user" style="margin-left: 20px;">Отменить</a>
	<?php $this->endWidget();?>
	</div>
</div>
