<?php $inCart = isset(Yii::app()->session['cart'][$product->id]);?>
<table class="compareProduct pd<?php echo $product->id; ?>">
	<tr>
		<td class="bottom_delete_compare">
			<a class="removeProduct" data-productId="<?php echo $product->id; ?>" href="#"><img src="/images/cross.gif" alt=""></a>
			<a class="removeProduct" data-productId="<?php echo $product->id; ?>" href="#">удалить</a>
		</td>
	</tr>
	<tr>
		<td class="thumb">
			<div class="pd-qty-block" style="display:none;"><input hidden value="1"></div>
			<div class="code pd-code"  style="display: none;">Код товара: <span><?php echo $product->id;?></span></div>
			<a class="imgUrl" href="<?php echo $product->link;?>">
				<span style="background-image: url('<?php echo $product->image_url; ?>')" class="img-vert">
						<img style="display:none" src="<?php echo $product->image_url; ?>" alt="">
					</span>
			</a>
		</td>
	</tr>
	<tr>
		<td class="name">
			<a href="<?php echo $product->link;?>">
				<?php echo $product->name;?>
			</a>
		</td>
	</tr>
</table>