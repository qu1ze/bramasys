<?php

class m130601_120528_order_table_add_mounting extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE  `order_product` ADD  `mounting` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `price`");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `order_product` DROP `mounting`;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}