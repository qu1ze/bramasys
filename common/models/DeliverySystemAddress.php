<?php

/**
 * This is the model class for table "delivery_system_address".
 *
 * The followings are the available columns in table 'delivery_system_address':
 * @property integer $id
 * @property integer $delivery_type_id
 * @property string $town
 * @property string $office
 * @property string $address
 *
 * The followings are the available model relations:
 * @property DeliveryType $deliveryType
 */
class DeliverySystemAddress extends ActiveRecord
{
	public $NOVAYA_POCHTA_ID = 2; 
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DeliverySystemAddress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
    public function getOffice_address() {
		return "$this->office ($this->address)";
	}
    
	public function importFromfile($excelFile)
	{
		Yii::import('common.extensions.EZendAutoloader', true);
		EZendAutoloader::$prefixes = array('PHPExcel', 'PHPExcel_Reader', 'PHPExcel_Writer');
		EZendAutoloader::$basePath = Yii::getPathOfAlias('common.extensions.phpexcel') . DS;
		Yii::registerAutoloader(array("EZendAutoloader", "loadClass"), true);
		
		if(!$excelFile->hasErrors())
			{
				$excelReader = new PHPExcel_Reader_Excel2007();
				if($excelFile->extensionName == 'xls')
				{
					$excelReader = new PHPExcel_Reader_Excel5();
				}
				//$excelReader->setLoadSheetsOnly('Філії');	
				

				if ($excelReader->canRead($excelFile->file))
				{
					$excel = $excelReader->load($excelFile->file);
					// = $excelReader->listWorksheetNames($excelFile->file);	
					$worksheet = $excel->getSheetByName('Філії');
					
					if(isset($worksheet)){
						$list = $worksheet->getCell('C2')->getValue();
						$eof = false;
						$row_i = 2;
						$town='';
						$transaction = Yii::app()->db->beginTransaction(); // Transaction begin
	            		DeliverySystemAddress::model()->deleteAllByAttributes(array('delivery_type_id' => $this->NOVAYA_POCHTA_ID));
	            		try{
							while (!$eof){
								$Acol=$worksheet->getCell('A'.$row_i)->getValue();
								$Bcol=$worksheet->getCell('B'.$row_i)->getValue();
								$Ccol=$worksheet->getCell('C'.$row_i)->getValue();
								//$excelFile->addError('data',  "Строка:$row_i A:$Acol B:$Bcol C:$Ccol ");
								if (!empty($Acol))
									{
										if(empty($Ccol))	{
											$town=$Acol;	
										}
										elseif ((!empty($Bcol)) && ((strpos($Bcol,'Відділення')!==false ) || (strpos($Bcol,'Отделение')!==false) ) ){
                                            //$excelFile->addError('data',  "Строка:$row_i A:$Acol B:\"$Bcol\" C:$Ccol ");
											$model =new DeliverySystemAddress();
											$model->office = $Acol;
											$model->town = $town;
											$model->address = $Bcol;
											$model->delivery_type_id = $this->NOVAYA_POCHTA_ID;
											if(!$model->save())
                                                {
                                                $excelFile->addErrors($model->getErrors());
                                                throw new Exception('Ошибка сохранения строки №'.$row_i." Строка:$row_i A:$Acol B:\"$Bcol\" C:$Ccol город: $town \n");
                                            }
												
										}			
									}
								else {
									$eof=true;
								}
								$row_i++;
							}
                             $transaction->commit();
						}
						catch (Exception $e){
	                            $transaction->rollBack();
								$excelFile->addError('data',  $e->getMessage());
								
	            		}
					//ob_start();
					//var_dump($marge);
					//echo '-------------';
					//var_dump($list);
					//$res = ob_get_clean();
					//ob_end_flush();	
					//$excelFile->addError('data',  $res );
					}
                    else {
                        $excelFile->addError('file', 'Файл несодержит нужного листа');
                    }
				}
				else
				{
				   $excelFile->addError('file', 'Возможно файл поврежден или имеет неверный формат, попробуйте пересахранить его');
				}
				
				
			}
			@unlink($excelFile->file);
		
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'delivery_system_address';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('delivery_type_id, town, office, address', 'required'),
			array('delivery_type_id', 'numerical', 'integerOnly'=>true),
			array('town', 'length', 'max'=>150),
			array('office', 'length', 'max'=>100),
			array('address', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, delivery_type_id, town, office, address', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'deliveryType' => array(self::BELONGS_TO, 'DeliveryType', 'delivery_type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'delivery_type_id' => 'Delivery Type',
			'town' => 'Город',
			'office' => 'Отделение',
			'address' => 'Адрес',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('delivery_type_id',$this->delivery_type_id);
		$criteria->compare('town',$this->town,true);
		$criteria->compare('office',$this->office,true);
		$criteria->compare('address',$this->address,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}