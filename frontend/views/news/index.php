<div id="container">
<?php
/*
 * 	<div class="update">
		<h3>Подпишись на обновления:</h3>
		<a href="#"><img src="/images/rssIcon.png" alt=""></a>
		<a href="#"><img src="/images/facebookIcon.png" alt=""></a>
		<a href="#"><img src="/images/vkontakteIcon.png" alt=""></a>
		<a href="#"><img src="/images/youtubeIcon.png" alt=""></a>
		<a href="#"><img src="/images/googleIcon.png" alt=""></a>
	</div>
 */
	Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl(true) . '/css/news.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl(true) . '/css/style.css');
?>     

<?php
$this->breadcrumbs = app()->categoryController->getCategoryBcs($this->categoryName);

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
?>

<div class="category-title"><?php echo end($this->breadcrumbs) ?></div>       
	<div class="tabbs-article">
					
	<?php
		$this->widget('bootstrap.widgets.TbListView', array(
			'dataProvider' => $dataProvider, //with category
			'itemView' => '_view',		
			'template' => "{sorter}\n{items}\n{pager}",
			// 'itemsTagName' => 'table',
			// 'itemsCssClass' => 'items table table-striped table-condensed',
			'emptyText' => '<i>Извините, но на данный момент у нас нет новостей.</i>',
		)); 
	?>
					   
		   
	  
	</div>


</div>