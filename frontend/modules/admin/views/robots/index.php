<?php
/* @var $robotsModel Robots */
?>
<a href="/admin/robots/textedit">Текстовый редактор</a><br><br>
<a href="/admin/robots/edit">Изменить</a>
<br><br>
<div id="fileDesc">
	Файл robots.txt разрешает индексировать все страницы, кроме тех, которые начинаются с:<br>
	<?php if(!empty($robotsModel->rules)):?>
	<?php 
		foreach ($robotsModel->rules as $rule)
		{
			echo $rule.';<br>';
		}
	?>
	<?php endif;?>
	<?php if(!empty($robotsModel->cleanParams)):?>
	Очищаются параметры:<br>
	<?php 
		foreach ($robotsModel->cleanParams as $cleanParam)
		{
			echo $cleanParam.';<br>';
		}
	?>
	<?php endif;?>
	<br>Задержка при индексации <?php echo $robotsModel->crawlDelay; ?> сек.
</div>
<br>
<div id="fileSource">
	Содержание файла:<br>
	<pre><?php echo $robotsModel->content; ?></pre>
</div>