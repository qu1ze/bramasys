<?php

class SeoController extends Controller
{		
	public function actionCreate()
	{
		$model = new CategoryMetadata(); 
				
		if (isset($_POST['CategoryMetadata']))
		{			
			$model->attributes = $_POST['CategoryMetadata'];
			if ($model->save())
				$this->redirect(array('/admin/seo'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new CategoryMetadata();	

		if (isset($_POST['CategoryMetadata']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['CategoryMetadata'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Метаданные успешно созданы'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать метаданные'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new CategoryMetadata('search');
		
		if (isset($_GET['CategoryMetadata']))
			$model->attributes = $_GET['CategoryMetadata'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = CategoryMetadata::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/seo'));				
				
		if (isset($_POST['CategoryMetadata']))
		{			
			$model->attributes = $_POST['CategoryMetadata'];
			if ($model->save())
				$this->redirect(array('/admin/seo'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('CategoryMetadata');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Метаданные успешно отредактированы')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
				CategoryMetadata::model()->deleteByPk( $id );					
				Yii::app()->user->setFlash('success', "Метаданные успешно удалены");
		}
		
		$this->redirect(array('/admin/seo'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = CategoryMetadata::model()->findByPk($id)) !== null)
		{
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Метаданные успешно удалены'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить метаданные'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Метаданные не найдены'));
	}
	
}