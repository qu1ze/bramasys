<?php
/*
    Убрал -articles из сылок с ними не работали стати, непонял как оно должнобыло работать
  
 * <div class="keywords">		
		Теги: <?php echo $data->keywords ?>		
	</div>
 * <a class="read_more_link" href="<?php echo Yii::app()->createUrl('/support/articles/' . $data->category->name . '/' . $data->name) ?>">
				Читать далее —>
			</a>
 *   */
Yii::app()->getClientScript()->registerCssFile('/css/rating.css');
?>
<div class="article-tab">
	<img src="<?php echo $data->thumb ? $data->thumb : '/images/article-img.jpg' ?>" width="140px" />
	<div class="date">
		<?php echo date('d.m.Y', $data->update_time) ?>
	</div>
	<a class="articles_title_link" href="<?php echo $link=Yii::app()->createUrl('/support/stati-publikatcii-obzory/' . $data->category->name . '/' . $data->name) ?>">
		<?php echo $data->title ?>
	</a>
	<div> 
		<?php echo $data->short_description ?>		
	</div>
	<div class="read_more_wrapper">
		<p>
			
		</p>
	</div>
	
	
	
	<div class="name_article review-product">
		
		<div class="heading-article">
			<table>
				<tr>
					<td>
                     <div class="rate-count">
                       <div class="vote-wrap">
						<div class="comment-rating">
                            <?php $colRating = Comment::model()->count("`article_id` = $data->id AND rating != 0 AND status = 1"); ?>
                            <?php $this->widget('common.components.RatingStars', array('rating' => $data->getRating())); ?> (<?php echo $colRating;?> оц.)
                        </div>
                           </div>
                         </div>
					</td>
					<td>
						<?php echo $data->views ?> просмотров
					</td>
					<td rowspan="2" valign="top">
						<?php $subject = urlencode($data->title)?>
						<?php $body = urlencode(Yii::app()->getBaseUrl(true) . '/support/stati-publikatcii-obzory/' . $data->category->name . '/' . $data->name)?>
						<a href="mailto:?subject=<?php echo $subject?>&body=<?php echo $body?>">
							<img src="/images/mail-to.png">
						</a>
                       
					</td>
                    
				</tr>
				<tr>
					<td>
                        Рубрика:<br/>
						<?php echo $data->category->title ?>
					</td>
					<td>
                        <?php $colComment = Comment::model()->count("`article_id` = $data->id AND status = 1 AND parent_id is NULL"); ?>
                        <?php if($colComment<1):?>
                        <a href="<?php echo $link."#post_comment"; ?>">
                        Оставить отзыв
                        <?php else:?>
                        <a href="<?php echo $link."#comment-title"; ?>">
                        <?php echo $colComment;?> отзывов
                        <?php endif;?>
                        </a>
					</td>				
				</tr>
			</table>						
		</div>
	</div>

</div>
