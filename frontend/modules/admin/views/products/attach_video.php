<?php
Yii::app()->clientScript->registerScript("addVideo", "
	$('#add-link').on('click', function(){
		var input = $('#field-pattern div').clone();
		var count = $('#links input').length;
		$(input).children('input').attr('name', $(input).children('input').attr('name') + '[' + count + ']');
		$('#links').append(input);
		$(input).slideDown(300);
	});
", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript("dropVideo", "
	$('.form-horizontal').on('click', '.dropVideo', function(){
		var that = $(this);
		$(that).parent().fadeOut(200, function()
		{
			$(this).remove();
		});
	});
", CClientScript::POS_READY);
	
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
   <div class="control-group ">
		<label class="control-label" for="">Ссылка на видео</label>
		<div id="field-pattern" class="hide">
			<div style="margin-bottom: 15px; display: none;">
				<input name="ProductVideo[link]" type="text" value="">
				<img class="dropVideo" src="/images/cross.gif" width="10"/>
			</div>
		</div>
		<div class="controls">
			<div style="display: inline-block; width: 100%;">
				<div id="links" style="float: left;">
				<?php if(!empty($videos)):?>
				<?php 
				$count = 0;
				foreach ($videos as $video): ?>
					<div style="margin-bottom: 15px;">
						<input name="ProductVideo[link][<?php echo $count; ?>]" type="text" value="<?php echo $video->link; ?>">
						<img class="dropVideo" src="/images/cross.gif" width="10"/>
					</div>
				<?php 
				$count++;
				endforeach; ?>
				<?php endif;?>
				</div>
			</div>
			<a id="add-link" style="float:none; margin-top: 5px; display: inline-block; cursor: pointer;">Добавить</a>
		</div>
	</div>

    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	
	
<?php $this->endWidget(); ?>