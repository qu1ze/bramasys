<?php

class m130806_180013_add_fields_to_order extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `delivery_type` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`title` varchar(50) NOT NULL,
				`description` text NOT NULL,
				PRIMARY KEY (`id`)
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			  ");
		$this->execute("
			CREATE TABLE IF NOT EXISTS `payment_type` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`title` varchar(50) NOT NULL,
				PRIMARY KEY (`id`)
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			  ");
		$this->execute("
                ALTER TABLE `order`  
					ADD `payment_type_id` INT(11) NOT NULL AFTER `id`,
					ADD `delivery_type_id` INT(11) NOT NULL AFTER `payment_type_id`,
					ADD `delivery_address` VARCHAR(255) NOT NULL AFTER `delivery_type_id`,
					ADD `comment` VARCHAR(255) NULL DEFAULT NULL AFTER `delivery_address`,
					ADD INDEX (`payment_type_id`),
					ADD INDEX (`delivery_type_id`);
				ALTER TABLE `order`
					ADD CONSTRAINT `FK_order_payment_type`
					FOREIGN KEY ( `payment_type_id`)
					REFERENCES  `payment_type` (`id`)
					ON DELETE RESTRICT ON UPDATE RESTRICT ;
				ALTER TABLE `order`
					ADD CONSTRAINT `FK_order_delivery_type`
					FOREIGN KEY ( `delivery_type_id`)
					REFERENCES  `delivery_type` (`id`)
					ON DELETE RESTRICT ON UPDATE RESTRICT ;
            ");
	}

	public function down()
	{
		$this->execute("ALTER TABLE  `order` DROP FOREIGN KEY  `FK_order_payment_type`;");
		$this->execute("ALTER TABLE  `order` DROP FOREIGN KEY  `FK_order_delivery_type`;");
		$this->execute("ALTER TABLE `order`
                DROP `payment_type_id`,
                DROP `delivery_type_id`,
				DROP `delivery_address`,
                DROP `comment`
                ");
		$this->dropTable("payment_type");
		$this->dropTable("delivery_type");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}