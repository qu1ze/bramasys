<?php
/**
 * create.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 11/16/12
 * Time: 5:28 PM
 */
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'ajax-tag-form',
	'type' => 'vertical',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=> true,
		'afterValidate'=>'js:function(f,d,e){
			if(!e)
			{
				var url = "'.$this->createUrl('/admin/currency/ajaxCreate').'";
		        $.ajax({
		            url: url,
		            data: f.serialize(),
		            dataType: "json",
		            type: "post",
		            success: function(d){						
		                if(typeof(d) == "object"){
							brama.ui.notify(d.title, d.msg, false);
						}
		                $.colorbox.close();
		                $("#currency-table").yiiGridView("update");					  						
		            }
		        });
			}
			return false;
		}'
	)
)); ?>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->textFieldRow($model, 'name');?>
<?php echo $form->textFieldRow($model, 'koef');?>
<?php echo $form->textFieldRow($model, 'label');?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Создать')); ?>

<?php $this->endWidget(); ?>
