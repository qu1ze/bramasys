<div id="container">
	<?php
	/*
	 * <div> 		
			<span class="article-gray-text">
				Теги:
			</span>
			<?php echo $model->keywords ?>
		</div>
	 * <img src="<?php echo $model->thumb ? $model->thumb : '/images/article-img.jpg' ?>" class="news_image" />
	 * 
	 */	
		Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl(true) . '/css/news_view.css');
		$this->widget('zii.widgets.CBreadcrumbs', array(
			'links' => array(		
					'О Компании' => '/company',
					'Новости' => '/company/news',
					$model->title => '/company/news/' . $model->name,
				),
			'separator' => ' > ',
			'homeLink' => CHtml::link('Главная', Yii::app()->homeUrl)
		));
	?>
	<div class="article-tab">
		<h1>
			<?php echo $model->title ?>
		</h1>
		<div>
			
			<span class="article-gray-text">
				<?php echo date('d.m.Y', $model->update_time) ?><br />
			</span>
			
			<?php echo $model->text ?>
		</div>
				
		
		<?php $this->widget('common.components.ProductSlider', array('productsIds' => array_values($related_products), 'title' => '<h4>Товары упомянутые в новости:</h4>')); ?>
		
		<div class="related_news">
			<h4>Читайте также:</h4>
			<?php foreach ($related_news as $related_news_item): ?>
				<div class="related_news_item">
					<a href="<?php echo '/company/news/' . $related_news_item->relatedNews->title  ?>"><?php echo $related_news_item->relatedNews->title  ?></a>
				</div>
			<?php endforeach ?>
		</div>
		
		<div class="clear"></div>
		<div class="name_article review-product">
			<div > <?php $this->widget('common.components.SocButtons'); ?></div>
			
						
			<?php $subject = urlencode($model->title)?>
			<?php $body = urlencode(Yii::app()->getBaseUrl(true) . '/company/news/' . $model->name)?>
			<a href="mailto:?subject=<?php echo $subject?>&body=<?php echo $body?>">
				<img src="/images/mail-to.png">
			</a>
						

			
		</div>
		<div> 		
			<?php echo $model->views ?> просмотров
		</div>
	</div>
	<div id="comment">
		<?php $this->widget('common.components.CommentTree', array('contentId' => $model->id, 'contentType' => 'news', 'plus_minus'=>false)); ?>
	</div>
</div>