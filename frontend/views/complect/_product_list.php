<p class="title">Комплектация</p>
<table class="complect">
	<thead>
	<tr class="row">
		<td width="450" style="padding-left: 20px;">Наименовение товара</td>
		<td align="center">Цена</td>
		<td width="50" align="center">Кол-во</td>
		<td align="center">Итого</td>
	</tr>
	</thead>
	<tbody>
	<?php
	$productCounts = CHtml::listData($complectProducts, 'product_id', 'count');

	$sum = 0;
	$discountSum = $complect->price;
	foreach ($complectProducts as $complectProduct):?>
	<?php
        $product = Products::model()->findByAttributes(array('id' => $complectProduct->product_id));
        $price = $product->price;
        $count = $productCounts[$product->id];
        $amount = $price * $count;
        $sum += $amount;
	?>
	<tr>
		<td>
			<img width="140" src="<?php echo $product->image_url; ?>" alt="" title="" align="left">
			<a href="<?php echo $product->link; ?>"><?php echo $product->name; ?></a>
			<div class="description"><br>
			<?php echo $product->description; ?>
			</div>
		</td>
		<td align="center"><?php echo CurrencySys::exchange($price).' '.CurrencySys::getLabel(); ?></td>
		<td align="center"><?php echo $count; ?></td>
		<td align="center"><?php echo CurrencySys::exchange($amount).' '.CurrencySys::getLabel(); ?></td>
	</tr>
	<?php endforeach;?>
	<tr class="complect-sum-row">
		<td>&nbsp;</td>
		<td colspan="3" class="complect-sum">
			<div class="complect-sum-row">
				<div class="words">Итоговая цена в рознице:</div>
				<div class="cyfres">
					<p class="crossing"><?php echo CurrencySys::exchange($sum).' '.CurrencySys::getLabel(); ?></p>
					<?php if (!CurrencySys::isDefaultInCookie()):?>
						<p class="grey"><?php echo CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY).' '.$sum; ?></p>
					<?php endif; ?>
				</div>
			</div>
			<div class="complect-sum-row">
				<div class="words">Цена в комплекте:</div>
				<div class="cyfres">
					<p class="red"><?php echo CurrencySys::exchange($discountSum).' '.CurrencySys::getLabel(); ?></p>
					<?php if (!CurrencySys::isDefaultInCookie()):?>
						<p class="grey"><?php echo CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY).' '.$discountSum; ?></p>
					<?php endif; ?>
				</div>
			</div>
			<div class="complect-sum-row">
				<div class="words bold">Вы экономите:</div>
				<div class="cyfres bold"><?php echo CurrencySys::exchange($sum - $discountSum).' '.CurrencySys::getLabel(); ?></div>
			</div>
		</td>
	</tr>
	</tbody>
</table>