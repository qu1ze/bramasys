<?php
/**
 * CallbackForm.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 8:37 PM
 */

class CallbackForm extends CFormModel
{
	const CALLBACK_MAIL = 'sales@bramasys.com.ua';
	
	public $phone = null;
	public $name = null;
	public $subject = null;
	
	/**
	 * Model rules
	 * @return array
	 */
	public function rules() {
		return array(
			array('phone, name, subject', 'required'),
			array('name, subject', 'length', 'max' => 255),
		);
	}

	/**
	 * Returns attribute labels
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'phone' => Yii::t('labels', 'Номер телефона'),
			'name' => Yii::t('labels', 'Ваше имя'),
			'subject' => 'Тема вопроса',
		);
	}
	
	public function send()
	{
        $mails = MailTemplate::model()->findAllByAttributes(array('name' => 'callback'));
        foreach($mails as $mail)
        {
            $mail->send(array(
                '$name' => $this->name,
                '$phone' => $this->phone,
                '$address' => $this->subject,
            ));
        }
		return true;
	}
}
