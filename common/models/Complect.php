<?php

/**
 * This is the model class for table "complect".
 *
 * The followings are the available columns in table 'complect':
 * @property integer $id
 * @property string $name
 * @property string img_url
 * @property integer $category_id
 * @property integer $actual_id
 * @property string $full_description
 * @property double $discount
 * 
 * Goods fields
 * @property boolean $inCompare
 * @property boolean $inCart
 * @property string $image_url
 * @property boolean $hasLabels
 * @property string $link
 * @property integer $rating
 * @property boolean $inBookmarks
 * @property string $categoryName
 * @property boolean $isComplect
 * @property string $cartPattern
 * @property string $description
 * @property integer $quantity
 * @property double $price
 * @property double $old_price
 * @property integer $commentsCount
 * @property boolean $hasComments
 * 
 * The followings are the available model relations:
 * @property Comment[] $comments
 * @property Category $category
 * @property ComplectProduct[] $complectProducts
 */
class Complect extends ActiveRecord implements Goods
{
	
	private $complectProduct = '';
	private $complectProductList = '';
	private $rating = null;
    private $votedCount = null;
	private $price = null;
    private $commentsArray = array();
    private $hasComments = null;
    private $photoArray = array();
    private $photosTitleAndAlt = array();

    public function getImage_url()
    {
        if(empty($this->img_url))
            $this->img_url = '/images/layout/noimg.png';
        return $this->img_url;
    }
	
	public function getDiscount()
	{
		return $this->discount;
	}
	
	public function setComplectProductsField($value)
	{
		$this->complectProduct = $value;
	}
	
	public function getComplectProductsField()
	{
        if(empty($this->complectProduct))
            $this->complectProduct = $this->getComplectProductList();
        return $this->complectProduct;
	}
	
	public function getQuantity()//TODO
	{
		return 1;
	}
	
	public function getNotDiscountPrice()
	{
		if(empty($this->price))
		{
			$price = 0;
			$complectProduct = ComplectProduct::model()->findAllByAttributes(array('complect_id' => $this->id));
			$complectProductList = array();
			foreach ($complectProduct as $complectProductModel)
			{
				array_push($complectProductList, $complectProductModel->product_id);
			}

			$products = Products::model()->findAllByAttributes(array('id' => $complectProductList));

			foreach ($complectProduct as $complectProductModel)
			{
				$productPrice = 0;
				foreach ($products as $product)
				{
					if($product->id == $complectProductModel->product_id)
					{
						$productPrice = $product->price;
						break(1);
					}
				}
				$price += $productPrice * $complectProductModel->count;
			}
			$this->price = $price;
		}
		return $this->price;
	}
	
	public function getPrice()
	{
		return round(100 * $this->getNotDiscountPrice() / 100 * (100 - $this->discount)) / 100;
	}
	
	public function getComplectProductList()
	{
		if(empty($this->complectProductList))
		{
			$complectProduct = ComplectProduct::model()->findAllByAttributes(array('complect_id' => $this->id), array('order' => '`order` ASC'));
			$complectProductList = array();
			foreach ($complectProduct as $complectProductModel)
			{
				array_push($complectProductList, $complectProductModel->product_id);
			}

			$products = Products::model()->findAllByAttributes(array('id' => $complectProductList));
			$models = CHtml::listData($products, 'id', 'model');

			$brands = CHtml::listData(Brand::model()->findAll(), 'id', 'name');
			$brandsIds = CHtml::listData($products, 'id', 'brand');
			$productValue = '';
			foreach ($complectProduct as $complectProductModel)
			{
				$brandId = $brandsIds[$complectProductModel->product_id];
				$productValue .= $brands[$brandId].' '.$models[$complectProductModel->product_id].' ('.$complectProductModel->count.' шт.)<br>';
			}
			$this->complectProductList = $productValue;
		}
		return $this->complectProductList;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Complect the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'complect';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, category_id, actual_id', 'required'),
            array('name, full_description', 'filter', 'filter'=>'trim'),
			array('name, img_url', 'length', 'max'=>255),
			array('category_id, actual_id', 'numerical', 'integerOnly'=>true),
            array('discount', 'numerical'),
            array('full_description', 'safe'),
            array('actual_id','unique', 'message'=>'Продукт с таким кодом существует (необходим уникальный код).'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_id, name, actual_id, full_description, discount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'comments' => array(self::HAS_MANY, 'Comment', 'complect_id'),
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'complectProducts' => array(self::HAS_MANY, 'ComplectProduct', 'complect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'category_id' => 'Категория',
            'actual_id' => 'Код',
            'full_description' => 'Подробное описание',
            'discount' => 'Скидка',
            'complectProductsField' => 'Продукты',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$comlectIds = array();
		if(!empty($this->complectProduct))
		{
			$criteria=new CDbCriteria;
			$nodes = explode(' ', $this->complectProduct);//Geting brandName and model
			$criteria->with='brand0';
			$criteria->select = 'id';
			if(sizeof($nodes) > 1)
			{
				$criteria->addSearchCondition('brand0.name', $nodes[0]);
				$criteria->addSearchCondition('model', $nodes[1]);
			}
			else if(sizeof($nodes) > 0)
			{
				$criteria->addSearchCondition('brand0.name', $nodes[0]);
				$criteria->addSearchCondition('model', $nodes[0], true, 'OR');
			}
			$product = Products::model()->findAll($criteria);
			if($product !== null)
			{
				$productsIds = array();
				foreach ($product as $productModel)
				{
					array_push($productsIds, $productModel->id);
				}
				$complectProduct = ComplectProduct::model()->findAllByAttributes(array('product_id' => $productsIds));
				if($complectProduct !== null)
				{
					foreach ($complectProduct as $complectProductModel)
					{
						array_push($comlectIds, $complectProductModel->complect_id);
					}
				}
			}
			if(empty($comlectIds))
				$comlectIds = false;
		}
		
		$criteria=new CDbCriteria;
		
		if($comlectIds === false)
			$criteria->compare('id', 0);
		else
			$criteria->compare('id', !empty($comlectIds) ? $comlectIds : $this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('category_id',$this->category_id);
        $criteria->compare('actual_id',$this->actual_id);
        $criteria->compare('full_description',$this->full_description,true);
        $criteria->compare('discount',$this->discount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getDescription() {
		return $this->getComplectProductList();
	}

	public function getHasLabels() {
		return false;
	}

	public function getInCart() {
		return isset(Yii::app()->session['cart']['complect'][$this->id]);
	}

	public function getInCompare() {
		return false;
	}

	public function getLink() {
		return app()->getBaseUrl(true)."/complect/".(($this->actual_id!==null)?$this->actual_id:$this->id);
	}

	public function getName() {
		return $this->name;
	}

	public function getOld_price() {
		return 0;
	}

	public function getRating() {
		if($this->rating == null)
		{
			$rating = 0;
			$comments = Comment::model()->findAll("`complect_id` = $this->id AND rating != 0 AND status = 1");
			if(!empty($comments))
			{
				foreach ($comments as $comment)
				{
					$rating += $comment->rating;
				}
				$rating = round($rating / sizeof($comments), 1);

			}
			$this->rating = $rating;
		}
		return $this->rating;
	}

	public function getInBookmarks() {
		$bookmarks = json_decode(Yii::app()->request->cookies['bookmarks'], true);
		if(isset($bookmarks['complect']))
		{
			return in_array($this->id, $bookmarks['complect']);
		}
		return false;
	}

	public function getCategoryName() {
		return Yii::app()->categoryController->getTitle($this->category_id);
	}

	public function getCartPattern()
	{
		return 'complPd'.$this->id;
	}

	public function getIsComplect()
	{
		return 1;
	}

	public function getH1()
	{
        $pathInfo = explode("/page/", Yii::app()->request->pathInfo);
		$metadata = CategoryMetadata::model()->findByAttributes(array('url' => "/".$pathInfo[0]));
		return !empty($metadata->H1) ? $metadata->H1 : str_replace('AAA', $this->name, $this->category->H1);
	}

	public function getMetaDescription()
	{
		return str_replace('AAA', $this->name, $this->category->product_description);
	}

	public function getMetaKeywords()
	{
		return str_replace('AAA', $this->name, $this->category->product_keywords);
	}

	public function getMetaTitle()
	{
		return str_replace('AAA', $this->name, $this->category->product_title);
	}

    public function getHasComments()
    {
        if($this->hasComments == null)
        {
            $this->commentsArray = Comment::model()->findAllByAttributes(array('complect_id' => $this->id, 'parent_id' => null, 'status' => 1));
            $this->hasComments = !empty($this->commentsArray);
        }
        return $this->hasComments;
    }

    public function getCommentsCount()
    {
        if(empty($this->commentsArray))
        {
            $this->commentsArray = array();
            if(!$this->getHasComments())
                return 0;
        }
        return sizeof($this->commentsArray);
    }

    public function getActualId()
    {
        return !empty($this->actual_id) ? $this->actual_id : $this->id;
    }

    private function loadPhotos()
    {
        $this->photoArray = array();
        $this->photosTitleAndAlt = array();
        foreach(ComplectProduct::model()->findAllByAttributes(array('complect_id' => $this->id), array('order' => '`order` ASC')) as $complectProduct)
        {
            $this->photoArray = CMap::mergeArray($this->photoArray, $complectProduct->product->photoArray);
            $this->photosTitleAndAlt = CMap::mergeArray($this->photosTitleAndAlt, $complectProduct->product->photosTitleAndAlt);
        }
    }

    public function getPhotoArray()
    {
        if(empty($this->photoArray))
        {
            $this->photoArray = array();
            foreach(ComplectProduct::model()->findAllByAttributes(array('complect_id' => $this->id), array('order' => '`order` ASC')) as $complectProduct)
            {
                $this->photoArray = CMap::mergeArray($this->photoArray, $complectProduct->product->photoArray);
            }
        }
        return $this->photoArray;
    }

    public function getPhotosTitleAndAlt()
    {
        if(empty($this->photosTitleAndAlt))
        {
            $this->loadPhotos();
        }
        return $this->photosTitleAndAlt;
    }

    protected function afterConstruct()
    {
        $this->discount = null;
        parent::afterConstruct();
    }

    public function getProductsId()
    {
        $ids = array();
        foreach($this->complectProducts as $complectProducts)
        {
            array_push($ids, $complectProducts->product_id);
        }
        return $ids;
    }

    public function getPhotoCount()
    {
        $count = 0;
        foreach($this->complectProducts as $complectProducts)
        {
            $count += $complectProducts->product->photoCount;
        }
        return $count;
    }


    public function getVotedCount()
    {
        if($this->votedCount == null)
        {
            $count = 0;
            $comments = Comment::model()->findAll("`complect_id` = $this->id AND rating != 0 AND status = 1");
            if(!empty($comments))
            {
                $count++;
            }
            $this->votedCount = $count;
        }
        return $this->votedCount;
    }
}