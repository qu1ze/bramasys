<?php

	$this->checkFlashes();
	
	$this->widget('bootstrap.widgets.TbButton', array(		
		'size' => 'normal',
		'label' => 'Change View',
		'url' => '?mode=' . ($mode == 'table' ? 'tree' : 'table')
	)); 
		
	if ($mode == 'tree')
		$this->renderPartial('_index_tree', array(
			'tree' => $tree,
			'list' => $list,			
		));
	else		
		$this->renderPartial('_index_table', array(
			'model' => $model,			
		));
?>