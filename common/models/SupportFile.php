<?php

/**
 * This is the model class for table "support_file".
 *
 * The followings are the available columns in table 'support_file':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $full_description
 * @property integer $category_id
 * @property integer $for_category_id
 * @property integer $brand_id
 * @property integer $type
 * @property string $path
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property Brand $brand
 */
class SupportFile extends ActiveRecord
{
	const SUPPORT_FILE_TYPE_UNKNOWN = 0;
	const SUPPORT_FILE_TYPE_XLS_XLSX = 1;
	const SUPPORT_FILE_TYPE_EXE = 2;
	const SUPPORT_FILE_TYPE_JPG_JPEG = 3;
	const SUPPORT_FILE_TYPE_DOC_DOCX = 4;
	const SUPPORT_FILE_TYPE_PDF = 5;
	
	private $supportFileTypes = array(
		self::SUPPORT_FILE_TYPE_XLS_XLSX => 'xls, xlsx',
		self::SUPPORT_FILE_TYPE_EXE => 'exe',
		self::SUPPORT_FILE_TYPE_JPG_JPEG => 'jpg, jpeg',
		self::SUPPORT_FILE_TYPE_DOC_DOCX => 'doc, docx',
		self::SUPPORT_FILE_TYPE_PDF => 'pdf',
	);
	
	private $supportFileImages = array(
		self::SUPPORT_FILE_TYPE_XLS_XLSX => 'logo-microsoft-excel.png',
		self::SUPPORT_FILE_TYPE_EXE => 'exe.png',
		self::SUPPORT_FILE_TYPE_JPG_JPEG => 'jpeg-icon.png',
		self::SUPPORT_FILE_TYPE_DOC_DOCX => 'logo-microsoft-word.png',
		self::SUPPORT_FILE_TYPE_PDF => 'pdf.png',
	);
	
	public $file;
	public $relevant;
	private $basePath = '';
	private $relativePath = '';
	private $uploader = '';
	
	public function setPaths()
	{
		$this->basePath = Yii::app()->basePath.DIRECTORY_SEPARATOR.'www';
		$this->relativePath = DIRECTORY_SEPARATOR.'supportFiles'.DIRECTORY_SEPARATOR;
	}
	
	protected function afterConstruct() {
		parent::afterConstruct();
		$this->setPaths();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SupportFile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'support_file';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, full_description, category_id, for_category_id, brand_id', 'required'),
            array('name, description, full_description', 'filter', 'filter'=>'trim'),
			array('name', 'unique'),
			array('file', 'file', 'types'=>'exe, jpg, jpeg, xls, xlsx, doc, docx, pdf', 'on' => 'insert'),
			array('category_id, for_category_id, brand_id, type', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('description, path', 'length', 'max'=>255),
			array('full_description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, full_description, category_id, for_category_id, brand_id, type, path', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'for_category' => array(self::BELONGS_TO, 'Category', 'for_category_id'),
			'brand' => array(self::BELONGS_TO, 'Brand', 'brand_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'description' => 'Краткое описание',
			'full_description' => 'Полное описание',
			'category_id' => 'Категория',
			'for_category_id' => 'Для категории',
			'brand_id' => 'Бренд',
			'type' => 'Тип',
			'path' => 'Путь',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('full_description',$this->full_description,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('for_category_id',$this->category_id);
		$criteria->compare('brand_id',$this->brand_id);
		$criteria->compare('type',$this->type);
		$criteria->compare('path',$this->path,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getSupportFileTypes($type = null)
	{
		return is_null($type) ? $this->supportFileTypes : $this->supportFileTypes[$type];
	}
	
	public function getPageUrl()
	{
		return app()->categoryController->getCategoryAddress('instruction_and_software').'/'.$this->id;
	}
	
	public function getImage()
	{
		if(empty($this->type))
			return '';
		return DS.'images'.DS.'fileTypes'.DS.$this->supportFileImages[$this->type];
	}
	
	public function getFullPath()
	{
		if(empty($this->relativePath))
			$this->setPaths();
		return $this->relativePath.$this->path;
	}
	
	protected function beforeValidate()
	{
		if(empty($this->path))
			$this->path = 0;
		if(empty($this->type))
		{
			$this->uploader = CUploadedFile::getInstance($this, 'file');
			if($this->uploader !== null)
			{
				switch ($this->uploader->extensionName)
				{
					case 'xls':
					case 'xlsx':
						$this->type = self::SUPPORT_FILE_TYPE_XLS_XLSX;
					break;
					case 'doc':
					case 'docx':
						$this->type = self::SUPPORT_FILE_TYPE_DOC_DOCX;
					break;
					case 'jpg':
					case 'jpeg':
						$this->type = self::SUPPORT_FILE_TYPE_JPG_JPEG;
					break;
					case 'exe':
						$this->type = self::SUPPORT_FILE_TYPE_EXE;
					break;
					case 'pdf':
						$this->type = self::SUPPORT_FILE_TYPE_PDF;
					break;
					default :
						$this->type = self::SUPPORT_FILE_TYPE_UNKNOWN;
					break;
				}
			}
		}
		return parent::beforeValidate();
	}
	
	protected function beforeSave()
	{
		if(empty($this->path))
		{
			$this->path = $this->name.'.'.$this->uploader->extensionName;
			if(!$this->uploader->saveAs($this->basePath.$this->relativePath.$this->path))
				return false;
		}
		return parent::beforeSave();
	}
	
	protected function beforeDelete()
	{
		$this->setPaths();
		@unlink($this->basePath.$this->relativePath.$this->path);
		return parent::beforeDelete();
	}
}