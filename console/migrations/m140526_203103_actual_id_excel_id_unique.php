<?php

class m140526_203103_actual_id_excel_id_unique extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE  `products` ADD UNIQUE (`excel_id`);
                        ALTER TABLE  `complect` ADD UNIQUE (`actual_id`);");
	}

	public function down()
	{
		$this->execute("ALTER TABLE products DROP INDEX excel_id;"
            . "ALTER TABLE complect DROP INDEX actual_id;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}