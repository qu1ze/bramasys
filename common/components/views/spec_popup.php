<?php
Yii::app()->clientScript->registerScript("showSpecPopup", "
	$('body').on('click', '.question', function(e) {
		var posX = e.pageX;
		var posY = e.pageY;
		var question = $(this);
		$('#specPopup').stop().fadeOut(100, function()
		{
			$.ajax(
			{
				url: '/site/ajaxGetSpecDesc',
				data: {id: $(question).attr('spec-id')},
				dataType: 'json',
				type: 'get',
				success: function(data){
					if(data.title == 'Success')
					{
					showPopUp(data, posX, posY);	
					}
				}
			});
		});
		return false;
	}); 
        
        $('body').on('click', '.pd-add-btn-block ul li', function(e) {
		var posX = $(this).position().left+($(this).width()/2);
		var posY = $(this).position().top+$(this).height();
		var question = $(this).attr('pageName');
		$('#specPopup').stop().fadeOut(100, function()
		{
			$.ajax(
			{
				url: '/products/ajaxGetMesage',
				data: {pageName: question},
				dataType: 'json',
				type: 'get',
				success: function(data){
					if(data.title == 'Success')
					{
					showPopUp(data, posX, posY);	
					}
				}
			});
		});
		return false;
	}); 

        function showPopUp(data, posX, posY)
        {
            $('#specPopup span').html(data.desc);
            var width = $('#specPopup').css('width').substr(0, $('#specPopup').css('width').length - 2);
            var height = $('#specPopup').css('height').substr(0, $('#specPopup').css('height').length - 2);
            var offsetLeft = posX - width;
            var offsetTop = posY + 10;
            if(offsetLeft < 50)
            {
                    $('#specPopup').css('left', posX - 20);
                    $('#specPopupTail').css('right', '');
                    $('#specPopupTail').css('left', 10);
            }
            else
            {
                    $('#specPopup').css('left', offsetLeft);
                    $('#specPopupTail').css('right', 10);
                    $('#specPopupTail').css('left', '');
            }
            $('#specPopup').css('top', offsetTop);
            $('#specPopup').fadeIn();
        }

	$('body').on('click', '#closeSpecPopup', function(){
		$('#specPopup').stop().fadeOut();
		return false;
	});

	$('body').on('click', function(e){
		if(e.target.id !== 'specPopup' && !$(e.target).parents().is('#specPopup') && !$(e.target).parents().is('.pd-add-btn-block'))
		{
			$('#specPopup').stop().fadeOut();
		}
	});
", CClientScript::POS_READY);
?>
<div id="specPopup" style="display: none">
	<div id="specPopupTail"></div>
	<span></span>
	<a class="close" id="closeSpecPopup" href="#"></a>
</div>