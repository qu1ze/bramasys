<?php

class m131128_223748_add_hiden_pages extends CDbMigration
{
//    public function up()
//    {
//
//    }
//
//    public function down()
//    {
//        echo "m131128_223748_add_hiden_pages does not support migration down.\n";
//        return false;
//    }

	/*
	// Use safeUp/safeDown to do migration with transaction*/
	public function safeUp()
	{
           $categories = array(
            'minioplata',
            'minidostavka',
            'minivosvrat',
            'minigarantia',
            'minimontazh',
            );
           $contents = array(
            'minioplata' => 'Оплата осуществляется одним из следующих способов:&nbsp;<br>
- Для физ. лиц и&nbsp;частных&nbsp;предпринимателей: на р/сч. СПД (ФОП) на основании 
счета, при помощи карты Visa/MasterCard или наличными в кассу продавца;&nbsp;<br>
<span style=\"text-align: -webkit-auto;\">- Для юр. лиц:</span>&nbsp;на р/сч. ООО (с НДС) 
           на основании счета.<br><div style="text-align: right;">
           <a href="/oplata_dostavka">Подробнее</a></div>',
            'minidostavka' => 'Доставка осуществляется:&nbsp;<br>
- Новой Почтой (по всей Украине);<br>
- Курьером (Днепропетровск,&nbsp;Запорожье,&nbsp;Киев);<br>
- Самовывозом (Запорожье). &nbsp;<br>

<div style="text-align: right;">
           <a href="/oplata_dostavka">Подробнее</a></div>' ,
            'minivosvrat' => 'Вы можете вернуть или обменять товар&nbsp;в течение 14-ти дней с момента покупки,&nbsp;в случае если он вам не подошел.&nbsp;<br>
<div style="text-align: right;">
           <a href="/vozvrat_garantiya">Подробнее</a></div>',
            'minigarantia' => 'Гарантия на все оборудование - от 12-ти до 36-ти месяцев.<br>
<div style="text-align: left;">
  <div style="text-align: right;">
           <a href="/vozvrat_garantiya">Подробнее</a></div>',
            'minimontazh' => 'Все&nbsp;оборудование,&nbsp;приобретенное&nbsp;у компании BramaSys, может быть установлено (смонтировано)
нашими специалистами - в городах Запорожье и Днепропетровске.<br>
<div style="text-align: right;">
           <a href="/uslugi">Подробнее</a></div>',
            );
        
        $category = new Category();
        $category->name = 'hidden_container';
        $category->title = 'hidden';
        $category->tag = CategoryTag::model()->findByAttributes(array('name'=>'static'))->id;
        if ($category->save()) {
            $id = $category->id;
            foreach ($categories as $categoryname){
                $category = new Category();
                $category->name = $categoryname;
                $category->title = $categoryname;
                $category->parent = $id;
                if (!$category->save())  return false;
                else {
                    $page = new StaticPage();
                    $page->category_id = $category->id;
                    $page->content = $contents[$categoryname];
                    if (!$page->save())  return false;
                }
            }
            
        }
        else return false;
        
        
	}

	public function safeDown()
	{
       $categories = array(
            'minioplata',
            'minidostavka',
            'minivosvrat',
            'minigarantia',
            'minimontazh',
            );
         
            foreach (Category::model()->findAllByAttributes(array('name' => $categories)) as $category)
            {
                $parent = $category->parent_category;
                
                if (!$category->staticPages[0]->delete()) {
                    
                    return false;
                }
                
                if(!$category->delete()) return false;
            }
            if(!$parent->delete()) return false;
       
	}
	
}