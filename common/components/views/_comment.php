<ul <?php echo $level > 15 ? 'class="comments"' : ''?>>
<?php

foreach ($comments->getChilds($parentId) as $rootComment):

$comment = $rootComment['data'];
?>
<li class="comment" id="comment<?php echo $comment->id; ?>" style="position: relative; <?php echo $this->commentCount > 1 ? 'display:none;"' : '"'; ?>>
		<?php if(!$comment->status): ?>
				<div style="
					position: absolute;
					width: 100%;
					height: 100%;
					margin: 0;
					background-color: rgba(237, 245, 244, 0.7);
          box-shadow: 0 0 10px #EDF5F4;
          z-index:1;
				">
					<table style="height: 100%;">
						<tr>
							<td style="text-align: center; font: 18px 'PTSN Bold';">
								Ваш отзыв будет доступен после проверки модератором
							</td>
						</tr>
					</table>
				</div>
		<?php endif; ?>
  <div class="comment-container">
		
    <div class="comment-author-name">
			<?php echo $comment->authorName; ?>,<span class="comment-post-date"><?php echo $comment->post_date; ?></span>
		</div>
		<?php if($comment->rating > 0): ?>
			<div class="comment-rating">
				<font style="float:left;">Оценка товара:</font>
				<?php 
				$starsCount = $comment->rating;
				for($i = 0; $i < 5; $i++): ?>
				<div class="star <?php echo $starsCount > 0 ? 'on' : ''; ?>"><span style="width: 100%;"><?php echo $i; ?></span></div>
				<?php 
				$starsCount--;
				endfor; ?>
			</div>
		<?php endif; ?>
		<div class="comment-content">
			<font style="float:left;">Комментарий:</font>
			<span><?php echo $comment->content; ?></span>
		</div>
	
		<?php if($level < 15): ?>
		<div class="comment-response">
			<a class="respone-link<?php echo $comment->id; ?>" comment-id="<?php echo $comment->id; ?>">Ответить</a>
		</div>
		<?php endif; ?>
	</div>
	<?php
	$this->commentCount++;
	$this->render('_comment', array(
			'comments' => $comments,
			'parentId' => $rootComment['id'],
			'level' => $level + 1,
			)
		);
	?>
</li>
<?php endforeach; ?>
</ul>