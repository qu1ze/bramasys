<?php

/**
 * This is the model class for table "related_article".
 *
 * The followings are the available columns in table 'related_article':
 * @property integer $id
 * @property integer $article_id
 * @property integer $related_article
 *
 * The followings are the available model relations:
 * @property Article $relatedArticle
 * @property Article $article
 */
class RelatedArticle extends ActiveRecord
{
    public $related_article_s = array(); 
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RelatedArticle the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'related_article';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('article_id, related_article', 'required'),
			//array('article_id, related_article', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, article_id, related_article', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'relatedArticle' => array(self::BELONGS_TO, 'Article', 'related_article'),
			'article' => array(self::BELONGS_TO, 'Article', 'article_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'article_id' => 'Статья',
			'related_article' => 'Связанная стат',
            'related_article_s' => 'Связанная статьи',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('article_id',$this->article_id);
		$criteria->compare('related_article',$this->related_article);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeValidate()
	{
		$r = parent::beforeValidate();
				
		if (!$this->article_id)		
			$this->addError('article_id', 'Oops!');	
		
		if (!$this->related_article)		
			$this->addError('related_article', 'Oops!');		
		
		return true;
	}
     public function saveRrelateds($related_articles, $article_id, $update=false)
    {
            $error=false;            
            $transaction = Yii::app()->db->beginTransaction(); // Transaction begin
            if($update) RelatedArticle::model()->deleteAllByAttributes(array('article_id' => $article_id));
            try{
                foreach ($related_articles as $value)
                {
                    $model = new RelatedArticle();
                    $model->article_id = $article_id;
                    $model->related_article = $value;
                    if (0==RelatedArticle::model()->countByAttributes(array('article_id'=>$article_id, 'related_article' => $value))){
                        $error=  !$model->save() | $error;  
                    }
                }
                if (!$error){
                    $transaction->commit();
                    return true;
                }
            }
            catch (Exception $e){
                            $transaction->rollBack();
            }
            return false;
    }
}