<h1 style="font-size: 20px;">Информация о заказе № <?php  echo $order->id; ?>:</h1>
<div class="form-horizontal" style="line-height: 30px;">
	<div class="control-group">
		<label class="control-label" for="">Доставка</label>
		<div class="controls">
			 <?php 
			 $deliveryTypes = DeliveryType::getList();
			 echo $deliveryTypes[$order->delivery_type_id]; ?>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="">Оплата</label>
		<div class="controls">
			 <?php 
			 $paymentTypes = PaymentType::getList();
			 echo $paymentTypes[$order->payment_type_id]; ?>
            
		</div>
       <?php if($order->payment_type_id == 1 || $order->payment_type_id == 2) : ?>
        <label class="control-label" for="">Статус оплаты</label>
		<div class="controls">
			 <?php  echo $order->paystatvar[$order->paystatus]; ?>
            
		</div>
        <?php endif; ?>
        
        <?php if($order->payment_type_id == 5 ) : ?>
        <label class="control-label" for="">Плательшик</label>
		<div class="controls">
			 <?php  echo $order->company_name; ?>
            
		</div>
        
        <?php endif; ?>
	</div>
	<?php if(!empty($order->comment)): ?>
	<div class="control-group">
		<label class="control-label" for="">Комментарий</label>
		<div class="controls">
			 <?php echo $order->comment ?>
		</div>
	</div>
	<?php endif; ?>
</div>
<h1 style="font-size: 20px;">Клиент:</h1>
<div class="form-horizontal" style="line-height: 30px;">
	<div class="control-group">
		<?php echo CHtml::label($client->getAttributeLabel('name'), '', array('class' => 'control-label'));?>
		<div class="controls">
			 <?php echo $client->name == $client->email ? '-' : $client->name; ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo CHtml::label($client->getAttributeLabel('lastname'), '', array('class' => 'control-label'));?>
		<div class="controls">
			 <?php echo $client->lastname == null ? '-' : $client->lastname; ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo CHtml::label($client->getAttributeLabel('middle_name'), '', array('class' => 'control-label'));?>
		<div class="controls">
			 <?php echo $client->middle_name == null ? '-' : $client->middle_name; ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo CHtml::label(User::model()->getAttributeLabel('email'), '', array('class' => 'control-label'));?>
		<div class="controls">
			 <?php echo $client->email == null ? '-' : $client->email; ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo CHtml::label($client->getAttributeLabel('phone'), '', array('class' => 'control-label'));?>
		<div class="controls">
			 <?php if($client->phone == null):
				  echo '-';
			 else:
				 foreach ($client->phones as $phone): ?>
					<?php echo $phone; ?><br>
				 <?php endforeach;
			 endif; ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo CHtml::label($client->getAttributeLabel('delivery_address'), '', array('class' => 'control-label'));?>
		<div class="controls">
			 <?php echo $order->delivery_address == null ? '-' : $order->delivery_address; ?>
		</div>
	</div>
</div>
<hr><h1 style="font-size: 20px;">Товары:</h1>
<table class="cart-table">
<thead>
	<tr class="header">
        <td class="product-img-cart">Товар</td>
		<td class="product-in-complect">В комплекте</td>
        <td class="product-price-cart">Стоимость</td>
        <td class="input-cart">Кол-во</td>
        <td class="checkbox-cart">Монтаж</td>
        <td class="product-price-cart">Сумма</td>
    </tr>
</thead>
<tbody>
	<?php
	$priceSum = 0;
	foreach ($orderProducts as $key => $prod)
	{
		if($prod->is_complect)
		{
			$complect = Complect::model()->findByPk($prod->product_id);
			$complectProducts = ComplectProduct::model()->findAllByAttributes(array('complect_id' => $complect->id));
			foreach ($complectProducts as $complectProd)
			{
				$product = Products::model()->findByPk($complectProd->product_id);
				$complectProdArray = array(
					'product_id' => $complectProd->product_id,
					'price' => round(100 * $product->price / 100 * (100 - $complect->discount)) / 100,
					'count' => $complectProd->count * $prod->count,
					'mounting' => $prod->mounting,
					'is_complect' => 0,
				);
				$amount = $complectProdArray['price'] * $complectProdArray['count'];
				$priceSum += $amount;
				$this->renderPartial('_product', array(
							'product' => $product,
							'amount' => $amount,
							'prod' => $complectProdArray,
							'inComplect' => $complect->name,
						));
			}
			unset($orderProducts[$key]);
		}
	}
	foreach ($orderProducts as $prod)
	{
		$product = Products::model()->findByPk($prod['product_id']);
		$amount = $prod['price'] * $prod['count'];
		$priceSum += $amount;
		$this->renderPartial('_product', array(
					'product' => $product,
					'amount' => $amount,
					'prod' => $prod,
					'inComplect' => 0,
				));
	}
	?>
</tbody>
</table>
<div class="total-cart">
	<div class="total-price-text">
		<p>Итого:</p>
		<p>(без учета доставки,
			без учета монтажа)</p>
	</div>

	<div class="total-price" >
		<p id="firstPrice">
			<?php echo CurrencySys::exchange($priceSum).' '.CurrencySys::getLabel(); ?>
		</p>
        <?php if (!CurrencySys::isDefaultInCookie()):?>
			<p id="lastPrice"><?php echo $priceSum.' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY)?></p>
		<?php endif; ?>
        <p id="firstPrice">
			по б/н <?php echo CurrencySys::exchange($priceSum , 2).' '.CurrencySys::getLabel(2); ?>
		</p>    
            
	</div>
</div>
