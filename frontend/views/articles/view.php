<?php
/*
 * <div> 		
			<span class="article-gray-text">
				Теги:
			</span>
			<?php echo $model->keywords ?>
		</div>
 */

?>
<div id="container">
	<?php	
		Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl(true) . '/css/article_view.css');
		$this->widget('zii.widgets.CBreadcrumbs', array(
			'links' => array(		
					'Техподдержка' => '/support',
					'Статьи, публикации, обзоры' => '/support/stati-publikatcii-obzory',
					$model->title => '/support/stati-publikatcii-obzory/' . $model->name,
				),
			'separator' => ' > ',
			'homeLink' => CHtml::link('Главная', Yii::app()->homeUrl)
		));
	?>
	<div class="article-tab">
		<h1>
			<?php echo $model->title ?>
		</h1>
		<div>
			<?php // <img src="<?php echo $model->thumb ? $model->thumb : '/images/article-img.jpg' ?/>" class="article_image" /?>
			<span class="article-gray-text">
				<?php echo date('d.m.Y', $model->update_time) ?><br />
			</span>
			
			<?php echo $model->text ?>
		</div>
		
		
		
		<?php $this->widget('common.components.ProductSlider', array('productsIds' => array_values($related_products), 'title' => '<h4>Товары упомянутые в статье:</h4>')); ?>
		
		<div class="related_articles">
			<h4>Читайте также:</h4>
			<?php foreach ($related_articles as $related_article_item): ?>
				<div class="related_article_item">
					<a href="<?php echo '/support/stati-publikatcii-obzory/' . $related_article_item->relatedArticle->title  ?>"><?php echo $related_article_item->relatedArticle->title  ?></a>
				</div>
			<?php endforeach ?>
		</div>
		
		<div class="clear"></div>
		<div class="name_article review-product bggradient">
            <div > <?php $this->widget('common.components.SocButtons'); ?></div>
						
			<?php $subject = urlencode($model->title)?>
			<?php $body = urlencode(Yii::app()->getBaseUrl(true) . '/support/stati-publikatcii-obzory/' . $model->name)?>
			<a href="mailto:?subject=<?php echo $subject?>&body=<?php echo $body?>">
				<img src="/images/mail-to.png">
			</a>
		
		</div>
		<div> 		
			<?php echo $model->views ?> просмотров
		</div>	
	</div>
	<div id="comment">
		<?php $this->widget('common.components.CommentTree', array('contentId' => $model->id, 'contentType' => 'article', 'plus_minus'=>false)); ?>
	</div>
</div>
