<?php

class BrandController extends Controller
{
	public $categoryName;
	public $categoryId;
	
	public static function getTag()
	{
		return 'static';
	}

	protected function beforeAction($action)
	{
		$this->categoryName = 'partners';
		$categoryController = app()->categoryController;
		if (is_bool($id = $categoryController->getCategoryId($this->categoryName)))
		{
            throw new CHttpException(404);
		}
		$this->categoryId = $id;
		return parent::beforeAction($action);
	}
	
	public function actionIndex($name)
	{
		$brand = Brand::model()->findByAttributes(array('name' => $name));
		if($brand !== null && $brand->isAnnounceable && $brand->partner)
		{
			$this->render('index', array(
				'model' => $brand,
			));
		}
	}
}
