<aside id="sideLeft">
	<ul class="navigation">
		<li><a href="<?php echo app()->getBaseUrl(true); ?>/user">Личный профиль</a></li>
		<li><a href="<?php echo app()->getBaseUrl(true); ?>/user/profile/bookmarks">Мои закладки</a></li>
		<li><a href="<?php echo app()->getBaseUrl(true); ?>/user/profile/mycart">Моя корзина</a></li>
		<li><a href="<?php echo app()->getBaseUrl(true); ?>/user/profile/myorders">Мои заказы</a></li>
		<li><a href="<?php echo app()->getBaseUrl(true); ?>/site/logout">Выйти</a></li>
	</ul>
</aside>
