<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property integer $excel_id
 * @property integer $actualId
 * @property integer $gallery_id
 * @property integer $brand
 * @property string $model
 * @property string $description
 * @property string $full_description
 * @property double $price
 * @property double $old_price
 * @property integer $category
 * @property integer $quantity
 * @property boolean $price_of_the_week
 * @property boolean $top_seller
 * @property boolean $novelty
 * @property boolean $hide
 * @property integer $photoCount
 * @property string[] $photoArray
 * 
 * Goods fields
 * @property boolean $inCompare
 * @property boolean $inCart
 * @property string $image_url
 * @property boolean $hasLabels
 * @property string $link
 * @property integer $rating
 * @property boolean $inBookmarks
 * @property string $categoryName
 * @property boolean $isComplect
 * @property string $cartPattern
 * @property integer $commentsCount
 * @property boolean $hasComments
 * 
 * The followings are the available model relations:
 * @property ArticleProduct[] $articleProducts
 * @property OrderProduct[] $orderProducts
 * @property ProductSpecValue[] $productSpecValues
 * @property Brand $brand0
 * @property Category $category0
 */
class Products extends ActiveRecord implements Goods
{
	const MIN_PRODUCT_PRICE = 0;
	const MAX_PRODUCT_PRICE = 100000;
	
	public $brand_id;
	public $brand_name;
	public $product_count;
	public $relevant;
	private $image_url = null;
	private $photoArray = array();
    private $photosTitleAndAlt = array();
	private $rating = null;
    private $votedCount = null;
	private $inCompare = null;
    private $commentsArray = array();
    private $hasComments = null;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	
        public function behaviors()
        {
            return array(
                'galleryBehavior' => array(
                    'class' => 'GalleryBehavior',
                    'idAttribute' => 'gallery_id',
                    'versions' => array(
                        'small' => array(
                            'quality' => 70,
                            'centeredpreview' => array(98, 98),
                            
                        ),
                        'medium' => array(
                            'quality' => 70,
                            'resize' => array(800, null),
                            
                        )
                    ),
                    'name' => true,
                    'description' => true,
                )
            );
        }
        
	public function getImage_url()
	{
		if(empty($this->image_url))
		{
			$galleryPhotos = GalleryPhoto::model()->findAllByAttributes(array('gallery_id' => $this->gallery_id), array('order'=>'rank'));
			if(!empty($galleryPhotos))
			{
				$this->image_url = DIRECTORY_SEPARATOR.GalleryPhoto::model()->galleryDir.DIRECTORY_SEPARATOR.'_'.$galleryPhotos[0]->id.'.jpg';
			}
			else
			{
				$this->image_url = '/images/layout/noimg.png';
			}
		}
		return $this->image_url;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brand, model, description, full_description, price, old_price, category, excel_id', 'required'),
            array('model, description, full_description', 'filter', 'filter'=>'trim'),
			array('brand, category, excel_id', 'numerical', 'integerOnly'=>true),
			array('quantity', 'numerical', 'integerOnly'=>true, 'allowEmpty'=>true),
            array('price_of_the_week, top_seller, novelty, hide', 'boolean'),
			array('price, old_price', 'numerical'),
			array('brand', 'uniqueWithModel'),
			array('description', 'length', 'max'=>255),
			array('model', 'length', 'max'=>255),
            array('excel_id','unique', 'message'=>'Продукт с таким кодом существует (необходим уникальный код).'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, brand, model, description, full_description, price, old_price, category, quantity, price_of_the_week, top_seller, novelty, hide', 'safe', 'on'=>'search'),
		);
	}

	public function uniqueWithModel()
	{
		if(($model = Products::model()->findByAttributes(array('brand' => $this->brand, 'model' => $this->model))) !== null)
		{
			if($model->id != $this->id)
				$this->addError('brand', 'Поля бренд-модель должны быть уникальными');
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'articleProducts' => array(self::HAS_MANY, 'ArticleProduct', 'product_id'),
			'orderProducts' => array(self::HAS_MANY, 'OrderProduct', 'product_id'),
			'productSpecValues' => array(self::HAS_MANY, 'ProductSpecValue', 'product_id'),
			'brand0' => array(self::BELONGS_TO, 'Brand', 'brand'),
			'category0' => array(self::BELONGS_TO, 'Category', 'category'),
			'gallery' => array(self::BELONGS_TO, 'gallery', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Сиc. id',
            'excel_id' => 'Код',
			'brand' => 'Бренд',
			'model' => 'Модель',
			'image_url' => 'Url картинки',
			'description' => 'Краткое описание',
			'full_description' => 'Подробное описание',
			'price' => 'Цена',
			'old_price' => 'Старая цена',
			'category' => 'Категория',
			'quantity' => 'Кол-во',
            'price_of_the_week' => 'Цена недели',
			'top_seller' => 'Лидер продаж',
			'novelty' => 'Новинка',
			'hide' => 'Скрыть',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('brand',$this->brand);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('full_description',$this->full_description,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('old_price',$this->old_price);
		$criteria->compare('category',$this->category);
		$criteria->compare('quantity',$this->quantity);
        $criteria->compare('price_of_the_week',$this->price_of_the_week);
		$criteria->compare('top_seller',$this->top_seller);
		$criteria->compare('novelty',$this->novelty);
		$criteria->compare('hide',$this->hide);
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function afterConstruct()
	{
		parent::afterConstruct();
		$this->price_of_the_week = null;
		$this->image_url = null;
		$this->top_seller = null;
		$this->novelty = null;
		$this->hide = null;
	}
	
	protected function beforeValidate()
	{
		if($this->full_description == null)
			$this->full_description = '-';
		if($this->price_of_the_week == null)
			$this->price_of_the_week = 0;
		if($this->top_seller == null)
			$this->top_seller = 0;
		if($this->novelty == null)
			$this->novelty = 0;
		if($this->hide == null)
			$this->hide = 0;
		if($this->image_url == null)
			$this->image_url = '/images/layout/noimg.png';
		return parent::beforeValidate();
	}
	
	public function getActualId()
	{
		if($this->excel_id != null)
			return $this->excel_id;
		return $this->id;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getHasLabels() {
		return true;
	}

	public function getInCompare() {
		if($this->inCompare == null)
		{
			$inCompare = 0;
			if(isset(Yii::app()->session['compare'][$this->category]))
			{
				$compareProducts = Yii::app()->session['compare'][$this->category];
				foreach ($compareProducts as $compareProduct)
				{
					if($compareProduct == $this->id)
					{
						$inCompare = 1;
						break(1);
					}
				}
			}
			$this->inCompare = $inCompare;
		}
		return $this->inCompare;
	}

	public function getLink() {
		return app()->getBaseUrl(true)."/".urlencode(strtolower($this->brand0->name))."/".urlencode(strtolower($this->model))."/".(($this->excel_id!==null)?$this->excel_id:$this->id);
	}

	public function getName() {
		return "{$this->brand0->name} $this->model";
	}

	public function getOld_price() {
		return $this->old_price;
	}

	public function getPrice() {
		return $this->price;
	}

	public function getQuantity() {
		return $this->quantity;
	}

	public function getInCart() {
		return isset(Yii::app()->session['cart'][$this->id]);
	}

	public function getRating() {
		if($this->rating == null)
		{
			$rating = 0;
			$comments = Comment::model()->findAll("`product_id` = $this->id AND rating != 0 AND status = 1");
			if(!empty($comments))
			{
				foreach ($comments as $comment)
				{
					$rating += $comment->rating;
				}
				$rating = round($rating / sizeof($comments), 1);

			}
			$this->rating = $rating;
		}
		return $this->rating;
	}

	public function getInBookmarks() {
		$bookmarks = json_decode(Yii::app()->request->cookies['bookmarks'], true);
		if(isset($bookmarks['product']))
		{
			return in_array($this->id, $bookmarks['product']);
		}
		return false;
	}

	public function getCategoryName() {
		return Yii::app()->categoryController->getTitle($this->category);
	}
	
	public function beforeDelete()
	{
		$r = parent::beforeDelete();
				
		if ($this->productSpecValues)		
			foreach ($this->productSpecValues as $productSpecValue)	
				$productSpecValue->delete();
		
		return $r;
	}

	public function getCartPattern()
	{
		return 'pd'.$this->id;
	}

	public function getIsComplect()
	{
		return 0;
	}

	public function getH1()
	{
        $pathInfo = explode("/page/", Yii::app()->request->pathInfo);
		$metadata = CategoryMetadata::model()->findByAttributes(array('url' => "/".$pathInfo[0]));
		return !empty($metadata->H1) ? $metadata->H1 : str_replace('AAA', $this->brand0->name, str_replace('XXX', $this->model, $this->category0->H1));
	}

	public function getMetaDescription()
	{
		return str_replace('AAA', $this->brand0->name, str_replace('XXX', $this->model, $this->category0->product_description));
	}

	public function getMetaKeywords()
	{
		return str_replace('AAA', $this->brand0->name, str_replace('XXX', $this->model, $this->category0->product_keywords));
	}

	public function getMetaTitle()
	{
		return str_replace('AAA', $this->brand0->name, str_replace('XXX', $this->model, $this->category0->product_title));
	}

    private function loadPhotos()
    {
        $this->photoArray = array();
        $this->photosTitleAndAlt = array();
        $galleryPhotos = GalleryPhoto::model()->findAllByAttributes(array('gallery_id' => $this->gallery_id), array('order'=>'rank'));
        foreach($galleryPhotos as $galleryPhoto)
        {
            array_push ($this->photoArray, DIRECTORY_SEPARATOR.GalleryPhoto::model()->galleryDir.DIRECTORY_SEPARATOR.'_'.$galleryPhoto->id.'.jpg');
            array_push ($this->photosTitleAndAlt, array(
                'alt' => $galleryPhoto->description,
                'title' => $galleryPhoto->name,
            ));
        }
        if(empty($galleryPhotos))
        {
            $this->photosTitleAndAlt = array(0, array('alt' => '', 'title' => '',));
        }
    }

	public function getPhotoArray()
	{
		if(empty($this->photoArray))
		{
            $this->loadPhotos();
		}
		return $this->photoArray;
	}

    public function getPhotosTitleAndAlt()
    {
        if(empty($this->photosTitleAndAlt))
        {
            $this->loadPhotos();
        }
        return $this->photosTitleAndAlt;
    }
	
	public function getPhotoCount()
	{
		return GalleryPhoto::model()->countByAttributes(array('gallery_id' => $this->gallery_id));
	}

    protected function afterSave()
    {
        $categorySpecs = CategorySpec::model()->findAllByAttributes(array('category_id' => app()->categoryController->AddParentsIds($this->category)));
        foreach ($categorySpecs as $categorySpec)
        {
            $productSpecValue = new ProductSpecValue();
            $productSpecValue->attributes = array('product_id' => $this->id, 'spec_id' => $categorySpec->spec_id);
            $productSpecValue->save();
        }
        return parent::afterSave();
    }

    public function getHasComments()
    {
        if($this->hasComments == null)
        {
            $this->commentsArray = Comment::model()->findAllByAttributes(array('product_id' => $this->id, 'parent_id' => null, 'status' => 1));
            $this->hasComments = !empty($this->commentsArray);
        }
        return $this->hasComments;
    }

    public function getCommentsCount()
    {
        if(empty($this->commentsArray))
        {
            $this->commentsArray = array();
            if(!$this->getHasComments())
                return 0;
        }
        return sizeof($this->commentsArray);
    }

    public function getVotedCount()
    {
        if($this->votedCount == null)
        {
            $count = 0;
            $comments = Comment::model()->findAll("`product_id` = $this->id AND rating != 0 AND status = 1");
            if(!empty($comments))
            {
                $count++;
            }
            $this->votedCount = $count;
        }
        return $this->votedCount;
    }
}