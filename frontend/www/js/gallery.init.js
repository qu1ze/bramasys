
function initAndShowGallery(indx, isDone, photosjson, galleria)
{
    indx = typeof indx !== 'undefined' ? indx : 0;
 if( isDone!=1)  { 
    //var photosjson = $("body").data("photosjson");
    var obj = jQuery.parseJSON(photosjson);
    $.each(obj, function (i, fb) {
            $('#item-for-carusel a').clone().appendTo('#galleria');
            //$('#carousel-small').last().attr('item_id',i);
            $('#galleria :last-child').attr('href','/gallery/'+fb.full);
            $('#galleria :last-child').children('img').attr('data-title',fb.name);
			$('#galleria :last-child').children('img').attr('data-description',fb.description);
            $('#galleria :last-child').children('img').attr('src','/gallery/'+fb.small);
            $('#galleria :last-child').children('img').attr('data-big','/gallery/'+fb.full);



        });

    $('#galleria').css('height','425px');
    Galleria.loadTheme('/js/galleria/classic/galleria.classic.min.js');
    Galleria.configure({
            show: indx,
            showInfo: false
            });
    Galleria.run('#galleria');
    $('.galleria-stage').appendTo('.galleria-container');
    $('#gallery div .close').on('click',function(){$('#gallery').fadeOut(200); return false;})
    $("body").data('galleria_done',1);
 }
 else
     {
       Galleria.ready(function(options) {
   galleria.show(indx);
 });        
     }

$('#gallery').fadeIn(200);
}