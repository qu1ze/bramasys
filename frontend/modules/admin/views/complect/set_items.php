<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js"></script>
<?php
    Yii::app()->getClientScript()->registerCssFile('/css/ui_autocomplete.css');

	Yii::app()->clientScript->registerScript("changeCount", "
        $('body').on('keyup', '.productCount', function() {
			var id = $(this).children(0).attr('rel');
			var count = $(this).children(0).val();
			if(!isNaN(parseFloat(count)) && isFinite(count) && count < 1)
			{
				count = 1;
			}
			if(id)
			{
				var url = '/admin/complect/ajaxChangeCount';
				$.ajax(
				{
					url: url,
					data: {Complect:{complectId: $currentComplect, productId: id, count: count}},
					dataType: 'json',
					type: 'post',
					success: function(data){
						if(typeof(data) == 'object')
						{
							brama.ui.notify(data.title, data.msg);
						}
					}
				});
			}
		});
    ", CClientScript::POS_READY);

    Yii::app()->clientScript->registerScript("removeProduct", "
        $('.form-horizontal').on('click', '.removeProduct', function(){
            var id = $(this).attr('value');
            var value = $('#removeProduct' + id + ' label').text();
            if (id)
            {
                var url = '/admin/complect/ajaxRemoveProduct';
                $.ajax(
                {
                    url: url,
                    data: {productId: id, complectId: $currentComplect},
					dataType: 'json',
                    type: 'get',
                    success: function(data){
                        $('#ComplectProduct_product_id').append('<option value=\"' + id + '\" >' + value + '</option>');
                        $('#removeProduct'+id).fadeOut('slow', function()
                        {
                            $('#removeProduct'+id).remove();
                        });
						brama.ui.notify(data.title, data.msg);
                    }
                });
            }
        });
    ", CClientScript::POS_READY);
        
    Yii::app()->clientScript->registerScript("addProduct", "
        $.widget(\"custom.catcomplete\", $.ui.autocomplete, {
            _renderMenu: function( ul, items ) {
                var that = this,
                currentCategory = '';
                $.each( items, function( index, item ) {
                    if ( item.category != currentCategory ) {
                        if($('ul[role=listbox] #cat-' + item.parentId).length < 1)
                            ul.append('<li id=\"cat-' + item.parentId + '\"><b>' + item.parentCategory + '</b></li>');
                        $(ul).append( '<li class=\"ui-autocomplete-category\"><i><b>' + item.category + '</b></i></li>' );
                        currentCategory = item.category;
                    }
                    that._renderItem(ul, item );
                });
            },
        });

		$('input[name=\"productBrand\"]').autocomplete({
			source: function(request, response) {
                $.ajax({
                  url: '/admin/products/getProductBrands',
                  dataType: 'json',
                  data: {
                    term : request.term,
					parentId : $this->parentId
                  },
                  success: function(data) {
                    response(data);
                  }
                });
            },
			minLength: 1,
		});

        $('input').click(function()
        {
            $(this).catcomplete('search', $(this).val());
        });

		$('input[name=\"productModel\"]').catcomplete({
			source: function(request, response) {
				var brandName = $('input[name=\"productBrand\"]').val();
                $.ajax({
                  url: '/admin/products/getProductModels',
                  dataType: 'json',
                  data: {
                    delay: 0,
                    term : request.term,
                    brandName : brandName,
					parentId : $this->parentId
                  },
                  success: function(data) {
                    response(data);
                  }
                });
            },
            open: function () {
                $(this).data('catcomplete').menu.element.addClass('sub-autocomplite');
            },
			minLength: 0,
		});
		
        $('.form-horizontal').on('click', '#buttonAdd', function(){
            var brand = $('input[name=\"productBrand\"]').val();
			var model = $('input[name=\"productModel\"]').val();
			var count = $('input[name^=addProductCount]').val();
			if(!isNaN(parseFloat(count)) && isFinite(count) && count < 1)
			{
				count = 1;
			}
            if (brand && model)
            {
                var url = '/admin/complect/ajaxAddProduct';
                $.ajax(
                {
                    url: url,
                    data: {Product:{brand: brand, model: model, complectId: $currentComplect, count: count, parentId: $this->parentId}},
                    dataType: 'json',
                    type: 'post',
                    success: function(data){
                        if(typeof(data) == 'object')
                        {
                            if(data.html)
                            {
                                $('#product-group').append(data.html);
                                //$('#ComplectProduct_product_id option[value=' + data.id + ']').remove();
                                $('#removeProduct'+data.id).fadeIn();
                            }
							brama.ui.notify(data.title, data.msg);
                        }
                    }
                });
            }
        });
    ", CClientScript::POS_READY);
?>
<div class="form-horizontal">
    <div id="product-group">
        <div class="control-group">
            <label class="control-label">Продукты:</label>
        </div>
        <script>
            $(function() {
                $( "#sortable" ).sortable({
                    revert: true,
                    stop: function() {
                        var list = [];
                        $('.prodContainer').each(function(){
                            var id = $(this).attr('prod-id');
                            list.push([id, list.length]);
                        });
                        $.ajax(
                            {
                                url: '/admin/complect/changeProdOrder',
                                data: {list: list},
                                dataType: 'json',
                                type: 'get',
                                success: function(data){
                                    if(typeof(data) == 'object')
                                    {
                                        brama.ui.notify(data.title, data.msg);
                                    }
                                }
                            });
                    }
                });
                $( ".specContainer" ).disableSelection();
            });
        </script>
        <div id="sortable">
        <?php foreach ($model as $product):?>
            <div class="control-group prodContainer" prod-id="<?php echo $product->id; ?>" id="removeProduct<?php echo $product->product_id; ?>">
                <label class="control-label"><?php echo $product->product->name; ?></label>
                <div class="controls productCount">
                    <?php echo CHtml::textField('productCount'.$product->product_id, $product->count, array('rel' => $product->product_id)); ?>
                    <img class="removeProduct" value="<?php echo $product->product_id; ?>" style="padding-top:10px;" src="/images/cross.gif" width="10"/>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label"><?php echo 'Добавить продукт'; ?></label>
        <div class="controls">
			<?php echo CHtml::textField('addProductCount', 1); ?>
			<?php echo CHtml::textField('productBrand', '', array('placeholder' => 'Бренд')); ?>
			<?php echo CHtml::textField('productModel', '', array('placeholder' => 'Модель')); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'type' => 'success',
                'label' => 'Добавить',
                'htmlOptions'=> array('id'=>'buttonAdd'),
                ));
            ?>
        </div>
    </div>
</div>