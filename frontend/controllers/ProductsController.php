<?php

class ProductsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */	
	public $layout='//layouts/main';
	public $categoryTitle = 'Результаты поиска';
	public $isSearch = false;
	public $categoryName = '';
	public $rootCategory = null;
	public $pathCategories = null;
	public $categoryIds = array();
	public $searchArray = array();
	public $searchTree = null;
	private $actionsWhiteList = array(
		'ajaxCarouselNext',
		'getBrandsOrModels',
		'ajaxAddToBookmarks',
		'ajaxRemoveBookmark',
        'ajaxGetMesage',
	);
	
	public static function getTag()
	{
		return 'catalog';
	}
	/**
	 * @return array action filters
	 */
	public function filters()
	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        protected function beforeAction($action)
        {
			$this->rootCategory = self::getTag();
			$this->pathCategories = isset($_GET['matches']) ? $_GET['matches'] : array();
			if(isset($_GET['min_price']))
			{
				$_GET['orig_min_price'] = $_GET['min_price'];
				$_GET['min_price'] = CurrencySys::exchange($_GET['min_price'], CurrencySys::DEFAULT_CURRENCY);
			}
			if(isset($_GET['max_price']))
			{
				$_GET['orig_max_price'] = $_GET['max_price'];
				$_GET['max_price'] = CurrencySys::exchange($_GET['max_price'], CurrencySys::DEFAULT_CURRENCY);
			}
            
			if (in_array($action->id, $this->actionsWhiteList))
			{
				unset($_GET['matches']);
				return parent::beforeAction($action);
			}
            else if ($action->id == 'index')
            {
				if(isset($_GET['brand']) && sizeof($_GET['brand']) > 1)
				{
					$brand = Brand::model()->findByAttributes(array('name' => end($_GET['matches'])));
					if($brand !== null)
					{
						$redirectArray = array('products/index');
						unset($_GET['matches'][sizeof($_GET['matches']) - 1]);
						$redirectArray['categoryNames'] = $_GET['matches'];
                        if ($_GET['ajax']) $redirectArray['ajax']=true; 
						$this->redirect($redirectArray);
					}
				}
				if(isset($_GET['brand']) && sizeof($_GET['brand']) == 1)
				{
					$brand = Brand::model()->findByPk(end($_GET['brand']));
					if($brand !== null)
					{
                        $_GET['ajax']=true;
						unset($_GET['brand']);
						$redirectArray = array('products/index');
						$redirectArray['categoryNames'] = $_GET['matches'];
						if(end($redirectArray['categoryNames']) !== $brand->name)
							array_push($redirectArray['categoryNames'], $brand->name);
                        if ($_GET['ajax']) $redirectArray['ajax']=true;
                        $this->redirect($redirectArray);
					}
				}
				$categoryController = app()->categoryController;
				if(!isset($_GET['brand']) && ($brand = Brand::model()->findByAttributes(array('name' => end($_GET['matches'])))) !== null)
				{
					$_GET['brand'] = array();
					array_pop($_GET['matches']);
					array_push($_GET['brand'], $brand->id);
				}
                if ($_GET['matches'][0] == 'products')
                {
                    if (isset($_GET['matches'][2]))
                    {
                        $_GET['Products_page'] = $_GET['matches'][2];
                    }
                    unset($_GET['matches']);
                    return parent::beforeAction($action);
                }
                $categories = $_GET['matches'];
                $key = end($categories);
                if (is_numeric($key) && sizeof($categories) > 2)
                {
                    $_GET['Products_page'] = array_pop($categories);
                    array_pop($categories);
                    $key = end($categories);
                }
                if (!is_bool($id = $categoryController->getCategoryId($key)))
                {
                    $categoryArray = $categoryController->addParentsIds($id);
                    if (is_array($categoryArray))
                    {
                        $categoryNames = $categoryController->getNames($categoryArray);
                        if ($categories == $categoryNames)
                        {
                            $children = array();
                            array_push($children, end($categoryArray));
                            $children = array_merge($children, $categoryController->getChildrenIds($children[0]));
							$_GET['rootCategory'] = $categoryArray[0];
                            $_GET['categoryId'] = $children;
                            $_GET['categoryNames'] = $categoryNames;
                            $_GET['categoryTitle'] = $categoryController->getTitle(end($categoryArray));
                            unset($_GET['matches']);
                            return parent::beforeAction($action);
                        }
                    }
                }
            }
            $isset = '';
            if ($action->id == 'view')
            {
                $isset .= 'Action is view. ';
                $model = Products::model();
				if (is_numeric($_GET['matches'][2]))
                {
                    $isset .= 'Id is numeric. ';

                    $product = $model->find("excel_id = ".$_GET['matches'][2]." AND hide != 1");
                    if ($product == null)
                    {
                        $product = $model->find("id = ".$_GET['matches'][2]." AND hide != 1");
                    }
                    if ($product != null)
					{
                        $isset .= 'Product founded ';
						$brand = $product->brand0->name;
						if (strtolower($brand) == strtolower($_GET['matches'][0]))
						{
                            $isset .= 'Brands are match. ';
							if (strtolower($product->model) == strtolower($_GET['matches'][1]))
							{
								$_GET['brand'] = $brand;
								$_GET['model'] = $product->model;
								$_GET['id'] = $product->id;
								unset($_GET['matches']);
								return parent::beforeAction($action);
							}
                            else
                            {
                                $isset .= 'Model is NOT match! ';
                            }
						}
                        else
                        {
                            $isset .= 'Brands is NOT match! ';
                        }
                    }
                    else
                    {
                        $isset .= 'Product is NOT founded! ';
                    }
				}
                else
                {
                    $isset .= 'Id is NOT Numeric! ';
                }
            }
            else
            {
                $isset .= 'Action NOT view. ';
            }
            throw new CHttpException(404, $isset);
        }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id, $scroll = '', $ajax=false)
	{
		$this->addViewed($id);
		$model = $this->loadModel($id);
		$categoryIds = Yii::app()->categoryController->addParentsIds($model->category);
		$this->rootCategory = $categoryIds[0];
		$specifications = array();
                $commentsCount = Comment::model()->countByAttributes(array('product_id'=>$id, 'status' => 1));
               
        /*Geting ids for specs names*/
        $specsIds = array();
        foreach (CategorySpec::model()->findAllByAttributes(array('category_id' => $categoryIds), array('order' => '`order`')) as $categorySpec)
        {
            array_push($specsIds, $categorySpec->spec_id);
        }
        /*Geting specs names*/
        foreach ($specsIds as $specsId)
        {
            $specValue = ProductSpecValue::model()->findByAttributes(array('product_id' => $id, 'spec_id' => $specsId));
            
            if($specValue !== null)
                array_push($specifications, $specValue);
        }
        $this->render('view',array(
				'model'=>$model,
				'specifications'=>$specifications,
				'scroll' => $scroll,
                                'commentsCount' =>$commentsCount,
                                
		));
	}
	
	public static function getRelevantSearchCriteria($search, $delimiter = 1, $allowDescription = true)
	{
		if(!empty($search))
		{
			$searchCriteria = self::getSearchCriteria($search, $allowDescription);
			if($searchCriteria !== null)
			{
				$search = substr($search, 0, 127);
				$searchArray = array_filter(preg_split( "/(\s|-|,|\.|\\|\/)/", $search));
                if(empty($searchArray))
                {
                    $searchArray = array($search);
                }
				$select = '';
				foreach ($searchArray as $searchNode)
				{
					$select .= " IF (`brand`.`name` LIKE '%$searchNode%', ".(1 / $delimiter).", 0) + IF (`products`.`model` LIKE '%$searchNode%', ".(1 / $delimiter).", 0) +";
                    if($allowDescription)
                        $select .= " IF (`products`.`full_description` LIKE '%$searchNode%', ".(0.1 / $delimiter).", 0) +";
				}
				if(!empty($select))
				{
					$select = rtrim($select, '+');
					$searchCriteria->select = "($select) as `relevant`";
					$searchCriteria->order = '`relevant` DESC';
				}
			}
			return $searchCriteria;
		}
		return null;
	}
	
	public static function getSearchCriteria($search, $allowDescription = true)
	{
        $search = substr($search, 0, 127);
        $searchArray = array_filter(preg_split( "/(\s|-|,|\.|\\|\/)/", $search));
        if(empty($searchArray))
        {
            $searchArray = array($search);
        }
        $searchCriteria = new CDbCriteria();
        foreach ($searchArray as $searchNode)
        {
            $searchCriteria->addSearchCondition('`brand`.`name`', $searchNode, true, 'OR');
            $searchCriteria->addSearchCondition('`products`.`model`', $searchNode, true, 'OR');
            if($allowDescription)
                $searchCriteria->addSearchCondition('`products`.`full_description`', $searchNode, true, 'OR');
        }
        return $searchCriteria;
	}
	
	/**
	 * Lists all models.
	 */
	public function actionIndex(array $categoryId = array(), array $categoryNames = array(), $categoryTitle = '', $rootCategory = null, $search = '', $sort = null, $limit = null, $ajax=false)
	{
            if(!empty($categoryId))
		{
			$this->categoryName = end($categoryNames);
			$this->categoryTitle = $categoryTitle;
			$this->categoryIds = $categoryId;
		}
	
            Yii::app()->request->cookies['lastCompare'] = new CHttpCookie('lastCompare', $this->createUrl('products/index', array('categoryNames' => $this->pathCategories)));
       	
        $this->searchTree = new Tree();
		$this->rootCategory = !empty($rootCategory) ? $rootCategory : self::getTag();
		$dataProvider=new CActiveDataProvider('Products');
		
		$criteria = new CDbCriteria;
		$criteria->alias = 'products';
		$criteria->select = 'products.*';		
		$criteria->join = 'RIGHT OUTER JOIN brand ON brand.id = brand';
		
		if(!empty($sort))
		{
			if($sort == 'high_price')
				$criteria->order = 'price DESC';
			else if($sort == 'low_price')
				$criteria->order = 'price ASC';
		}
		else
		{
			$criteria->select .= ', (SELECT count(*) FROM `comment_vote`
			RIGHT OUTER JOIN `comment` ON `comment_vote`.`comment_id` = `comment`.`id`
			WHERE `comment`.`product_id` = `products`.`id` AND `status` != 0) as `rating`';
			$criteria->order = 'rating DESC';
		}
		
		$criteria->addCondition('hide != 1');
		if(!empty($categoryId))
		{
			$criteria->addInCondition('category', $categoryId);
			$this->categoryName = end($categoryNames);
			$this->categoryTitle = $categoryTitle;
			$this->categoryIds = $categoryId;
		}
		else
		{
			if(empty($search))
                throw new CHttpException(404, '/products is used only on search!');
			$this->isSearch = true;
		}
		$searchCriteria = $this->getRelevantSearchCriteria($search);
		$filter = false;
		if(isset($_GET['max_price']) || isset($_GET['min_price']))
		{
			$criteria->addBetweenCondition('price', isset($_GET['min_price']) ? $_GET['min_price'] : Products::MIN_PRODUCT_PRICE, isset($_GET['max_price']) ? $_GET['max_price'] : Products::MAX_PRODUCT_PRICE);
			$filter = true;
		}
		if(isset($_GET['top_seller']))
			$offers['top_seller'] = $_GET['top_seller'];
		if(isset($_GET['novelty']))
			$offers['novelty'] = $_GET['novelty'];
		if(isset($_GET['price_of_the_week']))
			$offers['price_of_the_week'] = $_GET['price_of_the_week'];
		if(isset($offers['top_seller']) || isset($offers['novelty']) || isset($offers['price_of_the_week']))
		{
			$filter = true;
			if(isset($offers['top_seller']) + isset($offers['novelty']) + isset($_GET['price_of_the_week']) > 1)
			{
				$criteria->addColumnCondition(array(
					'top_seller' => isset($offers['top_seller']),
					'novelty' => isset($offers['novelty']),
					'price_of_the_week' => isset($offers['price_of_the_week'])
				), 'OR', 'AND');
			}
			else
			{
				$criteria->addCondition(key($offers).' = 1');
			}
		}
		
		if(isset($_GET['brand']))
		{
			$criteria->addInCondition('`brand`.`id`', $_GET['brand']);
			$filter = true;
		}
		
		if(isset($_GET['specs']))
		{
			$specCriteria = new CDbCriteria;
			foreach ($_GET['specs'] as $specId => $specValues)
			{
				$difSpecCriteria = new CDbCriteria;
				$specValCount = 1;
				foreach ($specValues as $specValue => $id)
				{
					$difSpecCriteria->addCondition("spec_id = '$specId' AND id = '$id'", 'OR');
					$specValCount++;
				}
				$specCriteria->condition .= "(SELECT `t$specValCount`.`product_id` FROM `product_spec_value` `t$specValCount` WHERE ($difSpecCriteria->condition) AND `t$specValCount`.`product_id` = `products`.`id` LIMIT 1) AND ";
			}
			$specCriteria->condition .= "1";
			$criteria->condition .= "AND ($specCriteria->condition)";
			$filter = true;
		}
		if(!empty($search) && $searchCriteria !== null)
		{
            $criteria->select .= ", ".$searchCriteria->select;
            $criteria->params += $searchCriteria->params;
            $criteria->condition .= " AND ".$searchCriteria->condition;
            $criteria->order .= ", ".$searchCriteria->order;
		}
		else if(!$filter)
		{
			//$criteria->select .= ', `complect`.*';
			//$criteria->
			//$criteria->join .= ' CROSS JOIN `complect` as `complect`';
		}
		$dataProvider->setCriteria($criteria);
        $dataProvider->pagination = false;
		//$dataProvider = new CSqlDataProvider($sql)
		if(empty($this->categoryIds))
		{
			$productCounts = array();
			foreach ($dataProvider->getData() as $product)
			{
				isset($productCounts[$product->category]) ? $productCounts[$product->category]++ : $productCounts[$product->category] = 1;
				if(!in_array($product->category, $this->categoryIds))
				{
					$categories = Yii::app()->categoryController->addParentsIds($product->category, true);
					foreach ($categories as $catId)
					{
						if(!in_array($catId, $this->categoryIds))
						{
							array_push($this->categoryIds, $catId);
						}
					}
				}
			}

            $complectDataProvider = new CActiveDataProvider('Complect');
            $complectCriteria = ComplectController::getRelevantSearchCriteria($search);
            if($complectCriteria !== null)
            {
                $complectCriteria->select = "`complect`.*, ".$complectCriteria->select;
                $complectDataProvider->setCriteria($complectCriteria);
                $complectDataProvider->pagination = false;
                foreach ($complectDataProvider->getData() as $complect)
                {
                    isset($productCounts[$complect->category_id]) ? $productCounts[$complect->category_id]++ : $productCounts[$complect->category_id] = 1;
                    if(!in_array($complect->category_id, $this->categoryIds))
                    {
                        $categories = Yii::app()->categoryController->addParentsIds($complect->category_id, true);
                        foreach ($categories as $catId)
                        {
                            if(!in_array($catId, $this->categoryIds))
                            {
                                array_push($this->categoryIds, $catId);
                            }
                        }
                    }
                }
            }
			$this->searchTree = Yii::app()->categoryController->buildTree($this->categoryIds, $productCounts);
		}
		$pageSize = 20;
		if(!empty($limit))
			$pageSize = $limit;
        $dataProvider->pagination = null;
		$dataProvider->setPagination(array(
			'pageSize' => $pageSize,
			));
//                
        if ($ajax){	
		$dataProvider->getData(true);
                $this->renderPartial('index_a',array(
				'dataProvider'=>$dataProvider,
				'isSearch' => $this->isSearch,
                ));}		
                else {
                    $this->render('frontend.views.layouts.load');}
                    
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Products::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='products-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionGetBrandsOrModels($term)
	{
		$autocompliteArray = array();
		$tokens = array_filter(explode(" ", substr($term, 0, 127)));
		if(sizeof($tokens) < 3 && sizeof($tokens) > 0)
		{
			$brandsCriteria = new CDbCriteria();
			$brandsCriteria->addSearchCondition('name', sizeof($tokens) < 2 ? $term : $tokens[0]);
			$productGroups = Brand::model()->findAll($brandsCriteria);
			foreach ($productGroups as $productGroup)
			{
				array_push($autocompliteArray, $productGroup->name);
			}
			$issetBrands = sizeof($productGroups);
			
			$modelsCriteria = new CDbCriteria();
			$modelsCriteria->group = 'model';
			if($issetBrands && sizeof($tokens) > 1)
			{
				$modelsCriteria->addCondition("brand.name = '$tokens[0]'");
				$modelsCriteria->with = array('brand0' => array('alias' => 'brand', 'together' => true,));
				$modelsCriteria->addSearchCondition('model', $tokens[1]);
				$productGroups = Products::model()->findAll($modelsCriteria);
			}
			else
			{
				$modelsCriteria->addSearchCondition('model', $tokens[0]);
				$modelsCriteria->addSearchCondition('brand.name', $tokens[0], true, 'OR');
				$modelsCriteria->with = array('brand0' => array('alias' => 'brand', 'together' => true,));
				$productGroups = Products::model()->findAll($modelsCriteria);
			}
			foreach ($productGroups as $productGroup)
			{
				array_push($autocompliteArray, $productGroup->brand0->name.' '.$productGroup->model);
			}
			
		}
		echo json_encode($autocompliteArray);
	}
	
	public function actionAjaxCarouselNext()
	{
		if (isset($_POST['offset']) && isset($_POST['count']) && Yii::app()->request->isAjaxRequest)
		{
			$categories = app()->categoryController->getChildrenIds($_POST['category']);
			$products = Products::model()->findAllByAttributes(array('category' => $categories, "{$_POST["type"]}" => 1), array('offset' => $_POST['offset'] * $_POST['count'], 'limit' => $_POST['count']));
			if($products !== null)
			{
				echo json_encode(array(
					'msg' => 'next',
					'item' => $this->renderPartial('carouselItem', array('products' => $products), true, false),
					));
				app()->end();
			}
		}
		echo json_encode(array('msg' => 'error'));
	}
	
	public function actionAjaxAddToBookmarks($id)
	{
		if(!isset(Yii::app()->request->cookies['bookmarks']))
			Yii::app()->request->cookies['bookmarks'] = new CHttpCookie('bookmarks', json_encode(array()));
		$bookmarks = json_decode(Yii::app()->request->cookies['bookmarks'], true);
		if(!isset($bookmarks['product']))
		{
			$bookmarks['product'] = array();
		}
		if(!in_array($id, $bookmarks['product']))
			array_push($bookmarks['product'], $id);
		Yii::app()->request->cookies['bookmarks'] = new CHttpCookie('bookmarks', json_encode($bookmarks));
		echo 'Success';
	}
	
	public function actionAjaxRemoveBookmark($id)
	{
		if(!isset(Yii::app()->request->cookies['bookmarks']))
			Yii::app()->request->cookies['bookmarks'] = new CHttpCookie('bookmarks', json_encode(array()));
		$bookmarks = json_decode(Yii::app()->request->cookies['bookmarks'], true);
		if(isset($bookmarks['product']))
		{
			if(in_array($id, $bookmarks['product']))
			{
				foreach ($bookmarks['product'] as $key => $bookmarkId)
				{
					if($bookmarkId == $id)
					{
						array_splice($bookmarks['product'], $key, 1);
					}
				}
				Yii::app()->request->cookies['bookmarks'] = new CHttpCookie('bookmarks', json_encode($bookmarks));
			}
		}
		echo 'Success';
	}
	
	public function addViewed($id)
	{
		if(!isset(Yii::app()->session['viewed']))
			Yii::app()->session['viewed'] = array('products' => array(), 'complects' => array());
		$viewed = Yii::app()->session['viewed'];
		if(!in_array($id, $viewed['products']))
			array_push($viewed['products'], $id);
		Yii::app()->session['viewed'] = $viewed;
	}

    protected function afterAction($action)
    {
        isset($_GET['orig_min_price']) ? $_GET['min_price'] = $_GET['orig_min_price'] : '';
        isset($_GET['orig_max_price']) ? $_GET['max_price'] = $_GET['orig_max_price'] : '';
        return parent::afterAction($action);
    }
    
    public function actionAjaxGetMesage($pageName)
	{
        $page = StaticPage::model()->findByAttributes(array('category_id' => app()->categoryController->getCategoryId($pageName)));
        if($page) echo json_encode (array('title' => 'Success', 'desc' => $page->content ) ); 
        else echo json_encode (array('title' => 'Error' ) );
    }
}
