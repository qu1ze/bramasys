<?php

/**
 * This is the model class for table "article_product".
 *
 * The followings are the available columns in table 'article_product':
 * @property integer $id
 * @property integer $article_id
 * @property integer $product_id
 *
 * The followings are the available model relations:
 * @property Products $product
 * @property Article $article
 */
class ArticleProduct extends ActiveRecord
{
    public $product_id_s = array(); 
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ArticleProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'article_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('article_id, product_id', 'required'),
			//array('article_id, product_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, article_id, product_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
			'article' => array(self::BELONGS_TO, 'Article', 'article_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'article_id' => 'Статья',
			'product_id' => 'Продукт',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('article_id',$this->article_id, true);
		$criteria->compare('product_id',$this->product_id, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeValidate()
	{
		$r = parent::beforeValidate();
			
		if (!$this->article_id)		
			$this->addError('article_id', 'Oops!');				
		
		if (!$this->product_id)		
			$this->addError('product_id', 'Oops!');	
		
		
		return $r;
	}
    
    public function saveProducts($product_ids, $article_id, $update=false)
    {
            $error=false;            
            $transaction = Yii::app()->db->beginTransaction(); // Transaction begin
            if($update) ArticleProduct::model()->deleteAllByAttributes(array('article_id' => $article_id));
            try{
                foreach ($product_ids as $value)
                {
                    $model = new ArticleProduct();
                    $model->article_id = $article_id;
                    $model->product_id = $value;
                    if (0==ArticleProduct::model()->countByAttributes(array('article_id'=>$article_id, 'product_id' => $value))){
                        $error=  !$model->save() | $error;  
                    }
                    

                }
                if (!$error){
                    $transaction->commit();
                    return true;
                }
            }
            catch (Exception $e){
                            $transaction->rollBack();
            }
            return false;
    }
}