<?php

class PaymentController extends Controller
{		
	public function actionCreate()
	{
		$model = new PaymentType; 
				
		if (isset($_POST['PaymentType']))
		{			
			$model->attributes = $_POST['PaymentType'];
			if ($model->save())
				$this->redirect(array('/admin/payment'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new PaymentType();	

		if (isset($_POST['PaymentType']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['PaymentType'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Тип оплаты успешно создан'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать тип оплаты'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new PaymentType('search');
		
		if (isset($_GET['PaymentType']))
			$model->attributes = $_GET['PaymentType'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = PaymentType::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/payment'));				
				
		if (isset($_POST['PaymentType']))
		{			
			$model->attributes = $_POST['PaymentType'];
			if ($model->save())
				$this->redirect(array('/admin/payment'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('PaymentType');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Тип оплаты успешно отредактирован')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
			if ((Order::model()->findByAttributes(array('payment_type_id' => $id))) === null)
			{
				PaymentType::model()->deleteByPk( $id );					
				Yii::app()->user->setFlash('success', "Тип оплаты успешно удален");
			}
			else
				Yii::app()->user->setFlash('error', "Существует связь с заказом");
		}
		
		$this->redirect(array('/admin/payment'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = PaymentType::model()->findByPk($id)) !== null)
		{
			if ((Order::model()->findByAttributes(array('payment_type_id' => $id))) === null)
				if ($model->delete())
					echo json_encode(array('title' => 'Success', 'msg' => 'Тип оплаты успешно удален'));
				else
					echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить тип оплаты'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с заказом'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Тип оплаты не найден'));
	}
	
	public function actionToggle($id, $attribute)
    {
		if (($model = PaymentType::model()->findByPk($id)) === null)
                app()->end();
		$model->setAttribute($attribute, $model->getAttribute($attribute) ? 0 : 1);
		$model->save();
		echo json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> "Значение $attribute изменено"));
    }
}