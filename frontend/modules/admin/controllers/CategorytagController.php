<?php

class CategoryTagController extends Controller
{		
	public function actionCreate()
	{
		$model = new CategoryTag; 
				
		if (isset($_POST['CategoryTag']))
		{			
			$model->attributes = $_POST['CategoryTag'];
			if ($model->save())
				$this->redirect(array('/admin/categorytag'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new CategoryTag();	

		if (isset($_POST['CategoryTag']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['CategoryTag'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Тег категории успешно создан'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать тег категории'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new CategoryTag('search');
		
		if (isset($_GET['CategoryTag']))
			$model->attributes = $_GET['CategoryTag'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = CategoryTag::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/categorytag'));				
				
		if (isset($_POST['CategoryTag']))
		{			
			$model->attributes = $_POST['CategoryTag'];
			if ($model->save())
				$this->redirect(array('/admin/categorytag'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('CategoryTag');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Тег категории успешно отредактирован')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
			$tag = CategoryTag::model()->findByPk($id);
			if(!$tag->isSystemTag())
			{
				if (($category = Category::model()->findByAttributes(array('tag' => $id))) === null)
				{
					CategoryTag::model()->deleteByPk( $id );					
					Yii::app()->user->setFlash('success', "Тег категории успешно удален");
				}
				else
					Yii::app()->user->setFlash('error', "Существует связь с категориями");
			}
			else
				Yii::app()->user->setFlash('error', "Невозможно удалить системный тег.");
		}
		
		$this->redirect(array('/admin/categorytag'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = CategoryTag::model()->findByPk($id)) !== null)
		{
			$tag = CategoryTag::model()->findByPk($id);
			if(!$tag->isSystemTag())
			{
				if (($categories = Category::model()->findByAttributes(array('tag' => $id))) === null)
					if ($model->delete())
						echo json_encode(array('title' => 'Success', 'msg' => 'Тег категории успешно удален'));
					else
						echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить тег категории'));
				else
					echo json_encode(array('title' => 'Error', 'msg' => 'Существует связь с категориями'));
			}
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить системный тег'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Тег категории не найден'));
	}
	
}