<?php
foreach ($prod as $id => $complect):
$amount = $complect['price'] * $complect['count'];
$complectModel = Complect::model()->findByPk($id);
$complectProducts = ComplectProduct::model()->findAllByAttributes(array('complect_id' => $id));
?>
<div class="order-price complect_<?php echo $id?>" style="border: none;">
	<div class="order-price_img">
		<a href="<?php echo $complectModel->link; ?>"><img style="width: 72px;" src="<?php echo $complectModel->image_url;?>"></a>
	</div>
	<div class="order-price_link" style="display: table-cell; vertical-align: middle;">
		<a href="<?php echo $complectModel->link; ?>"><?php echo $complectModel->name; ?></a>
        <p class="cart-prod-count">x<?php echo $complect['count'] ?> шт.</p>
	</div>
</div>
<div class="order-price complect complect_<?php echo $id?>">
<?php
foreach ($complectProducts as $complectProduct)
{
	$product = Products::model()->findByPk($complectProduct->product_id);
	$prodArray['count'] = $complectProduct->count;
	$prodArray['price'] = $product->price;
	$this->renderPartial('_product', array(
				'id' => $complectProduct->product_id,
				'prod' => $prodArray,
				'product' => $product,
				'withPrice' => false,
			));
}
?>
</div>
<div class="order-price complect_<?php echo $id?>" style="margin-left: 24px; width: 90%;">
	<div class="order-price_text product-price-cart">
		<p>Цена за комплект:</p>
		<p id="firstPrice"><?php echo CurrencySys::exchange($amount).' '.CurrencySys::getLabel(); ?></p>
	</div>
</div>
<?php endforeach;?>