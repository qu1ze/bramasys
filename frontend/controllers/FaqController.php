<?php

class FaqController extends StaticController
{
	public function actionIndex()
	{
		$categories = app()->categoryController->getChilds(ProductsController::getTag());
		$faqCategories = array();
		$faqCategories['Общие вопросы'] = Faq::model()->findAllByAttributes(array('category_id' => null));
		foreach ($categories as $category)
		{
			$faqCategories[$category['data']['title']] = Faq::model()->findAllByAttributes(array('category_id' => $category['id']));
		}
		$this->render('index', array(
			'faqCategories' => $faqCategories,
		));
	}
}
