<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js"></script>
<?php
    Yii::app()->clientScript->registerScript("removeSupport", "
        $('.form-horizontal').on('click', '.removeSupport', function(){
            var id = $(this).attr('value');
            var value = $('#removeSupport' + id + ' label').text();
            if (id)
            {
                var url = '/admin/products/ajaxRemoveSupport';
                $.ajax(
                {
                    url: url,
                    data: {supportId: id},
					dataType: 'json',
                    type: 'get',
                    success: function(data){
                        $('#removeSupport'+id).fadeOut('slow', function()
                        {
                            $('#removeSupport'+id).remove();
                        });
						brama.ui.notify(data.title, data.msg);
                    }
                });
            }
        });
    ", CClientScript::POS_READY);
        
    Yii::app()->clientScript->registerScript("addSupport", "
		$('input[name=\"productSupport\"]').autocomplete({
			source: function(request, response) {
                $.ajax({
                  url: '/admin/products/getProductSupport',
                  dataType: 'json',
                  data: {
                    term : request.term,
					categoryId : $categoryId
                  },
                  success: function(data) {
                    response(data);
                  }
                });
            },
			minLength: 2,
		});
		
        $('.form-horizontal').on('click', '#buttonAdd', function(){
            var support = $('input[name=\"productSupport\"]').val();
            if (support)
            {
                var url = '/admin/products/ajaxAddSupport';
                $.ajax(
                {
                    url: url,
                    data: {Product:{support: support, id: $currentProduct}},
                    dataType: 'json',
                    type: 'post',
                    success: function(data){
                        if(typeof(data) == 'object')
                        {
                            if(data.html)
                            {
                                $('#support-group').append(data.html);
                                $('#removeSupport'+data.id).fadeIn();
                            }
							brama.ui.notify(data.title, data.msg);
                        }
                    }
                });
            }
        });
    ", CClientScript::POS_READY);

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
));
?>
    <div id="support-group">
        <div class="control-group">
            <label class="control-label">Техдокументация и ПО:</label>
        </div>
    <?php
	foreach ($model as $productSupport):
	$supportFile = $productSupport->supportFile;
	?>
        <div class="control-group" id="removeSupport<?php echo $productSupport->id; ?>">
            <label class="control-label"><?php echo $supportFile->name; ?></label>
            <div class="controls productCount">
                <img class="removeSupport" value="<?php echo $productSupport->id; ?>" style="padding-top:10px;" src="/images/cross.gif" width="10"/>
            </div>
        </div>
	<?php endforeach; ?>
    </div>
    <div class="control-group">
        <label class="control-label"><?php echo 'Добавить техдокументацию и ПО'; ?></label>
        <div class="controls">
			<?php echo CHtml::textField('productSupport', '', array('placeholder' => 'Название файла')); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'type' => 'success',
                'label' => 'Добавить',
                'htmlOptions'=> array('id'=>'buttonAdd'),
                ));
            ?>
        </div>
    </div>
    <?php echo CHtml::hiddenField('Products_page', $page); ?>

    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Назад')); ?>
<?php $this->endWidget(); ?>