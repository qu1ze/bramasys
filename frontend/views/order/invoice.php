<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head>
	<meta http-equiv="CONTENT-TYPE" content="text/html; charset=utf-8">
	<title></title>
	<style type="text/css">
		@page { size: 21cm 29.7cm; margin-right: 1cm; margin-top: 0.25cm; margin-bottom: 1cm }
		P { margin-bottom: 0cm; direction: ltr; color: #000000; line-height: 100%; widows: 2; orphans: 2 }
		P.western { font-family: "Arial", sans-serif; font-size: 10pt; so-language: ru-RU }
		P.cjk { font-family: "Times New Roman", serif; font-size: 10pt }
		P.ctl { font-family: "Arial", sans-serif; font-size: 10pt; so-language: ar-SA }
		A:link { color: #0000ff; text-decoration: underline }
        hr {color: #000000; border: solid 1px #000000;  margin: 1; clear: both;}
        span.u{ border-bottom: 1px solid #000000; padding-bottom: 0px; }
	</style>
</head>
<body lang="ru-RU" text="#000000" link="#0000ff" vlink="#800000" dir="LTR">
<p class="western" align="LEFT" style="line-height: 100%"><br>
</p>
<p class="western" align="LEFT" style=" ">
<font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><u><b>Поставщик:</b></u></font></font></p>
<img style="float:left;" src="/images/inv1.jpg" align="left"  width="169" height="157" border="0"><p class="western" style="line-height: 1.5;" align="RIGHT"><font face="Cambria, Cambria, serif"><b>ФЛП
(СПД) Вдовина Ольга Станиславовна</b><br>
ЕГРПОУ (ИНН): 3315307705<br>
р\сч. 26008055704011 в ПАО КБ «ПриватБанк», МФО: 313399<br>
69057, г. Запорожье, пр. Ленина 158, оф. 302<br>
Тел.: (061) 701-19-92, (093) 098-89-08<br>
Является плательщиком единого налога<br>
<a href="http://www.bramasys.com.ua">www.bramasys.com.ua</a>
<br><a href="mailto:info@bramasys.com.ua">info@bramasys.com.ua</a></font>  </p>
<hr size="1" >
<hr size="1" >
<br><br>
<br><br>
<p class="western" align="LEFT" style="margin-bottom: 0.35cm; line-height: 115%">
<font face="Calibri, sans-serif"><b>Получатель
        (плательщик): </b><font size="4"><span class="u">&nbsp;&nbsp;<?php echo $client->lastname == null ? '' : $client->lastname;
echo ' '.$client->name;
echo ' '.($client->middle_name == null) ? '' : $client->middle_name; ?>&nbsp;&nbsp;</span></font></font></p>
<p class="western" align="CENTER" style="margin-bottom: 0.35cm; line-height: 115%">
<br><br>
</p>
<p class="western" align="CENTER" style="margin-bottom: 0.35cm; padding-bottom: 3px; line-height: 115%">
<font face="Calibri, sans-serif"><font size="2" style="font-size: 11pt"><font face="Cambria, Cambria, serif"><font size="4"><b>СЧЕТ
№ </b></font></font><font face="Cambria, Cambria, serif"><font size="4"><span lang="en-US"><b>CB</b></span></font></font><font face="Cambria, Cambria, serif"><font size="4"><b>
            <span class="u">&nbsp;&nbsp;<?php $order->dateToTime(); echo date("mdy", $order->create_time).'-'.$order->invoice_num; ?>&nbsp;&nbsp;</span></b></font></font></font></font></p></u>
<p class="western" align="CENTER" style="margin-bottom: 0.35cm; line-height: 115%">
<font face="Calibri, sans-serif"><font size="2" style="font-size: 11pt"><font face="Cambria, Cambria, serif"><font size="4"><b>от
<span class="u">&nbsp;&nbsp;<?php echo date("m.d.Y", $order->create_time);   ?>&nbsp;&nbsp; </span>г.</b></font></font></font></font></p>
<p class="western" align="CENTER" style="margin-bottom: 0.35cm; line-height: 115%">
<br><br>
</p>
<center>
    <table width="678" style="border-spacing:0" border="1" bordercolor="#000000" cellpadding="7" cellspacing="0">
		<colgroup><col width="15">
		<col width="324">
		<col width="48">
		<col width="43">
		<col width="17">
		<col width="24">
		<col width="52">
		<col width="42">
		</colgroup><tbody>
            
        <tr>
			<td width="15">
				<p class="western" align="CENTER">№</p>
			</td>
			<td width="374">
				<p class="western" align="CENTER"><font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><b>Наименование</b></font></font></p>
			</td>
			<td colspan="2" width="105">
				<p class="western" align="CENTER"><font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><b>Цена,
				без НДС (грн.)</b></font></font></p>
			</td>
			<td colspan="2" width="55">
				<p class="western" align="CENTER"><font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><b>Кол-во</b></font></font></p>
			</td>
			<td colspan="2" width="109">
				<p class="western" align="CENTER"><font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><b>Всего,
				без НДС (грн.)</b></font></font></p>
			</td>
		</tr>
		<?php 
        $priceSum=0;
        $number=0;
        foreach ($orderProducts as $key => $prod)
        {
		if($prod->is_complect)
		{
			
            $complect = Complect::model()->findByPk($prod->product_id);
			$complectProducts = ComplectProduct::model()->findAllByAttributes(array('complect_id' => $complect->id));
			foreach ($complectProducts as $complectProd)
			{
                $number++;
				$product = Products::model()->findByPk($complectProd->product_id);
				$ProdArray = array(
                    'id'=>$product->id,
					'name' => $product->name,
					'price' => round(100 * $product->price / 100 * (100 - $complect->discount)) / 100,
					'count' => $complectProd->count * $prod->count,
                    'number' => $number	
				);
				$amount = $ProdArray['price'] * $ProdArray['count'];
				$priceSum += $amount;
				$this->renderPartial('_product_table', array(
							'amount' => $amount,
							'prod' => $ProdArray,
							));
			}
			unset($orderProducts[$key]);
		}
	}
	foreach ($orderProducts as $prod)
	{
        $number++;
		$product = Products::model()->findByPk($prod['product_id']);
        $ProdArray = array(
                    'id'=>$product->id,
					'name' => $product->name,
					'price' => $product->price,
					'count' => $prod->count,
                    'number' => $number	
				);
		$amount = $prod['price'] * $prod['count'];
		$priceSum += $amount;
		$this->renderPartial('_product_table', array(
							'amount' => $amount,
							'prod' => $ProdArray,
							));
	} 
    
    $deliveryT = DeliveryType::model()->findByPk($order->delivery_type_id);
        if ($deliveryT !== null){
           $delivery = $deliveryT->description;
           $freed = $order->getFreedelivery();
           $freed = CurrencySys::exchange($freed,-1,1);
           if (($order->delivery_type_id==2) and (CurrencySys::exchange($priceSum)>$freed)) {$delivery =0; }
		   if ($delivery != 0) $delivery = CurrencySys::exchange($delivery, -1 ,1);
		} 
    $total=$priceSum+$delivery;
    
    
    if (!is_string($delivery) && $delivery!==0):
    $number++;    
    ?>
		
     <tr valign="TOP">
			<td width="15" height="13">
				<p class="western" align="CENTER"><font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><?php echo $number; ?>.</font></font></p>
			</td>
			<td width="374">
				<p class="western" align="LEFT">Доставка <?php echo $deliveryT->title;  ?>
				</p>
			</td>
			<td width="48">
				<p class="western" align="LEFT"><?php echo $delivery; ?>
				</p>
			</td>
			<td width="43">
				<p class="western" align="LEFT"><font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><?php echo CurrencySys::getLabel() ?></font></font></p>
			</td>
			<td width="17">
				<p class="western" align="LEFT">1 
				</p>
			</td>
			<td width="24">
				<p class="western" align="LEFT"><font face="Calibri, sans-serif"><font size="2" style="font-size: 11pt"><font face="Cambria, Cambria, serif">шт.</font></font></font></p>
			</td>
			<td width="52">
				<p class="western" align="LEFT"><?php echo $delivery; ?>
				</p>
			</td>
			<td width="42">
				<p class="western" align="LEFT"><font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><?php echo CurrencySys::getLabel() ?></font></font></p>
			</td>
</tr>
<?php endif; ?>
        
		<tr>
			<td colspan="6" width="590" height="5">
				<p class="western" align="RIGHT"><font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><b>Итого,
				без НДС </b></font></font>
				</p>
			</td>
			<td width="52">
				<p class="western" align="RIGHT"><?php echo CurrencySys::exchange($total); ?>
				</p>
			</td>
			<td width="42">
				<p class="western" align="LEFT"><font face="Cambria, Cambria, serif"><font size="2" style="font-size: 11pt"><b><?php echo CurrencySys::getLabel() ?></b></font></font></p>
			</td>
		</tr>
	</tbody></table>
</center>
<p class="western" align="LEFT" style="margin-bottom: 0.35cm; line-height: 115%">
           
</p>
<p class="western" align="LEFT" style="margin-bottom: 0.35cm; line-height: 115%">
<font face="Calibri, sans-serif"><font size="2" style="font-size: 11pt"><font face="Cambria, Cambria, serif"><font size="4" style="font-size: 16pt"><b>Сумма:</b></font></font><font face="Cambria, Cambria, serif"><font size="4" style="font-size: 16pt"><span class="u"><b>
    <?php echo CurrencySys::exchange($total).' '.CurrencySys::getLabel(); ?> </b></span></font></font></font></font>
</p>

<p class="western" align="RIGHT" style=" margin-bottom: 0.35cm; line-height: 115%">
<img src="/images/inv2.jpg" name="Графический объект2" align="BOTTOM" width="405" height="192" border="0"></p>

</body></html>