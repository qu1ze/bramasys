<?php

class m130502_174829_products_table_relations_fix extends CDbMigration
{
//	public function up()
//	{
//	}
//
//	public function down()
//	{
//		echo "m130502_174829_products_table_relations_fix does not support migration down.\n";
//		return false;
//	}

	public function safeUp()
	{
		$this->execute("
                    ALTER TABLE  `products` ADD CONSTRAINT `FK_products_brand` FOREIGN KEY (  `brand` ) REFERENCES  `brand` (
                    `id`
                    ) ON DELETE RESTRICT ON UPDATE RESTRICT ;
                    ALTER TABLE  `products` ADD CONSTRAINT `FK_products_category` FOREIGN KEY (  `category` ) REFERENCES  `category` (
                    `id`
                    ) ON DELETE RESTRICT ON UPDATE RESTRICT ;
		");
	}

	public function safeDown()
	{
		$this->execute("
                    ALTER TABLE  `products` DROP FOREIGN KEY  `FK_products_brand` ;
                    ALTER TABLE  `products` DROP FOREIGN KEY  `FK_products_category` ;
		");
	}

}