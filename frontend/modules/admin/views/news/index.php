<?php
// cs()->registerCoreScript('yiiactiveform');

$grid_id = $model->getGridId();
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $grid_id,
	'type' => 'bordered striped',
	'dataProvider' => $model->search(),
	'filter' => $model,	
	'template' => "{items}\n{pager}",
	'columns' => array(
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'name',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/news/ajaxUpdate'),
				 'success' => 'js: function(data){					
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),	
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'title',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/news/ajaxUpdate'),
				 'success' => 'js: function(data){					
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),			
		'views',
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('nowrap' => 'nowrap'),			
			'template' => '{delete} {update}',	
			'updateButtonUrl'=>'Yii::app()->controller->createUrl("update", array("id" => $data->primaryKey))',
			'deleteButtonUrl'=>'Yii::app()->controller->createUrl("ajaxDelete", array("id" => $data->primaryKey))',
			// 'header' => EHtml::addGridButton($this->createAbsoluteUrl('/admin/news/ajaxCreate'), 'Добавить', $height=350, 'icon-question-sign'),
			'deleteConfirmation' => 'Вы уверенны что хотите удалить?',
			'afterDelete' => 'function(link,success,data){
				if(success){data = $.parseJSON(data);brama.ui.notify(data.title, data.msg, false);}
				else{brama.ui.notify("Oops!", data.responseText )};
			}',
		)
	)
));

