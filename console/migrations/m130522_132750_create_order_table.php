<?php

class m130522_132750_create_order_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `order` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`create_time` timestamp NOT NULL,
				`status` tinyint(1) NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			");
	}

	public function down()
	{
		$this->dropTable("order");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}