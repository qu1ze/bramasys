<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$jsonPhotosArray = array();
$gallery_photos = array();
foreach($complectProducts as $complectProduct)
{
    $gallery_photos = CMap::mergeArray($gallery_photos, GalleryPhoto::model()->findAllByAttributes(array('gallery_id'=>$complectProduct->product->gallery_id),array('order'=>'rank')));
}
foreach ($gallery_photos as $gallery_photo)
{
        $tmparray = array('name'=>$gallery_photo->name, 'description'=>$gallery_photo->description,'small'=>$gallery_photo->id.'small.jpg','full'=>$gallery_photo->id.'.jpg');
        array_push($jsonPhotosArray, $tmparray);
}
$json=  json_encode($jsonPhotosArray);
Yii::app()->clientScript->registerScript("gallerydata","
		var photosjson='$json';
		$(\"body\").data(\"photosjson\", photosjson);
		
		$('body div').on('click', function(e){
			if(e.target.id !== '#gallery' && !$(e.target).parents().is('#gallery') && $('#gallery').css('opacity') == 1)
				$('#gallery').fadeOut(200);
		});

		$('.image-container .small-img').on('click', function(){
			var index = $(this).attr('img-indx') ? $(this).attr('img-indx') : 0;
			initAndShowGallery(index, $('body').data('galleria_done'), $('body').data('photosjson'), $('#galleria').data('galleria'));
		});

		$('.image-container img').on('click', function(){
			var index = $(this).attr('img-indx') ? $(this).attr('img-indx') : 0;
			initAndShowGallery(index, $('body').data('galleria_done'), $('body').data('photosjson'), $('#galleria').data('galleria'));
		});
", CClientScript::POS_READY);
    
$baseUrl = Yii::app()->baseUrl; 
  $cs = Yii::app()->getClientScript();
  $cs->registerScriptFile($baseUrl.'/js/gallery.init.js');
  $cs->registerScriptFile($baseUrl.'/js/galleria/galleria-1.2.9.js');
?>

<div id="item-for-carusel" style="display:none;">
   <a href="">
                <img 
                    src=""
                    data-big=""
                    data-title=""
                    data-description=""
                >
            </a>
</div>
<div id="gallery" class="galleryBox" style="display:none;">
    <div class="gallery_header">
        <h3><?php echo $model->name; ?></h3>
        <a class="close hover" id="close" href="#">
        </a>
    </div>


        <div id="galleria">
         
        </div>


    <div id="price" class="<?php echo $model->cartPattern ?>"  style="text-align: center;">
        <span style="display: inline-block;">
           <div class="pd-price-block">
               <?php if(!empty($model->old_price)): ?>
                   <div class="pd-old-price"><?php echo CurrencySys::exchange($model->old_price).' '.CurrencySys::getLabel(); ?></div>
               <?php endif; ?>
               <div class="pd-new-price"><?php echo CurrencySys::exchange($model->price).' '.CurrencySys::getLabel(); ?></div>
               <?php if (!CurrencySys::isDefaultInCookie()):?>
                   <div class="pd-usd-price"><?php echo CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY).' '.$model->price; ?></div>
               <?php endif; ?>
           </div>
            <?php if ($model->quantity != null):?>
                <?php if ($model->quantity > 0):?>
                    <div class="btn-buy-<?php echo $model->id; ?> enabled" style="<?php echo $inCart ? 'display: none;' : ''?>">
                        <a class="catalog btn-buy-buy" data-is_complect="<?php echo $model->isComplect ?>" href="#" id="<?php echo $model->cartPattern ?>">Купить</a>
                    </div>
                    <div class="btn-buy-<?php echo $model->id; ?> disabled" id="<?php echo $model->cartPattern ?>"  style="<?php echo $inCart ? '' : 'display: none;'?>">
                        <a data-is_complect="<?php echo $model->isComplect ?>" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
                    </div>
                <?php else:?>
                    <a class="catalog btn-buy-order" data-is_complect="<?php echo $model->isComplect ?>" href="#" id="<?php echo $model->cartPattern ?>">Заказать</a>
                <?php endif;?>
            <?php else:?>
                <a class="catalog btn-buy-soon" data-is_complect="<?php echo $model->isComplect ?>" href="#" id="<?php echo $model->cartPattern ?>" style="font-size: 18px; line-height:23px;">Уведомить о наличии</a>
            <?php endif;?>
		</span>
    </div>
</div>