<?php

class DeliverypaymentController extends Controller
{		
	public function actionCreate()
	{
		$model = new DeliveryPayment();
		
		$this->performAjaxValidation($model);
		
		if (isset($_POST['DeliveryPayment']))
		{			
			$model->attributes = $_POST['DeliveryPayment'];
			if ($model->save())
				$this->redirect(array('/admin/deliverypayment'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new DeliveryPayment();

		if (isset($_POST['DeliveryPayment']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['DeliveryPayment'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Связь доставка-оплата успешно создана'));
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Невозможно создать связь'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new DeliveryPayment('search');
		
		if (isset($_GET['DeliveryPayment']))
			$model->attributes = $_GET['DeliveryPayment'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = DeliveryPayment::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/deliverypayment'));
				
		if (isset($_POST['DeliveryPayment']))
		{			
			$model->attributes = $_POST['DeliveryPayment'];
			if ($model->save())
				$this->redirect(array('/admin/deliverypayment'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('DeliveryPayment');
		$es->update();		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Связь доставка-оплата успешно отредактирована')));
	}	
	
	public function actionAjaxDelete($id)
	{		
		if (($model = DeliveryPayment::model()->findByPk($id)) !== null)
		{			
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Связь успешно удалена'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить связь'));
			
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить: Связь доставка-оплата не найдена'));
	}
	
}