<?php

class StaticController extends Controller
{
	public $rootCategoryName;#mounting-btn
	public $categoryName;
	public $categoryId;
	
	public static function getTag()
	{
		return 'static';
	}

	protected function beforeAction($action)
	{
		
        $categoryNames = $_GET['categoryNames'];
		$categoryController = app()->categoryController;
        if (is_bool($id = $categoryController->getCategoryId(end($categoryNames)))  )
        {
            throw new CHttpException(404, end($categoryNames).' | '.Yii::app()->request->pathInfo);
        }
        $categoryArray = $categoryController->addParentsIds($id);
        if (is_array($categoryArray))
        {
            $controllerCategoryNames = $categoryController->getNames($categoryArray);
            if ($categoryNames != $controllerCategoryNames)
            {
                throw new CHttpException(404);
            }
        }
        else
        {
            throw new CHttpException(404);
        }
        $this->categoryId = $id;
		$this->categoryName = end($categoryNames);
		$this->rootCategoryName = $categoryNames[0];
		return parent::beforeAction($action);
	}
	
	public function actionIndex()
	{
		$this->render('index', array(
			'model' => StaticPage::model()->findByAttributes(array('category_id' => $this->categoryId)),
		));
	}
}
