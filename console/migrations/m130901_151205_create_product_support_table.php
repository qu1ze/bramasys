<?php

class m130901_151205_create_product_support_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `product_support` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `product_id` int(11) NOT NULL,
			  `support_file_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  CONSTRAINT `FK_product_support_support` FOREIGN KEY (`support_file_id`)
					REFERENCES `support_file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
			  CONSTRAINT `FK_product_support_product` FOREIGN KEY (`product_id`)
					REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->dropTable('product_support');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}