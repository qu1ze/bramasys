<?php

class m130510_123050_products_quantity_fild extends CDbMigration
{
	public function up()
	{
            $this->execute("ALTER TABLE  `products` ADD  `quantity` INT NOT NULL;");
	}

	public function down()
	{
            $this->execute("ALTER TABLE `products` DROP `quantity`");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}