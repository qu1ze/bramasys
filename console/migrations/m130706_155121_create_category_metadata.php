<?php

class m130706_155121_create_category_metadata extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `category_metadata` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `title` varchar(50) NOT NULL,
			  `description` varchar(255) NOT NULL,
			  `keywords` varchar(255) NOT NULL,
			  `category_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  CONSTRAINT `FK_category` FOREIGN KEY (`category_id`)
						  REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->dropTable('category_metadata');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}