<?php
/* @var $this BottomTabs */
/* @var $model Products */
/* @var $product Products */

Yii::app()->clientScript->registerScript("ChangeBottomTab", "
	$('.bottom-tabs').on('click', '.tabName', function() {
		$('.bottom-carousel').addClass('hide');
		$('#' + $(this).attr('id') + '_tabcontent').removeClass('hide');
		$('.tabName').removeClass('active');
		$(this).addClass('active');
	});
", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript("SlideTabs", "
    $('#compare_tabcontent').carousel({
  interval: 0
});
	$('#compare_tabcontent').on('click', '.carousel-control.left', function() {
		$('#compare_tabcontent').carousel('prev');
	});

	$('#compare_tabcontent').on('click', '.carousel-control.right', function() {
		$('#compare_tabcontent').carousel('next');
	});
", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("SlideTabs2", "
  $('#viewed_tabcontent').carousel({
  interval: 0
});	
$('#viewed_tabcontent').on('click', '.carousel-control.left', function() {
		$('#viewed_tabcontent').carousel('prev');
	});

	$('#viewed_tabcontent').on('click', '.carousel-control.right', function() {
		$('#viewed_tabcontent').carousel('next');
	});
", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("addToCompare", "


$('body').on('click', '.add-to-compared', function() {
		var compareButton = $(this);
        var inCompare = $(compareButton).attr('inCompare');
		var productId = $(compareButton).attr('data-productId');
		if(inCompare !== undefined && inCompare != 1)
		{
			var url = '/compare/addToCompare';
			$.ajax({
					url: url,
					data: {productId: productId, lastCompare: window.location.href},
					dataType: 'json',
					type: 'get',
					success: function(data){
						if(data.title == 'Success')
						{
							$(compareButton).text('К сравнению');
							$(compareButton).attr('inCompare', '1');
							$(compareButton).attr('href', '".app()->getBaseUrl(true)."/compare');
							
							var added = 0;
							var count = 0;
							$('.bottom-tabs #compare_tabcontent .item').each(function(key, value){
								if($(value).children('.compareProduct').length < 4)
								{
									$(value).append(data.html);
									added = 1;
								}
							})
							if(added == 0)
							{
								var item = 'item';
								if($('.bottom-tabs .item').length < 1)
								{
									item += ' active';
								}
								$('.bottom-tabs .carousel-inner').append('<div class=\"' + item + '\">' + data.html + '</div>');
							}
							count += $('.bottom-tabs #compare_tabcontent .item').children('.compareProduct').length;
							$('.bottom-tabs #compare span').text(count);
                            if (count>0) $('#compare-href').show();
							$('.bottom-tabs').fadeIn();
						}
					}
			});
			return false;
		}
	});
", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("removeFromCompare", "
	$('body').on('click', '.removeProduct', function() {
		var productId = $(this).attr('data-productId');
		var url = '/compare/removeFromCompare';
		$.ajax({
				url: url,
				data: {productId: productId},
				dataType: 'json',
				type: 'get',
				success: function(data){
					if(data.title == 'Success')
					{
						var compareButton = $('.add-to-compared[data-productId=\"' + productId + '\"]');
						$(compareButton).text('Сравнить');
						$(compareButton).attr('inCompare', '0');
						$(compareButton).attr('href', '#');
						$('.compareProduct.pd' + productId).fadeOut('slow', function(){
							var compareCarousel = $('.compareProduct.pd' + productId).parent('ul').parent('.compareCarousel');
							$('.compareProduct.pd' + productId).remove();
							if($(compareCarousel).length != 0)
							{
								var categoryId = $(compareCarousel).attr('catId');
								$('#compareProductsCarousel' + categoryId).jcarousel('reload');
								$('#compareSpecsCarousel' + categoryId).jcarousel('reload');
								var count = $('.bottom-tabs #compare span').text();
								$('.bottom-tabs #compare span').text(count - 1);

								var products = [];
								var count = 0;
								var activeOnCount = 0;
								$('#compare_tabcontent .item').children('').each(function(key, child){
									products.push($('<div>').append($(child).clone()).html());
								});
								$('#compare_tabcontent .item').each(function(key, value){
									count++;
									if($(value).hasClass('.active'))
										activeOnCount = count;
									$(value).remove();
								});
								var count = 0;
								var item = 'item';
								while(products.length > 0)
								{
									if(count == activeOnCount)
									{
										item += ' active';
									}
									var length = products.length;
									var htmlItem = '<div class=\"' + item + '\">';
									for(var i = 0; i < length; i++)
									{
										if(i > 3)
											break;
										htmlItem += products.shift();
									}
									htmlItem += '</div>';
									$('.bottom-tabs #compare_tabcontent .carousel-inner').append(htmlItem);
									count++;
									item = 'item';
								}
							}
                            $('#compare.tabName span').text($('.carousel-inner .item table').length);
                            if ($('#compare.tabName span').text()!=0)
                                $('#compare-href').show();
                            else
                                $('#compare-href').hide();
                                
						});
					}
				}
		});
		return false;
	});
", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("close-open", "
	$('body').on('click', '#closeBottomTabs', function(){
		$('.bottom-tabs .content').removeClass('show');
		$('.bottom-tabs .content').addClass('hide');
		$('.bottom-tabs #closeBottomTabs').hide();
		return false;
	});
	
	$('body').on('click', function(e){
		if(!$(e.target).hasClass('bottom-tabs') && !$(e.target).parents().is('.bottom-tabs'))
		{
			$('.bottom-tabs .content').removeClass('show');
			$('.bottom-tabs .content').addClass('hide');
			$('.bottom-tabs #closeBottomTabs').hide();
		}
	});
	
	$('body').on('click', '.bottom-tabs', function(){
		$('.bottom-tabs .content').removeClass('hide');
		$('.bottom-tabs .content').addClass('show');
		$('.bottom-tabs #closeBottomTabs').show();
	});
", CClientScript::POS_END);

if(Yii::app()->controller->id != 'compare'):?>
<div class="bottom-tabs-container">
	<div class="bottom-tabs" <?php echo !empty($compareProducts) || !empty($viewed['products']) || !empty($viewed['complects']) ? '' : 'style="display:none;"'?>>
		<div class="tabNames">
			<div id="viewed" class="tabName <?php echo !empty($viewed['products']) || !empty($viewed['complects']) ? 'active' : ''?>">
				<?php
				$count = 0;
				foreach ($viewed as $value)
				{
					$count += sizeof($value);
				}
				?>
				Просмотренные товары (<span><?php echo $count; ?></span>)
			</div>
			<div id="compare" class="tabName <?php echo !empty($compareProducts) && empty($viewed['products']) && empty($viewed['complects']) ? 'active' : ''?>">
				<?php
				$count = 0;
				foreach ($compareArray as $value)
				{
					$count += sizeof($value);
				}
				?>
				Сравнение товаров (<span><?php echo $count; ?></span>)
			</div>
			<a class="close" id="closeBottomTabs" href="#" style="display: none; background-image: url(/images/minimize.png);"></a>
		</div>

		<div class="content hide">
			<div class="bottom-carousel various-product-block carousel display-block slide <?php echo !empty($viewed['products']) || !empty($viewed['complects']) ? '' : 'hide'?>" id="viewed_tabcontent">
				<div class="carousel-inner show" style="min-height: 276px">
					<div class="item active">
						<?php
						foreach($viewed as $key => $idArray):
							$count = 0;
							foreach($idArray as $id):
							$model = $this->getModel($key, $id);
							if($count > 3):	?>
						</div><div class="item">
							<?php 
							$count = 0;
							endif;
							$count++;
                            if (!$model==null):
							?>
							<div class="tab-product <?php echo $model->cartPattern; ?>">
								<div class="pd-qty-block hide"><input hidden value="1"></div>
								<div class="code pd-code" style="display: none;">Код товара: <span><?php echo $model->id;?></span></div>
								<div class="thumb">
									<a href="<?php echo $model->link;?>">
										<span style="background-image: url('<?php echo $model->image_url; ?>')" class="img-vert">
                                            <img style="display:none" src="<?php echo $model->image_url; ?>" alt="">
                                        </span>
									</a>
								</div>
								<div class="name"><a href="<?php echo $model->link;?>"><?php echo $model->name;?></a></div>
								<div class="price"><?php echo CurrencySys::exchange($model->price).' '.CurrencySys::getLabel(); ?></div>
								<div class="pd-usd-price"><?php echo CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY).' '.$model->price; ?></div>
                                <?php if ($model->quantity != null):?>
                                    <?php if ($model->quantity > 0):?>
                                        <div id="btn" class="btn-buy-<?php echo $model->id; ?> enabled" style="<?php echo $model->inCart ? 'display: none;' : ''?>">
                                            <a data-is_complect="0" id="<?php echo $model->cartPattern; ?>" href="#" class="btn-buy-buy index-buy-but">Купить</a>
                                        </div>
                                        <div class="btn-buy-<?php echo $model->id; ?> disabled" id="<?php echo $model->cartPattern; ?>-div"  style="<?php echo $model->inCart ? '' : 'display: none;'?>">
                                            <a data-is_complect="0" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
                                        </div>
                                    <?php else:?>
                                        <a class="catalog btn-buy-order" data-is_complect="0" href="#" id="pd<?php echo $model->id; ?>" style="margin-left: 28px;">Заказать</a>
                                    <?php endif;?>
                                <?php else:?>
                                    <a class="catalog btn-buy-soon" data-is_complect="0" href="#" id="pd<?php echo $model->id; ?>" style="font-size: 18px; line-height:23px; margin-left: 28px;">Уведомить о наличии</a>
                                <?php endif;?>
							</div>
							<?php endif; endforeach; ?>
						<?php endforeach; ?>
					</div>
				</div>
				<a class="carousel-control left" style="margin-top: 0;" href="#" data-slide="prev">
					<img alt="" src="/images/pageListLeft.png" style="height: 24px; width: 15px;">
				</a>
				<a class="carousel-control right" style="margin-top: 0;" href="#" data-slide="next">
					<img alt="" src="/images/pageListRight.png" style="height: 24px; width: 15px;">
				</a>
			</div>
			<div class="bottom-carousel carousel display-block slide <?php echo !empty($compareProducts) && empty($viewed['products']) && empty($viewed['complects']) ? '' : 'hide'?>" id="compare_tabcontent">
				<div class="carousel-inner" style="min-height: 225px">
					<div class="item active">
						<?php 
						$count = 0;
						foreach($products as $product):
						if($count > 3):	?>
					</div><div class="item">
						<?php 
						$count = 0;
						endif;
						$count++;
						?>
						<table class="compareProduct <?php echo $product->cartPattern; ?>">
							<tr>
								<td class="bottom_delete_compare">
                                    <a class="removeProduct" style="border:none" data-productId="<?php echo $product->id; ?>" href="#"><img src="/images/cross.gif" alt=""></a>
									<a class="removeProduct" data-productId="<?php echo $product->id; ?>" href="#">удалить</a>
								</td>
							</tr>
							<tr>
								<td class="thumb">
									<div class="pd-qty-block" style="display:none;"><input hidden value="1"></div>
									<div class="code pd-code" style="display: none;">Код товара: <span><?php echo $product->id;?></span></div>
									<a class="imgUrl" href="<?php echo $product->link;?>">
										<span style="background-image: url('<?php echo $product->image_url; ?>')" class="img-vert">
                                            <img style="display:none" src="<?php echo $product->image_url; ?>" alt="">
                                        </span>
									</a>
								</td>
							</tr>
							<tr>
								<td class="name">
                                    <a style="border-bottom: 1px solid #9FA4A6;" href="<?php echo $product->link;?>">
										<?php echo $product->name?>
									</a>
								</td>
							</tr>
						</table>
						<?php endforeach; ?>
					</div>
				</div>
				<a class="carousel-control left" href="#" data-slide="prev">
					<img alt=""  src="/images/pageListLeft.png" style="height: 24px; width: 15px;">
				</a>
				<a class="carousel-control right" href="#" data-slide="next">
					<img alt="" src="/images/pageListRight.png" style="height: 24px; width: 15px;">
				</a>
				<div id="compare-href">
					<a href="<?php echo app()->getBaseUrl(true); ?>/compare">Сравнить</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif;?>