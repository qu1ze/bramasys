<?php

/**
 * This is the model class for table "client".
 *
 * The followings are the available columns in table 'client':
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $lastname
 * @property string $address
 * @property string $delivery_address
 * @property string $phone
 * @property string $image
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property string $password_strategy
 * @property integer $requires_new_password
 * @property string $email
 * @property integer $login_attempts
 * @property integer $login_time
 * @property string $login_ip
 * @property string $validation_key
 * @property integer $created_id
 * @property integer $created_time
 * @property integer $update_id
 * @property integer $update_time
 * @property string $middle_name
 * @property integer $date_of_birth
 * 
 * The followings are the available model relations:
 * @property Cart[] $carts
 */
class Client extends ActiveRecord
{
	const ADDRESS_TOWN_LABEL = 'г.';
	const ADDRESS_STREET_LABEL = 'ул.';
	const ADDRESS_HOUSE_LABEL = '';
	const ADDRESS_FLAT_LABEL = 'кв.';
	const ADDRESS_ADDITIONAL_INFO_LABEL = 'доп.';
	
    //public $service;
    
	public $origName;
	public $origEmail;

	public $oldPassword;
	/**
	 * @var string attribute used for new passwords on user's edition
	 */
	public $newPassword;

	/**
	 * @var string attribute used to confirmation fields
	 */
	public $passwordConfirm;

	private $phone = '';
	private $delivery_address = null;
	private $deliveryAddressNodes = null;
	
	public function getOriginalName()
	{
		return $this->origName;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'client';
	}
	
		/**
	 * Behaviors
	 * @return array
	 */
	public function behaviors()
	{
		Yii::import('common.extensions.behaviors.password.*');
		return array(
			// Password behavior strategy
			"APasswordBehavior" => array(
				"class" => "APasswordBehavior",
				"defaultStrategyName" => "bcrypt",
				"strategies" => array(
					"bcrypt" => array(
						"class" => "ABcryptPasswordStrategy",
						"workFactor" => 14,
						"minLength" => 6
					),
					"legacy" => array(
						"class" => "ALegacyMd5PasswordStrategy",
						'minLength' => 6
					)
				),
			)
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'required'),
            array('name, lastname, address, image, username, password, email, middle_name', 'filter', 'filter'=>'trim'),
			array('name, lastname, middle_name, phone', 'required', 'on' => 'order, registerOnOrder'),
			//array('email', 'required', 'on' => 'checkout'),
			array('email', 'unique', 'on' => 'checkout', 'message' => Yii::t('validation', 'Email уже занят')),
			array('email', 'email'),
			array('email', 'unique'),
			array('passwordConfirm', 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('validation', "Пароли не совпадают")),
			array('newPassword, password_strategy ', 'length', 'max' => 50, 'min' => 6, 'on'=>'register, registerOnOrder'),
			array('newPassword', 'length', 'max' => 50, 'min' => 6, 'on'=>'update'),
			array('email, password, salt', 'length', 'max' => 255),
			array('name, lastname, middle_name', 'length', 'max'=>50),
            array('date_of_birth','match', 'pattern'=>'/^([+]?\d\d\.\d\d\.\d\d\d\d)$/','message'=>'Дата рождения должна быть в формате дд.мм.гггг'),
            //array('phone','match', 'pattern'=>'/^([+]?\(\d\d\d\)\d\d\d\-\d\d\-\d\d)$/'),
            //array('date_of_birth', 'date', 'format'=>'dd/MM/yyyy'),
            //array('date_of_birth', 'numerical', 'integerOnly' => true, 'on' => 'update'),
			array('requires_new_password, login_attempts', 'numerical', 'integerOnly' => true),
			array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty' => true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, lastname, image, password, salt, password_strategy , requires_new_password , email, phone, phonesList', 'safe', 'on' => 'search'),
            array('date_of_birth', 'safe'),
            
			// Правила регистрации
			array('newPassword, passwordConfirm, email', 'required', 'on'=>'register, registerOnOrder'),
			array('passwordConfirm', 'compare', 'compareAttribute'=>'newPassword', 'on'=>'register, registerOnOrder'),
		);
	}

	/**
	 * @return array relational rules.
	 */
    
    function genPass($length = 8) {
          $Limit=explode(',','65-78,80-90,97-107,109-122,50-57');
          mt_srand(time());
          $result = '';
          for ($i = 1; $i <= $length; $i++) {
            $nrand = mt_rand(0, count($Limit)-1);
            list($min,$max) = explode('-', $Limit[$nrand]);
            $result.=chr(mt_rand($min,$max));
            }
          return $result;
      }
    
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'carts' => array(self::HAS_MANY, 'Cart', 'client_id'),
            'phones0' => array(self::HAS_MANY, 'Phone', 'client_id'),
            'address0' => array(self::HAS_MANY, 'deliveryaddress', 'client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'lastname' => 'Фамилия',
			'address' => 'Адрес',
			'delivery_address' => 'Адрес доставки',
			'phone' => 'Номер телефона',
			'image' => 'Изображение',
			'middle_name' => 'Отчество',
			'date_of_birth' => 'Дата рождения',
			'username' => Yii::t('labels', 'Логин'),
			'password' => Yii::t('labels', 'Пароль'),
			'newPassword' => Yii::t('labels', 'Пароль'),
			'oldPassword' => Yii::t('labels', 'Старый пароль'),
			'passwordConfirm' => Yii::t('labels', 'Подтверждение пароля'),
			'email' => Yii::t('labels', 'Электронная почта'),
            'phonesList' => 'Телефоны',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('delivery_address',$this->delivery_address,true);
        if(!empty($this->phone))
        {
            $criteria->join ='INNER JOIN phone ON phone.client_id = `t`.`id`';
            $criteria->compare('value',$this->phone,true);
        }
		$criteria->compare('image',$this->image,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('password_strategy',$this->password_strategy,true);
		$criteria->compare('requires_new_password',$this->requires_new_password);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('login_attempts',$this->login_attempts);
		$criteria->compare('login_time',$this->login_time);
		$criteria->compare('login_ip',$this->login_ip,true);
		$criteria->compare('validation_key',$this->validation_key,true);
		$criteria->compare('created_id',$this->created_id);
		$criteria->compare('created_time',$this->created_time);
		$criteria->compare('update_id',$this->update_id);
		$criteria->compare('update_time',$this->update_time);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('date_of_birth',$this->update_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	private function timeToDate($time = null)
	{
		if(empty($time))
		{
			if($this->date_of_birth !== null && is_numeric($this->date_of_birth))
				$this->date_of_birth = date('d.m.Y', $this->date_of_birth);
		}
		else
			return date('d.m.Y', $time);
	}
	
	private function dateToTime($date = null)
	{
		if(empty($date))
		{
			if(!is_numeric($this->date_of_birth) && !empty($this->date_of_birth))
			{
				$timeArray = explode(".", $this->date_of_birth);
				$this->date_of_birth = sizeof($timeArray) > 2 ? mktime(0, 0, 0, $timeArray[1], $timeArray[0], $timeArray[2]) : 0;
			}
		}
		else
		{
			$timeArray = explode(".", $date);
			return sizeof($timeArray) > 2 ? mktime(0, 0, 0, $timeArray[0], $timeArray[1], $timeArray[2]) : 0;
		}
	}
	
	protected function afterFind()
	{
		$this->origName = $this->name;
		$this->origEmail = $this->email;
		if(empty($this->name))
		{
			$this->name = $this->email;
		}
		$this->timeToDate();
		$this->setDelivery();
        return parent::afterFind();
	}
	
	/**
	 * Generates a new validation key (additional security for cookie)
	 */
	public function regenerateValidationKey()
	{
		$this->saveAttributes(array(
			'validation_key' => md5(mt_rand() . mt_rand() . mt_rand()),
		));
	}
	
	protected function afterConstruct() {
		$this->lastname = '';
        return parent::afterConstruct();
	}
	
	protected function beforeValidate()
	{
		
		$this->username = $this->email;
		return  parent::beforeValidate();
	}
	
	protected function beforeSave()
	{
		if($this->origEmail == $this->name)
			$this->name = $this->origName;
        $this->dateToTime();
		return parent::beforeSave();
	}

    public function sendMail($email, $password, $name, $lastname, $middleName)
    {
        $mails = MailTemplate::model()->findAllByAttributes(array('name' => 'registration'));
        foreach($mails as $mail)
        {
            $mail->send(array(
                '$name' => $name,
                '$lastname' => $lastname,
                '$middleName' => $middleName,
                '$email' => $email,
                '$password' => $password,
            ));
        }
        return true;
    }

	protected function afterSave()
	{
        $this->timeToDate();
		$this->origName = $this->name;
		if(empty($this->name))
		{
			$this->name = $this->email;
		}
        if($this->scenario == 'register')
        {
            $this->sendMail(
                $this->email,
                $this->newPassword,
                $this->name,
                $this->lastname,
                $this->middle_name
            );
            Yii::app()->user->setState('id', $this->id);
        }
		return parent::afterSave();
	}
	
	public function getPhone()
	{
		if($this->phone == null)
		{
			$phone = Phone::model()->findByAttributes(array('client_id' => $this->id));
			if($phone !== null)
				$this->phone = $phone->getPhoneTypes($phone->type).' '.$phone->value;
		}
		return $this->phone;
	}
	
	public function getPhones($valueToKey = false)
	{
		$phones = Phone::model()->findAllByAttributes(array('client_id' => $this->id));
		$phonesArray = array();
		foreach ($phones as $phone)
		{
			$phoneLine = $phone->getPhoneTypes($phone->type).' '.$phone->value;
			if($valueToKey)
				$phonesArray[$phoneLine] = $phoneLine;
			else
				array_push($phonesArray, $phoneLine);
		}
		return $phonesArray;
	}

    public function getPhonesList()
    {
        if(empty($this->phonesList))
        {
            $phonesArray = $this->getPhones();
            $list = "";
            foreach($phonesArray as $phone)
            {
                $list .= $phone."<br>";
            }
            $this->phonesList = $list;
        }
        return $this->phonesList;
    }

    public function setPhonesList($value)
    {
        $this->phonesList = $value;
        $this->phone = $value;
    }

	private function setDelivery()
	{
		$deliveryAddress = DeliveryAddress::model()->findByAttributes(array('client_id' => $this->id));
		if($deliveryAddress !== null)
		{
			$this->deliveryAddressNodes = array();
			$this->deliveryAddressNodes[self::ADDRESS_TOWN_LABEL] = $deliveryAddress->town;
			$this->deliveryAddressNodes[self::ADDRESS_STREET_LABEL] = $deliveryAddress->street;
			$this->deliveryAddressNodes[self::ADDRESS_HOUSE_LABEL] = $deliveryAddress->house;
			$this->deliveryAddressNodes[self::ADDRESS_FLAT_LABEL] = $deliveryAddress->flat;
			$this->deliveryAddressNodes[self::ADDRESS_ADDITIONAL_INFO_LABEL] = $deliveryAddress->additional_info;
			
			$this->delivery_address = 
					self::ADDRESS_TOWN_LABEL.' '.$deliveryAddress->town.' '.
					self::ADDRESS_STREET_LABEL.' '.$deliveryAddress->street.' '.
					self::ADDRESS_HOUSE_LABEL.' '.$deliveryAddress->house.' '.
					self::ADDRESS_FLAT_LABEL.' '.$deliveryAddress->flat;
			if(!empty($deliveryAddress->additional_info))
				$this->delivery_address	.= ' '.self::ADDRESS_ADDITIONAL_INFO_LABEL.' '.$deliveryAddress->additional_info;
		}
	}
	
	public function getDelivery_address()
	{
		if($this->delivery_address == null)
		{
			$this->setDelivery();
		}
		return $this->delivery_address;
	}
	
	public function getDeliveryAddresses($valueToKey = false)
	{
		$deliveryAddresses = DeliveryAddress::model()->findAllByAttributes(array('client_id' => $this->id));
		$addressesArray = array();
		foreach ($deliveryAddresses as $deliveryAddress)
		{
			$address = self::ADDRESS_TOWN_LABEL.' '.$deliveryAddress->town.' '.
						self::ADDRESS_STREET_LABEL.' '.$deliveryAddress->street.' '.
						self::ADDRESS_HOUSE_LABEL.' '.$deliveryAddress->house.' '.
						self::ADDRESS_FLAT_LABEL.' '.$deliveryAddress->flat.' '.
						self::ADDRESS_ADDITIONAL_INFO_LABEL.' '.$deliveryAddress->additional_info;
			if($valueToKey)
				$addressesArray[$address] = $address;
			else
				array_push($addressesArray, $address);
		}
		return $addressesArray;
	}
	
	public function getDeliveryAddressNodes()
	{
		if($this->deliveryAddressNodes == null)
		{
			$this->setDelivery();
		}
		return $this->deliveryAddressNodes;
	}
}
