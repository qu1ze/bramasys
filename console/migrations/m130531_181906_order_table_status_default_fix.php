<?php

class m130531_181906_order_table_status_default_fix extends CDbMigration
{
	public function up()
	{
            $this->execute("ALTER TABLE  `order` CHANGE  `status`  `status` TINYINT( 1 ) NOT NULL DEFAULT  '1'");
	}

	public function down()
	{
            $this->execute("ALTER TABLE  `order` CHANGE  `status`  `status` TINYINT( 1 ) NOT NULL DEFAULT  '0'");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}