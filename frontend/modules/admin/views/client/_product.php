<?php
	$checked = $prod['mounting'];
?>
	<tr class="product_<?php echo $product->id?>">
		<td class="product-img-cart">
			<a href="<?php echo $product->link; ?>">
				<img style="width: 72px;" src="<?php echo $product->image_url;?>">
			</a>
			<div class="product-name-cart">
				<a href="<?php echo $product->link; ?>"><?php echo $product->name; ?></a>
			</div>
		</td>
		<td class="product-in-complect">
			<?php if ($inComplect !== 0):?>
				<?php echo $inComplect;?>
			<?php else: ?>
				<?php echo 'Нет';?>
			<?php endif; ?>
		</td>
		<td class="product-price-cart">
			<p><?php echo CurrencySys::exchange($prod['price']).' '.CurrencySys::getLabel(); ?></p>
			<?php if (!CurrencySys::isDefaultInCookie()):?>
				<p><?php echo $prod['price'].' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY)?></p>
			<?php endif; ?>
		</td>
		<td class="input-cart">
			<?php echo $prod['count']?> шт.
		</td>
		<td class="checkbox-cart">
			<?php $checked = $checked > 0 ? "Да" : "Нет";?>
			<?php echo $checked;?>
		</td>
		<td class="product-price-cart">
			<p id="firstPrice">
				<?php echo CurrencySys::exchange($amount).' '.CurrencySys::getLabel(); ?>
			</p>
			<?php if (!CurrencySys::isDefaultInCookie()):?>
				<p id="lastPrice"><?php echo $amount.' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY)?></p>
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td colspan="6" style="padding: 0px;"><hr style="margin: 0px;border-top-color: #ccc;"></td>
	</tr>