<?php

class OrderController extends Controller
{		
    public function actionIndex()
    {
        $model = new Order('search');
        if (isset($_GET['Order']))
		{
            $model->attributes = $_GET['Order'];
			$model->afterDate = $_GET['Order']['afterDate'];
			$model->beforeDate = $_GET['Order']['beforeDate'];
		}
        $this->render('index', array(
                'model' => $model,
                )
        );
    }
	
	public function actionOrderInfo($id)
    {
        $client = Cart::model()->findByAttributes(array('order_id' => $id))->client;
		$orderProducts = OrderProduct::model()->findAllByAttributes(array('order_id' => $id));
        $this->render('order_info', array(
                'client' => $client,
				'orderProducts' => $orderProducts,
				'order' => Order::model()->findByPk($id),
                )
        );
    }
	
//	public function actionAjaxDelete($id)
//    {		
//        if (($model = Order::model()->findByPk($id)) !== null)
//        {
//                if ($model->delete())
//                        echo json_encode(array('title' => 'Success', 'msg' => 'Заказ упешно удален'));
//                else
//                        echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить заказ'));
//        } else
//                echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Заказ не найден'));
//    }

    public function actionUpdate($id)
    {
        if (($model = Order::model()->findByPk($id)) === null)
                $this->redirect(array('/admin/order'));				

        if (isset($_POST['Order']))
        {			
                $model->attributes = $_POST['Order'];
                if ($model->save())
                        $this->redirect(array('/admin/order'));
        }

        $this->render('update', array(
                'model' => $model,
                )
        );
    }

    public function actionAjaxUpdate()
    {
        Yii::import('bootstrap.widgets.TbEditableSaver', true);
        $es = new TbEditableSaver('Order');

        $es->update();

        die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Заказ упешно отредактирован')));			
    }
}