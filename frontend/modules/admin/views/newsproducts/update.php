<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => $model->getFormId(),
		'type' => 'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

	<?php $this->renderPartial('_form', array('form' => $form, 'model' => $model)) ?>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить', 'id' => 'submit')); ?>
	
<?php $this->endWidget(); ?>