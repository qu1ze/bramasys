<div id="container">
	<?php
	$this->breadcrumbs = CMap::mergeArray(
		app()->categoryController->getCategoryBcs($this->categoryName, true),
		array(
			$model->name,
		));

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
	?>
	<div class="partners">
		<h3 style="display: inline-block;"><?php echo $model->name; ?></h3> <a href="<?php echo $model->fullPath; ?>"><h3 style="display: inline-block;">(Скачать)</h3></a>
		<div class="content_partners">
			<div class="partners-group" style="display: inline-block;">
				<a class="imgUrl" style="float:left; margin: 10px;" href="<?php echo $model->fullPath; ?>"><img width="150" src="<?php echo $model->image; ?>"></a>
				<p style="font: 16px 'PTSN Regular'"><?php echo $model->full_description; ?></p>
			</div>
			<div class="arrow-up">
				<a href="#">Вверх<img src="/images/arrowup.jpg"></a>
			</div>
		</div>
	</div>
</div>
