<?php

class m131214_215040_client_add_colum_for_social extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE  `client` ADD  `service` VARCHAR( 100 ) DEFAULT NULL ,
                                                ADD  `identity` VARCHAR( 100 ) DEFAULT NULL ;");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `order` DROP `service`, DROP `identity` ;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}