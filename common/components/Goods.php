<?php
interface Goods
{
	public function getName();
	public function getInCompare();
	public function getInCart();
	public function getImage_url();
	public function getHasLabels();
	public function getOld_price();
	public function getPrice();
	public function getQuantity();
	public function getDescription();
	public function getLink();
	public function getRating();
    public function getVotedCount();
	public function getInBookmarks();
	public function getCategoryName();
	public function getIsComplect();
	public function getCartPattern();
	public function getMetaTitle();
	public function getMetaDescription();
	public function getMetaKeywords();
	public function getH1();
    public function getHasComments();
    public function getCommentsCount();
}
