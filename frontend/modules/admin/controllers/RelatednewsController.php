<?php

class RelatednewsController extends Controller
{		
	public function actionCreate()
	{
		$model = new RelatedNews; 
		
		$this->performAjaxValidation($model);
		
		if (isset($_POST['RelatedNews']))
		{			
			 if (isset($_POST['RelatedNews']['related_news_id_s']) && is_array($_POST['RelatedNews']['related_news_id_s']))
                if($model->saveRrelateds($_POST['RelatedNews']['related_news_id_s'],$_POST['RelatedNews']['news_id']))
                   	$this->redirect(array('/admin/relatednews'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new RelatedNews();	

		if (isset($_POST['RelatedNews']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['RelatedNews'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Связь для новостей успешно создана'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать связь для новостей'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new RelatedNews('search');
		
		if (isset($_GET['RelatedNews']))
			$model->attributes = $_GET['RelatedNews'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = RelatedNews::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/relatednews'));				
				
		if (isset($_POST['RelatedNews']))
		{			
			if (isset($_POST['RelatedNews']['related_news_id_s']) && is_array($_POST['RelatedNews']['related_news_id_s'])){
                if($model->saveRrelateds($_POST['RelatedNews']['related_news_id_s'],$_POST['RelatedNews']['news_id'],true)){
                    $this->redirect(array('/admin/relatednews'));
           }}
		}
		$mass = RelatedNews::model()->findAllByAttributes(array('news_id' => $model->news_id)); 
        
        foreach ($mass as $value)
            $model->related_news_id_s[]=$value->related_news_id;
				
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('RelatedNews');	
		$es->update();		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Связь для новостей успешно отредактирована')));			
	}	
	
	public function actionAjaxDelete($id)
	{		
		if (($model = RelatedNews::model()->findByPk($id)) !== null)
		{			
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Связь для новостей успешно удалена'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить связь для новостей'));
			
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Связь не найдена'));
	}
	
}