<?php
	
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'horizontal',
	'htmlOptions' => array('enctype' => 'multipart/form-data', 'style' => 'margin: 0px'),
));?>
	<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->textFieldRow($model, 'question');?>
	<?php echo $form->textAreaRow($model, 'answer');?>
	<?php echo $form->dropDownListRow($model, 'category_id', $this->getCategories(true)); ?>
	


	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
<?php $this->endWidget();?>