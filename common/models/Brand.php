<?php

/**
 * This is the model class for table "brand".
 *
 * The followings are the available columns in table 'brand':
 * @property integer $id
 * @property string $name
 * @property string $short_description
 * @property string $full_description
 * @property string $image_url
 * @property string $url
 * @property boolean $partner
 *
 * The followings are the available model relations:
 * @property Products[] $products
 * @property SupportFile[] $supportFiles
 */
class Brand extends ActiveRecord
{
	const BRAND_DEFAULT_IMAGE_URL = '/images/partners.jpg';
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Brand the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'brand';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
            array('name, short_description, full_description, image_url, url', 'filter', 'filter'=>'trim'),
			array('name', 'unique'),
			array('partner', 'boolean'),
			array('name, short_description, image_url, url', 'length', 'max'=>255),
			array('full_description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, short_description, full_description, image_url, url, partner', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products' => array(self::HAS_MANY, 'Products', 'brand'),
			'supportFiles' => array(self::HAS_MANY, 'SupportFile', 'brand_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'short_description' => 'Краткое описание',
			'full_description' => 'Полное описание',
			'image_url' => 'Url картинки',
			'url' => 'Ссылка на сайт',
			'partner' => 'Партнер',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('short_description',$this->short_description,true);
		$criteria->compare('full_description',$this->full_description,true);
		$criteria->compare('image_url',$this->image_url,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('partner',$this->partner);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getIsAnnounceable()
	{
		if(!empty($this->short_description) && !empty($this->full_description))
		{
			return true;
		}
		return false;
	}
	
	public function getPageUrl()
	{
		$url = '/';
		if($this->getIsAnnounceable() && $this->partner)
		{
			$url = app()->categoryController->getCategoryAddress('partners').'/'.$this->name;
		}
		return $url;
	}
	
	public function getImg_url()
	{
		if(empty($this->image_url))
		{
			$this->image_url = self::BRAND_DEFAULT_IMAGE_URL;
		}
		return $this->image_url;
	}
	
    public static function getListData($first = false)
	{
		$data = CHtml::listData(self::model()->findAll(array('index' => 'id')), 'id', 'name');
		return $first ? array('' => '') + $data : $data;
	}
	
	protected function afterConstruct()
	{
		parent::afterConstruct();
		$this->partner = null;
	}
}