<?php
/**
 * register.php
 */
$this->pageTitle = 'Registration';
$this->breadcrumbs = array(
	'Registration',
);
?>
<h1>Registration</h1>

<p>Please fill out the following form with registration credentials:</p>


<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'registration-form',
	'type' => 'vertical',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>	
	
	<?php echo $form->textFieldRow($model, 'username'); ?>
	<?php echo $form->textFieldRow($model, 'name'); ?>
	<?php echo $form->textFieldRow($model, 'email'); ?>
	<?php echo $form->passwordFieldRow($model, 'newPassword'); ?>
	<?php echo $form->passwordFieldRow($model, 'passwordConfirm'); ?>
	
	<div class="actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Register')); ?>
	</div>

<?php $this->endWidget(); ?>