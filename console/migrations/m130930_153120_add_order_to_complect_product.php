<?php

class m130930_153120_add_order_to_complect_product extends CDbMigration
{
    public function up()
    {
        $this->execute("
			ALTER TABLE `complect_product`
				ADD `order` int DEFAULT NULL;
				");
    }

    public function down()
    {
        $this->execute("
			ALTER TABLE `complect_product`
				DROP `order`;
				");
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}