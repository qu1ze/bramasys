<?php

class m130421_181210_category_update extends CDbMigration
{
	public function up()
	{
		$this->addColumn('category', 'tag', 'int');
		$this->addColumn('category', 'order_number', 'int');
	}

	public function down()
	{
		$this->dropColumn('category', 'tag');
		$this->dropColumn('category', 'order_number');
	}

}