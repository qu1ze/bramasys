<?php

class RobotsController extends Controller
{
	public function actionIndex()
	{
		$robotsModel = new Robots();
		$robotsModel->load();
		$this->render('index', array(
			'robotsModel' => $robotsModel
		));
	}
	
	public function actionTextEdit()
	{
		$robotsModel = new Robots();
		$robotsModel->load(true);
		
		if (isset($_POST['Robots']))
		{
			if($robotsModel->save($_POST['Robots']['content']))
			{
				$this->redirect('/admin/robots');
			}
		}
													
		$this->render('textedit', array(
			'robotsModel' => $robotsModel
		));
	}
	
	public function actionEdit()
	{
		$robotsModel = new Robots();
		$robotsModel->load();
		
		if (isset($_POST['Robots']))
		{
			$robotsModel->attributes = $_POST['Robots'];
			$robotsModel->rules = is_array($_POST['Robots']['rules']) ? $_POST['Robots']['rules'] : array();
			$robotsModel->cleanParams = is_array($_POST['Robots']['cleanParams']) ? $_POST['Robots']['cleanParams'] : array();
			if($robotsModel->save())
			{
				$this->redirect('/admin/robots');
			}
		}
													
		$this->render('edit', array(
			'robotsModel' => $robotsModel
		));
	}
}