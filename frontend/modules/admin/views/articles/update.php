<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => $model->getFormId(),
		'type' => 'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

	<?php $this->renderPartial('_form', array(
		'form' => $form,
		'model' => $model,
		'articles_sub_categories' => $articles_sub_categories
	)) ?>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	
<?php $this->endWidget(); ?>