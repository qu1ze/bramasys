<?php

class SupportController extends StaticController
{	
	public static function getRelevantSearchCriteria($search)
	{
		if(!empty($search))
		{
			$searchCriteria = self::getSearchCriteria($search);
			if($searchCriteria !== null)
			{
                $search = substr($search, 0, 127);
                $searchArray = array_filter(preg_split( "/(\s|-|,|\.|\\|\/)/", $search));
                if(empty($searchArray))
                {
                    $searchArray = array($search);
                }
				$select = '';
				foreach ($searchArray as $searchNode)
				{
					$select .= " IF (`name` LIKE '%$searchNode%', 1, 0) + IF (`description` LIKE '%$searchNode%', 0.1, 0) + IF (`full_description` LIKE '%$searchNode%', 0.1, 0) +";
				}
				if(!empty($select))
				{
					$select = rtrim($select, '+');
					$searchCriteria->select = "($select) as `relevant`";
					$searchCriteria->order = '`relevant` DESC';
				}
			}
			return $searchCriteria;
		}
		return null;
	}
	
	public static function getSearchCriteria($search)
	{
        $search = substr($search, 0, 127);
        $searchArray = array_filter(preg_split( "/(\s|-|,|\.|\\|\/)/", $search));
        if(empty($searchArray))
        {
            $searchArray = array($search);
        }
        $searchCriteria = new CDbCriteria();
        foreach ($searchArray as $searchNode)
        {
            $searchCriteria->addSearchCondition('`name`', $searchNode, true, 'OR');
            $searchCriteria->addSearchCondition('`description`', $searchNode, true, 'OR');
            $searchCriteria->addSearchCondition('`full_description`', $searchNode, true, 'OR');
        }
        return $searchCriteria;
	}
	
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => SupportFile::model()->findByPk($id),
		));
	}
	
	public function actionIndex($forCategory = null, $brand = null, $search = null)
	{
		$mainCategory = Category::model()->findByAttributes(array('name' => substr($this->categoryName, 0, strlen($this->categoryName) - 8)));
		if($mainCategory !== null)
		{
			$criteria = new CDbCriteria();
			$criteria->addCondition('category_id = '.$mainCategory->id);
			$categoriesIds = Yii::app()->categoryController->getChildrenIds($mainCategory->id);
			if(!empty($forCategory))
				$criteria->addCondition('for_category_id = '.$forCategory);
			if(!empty($brand))
				$criteria->addCondition('brand_id = '.$brand);
			if(!empty($search))
			{
				$searchCriteria = self::getRelevantSearchCriteria($search);
				$criteria->mergeWith($searchCriteria);
				$criteria->select = '*, '.$searchCriteria->select;
			}
			$brandsCriteria = new CDbCriteria();
			$brandsCriteria->with = array('products'=>array('alias'=>'products',));
			$brandsCriteria->addInCondition('`products`.`category`', $categoriesIds);
			$this->render('index', array(
				'supportFiles' => SupportFile::model()->findAll($criteria),
				'brands' => array('' => '') + CHtml::listData(Brand::model()->findAll($brandsCriteria), 'id', 'name'),
				'forCategories' => array('' => '') + CHtml::listData(Category::model()->findAllByPk($categoriesIds), 'id', 'title'),
				'brand' => $brand,
				'forCategory' => $forCategory,
				'search' => $search,
			));
		}
		else
		{
            throw new CHttpException(404);
		}
	}
}
