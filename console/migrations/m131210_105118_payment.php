<?php

class m131210_105118_payment extends CDbMigration
{
	public function up()
	{
		
		$this->execute("ALTER TABLE  `payment_type` ADD  `commission` FLOAT NOT NULL ;");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `payment_type` DROP `commission`;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}