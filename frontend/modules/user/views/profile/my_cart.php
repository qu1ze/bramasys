<?php //Yii::app()->getClientScript()->registerCssFile('/css/profile.css');
$this->breadcrumbs=array(
	'Моя корзина',
);

$this->renderPartial('_side'); ?>
<div id="container">
	<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
	?>
	<div id="cart-box">
	<h3 style="font-family: 'PTSN Bold'">Моя корзина</h3>
	<div class="cart-content">
	<table class="cart-table" style="width: 100%">
	<thead>
		<tr class="header" style="border:1px solid #ccc;">
			<td class="product-img-cart" style="width: 350px; min-width: 350px; max-width: 350px;">Товар</td>
			<td class="product-price-cart">Стоимость</td>
			<td class="input-cart" style="width: 70px;">Кол-во</td>
			<td class="checkbox-cart" style="width: 70px;">Монтаж</td>
			<td class="product-price-cart">Сумма</td>
		</tr>
	</thead>
	<tbody>
		<?php
		$priceSum = 0;
		foreach ($cart as $id => $prod)
		{
			if($id === 'complect')
			{
				foreach ($prod as $id => $complectProd)
				{
					$product = Complect::model()->findByPk($id);
					$checked = $complectProd['mounting'];
					$amount = $complectProd['price'] * $complectProd['count'];
					$priceSum += $amount;
					$this->renderPartial('_cart_view', array(
						'product' => $product,
						'amount' => $amount,
						'checked' => $checked,
						'isComplect' => true,
						'price' => $complectProd['price'],
						'count' => $complectProd['count'],
						'id' => $id,
					));
				}
			}
			else
			{
				$product = Products::model()->findByPk($id);
				$checked = $prod['mounting'];
				$amount = $prod['price'] * $prod['count'];
				$priceSum += $amount;
				$this->renderPartial('_cart_view', array(
					'product' => $product,
					'amount' => $amount,
					'checked' => $checked,
					'isComplect' => false,
					'price' => $prod['price'],
					'count' => $prod['count'],
					'id' => $id,
				));
			}
		}
		?>
	</tbody>
	</table>
	</div>
	<table class="cart-table" style="width: 100%; padding-right: 10px;">
		<tr style="border:1px solid #ccc;">
			<td colspan="6">
					<a class="cart-btn" href="<?php echo app()->getBaseUrl(true); ?>/order/placeorder">Oформить заказ</a>

					<a class="link-cart"  id="closeCart" href="#">Продолжить покупки</a>

				<div class="total-cart">
					<div class="total-price-text">
						<p>Итого:</p>
						<p>(без учета доставки,
							без учета монтажа)</p>
					</div>

					<div class="total-price" >
						<p id="firstPrice">
							<?php echo CurrencySys::exchange($priceSum).' '.CurrencySys::getLabel(); ?>
						</p>
						<?php if (!CurrencySys::isDefaultInCookie()):?>
							<p id="lastPrice"><?php echo $priceSum.' '.CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY)?></p>
						<?php endif; ?>
					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
</div>