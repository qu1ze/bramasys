<?php

class NewsController extends Controller
{
	public function actionIndex()
	{
		$seo = CategoryMetadata::model()->findByAttributes(array('url'=>'/'));
        header('Content-type: application/rss+xml; charset=utf-8');
        $criteria_news = new CDbCriteria();
		$criteria_news->order = 'create_time DESC';
		$criteria_news->limit = 20;
		$last_news = News::model()->findAll($criteria_news);
        
        $this->renderPartial('rss', array(
			'seo' => $seo,
			'model' => $last_news,
		));   
		
	}
	
}