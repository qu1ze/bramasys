<?php
Yii::app()->clientScript->registerScript("new-pass","
    $('body').on('click', '#rememberPass', function () {
        $('.login-form-wrapper').slideUp(600);
		$('.login-form-container').fadeOut(300);
        $('#login-form-wrapper').slideDown(600);
        $('#login-form-container').fadeIn(800);
        return false;
    });
    $('body').on('click', '#newPass', function () {
        $('.login-form-wrapper').slideUp(600);
		$('.login-form-container').fadeOut(300);
		$('#newPassForm').fadeIn(1);
        $('#newPassWrapper').slideDown(600);
        $('#newPassContainer').fadeIn(800);
        return false;
    });

    $('#newPassContainer').on('click', '#sendPassRequest', function () {
        $('#newPassContainer .alert').fadeOut();
        var errorList = $('#newPassContainer .error-list');
        var errorLine = $($(errorList).children()[0]).clone();
        $(errorList).children().remove()
        $(errorList).append(errorLine);
        $.ajax({
           url: '/site/ajaxSetPassword',
           data: {email: $('#newPassContainer input[name=\"email\"]').val()},
           dataType: 'json',
           type: 'post',
           success: function(data){
                if(typeof(data) == 'object' && data.title == 'Success')
                {
                    $('#newPassForm').fadeOut(300, function(){
                        $('#newPassResponse a').attr('href', '//' + data.message)
                        $('#newPassResponse').fadeIn();
                    });
                }
                else if (data.message)
                {
                    var newLine = $(errorLine).clone();
                    $(errorList).append($(newLine).text(data.message));
                    $(newLine).fadeIn();
                    $('#newPassContainer .alert').fadeIn();
                }
           }
       });
    });
", CClientScript::POS_READY);
?>
<table id="newPassWrapper" class="login-form-wrapper" style="display:none;"><tr><td>
    <div id="newPassContainer" class="login-form-container" style="display:none; max-width: 450px;">
        <div class="login-form-header">
            <h3>Восстановление пароля</h3>
            <a class="close" id="close2" href="#" style="top: -5px;">
            </a>
        </div>
        <div class="login-form-content" style="padding: 40px 40px 40px 40px;">
            <div id="newPassResponse" style="display: none; margin-bottom: 10px">На вашу электронную почту было высланно письмо с ссылкой на востановление пароля. <a>Проверить почту</a></div>
            <div id="newPassForm">
                <div class="alert alert-block alert-error" style="display:none;"><p>Необходимо исправить следующие ошибки:</p>
                    <ul class="error-list">
                        <li style="display:none;"></li>
                    </ul>
                </div>
                <div class="control-group " style="width: 300px;">
                    <label class="control-label required" style="width: 100%; max-width: 100%;">
                        Введите ваш адрес электронной почты,<br>мы пришлем вам ссылку для восстановления пароля
                    </label>
                    <div class="controls">
                        <input name="email" type="text">
                        <span class="help-inline error" id="LoginForm2_username_em_" style="display: none">
                        </span>
                    </div>
                    <input type="button" class="blue-button" id="sendPassRequest" value="Отправить"> 
                    <a id="rememberPass" style="float:right; "  href="#">&lt;-- Я вспомнил пароль</a>
                </div>
                
            </div>
        </div>
    </div>
</td></tr></table>
