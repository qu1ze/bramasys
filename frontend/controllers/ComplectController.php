<?php

class ComplectController extends Controller
{
	public $layout='//layouts/main';
	public $categoryTitle = 'Результаты поиска';
	public $isSearch = false;
	public $categoryName = '';
	public $rootCategory = null;
	public $pathCategories = null;
	public $categoryIds = array();
	public $searchArray = array();
	public $searchTree = null;
	private $actionsWhiteList = array(
		'ajaxCarouselNext',
		'ajaxAddToBookmarks',
		'ajaxRemoveBookmark',
	);
	
	public static function getTag()
	{
		return 'catalog';
	}
	
	protected function beforeAction($action)
        {
			$this->rootCategory = self::getTag();
			$this->pathCategories = isset($_GET['matches']) ? $_GET['matches'] : array();
			
			if(isset($_GET['min_price']))
			{
				$_GET['orig_min_price'] = $_GET['min_price'];
				$_GET['min_price'] = CurrencySys::exchange($_GET['min_price'], CurrencySys::DEFAULT_CURRENCY);
			}
			if(isset($_GET['max_price']))
			{
				$_GET['orig_max_price'] = $_GET['max_price'];
				$_GET['max_price'] = CurrencySys::exchange($_GET['max_price'], CurrencySys::DEFAULT_CURRENCY);
			}
            
			if (in_array($action->id, $this->actionsWhiteList))
			{
				unset($_GET['matches']);
				return parent::beforeAction($action);
			}
            else if ($action->id == 'index')
            {
				$categoryController = app()->categoryController;
                if ($_GET['matches'][0] == 'complect')
                {
                    if (isset($_GET['matches'][2]))
                    {
                        $_GET['Complect_page'] = $_GET['matches'][2];
                    }
                    unset($_GET['matches']);
                    return parent::beforeAction($action);
                }
                $categories = $_GET['matches'];
                $key = end($categories);
                if (is_numeric($key) && sizeof($categories) > 2)
                {
                    $_GET['Complect_page'] = array_pop($categories);
                    array_pop($categories);
                    $key = end($categories);
                }
                if (!is_bool($id = $categoryController->getCategoryId($key)))
                {
                    $categoryArray = $categoryController->addParentsIds($id);
                    if (is_array($categoryArray))
                    {
                        $categoryNames = $categoryController->getNames($categoryArray);
                        if ($categories == $categoryNames)
                        {
                            $children = array();
                            array_push($children, end($categoryArray));
                            $children = array_merge($children, $categoryController->getChildrenIds($children[0]));
							$_GET['rootCategory'] = $categoryArray[0];
                            $_GET['categoryId'] = $children;
                            $_GET['categoryNames'] = $categoryNames;
                            $_GET['categoryTitle'] = $categoryController->getTitle(end($categoryArray));
                            unset($_GET['matches']);
                            return parent::beforeAction($action);
                        }
                    }
                }
            }
            else if ($action->id == 'view')
            {
                    $model=Complect::model()->findByAttributes(array('actual_id'=>$_GET['id']));
                    if($model===null){
                    $model=Complect::model()->findByPk($_GET['id']);
                    }
                    if($model===null)
                        throw new CHttpException(404,'The requested page does not exist.');
                    $_GET['id'] = $model->id;
                return parent::beforeAction($action);
            }
            throw new CHttpException(404);
        }

    public static function getRelevantSearchCriteria($search, $delimiter = 1)
    {
        if(!empty($search))
        {
            $searchCriteria = self::getSearchCriteria($search);
            if($searchCriteria !== null)
            {
                $search = substr($search, 0, 127);
                $searchArray = array_filter(preg_split( "/(\s|-|,|\.|\\|\/)/", $search));
                if(empty($searchArray))
                {
                    $searchArray = array($search);
                }
                $select = '';
                foreach ($searchArray as $searchNode)
                {
                    $select .= " IF (`complect`.`name` LIKE '%$searchNode%', ".(1 / $delimiter).", 0) + IF (`complect`.`full_description` LIKE '%$searchNode%', ".(0.1 / $delimiter).", 0) +";
                }
                if(!empty($select))
                {
                    $select = rtrim($select, '+');
                    $searchCriteria->select = "($select) as `complect_relevant`";
                    $searchCriteria->order = '`complect_relevant` DESC';
                }
                $searchProductsCriteria = ProductsController::getRelevantSearchCriteria($search, 10, false);
                $searchCriteria->select .= ", ".$searchProductsCriteria->select;
                $searchCriteria->join = "INNER JOIN `complect_product` as `complect_product` ON `complect_product`.`complect_id` = `complect`.`id`
                INNER JOIN `products` as `products` ON `complect_product`.`product_id` = `products`.`id`
                INNER JOIN `brand` `brand` ON brand.id = brand";
                $searchCriteria->params += $searchProductsCriteria->params;
                $searchCriteria->condition .= " OR ".$searchProductsCriteria->condition;
                $searchCriteria->order .= ", ".$searchProductsCriteria->order;
                $searchCriteria->group = '`complect`.`id`';
            }
            return $searchCriteria;
        }
        return null;
    }

    public static function getSearchCriteria($search)
    {
        $search = substr($search, 0, 127);
        $searchArray = array_filter(preg_split( "/(\s|-|,|\.|\\|\/)/", $search));
        if(empty($searchArray))
        {
            $searchArray = array($search);
        }
        $searchCriteria = new CDbCriteria();
        $searchCriteria->alias = 'complect';
        foreach ($searchArray as $searchNode)
        {
            $searchCriteria->addSearchCondition('`complect`.`name`', $searchNode, true, 'OR');
            $searchCriteria->addSearchCondition('`complect`.`full_description`', $searchNode, true, 'OR');
        }
        return $searchCriteria;
    }

	public function actionIndex(array $categoryId = array(), array $categoryNames = array(), $categoryTitle = '', $rootCategory = null, $search = '', $sort = null, $limit = null, $ajax=false)
	{
            if(!empty($categoryId))
		{
			$this->categoryName = end($categoryNames);
			$this->categoryTitle = $categoryTitle;
			$this->categoryIds = $categoryId;
		}
        if ($ajax){	
                Yii::app()->request->cookies['lastCompare'] = new CHttpCookie('lastCompare', $this->createUrl('complect/index', array('categoryNames' => $this->pathCategories)));
        $this->rootCategory = $rootCategory;
		$dataProvider=new CActiveDataProvider('Complect');
		$criteria = new CDbCriteria();
        $this->isSearch = false;

        $criteria->alias = 'complect';
        $criteria->select = 'complect.*';
        $criteria->join = "INNER JOIN (
            SELECT sum(`products`.`price` * `complect_product`.`count`) as `additional_price`, `complect_id` FROM `complect_product`
            INNER JOIN `products` as `products` ON `complect_product`.`product_id` = `products`.`id` GROUP BY `complect_id`
        ) `additional` on `additional`.`complect_id` = `complect`.`id`";
        if(!empty($sort))
        {
            if($sort == 'high_price')
                $criteria->order = '`additional_price` DESC';
            else if($sort == 'low_price')
                $criteria->order = '`additional_price` ASC';
        }
        else
        {
            $criteria->select .= ', (SELECT count(*) FROM `comment_vote`
			RIGHT OUTER JOIN `comment` ON `comment_vote`.`comment_id` = `comment`.`id`
			WHERE `comment`.`complect_id` = `complect`.`id`) as `rating`';
            $criteria->order = 'rating DESC';
        }

		if(!empty($categoryId))
		{
			$criteria->addInCondition('category_id', $categoryId);
			$this->categoryName = end($categoryNames);
			$this->categoryTitle = $categoryTitle;
			$this->categoryIds = $categoryId;
		}
        else
        {
            if(empty($search))
                throw new CHttpException(404, '/complects is used only on search!');
            $this->isSearch = true;
        }
        $searchCriteria = $this->getRelevantSearchCriteria($search);
        if(!empty($search) && $searchCriteria !== null)
        {
            $criteria->select .= ", ".$searchCriteria->select;
            $criteria->params += $searchCriteria->params;
            $criteria->condition .= " AND ".$searchCriteria->condition;
            $criteria->order .= ", ".$searchCriteria->order;
            $criteria->join .= " ".$searchCriteria->join;
            $criteria->group = $searchCriteria->group;
        }

        if(isset($_GET['max_price']) || isset($_GET['min_price']))
        {
            $criteria->addBetweenCondition('`additional_price`', isset($_GET['min_price']) ? $_GET['min_price'] : Products::MIN_PRODUCT_PRICE, isset($_GET['max_price']) ? $_GET['max_price'] : Products::MAX_PRODUCT_PRICE);
        }


		$dataProvider->setCriteria($criteria);

		$pageSize = 20;
		if(!empty($limit))
			$pageSize = $limit;
        $dataProvider->pagination = null;
		$dataProvider->setPagination(array(
			'pageSize' => $pageSize,
			));
        $dataProvider->getData(true);
		$this->renderPartial('index_a',array(
				'dataProvider'=>$dataProvider,
				'isSearch' => $this->isSearch,
                ));}		
                else { $this->render('frontend.views.layouts.load');}   
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id, $scroll = '', $ajax=false)
	{
        
        $model = $this->loadModel($id);
        $this->addViewed($id);
        $categoryIds = Yii::app()->categoryController->addParentsIds($model->category_id);
        $this->rootCategory = $categoryIds[0];
        $commentsCount = Comment::model()->countByAttributes(array('complect_id'=>$id, 'status' => 1));
        if(!$ajax){       
		$this->render('view',array(
				'model'=>$model,
				'scroll' => $scroll,
                'complectProducts' => ComplectProduct::model()->findAllByAttributes(array('complect_id' => $model->id), array('order' => '`order` ASC')),
                    'commentsCount' => $commentsCount,
		));
        }else{
        $this->renderPartial('view',array(
				'model'=>$model,
				'scroll' => $scroll,
                'complectProducts' => ComplectProduct::model()->findAllByAttributes(array('complect_id' => $model->id), array('order' => '`order` ASC')),
                    'commentsCount' => $commentsCount,
		));
        }
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{                                                     
        $model=Complect::model()->findByPk($id);
        
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
        return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='products-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionAjaxCarouselNext()
	{
		if (isset($_POST['offset']) && isset($_POST['count']) && Yii::app()->request->isAjaxRequest)
		{
            $complects = Complect::model()->findAll(array('offset' => $_POST['offset'] * $_POST['count'], 'limit' => $_POST['count']));
			if($complects !== null)
			{
				echo json_encode(array(
					'msg' => 'next',
					'item' => $this->renderPartial('carouselItem', array('complects' => $complects), true, false),
					));
				app()->end();
			}
		}
		echo json_encode(array('msg' => 'error'));
	}
	
	public function actionAjaxAddToBookmarks($id)
	{
		if(!isset(Yii::app()->request->cookies['bookmarks']))
			Yii::app()->request->cookies['bookmarks'] = new CHttpCookie('bookmarks', json_encode(array()));
		$bookmarks = json_decode(Yii::app()->request->cookies['bookmarks'], true);
		if(!isset($bookmarks['complect']))
		{
			$bookmarks['complect'] = array();
		}
		if(!in_array($id, $bookmarks['complect']))
			array_push($bookmarks['complect'], $id);
		Yii::app()->request->cookies['bookmarks'] = new CHttpCookie('bookmarks', json_encode($bookmarks));
		echo 'Success';
	}
	
	public function actionAjaxRemoveBookmark($id)
	{
		if(!isset(Yii::app()->request->cookies['bookmarks']))
			Yii::app()->request->cookies['bookmarks'] = new CHttpCookie('bookmarks', json_encode(array()));
		$bookmarks = json_decode(Yii::app()->request->cookies['bookmarks'], true);
		if(isset($bookmarks['complect']))
		{
			if(in_array($id, $bookmarks['complect']))
			{
				foreach ($bookmarks['complect'] as $key => $bookmarkId)
				{
					if($bookmarkId == $id)
					{
						array_splice($bookmarks['complect'], $key, 1);
					}
				}
				Yii::app()->request->cookies['bookmarks'] = new CHttpCookie('bookmarks', json_encode($bookmarks));
			}
		}
		echo 'Success';
	}
	
	public function addViewed($id)
	{
		if(!isset(Yii::app()->session['viewed']))
			Yii::app()->session['viewed'] = array('products' => array(), 'complects' => array());
		$viewed = Yii::app()->session['viewed'];
		if(!in_array($id, $viewed['complects']))
			array_push($viewed['complects'], $id);
		Yii::app()->session['viewed'] = $viewed;
	}

    protected function afterAction($action)
    {
        isset($_GET['orig_min_price']) ? $_GET['min_price'] = $_GET['orig_min_price'] : '';
        isset($_GET['orig_max_price']) ? $_GET['max_price'] = $_GET['orig_max_price'] : '';
        return parent::afterAction($action);
    }
}
