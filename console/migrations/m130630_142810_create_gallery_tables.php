<?php

class m130630_142810_create_gallery_tables extends CDbMigration
{
	
	public function up()
    {
        //return true;
        $this->execute(<<<SQL
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `versions_data` text NOT NULL,
  `name` tinyint(1) NOT NULL DEFAULT '1',
  `description` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `gallery_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `name` varchar(512) NOT NULL,
  `description` text NOT NULL,
  `file_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gallery_photo_gallery1_idx` (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `fk_gallery_photo_gallery1` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
SQL
        );
		$this->execute("ALTER TABLE `products` DROP `image_url`,
			ADD  `gallery_id` INT(11) DEFAULT NULL,
			ADD KEY `gallery_id` (`gallery_id`);
			ALTER TABLE `products` ADD CONSTRAINT `FK_gallery` FOREIGN KEY (`gallery_id`)
						  REFERENCES `gallery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
		");
    }

    public function down()
    {
        $this->execute(<<<SQL
DROP TABLE IF EXISTS `gallery_photo`;
DROP TABLE IF EXISTS `gallery`;
SQL
        );
		$this->execute("
				ALTER TABLE  `products` DROP FOREIGN KEY  `FK_gallery`,
				DROP  `gallery_id`,
                ADD  `image_url` VARCHAR(255) NOT NULL DEFAULT '/images/layout/noimg.png' AFTER `full_description`;
            ");
        return true;
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}