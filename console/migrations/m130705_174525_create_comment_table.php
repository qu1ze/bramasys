<?php

class m130705_174525_create_comment_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
				CREATE TABLE IF NOT EXISTS `comment` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`author_id` int(11) DEFAULT NULL,
					`author_name` varchar(50) DEFAULT NULL,
					`email` varchar(255) DEFAULT NULL,
					`rating` tinyint(1) NOT NULL DEFAULT '0',
					`content` text NOT NULL,
					`pros` text,
					`cons` text,
					`useful_count` int(11) NOT NULL DEFAULT '0',
					`post_date` int(11) NOT NULL,
					`product_id` int(11) DEFAULT NULL,
					`complect_id` int(11) DEFAULT NULL,
					`article_id` int(11) DEFAULT NULL,
					`parent_id` int(11) DEFAULT NULL,
					`status` tinyint(4) NOT NULL DEFAULT '0',
					PRIMARY KEY (`id`),
					CONSTRAINT `FK_author` FOREIGN KEY (`author_id`)
						  REFERENCES `client` (`id`) ON DELETE CASCADE,
					CONSTRAINT `FK_product` FOREIGN KEY (`product_id`)
						  REFERENCES `products` (`id`) ON DELETE CASCADE,
					CONSTRAINT `FK_parent` FOREIGN KEY (`parent_id`)
						  REFERENCES `comment` (`id`) ON DELETE CASCADE,
					CONSTRAINT `FK_complect` FOREIGN KEY (`complect_id`)
						  REFERENCES `complect` (`id`) ON DELETE CASCADE,
					CONSTRAINT `FK_article` FOREIGN KEY (`article_id`)
						  REFERENCES `article` (`id`) ON DELETE CASCADE
				) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
		$this->execute("
				CREATE TABLE IF NOT EXISTS `comment_vote` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`comment_id` int(11) NOT NULL,
					`client_id` int(11) DEFAULT NULL,
					`ip_address` varchar(50) NOT NULL,
					PRIMARY KEY (`id`),
					CONSTRAINT `FK_comment` FOREIGN KEY (`comment_id`)
						  REFERENCES `comment` (`id`) ON DELETE CASCADE,
					CONSTRAINT `FK_client` FOREIGN KEY (`client_id`)
						  REFERENCES `client` (`id`) ON DELETE CASCADE
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			");
	}

	public function down()
	{
		$this->dropTable('comment_vote');
		$this->dropTable('comment');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}