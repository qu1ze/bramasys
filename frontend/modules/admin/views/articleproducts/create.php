<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => $model->getFormId(),
		'type' => 'horizontal',
        'htmlOptions'=>array( 'onsubmit'=>'return false;'),
	) );
?>
	<?php echo $form->errorSummary($model); ?>

	<?php $this->renderPartial('_form', array('form' => $form, 'model' => $model, 'create' => true)); 
 
     $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Создать', 'id' => 'submit')); ?>
	
<?php $this->endWidget(); ?>