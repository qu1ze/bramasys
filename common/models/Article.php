<?php

/**
 * This is the model class for table "article".
 *
 * The followings are the available columns in table 'article':
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $text
 * @property integer $category_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * The followings are the available model relations:
 * @property ArticleProduct[] $articleProducts
 * @property RelatedArticle[] $relatedArticles
 * @property RelatedArticle[] $relatedArticles1
 */
class Article extends ActiveRecord
{	
    
    public $rating;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'article';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, title, text, category_id, create_time, update_time', 'required'),
            array('name, title, text, thumb, short_description, keywords', 'filter', 'filter'=>'trim'),
			array('category_id, create_time, update_time', 'numerical', 'integerOnly'=>true),
			array('name, title', 'length', 'max'=>255),
			array('thumb, views', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, title, text, category_id, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'articleProducts' => array(self::HAS_MANY, 'ArticleProduct', 'article_id'),
			'relatedArticles' => array(self::HAS_MANY, 'RelatedArticle', 'related_article'),
			'relatedArticles1' => array(self::HAS_MANY, 'RelatedArticle', 'article_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Машинное имя',
			'title' => 'Наименование',
			'text' => 'Текст',
			'category_id' => 'Категория',
			'thumb' => 'Иконка',
			'views' => 'Просмотры',
			'create_time' => 'Дата создания',
			'update_time' => 'Дата обновления',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('thumb',$this->thumb,true);
		$criteria->compare('views',$this->views);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('update_time',$this->update_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeValidate()
	{
		$r = parent::beforeValidate();
		
		if ($this->isNewRecord)
			$this->create_time = time();
	
		$this->update_time = time();
		
		return $r;		
	}
	
	
	public function afterSave()
	{
		parent::afterSave();
		
		Keyword::model()->deleteAllByAttributes(array('object_id' => $this->id, 'object_type' => Keyword::OBJECT_TYPE_ARTICLES));
		if ($this->keywords)
		{			
			$keywords = explode(',', $this->keywords);
			foreach ($keywords as $keyword)
			{
				$new_keyword = new Keyword();
				$new_keyword->keyword = $keyword;
				$new_keyword->object_type = Keyword::OBJECT_TYPE_ARTICLES;
				$new_keyword->object_id = $this->id;
				$new_keyword->save();					
			}
		}				
	}
	
	public function afterFind()
	{
		parent::afterFind();
		
		$keywords = Keyword::model()->findAllByAttributes(array('object_id' => $this->id, 'object_type' => Keyword::OBJECT_TYPE_ARTICLES));
		if ($keywords)
			$this->keywords = implode(', ', array_values(CHtml::listData($keywords, 'id', 'keyword')));
	}
	
	public function afterDelete()
	{
		parent::afterDelete();		
		Keyword::model()->deleteAllByAttributes(array('object_id' => $this->id, 'object_type' => Keyword::OBJECT_TYPE_ARTICLES));
	}
	
	public static function getAritclesSubCategories()
	{
		$articles_sub_categories = array();
		if (($category = Category::model()->findByAttributes(array('name' => 'stati-publikatcii-obzory'))) !== null)
		{
			$children_categories = app()->categoryController->getChilds($category->id);
			foreach ($children_categories as $child_category)
				$articles_sub_categories[$child_category['id']] = $child_category['data']['title'];
		}		
		
		return $articles_sub_categories;
	}
    public function getLink() {
		return  app()->getBaseUrl(true). Yii::app()->createUrl('/support/stati-publikatcii-obzory/' . $this->category->name . '/' . $this->name);
	}
    public function getRating() {
		if($this->rating == null)
		{
			$rating = 0;
			$comments = Comment::model()->findAll("`article_id` = $this->id AND rating != 0 AND status = 1");
			if(!empty($comments))
			{
				foreach ($comments as $comment)
				{
					$rating += $comment->rating;
				}
				$rating = round($rating / sizeof($comments), 1);

			}
			$this->rating = $rating;
		}
		return $this->rating;
	}
}