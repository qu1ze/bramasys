<?php
cs()->registerCoreScript('yiiactiveform');

$grid_id = 'client-table';
$gridView = $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $grid_id,
	'type' => 'bordered striped',
	'dataProvider' => $model->search(),
	'filter' => $model,	
	'afterAjaxUpdate' => 'js:function(){
		jQuery(".tooltip").remove();
		jQuery("a[rel=tooltip]").tooltip();				
	}',
	'template' => "{items}\n{pager}",
	'columns' => array(
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'name',
			'editable' => array(
				'type' => 'text',
				'url' => app()->createUrl('/admin/client/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'lastname',
            'editable' => array(
                'type' => 'text',
                'url' => app()->createUrl('/admin/client/ajaxUpdate'),
                'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
            )
        ),
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'middle_name',
            'editable' => array(
                'type' => 'text',
                'url' => app()->createUrl('/admin/client/ajaxUpdate'),
                'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
            )
        ),
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'date_of_birth',
            'editable' => array(
                'type' => 'text',
                'url' => app()->createUrl('/admin/client/ajaxUpdate'),
                'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
            )
        ),
        array(
            'name' => 'phonesList',
        ),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('nowrap' => 'nowrap'),
            'template' => '{update} {orderInfo}',
            'buttons'=>array
            (
                'orderInfo' => array
                (
                    'label'=>'Инормация о заказах',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/admin/info.png',
                    'url'=>'Yii::app()->createUrl("admin/client/orderInfo", array("id"=>$data->primaryKey, "page" => isset($_GET["Client_page"]) ? $_GET["Client_page"] : "index"))',
                ),
            ),
			'updateButtonUrl'=>'Yii::app()->controller->createUrl("edit", array("id" => $data->primaryKey, "page" => isset($_GET["Client_page"]) ? $_GET["Client_page"] : "index"))',
			'header' => EHtml::addGridButton($this->createAbsoluteUrl('/admin/client/ajaxCreate'), 'Добавить', $height=350, 'icon-question-sign'),
		)
	)
),true);

echo str_replace('&lt;br&gt;','<br>', $gridView);

