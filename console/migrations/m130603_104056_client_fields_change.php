<?php

class m130603_104056_client_fields_change extends CDbMigration
{
	public function up()
	{
		$this->execute("
                ALTER TABLE `client`
					ADD `username` VARCHAR(45) NULL DEFAULT NULL,
					ADD `password` VARCHAR(255) NULL DEFAULT NULL,
					ADD `salt` VARCHAR(255) NULL DEFAULT NULL,
					ADD `password_strategy` VARCHAR(50) NULL DEFAULT NULL,
					ADD `requires_new_password` TINYINT(1) NULL DEFAULT NULL,
					ADD `email` VARCHAR(255) NULL DEFAULT NULL,
					ADD `login_attempts` INT(11) NULL DEFAULT NULL,
					ADD `login_time` INT(11) NULL DEFAULT NULL,
					ADD `login_ip` VARCHAR(32) NULL DEFAULT NULL,
					ADD `validation_key` VARCHAR(255) NULL DEFAULT NULL,
					ADD `created_id` INT(11) NULL DEFAULT NULL,
					ADD `created_time` INT(11) NULL DEFAULT NULL,
					ADD `update_id` INT(11) NULL DEFAULT NULL,
					ADD `update_time` INT(11) NULL DEFAULT NULL;
				ALTER TABLE  `client` ADD UNIQUE  `username` (  `username` );
				ALTER TABLE  `client` ADD UNIQUE  `email` (  `email` );
				ALTER TABLE  `client` DROP FOREIGN KEY  `client_ibfk_2` ;
				ALTER TABLE  `client` DROP INDEX `user_id`;
				ALTER TABLE  `client` DROP  `user_id` ;
            ");
			$this->execute("
				ALTER TABLE  `cart` DROP FOREIGN KEY  `cart_ibfk_3` ;
				TRUNCATE `client`;
				ALTER TABLE  `cart` ADD CONSTRAINT `cart_ibfk_3` FOREIGN KEY (  `client_id` ) REFERENCES  `client` (
					`id`
					) ON DELETE RESTRICT ON UPDATE RESTRICT ;
					");
	}

	public function down()
	{
		$this->execute("
			ALTER TABLE `client` DROP INDEX `username`;
			ALTER TABLE `client` DROP INDEX `email`;
			ALTER TABLE `client`
                DROP `username`,
                DROP `password`,
                DROP `salt`,
				DROP `password_strategy`,
                DROP `requires_new_password`,
                DROP `email`,
				DROP `login_attempts`,
                DROP `login_time`,
                DROP `login_ip`,
                DROP `validation_key`,
				DROP `created_id`,
                DROP `created_time`,
                DROP `update_id`,
				DROP `update_time`;
				");
		 $this->execute("
                ALTER TABLE `client`
				ADD `user_id` INT(11) NOT NULL;
				ALTER TABLE  `client` ADD UNIQUE  `user_id` (  `user_id` );
				");
		$this->execute("ALTER TABLE  `client` ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
				`id`
				) ON DELETE RESTRICT ON UPDATE RESTRICT ;
                ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}