<?php

class m130801_152005_create_term_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `term` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`title` varchar(50) NOT NULL,
				`definition` text NOT NULL,
				`content` text NOT NULL,
				`references` text DEFAULT NULL,
				`photo` varchar(255) DEFAULT NULL,
				`author` varchar(255) DEFAULT NULL,
				`source` varchar(255) DEFAULT NULL,
				`source_date` varchar(50) DEFAULT NULL,
				`update_time` int(11) NOT NULL,
				`create_time` int(11) NOT NULL,
				PRIMARY KEY (`id`)
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->dropTable('term');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}