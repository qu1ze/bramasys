<?php

class m130519_181719_add_filds_to_product_table extends CDbMigration
{
	public function up()
	{
            $this->execute("
                ALTER TABLE `products`
                ADD `price_of_the_week` BOOLEAN NOT NULL DEFAULT FALSE,
                ADD `top_seller` BOOLEAN NOT NULL DEFAULT FALSE,
                ADD `novelty` BOOLEAN NOT NULL DEFAULT FALSE,
                ADD `hide` BOOLEAN NOT NULL DEFAULT FALSE
            ");
            $this->execute("
                ALTER TABLE  `products` CHANGE  `quantity`  `quantity` INT NULL DEFAULT NULL
            ");
	}

	public function down()
	{
            $this->execute("ALTER TABLE `products`
                DROP `price_of_the_week`,
                DROP `top_seller`,
                DROP `novelty`,
                DROP `hide`;
                ");
            $this->execute("
                ALTER TABLE  `products` CHANGE  `quantity`  `quantity` INT NOT NULL
            ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}