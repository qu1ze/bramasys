<?php

class MailtemplateController extends Controller
{		
	public function actionCreate()
	{
		$model = new MailTemplate();
				
		if (isset($_POST['MailTemplate']))
		{			
			$model->attributes = $_POST['MailTemplate'];
			if ($model->save())
				$this->redirect(array('/admin/mailtemplate'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}

	public function actionIndex()
	{		
		$model = new MailTemplate('search');
		
		if (isset($_GET['MailTemplate']))
			$model->attributes = $_GET['MailTemplate'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = MailTemplate::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/mailtemplate'));
				
		if (isset($_POST['MailTemplate']))
		{			
			$model->attributes = $_POST['MailTemplate'];
			if ($model->save())
				$this->redirect(array('/admin/mailtemplate'));
		}
				
		$this->render('update', array(
			'model' => $model,			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('MailTemplate');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Шаблон успешно отредактирован')));
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
            $mailTemplate = MailTemplate::model()->findByPk($id);
            if($mailTemplate !== null && !$mailTemplate->isSystemTemplate())
            {
                MailTemplate::model()->deleteByPk( $id );
                Yii::app()->user->setFlash('success', "Шаблон успешно удален");
            }
            else
            {
                Yii::app()->user->setFlash('error', "Невозможно удалить системный шаблон.");
            }
		}
		
		$this->redirect(array('/admin/mailtemplate'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = MailTemplate::model()->findByPk($id)) !== null)
		{
            if(!$model->isSystemTemplate())
            {
                if ($model->delete())
                    echo json_encode(array('title' => 'Success', 'msg' => 'Шаблон успешно удален'));
                else
                    echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить шаблон'));
            }
            else
                echo json_encode(array('title' => 'Error', 'msg' => 'Невозможно удалить системный шаблон'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Шаблон не найден'));
	}
	
}