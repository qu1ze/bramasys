<?php
/* @var $product Products */

Yii::app()->clientScript->registerScript("SlideProductTabs", "
	$('#prod-carousel').on('click', '.carousel-control.left', function() {
		$('#prod-carousel').carousel('prev');
	});

	$('#prod-carousel').on('click', '.carousel-control.right', function() {
		$('#prod-carousel').carousel('next');
	});
", CClientScript::POS_END);
?>
<?php echo $this->title ?>
<div class="carousel slide various-product-block" id="prod-carousel">
	<div class="carousel-inner show" style="height: 276px; width: 775px;">
		<div class="item active">
			<?php
			$count = 0;
			foreach($products as $product):
				if($count > 3):	?>
				</div><div class="item">
				<?php 
				$count = 0;
				endif;
				$count++;
				?>
				<div class="tab-product <?php echo $product->cartPattern; ?>">
					<div class="pd-qty-block hide"><input hidden value="1"></div>
					<div class="code pd-code" style="display: none;">Код товара: <span><?php echo $product->id;?></span></div>
					<div class="thumb">
						<a href="<?php echo $product->link;?>">
                            <span class="img-vert" style="background-image: url('<?php echo $product->image_url;?>')">
								<img style="display:none; width: 120px;max-height: 150px" src="<?php echo $product->image_url;?>" alt="">
							</span>
						</a>
					</div>
					<div class="name"><a href="<?php echo $product->link;?>"><?php echo $product->name;?></a></div>
					<div class="price"><?php echo CurrencySys::exchange($product->price).' '.CurrencySys::getLabel(); ?></div>
					<div class="pd-usd-price"><?php echo CurrencySys::getLabel(CurrencySys::DEFAULT_CURRENCY).' '.$product->price; ?></div>
					<div id="btn" class="btn-buy-<?php echo $product->id; ?> enabled" style="<?php echo $product->inCart ? 'display: none;' : ''?>">
						<a data-is_complect="<?php echo $product->isComplect;?>" id="<?php echo $product->cartPattern; ?>" href="#" class="btn-buy-buy index-buy-but">Купить</a>
					</div>
					<div class="btn-buy-<?php echo $product->id; ?> disabled" id="<?php echo $product->cartPattern; ?>"  style="<?php echo $product->inCart ? '' : 'display: none;'?>">
						<a data-is_complect="<?php echo $product->isComplect;?>" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<a class="carousel-control left" href="#" data-slide="prev">
		<img alt="<" src="/images/pageListLeft.png" height="24" width="15">
	</a>
	<a class="carousel-control right" href="#" data-slide="next">
		<img alt=">" src="/images/pageListRight.png" height="24" width="15">
	</a>
</div>