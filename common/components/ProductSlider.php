<?php
class ProductSlider extends CWidget
{
	public $productsIds = array();
	public $title;
	
	public function init()
	{
		
	}
	
    public function run()
    {
		$this->render("product_slider", array(
				'products' => Products::model()->findAllByPk($this->productsIds),
			)
		);
    }
}
?>
