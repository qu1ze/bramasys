<?php Yii::app()->clientScript->registerScript("scrollTo","
			$('#addprodtolist').on('click',function(){
                addtolist();
            });
            $('#removeprodtolist').on('click',function(){
                $('#NewsProduct_product_id option:selected').remove();
            });
            $('#submit').on('click',function(){
                $('#NewsProduct_product_id option').prop('selected', true);
                $('#".$model->getFormId()."').attr('onsubmit','return true;');
                $('#".$model->getFormId()."').submit();
                    
            });
            function addtolist()
            {
            if($('#selectedvalue').val()>0)
                {
                $('#NewsProduct_product_id').
                    append($('<option></option>').
                    attr('value', $('#selectedvalue').val()).
                    text($('#searchbox').val()+'.'));
                $('#selectedvalue').val(0);
                }
            }
            $('#searchbox').on('keydown',function( event ) {
            if (event.which == 13) {
                addtolist();
                return false;
              }
              
            });
", CClientScript::POS_READY);
$htmlOpt = array('multiple'=>'multiple', 'size'=> '6' );
echo $form->dropDownListRow($model, 'news_id', CHtml::listData(News::model()->findAll(), 'id', 'title')); ?>

<?php if(empty($create)) echo $form->dropDownListRow($model, 'product_id', CHtml::listData(NewsProduct::model()->findAllByAttributes(array('news_id' => $model->news_id)), 'product_id', 'product.model'),$htmlOpt);
    else echo $form->dropDownListRow($model, 'product_id', array(),$htmlOpt);?>
    
  <div class="controls" style="clear:both">   
<button class="btn" id="removeprodtolist" type="button">Удалить из списка</button><br/><br/>
</div>
 <div class="controls">    
        <?php
    
    echo CHtml::hiddenField('selectedvalue','');
    
         $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'name'=>'searchbox',
            'value'=>'',
            'source'=>CController::createUrl('/admin/articleproducts/autocomplete'),
            'options'=>array(
            'showAnim'=>'fold',         
            'minLength'=>'2',
            'select'=>'js:function( event, ui ) {
                        $("#searchbox").val( ui.item.label );
                        $("#selectedvalue").val( ui.item.value );
                        return false;
                  }',
            ),
            'htmlOptions'=>array(
            'onfocus' => 'js: this.value = null; $("#searchbox").val(null); $("#selectedvalue").val(null);',
            'class' => 'search-query',
            'placeholder' => "Найти и добавить...",
             
            ),
            ));
            echo '<button class="btn" id="addprodtolist" type="button">+</button></div>'; 