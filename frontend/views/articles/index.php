<?php
	/*
     * <div class="update">
		<h3>Подпишись на обновления:</h3>
		<a href="#"><img src="/images/rssIcon.png" alt=""></a>
		<a href="#"><img src="/images/facebookIcon.png" alt=""></a>
		<a href="#"><img src="/images/vkontakteIcon.png" alt=""></a>
		<a href="#"><img src="/images/youtubeIcon.png" alt=""></a>
		<a href="#"><img src="/images/googleIcon.png" alt=""></a>
	</div>
     */
    Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl(true) . '/css/articles.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl(true) . '/css/style.css');
?>    

<?php
$this->breadcrumbs = app()->categoryController->getCategoryBcs($this->categoryName);

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
?>

<div class="category-title"><?php echo end($this->breadcrumbs) ?></div> 

<div class="tabbs-article">
	<div class="article-category">
		<div class="left-line"></div>
		<div class="name-title">РУБРИКИ</div>
		<div class="right-line"></div>
	</div>
	<div class="article_categories ">
		<?php foreach ($sub_categories as $sub_category) : ?>
			<div class="article_category <?php echo $sub_category->id == $this->categoryId ? 'stati-publikatcii-obzory' : "nonactive"?>">	
				<a class="article_category_link" href="<?php echo Yii::app()->createUrl('support/stati-publikatcii-obzory/' . $sub_category->name) ?>">
					<div class="article_category_text_wrapper">		
						<div class="article_category_text">
							<?php echo $sub_category->title;?>				
						</div>
					</div>
				</a>
			</div>
		<?php endforeach;?>
	</div>					
	<div class="clear"></div>
		<?php
			$this->widget('bootstrap.widgets.TbListView', array(
				'dataProvider' => $dataProvider, //with category
				'itemView' => '_view',		
				'template' => "{sorter}\n{items}\n{pager}",
				// 'itemsTagName' => 'table',
				// 'itemsCssClass' => 'items table table-striped table-condensed',
				'emptyText' => '<i>Извините, но на данный момент у нас нет статей.</i>',
			)); 
		?>								   		   	
	
	
</div>
          
          