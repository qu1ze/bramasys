<?php

class ArticlesController extends Controller
{		
	public function actionCreate()
	{
		$model = new Article; 
		
		$this->performAjaxValidation($model);
		
		if (isset($_POST['Article']))
		{			
			$model->attributes = $_POST['Article'];
			if ($model->save())
				$this->redirect(array('getGridId'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
			'articles_sub_categories' => Article::getAritclesSubCategories(),			
		));
	}	
	
	public function actionAjaxCreate()
	{	
		$model = new Article();	

		if (isset($_POST['Article']) && Yii::app()->request->isAjaxRequest)
		{
			$model->attributes = $_POST['Article'];
			if ($model->save())			
				echo json_encode(array('title'=>'Success!', 'msg'=>'Статья успешно создана'));			
			else	
				echo json_encode(array('title'=>'Oops!', 'msg'=>'Не возможно создать статью'));
			
			app()->end();
		}
		
		cs()->reset();
		cs()->corePackages=array();
		echo $this->renderPartial('ajax_create', array(
			'model' => $model,
			'articles_sub_categories' => Article::getAritclesSubCategories(),			
		), true, true);					
	}

	public function actionIndex()
	{		
		$model = new Article('search');
		
		if (isset($_GET['Article']))
			$model->attributes = $_GET['Article'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = Article::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/articles'));				
				
		if (isset($_POST['Article']))
		{			
			$model->attributes = $_POST['Article'];
			if ($model->save())
				$this->redirect(array('/admin/articles'));
		}
				
		$this->render('update', array(
			'model' => $model,			
			'articles_sub_categories' => Article::getAritclesSubCategories(),			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('Article');		
		$es->update();		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Статья успешно отредактирована')));			
	}	
	
	public function actionAjaxDelete($id)
	{		
		if (($model = Article::model()->findByPk($id)) !== null)
		{
			if (!$model->articleProducts && !$model->relatedArticles)
				if ($model->delete())
					echo json_encode(array('title' => 'Success', 'msg' => 'Статья успешно удалена'));
				else
					echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить статью'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Существуют связи с статьей/продуктом'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Статья не найдена'));
	}
	
	
}