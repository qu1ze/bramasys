<?php
class LoginFormWidget extends CWidget
{
	public $model = null;
	public $type = null;
	public $htmlOptions = array();
	
    public function run()
    {
        $this->render("login_form", array(
            'model' => $this->model,
			'type' => $this->type,
			'htmlOptions' => $this->htmlOptions,
        ));
    }
}
?>
