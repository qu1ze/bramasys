<?php

class OrderController extends Controller
{
    
    private 
    $cart;

    protected function beforeAction($action)
    {
		$this->cart = Yii::app()->session['cart'];
		return true;
    }
	
	public function actionPlaceOrder()
	{
        
        $order = new Order();
        
		$deliveryAddress = new DeliveryAddress();
		
        $NP=array();
        
        //Курьер
        if(isset($_POST['DeliveryAddress'])&& ($_POST['Order']['delivery_type_id']==3 || $_POST['Order']['delivery_type_id']==4) )
		{
			$deliveryAddress->attributes = $_POST['DeliveryAddress'];
			$deliveryAddress->validate(array('town', 'street', 'house', 'flat'));
            $order->addErrors($deliveryAddress->getErrors());
		}
        elseif (isset($_POST['Order']['delivery_type_id']) && $_POST['Order']['delivery_type_id']==2) { //Новая почта
            if( empty($_POST['NP']['id'])) $order->addError ('NP[id]', 'Неуказано отделение Новой почты');
            if( !empty($_POST['NP']['id'])) $NP['id']=$_POST['NP']['id']; 
            if( !empty($_POST['NP']['town'])) $NP['id']=$_POST['NP']['town']; 
        }
		
		
        $phone = new Phone();
		if(isset($_POST['Phone']))
		{
			$phone->attributes = $_POST['Phone'];
			$phone->validate(array('type', 'value'));
            $order->addErrors($phone->getErrors());
		}
		$confirm = -1;
		$clientValid = false;
		
		if(isset($_POST['Order']))
		{
			$order->attributes = $_POST['Order'];
			if(isset($_POST['Order']['payment_type_id']) && $_POST['Order']['payment_type_id']==5)
			{
				if(empty($_POST['Order']['company_name']))
				{
				 $order->addError('company_name','Введите название компании');
				}
				else {
					$order->company_name = $_POST['Order']['company_name'];
				}
			}
		}
		$client = new Client('register');
		$login = new LoginForm();
		if(empty($this->cart) || Yii::app()->user->getState('isAdmin'))
		{
            $this->redirect("/");
		}
		else
		{
			if(Yii::app()->user->id == null && !Yii::app()->user->getState('isAdmin'))
			{
				if (isset($_POST['Client']))
				{
					$_POST['Client']['password'] = $_POST['Client']['newPassword'];
					$client->attributes=$_POST['Client'];
					if($client->save())
					{
						$clientValid = true;
						$login->username = $client->username;
						$login->password = $client->newPassword;
						if($login->login())
						{
							$order->attributes = $_POST['Order'];
							if($order->validate(array('payment_type_id', 'delivery_type_id')))
								$confirm = 1;
							else
								$confirm = 0;
						}
					}
				}
				else if (isset($_POST['LoginForm']))
				{
					$login->attributes = $_POST['LoginForm'];
					if ($login->validate() && $login->login())
						$confirm = 0;
				}
				if($confirm == -1)
				{
					$registerClient = new Client('registerOnOrder');
					$registerClient->attributes = isset($_POST['Client']) ? $_POST['Client'] : array();
					$registerClient->addErrors($client->errors);
					$this->render('getuser', array(
								'order' => $order,
								'client' => $registerClient,
								'login' => $login,
								'cart' => $this->cart,
								'deliveryAddress' => $deliveryAddress,
								'phone' => $phone,
                                'NP' => $NP
								));
					app()->end();
				}
			}
			
			if(isset($_POST['confirm']))
				$confirm = $_POST['confirm'];
			$client = Client::model()->findByPk(app()->user->id);
			$client->scenario = 'order';
			if (isset($_POST['Client']))
			{
				$client->attributes = $_POST['Client'];
				$client->passwordConfirm = $client->newPassword = '';
			}
			if ($confirm > 0)
			{
				if(isset($_POST['DeliveryAddress']) && ($_POST['Order']['delivery_type_id']==3 || $_POST['Order']['delivery_type_id']==4))
				{
					$deliveryAddress->client_id = $client->id;
					$deliveryAddress->save();
					$order->delivery_address = $client->delivery_address;
				}
                elseif(isset($_POST['Order']['delivery_type_id']) && $_POST['Order']['delivery_type_id']==2) {
                    if( !empty($_POST['NP']['id'])) {
                        $NPaddress = DeliverySystemAddress::model()->findByPk($_POST['NP']['id']);
                        if ($NPaddress){ $order->delivery_address = 'Отделение новой почты <br> Город:'.$NPaddress->town.' <br> '. $NPaddress->office.' <br>'.$NPaddress->address; }
                        else
                        $order->addError('1','нету в базе'.$_POST['NP']['id']);
                    }
                    
                }
                else {$order->delivery_address = ' Самовывоз ';}
                
				if($client->phone == null)
				{
					if(isset($_POST['Phone']))
					{
						$phone->client_id = $client->id;
						$phone->save();
					}
					$phoneValid = $phone->validate(array('type', 'value'));
				}
				else
				{
					$phoneValid = true;
				}
				$clientValid = $client->validate();
				if(!$phoneValid)
				{
					$order->addErrors($phone->getErrors('type'));
					$order->addErrors($phone->getErrors('value'));
				}
                if(!$clientValid)
				{
					$order->addErrors($client->getErrors());
				}
                
                
				if($clientValid && $phoneValid && !empty($order->delivery_address) && !$order->hasErrors() )
				{
					$client->save();
					$order->attributes = $_POST['Order'];
					$order->validate(array('payment_type_id', 'delivery_type_id', 'delivery_address'));
					if(!$order->hasErrors())
					{
						$get = "Order[payment_type_id]=$order->payment_type_id&Order[delivery_type_id]=$order->delivery_type_id&Order[delivery_address]=$order->delivery_address";
						$get .= isset($_POST['Order']['comment']) ? "&Order[comment]={$_POST['Order']['comment']}" : '';
						if($order->payment_type_id ==5)
								{$get .= "&Order[company_name]={$order->company_name}";  }
						$this->redirect("/order/create/?$get");
					}
				}
			}
			$this->render('getuser', array(
						'order' => $order,
						'client' => $client,
						'confirm' => $confirm,
						'cart' => $this->cart,
						'deliveryAddress' => $deliveryAddress,
						'phone' => $phone,
						));
		}
		app()->end();		
	}
	
	public function actionCreate()
	{
		if(Yii::app()->user->id != null && !Yii::app()->user->getState('isAdmin'))
		{
			$order = new Order();
			$order->attributes = $_GET['Order'];
			$order->create_time = date('d/m/Y', time());
            $order->invoice_num = Order::model()->countByAttributes(array('create_time'=>'NOW()'))+1;
			$order->save();
			
			$cart = new Cart();
			$cart->client_id = Yii::app()->user->id;
			$cart->order_id = $order->id;
			$cart->save();

			foreach($this->cart as $id => $product)
			{
				$orderProduct = new OrderProduct();
				if($id === 'complect')
				{
					$complect = $product;
					foreach($complect as $id => $product)
					{
						$orderProduct->is_complect = true;
						$orderProduct->product_id = $id;
						$orderProduct->count = $product['count'];
						$orderProduct->price = Complect::model()->findByPk($id)->price;
						$orderProduct->mounting =  $product['mounting'];
						$orderProduct->order_id = $order->id;
					}
				}
				else
				{
					$orderProduct->product_id = $id;
					$orderProduct->count = $product['count'];
					$orderProduct->price = Products::model()->findByPk($id)->price;
					$orderProduct->mounting =  $product['mounting'];
					$orderProduct->order_id = $order->id;
				}
                
				$orderProduct->save();
			}
			unset(Yii::app()->session['cart']);
		}
        $client = Cart::model()->findByAttributes(array('order_id' => $order->id))->client;
        if ($order->payment_type_id==3 && $order->payment_type_id==4)
        {
         # You can easily override default constructor's params
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
            
            
            $orderProducts = OrderProduct::model()->findAllByAttributes(array('order_id' => $order->id));
            # render (full page)
            $mPDF1->WriteHTML($this->renderPartial('invoice', array(
                            'order' => $order,
                            'client' => $client,
                            'orderProducts' => $orderProducts
                            ), true));

            $file['file']=$mPDF1->Output('','S');
            $file['name']='invoice.pdf';
        }
        else {$file=NULL;}
        $mail = MailTemplate::model()->findByAttributes(array('name' => 'order'));
        $sendArg = array(
             '$email' => $client->email,
             '$name' => ($client->lastname == null ? '' : $client->lastname).' '.$client->name . ' '.(($client->middle_name == null) ? '' : $client->middle_name),
             ); 
        $mail->send($sendArg,$file);
    	$this->redirect("/order/confirm?id=".$order->id);
	}
    
    
	public function actionConfirm($id)
	{
        $client = Cart::model()->findByAttributes(array('order_id' => $id))->client;
        if (app()->user->id == $client->id)
        {
            $order = Order::model()->findByPk($id);

            $this->render('confirm', array(
                            'order' => $order,
                            ));
        }
	}
	
    public function actionPaid($id)
    {
        $order = Order::model()->findByPk($id);
        
        if(isset($_POST['operation_xml']))
        {
            $merc_sig = 'M1XTqyFqsAK0rvgZmmyti25AYvRw2lQlU';
            $xml_decoded=base64_decode($_POST['operation_xml']);
            $sign=base64_encode(sha1($merc_sig.$xml_decoded.$merc_sig,1)); 
            $signature=$_POST['signature'];
            if($signature===$sign)
            {
                preg_match_all('/status>([^<]*)/',$xml_decoded,$matches);
                $order->paystatus=$matches[1][0];
                if (!$order->save()) echo 'false';
                else {echo 'true';}
            }
            echo 'false';
         
            
        }
        else 
            echo 'false';
        
    }
    
        public function actionInvoice($id)
    {
            
        $order = Order::model()->findByPk($id);
        $client = Cart::model()->findByAttributes(array('order_id' => $id))->client;
		$orderProducts = OrderProduct::model()->findAllByAttributes(array('order_id' => $id));
        if (app()->user->id == $client->id)
        {
            # You can easily override default constructor's params
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');

            # render (full page)
            $mPDF1->WriteHTML($this->renderPartial('invoice', array(
                            'order' => $order,
                            'client' => $client,
                            'orderProducts' => $orderProducts
                            ), true));

            $mPDF1->Output('invoice'.$id,'D');
        }
        else {throw new CHttpException(401,'Access denied.');}
    }
    
	 public function actionGetCard($currency, $payment_id, $delivery_id)
	{
         if ($currency ==2)
            $delivery_cur=1;
         else $delivery_cur=$currency;
         
         Yii::app()->request->cookies['currency'] = new CHttpCookie('currency', $currency);
         $order = new Order();
         $price = $order->getProdPriceNewOrder();
         $delivery =0; 
         $deliverymodel = DeliveryType::model()->findByPk($delivery_id);
        $price = CurrencySys::exchange($order->getProdPriceNewOrder());
        $delivery_title="Способ доставки не выбран.";
        $commissia_title="0";
        
        if ($deliverymodel !== null){
            
           if (is_numeric($deliverymodel->description)) 
                $delivery = CurrencySys::exchange($deliverymodel->description,$delivery_cur,1);
           else 
               $delivery = 0;
           $order = new Order();
           
           $freed = $order->getFreedelivery();
           $freed = CurrencySys::exchange($freed, $delivery_cur, 1);
           if (($delivery_id==2) and ($price>$freed)) {$delivery =0; $deliverymodel->description ="Бесплатно";}
        $delivery_title = $deliverymodel->description;
        }
           
           if($payment_id>0)
		{
			$commissia = PaymentType::model()->findByPk($payment_id)->commission;
            
        }
        else 
            $commissia = 0; 
        $commissia = round(100*(($price+$delivery)*($commissia/100)))/100;
        if($payment_id == 4){ 
               $commissia=$commissia+CurrencySys::exchange(17,-1,1);
           }
        $commissia_title = $commissia .' '. CurrencySys::getLabel();
		if (is_numeric($delivery_title)) $delivery_title = CurrencySys::exchange($delivery_title, $delivery_cur, 1) . ' '. CurrencySys::getLabel();
        
        $sum = $price + $commissia + $delivery;
		$cart = $this->renderPartial('my-order',
            array(
                'cart'=>$this->cart,
                'delivery' => $delivery_title,
                'commissia' => $commissia_title,
                'sum' => $sum,
            ),true);
		echo json_encode(array(
                'title' => 'success',
                'value' => $cart,
                'price' => $price .' '. CurrencySys::getLabel()
            ));
		
	}
	

    public function actionGetDeliveryDescription($id)
    {
        $delivery = DeliveryType::model()->findByPk($id);
        if ($delivery !== null){
           echo json_encode(array(
                'title' => 'success',
                'payments' => CHtml::listData(DeliveryPayment::model()->findAllByAttributes(array('delivery_type_id' => $id)), 'paymentType.id', 'paymentType.title'),
            ));
        }
        else
            echo json_encode(array('title' => 'success', 'value' => 'Способ доставки не выбран.', 'payments' => array('' => 'Выберите способ доставки')));
    }
	
	public function actionGetCommission($id, $delivery_id)
    {
    	$model = new Order();
	   	$total_commission = $model->getTotalcommissiaNewOrder($delivery_id, $id);
		 
        $paymenttype = PaymentType::model()->findByPk($id);
        if ($paymenttype !== null){
            $commission=$paymenttype->commission;
            if ($paymenttype->commission==0){ $commission="Бесплатно";
            	$total_commission=0; }
            if($id == 3){ 
               $commission="Комиссия оплачивается покупателем отдельно, сумма варьируется в зависимости от того, в каком банке совершается оплата (в среднем составляет 1% от суммы).";
               $total_commission=0;
            }
            if($id == 4){ 
               $commission=$commission."%+17грн";
               $total_commission=$total_commission+CurrencySys::exchange(17,-1,1);
           }
            echo json_encode(array(
                'title' => 'success',
                'value' => $commission,
                'sum' => $total_commission                             
            ));
        }
		else echo $total_commission;
			
       
    }
	
     public function actionGetDeliverySysyemAddress($town)
    {
        $deliveryAdress = DeliverySystemAddress::model()->findAllByAttributes(array('town'=>$town));
        if ($deliveryAdress !== null)
            echo json_encode(array(
                'title' => 'success',
                'office' => CHtml::listData(DeliverySystemAddress::model()->findAllByAttributes(array('town'=>$town)), 'id', 'office_address'),
            ));
        else
            echo json_encode(array('title' => 'erroe', 'value' => ''));
    }
	
	public function actionGetTotalPrice($delivery_id, $payment_id)
    {
    	//json_encode(array('title' => 'erroe', 'value' => '--'));
		
       $model = new Order();
	   if($total_price = $model->getTotalpriceNewOrder($delivery_id, $payment_id))
	   		echo json_encode(array('title' => 'success', 'value' => $total_price));
	   else {
		   	echo json_encode(array('title' => 'erroe', 'value' => '--'));
	   }
	   
    }
}