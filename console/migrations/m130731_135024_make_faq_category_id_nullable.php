<?php

class m130731_135024_make_faq_category_id_nullable extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE `faq` CHANGE `category_id` `category_id` int(11) DEFAULT NULL;");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `faq` CHANGE `category_id` `category_id` int(11) NOT NULL;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}