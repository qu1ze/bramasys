<?php
/* @var $this DefaultController */
Yii::app()->getClientScript()->registerCssFile('/css/profile.css');
$this->breadcrumbs=array(
	'Изменение пароля',
);
?>
<?php $this->renderPartial('_side'); ?>
<div id="container">
	<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
	?>
	<h3>Изменение пароля</h3>
	<div class="profile-content">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id'=>'horizontalForm',
			'type'=>'horizontal',
			'htmlOptions' => array('enctype' => 'multipart/form-data', 'style' => 'margin: 0px'),
		));?>
	
	<?php echo $form->errorSummary($client); ?>

	<?php echo $form->passwordFieldRow($client, 'oldPassword'); ?>
	<?php echo $form->passwordFieldRow($client, 'newPassword'); ?>
	<?php echo $form->passwordFieldRow($client, 'passwordConfirm'); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить', 'htmlOptions' => array('class' => 'blue-button'))); ?> <a class="hover_color" href="<?php echo app()->getBaseUrl(true); ?>/user" style="margin-left: 20px;">Отменить</a>
<?php $this->endWidget();?>
	</div>
	<h3>Личный профиль</h3>
    <a class="hover_color" href="<?php echo app()->getBaseUrl(true); ?>/user/profile/edit">Редактировать личный профиль</a>
</div>
