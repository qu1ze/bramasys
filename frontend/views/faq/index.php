<div id="container">
<?php
Yii::app()->clientScript->registerScript("faqShow","
	 $('.faq-question').on('click', function () {
		var answer = $(this).parent().children('.faq-answer');
		if($(answer).css('display') == 'none')
			$(answer).slideDown(300);
		else
			$(answer).slideUp(300);
		return false;
	 });
	", CClientScript::POS_READY);
$this->breadcrumbs = app()->categoryController->getCategoryBcs($this->categoryName);

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
?>
	<div class="faq">
		<h3><?php echo app()->categoryController->getTitle($this->categoryId); ?></h3>
		<?php foreach ($faqCategories as $title => $faqs) : ?>
		<div class="content_faq">
			<div class="question-faq">
				<p><?php echo $title; ?></p>
				<?php if($title !== 'Общие вопросы') : ?>
				<div class="arrow-up">
					<a href="#">Вверх<img src="/images/arrowup.jpg"></a>
				</div>
				<?php endif; ?>
			</div>
			<div class="faq-notify">
				<ul>
					<?php foreach ($faqs as $faq) : ?>
					<li>
						<a href="#" class="faq-question">-<?php echo $faq->question; ?></a>
						<div class="faq-answer"><p><?php echo $faq->answer; ?></p></div>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<?php endforeach;?>
		<!--<div class="ask_question">
			<p class="text-faq">Задать вопрос</p>

			<form class="login-form order_form">
				<div class="login-username reg-form order-form faq-form">
					<label>Ваше имя или <a href="#">авторизуйтесь:</a></label>
					<input type="text">
				</div>
				<div class="login-username reg-form order-form faq-form">
					<label>Электронная почта:<br> <p>(для уведомления об ответе)</p></label>
					<input type="text">
				</div>
				<div class="login-username reg-form order-form faq-form">
					<label>Вопрос:</label>
					<textarea></textarea>

				</div>

				<div class="lostpass_btn edit-profile_btn faq_btn" >

					<div class="lostpass-btn edit-prof_btn tab-blue-btn faq-btn">
						<a href="#">Задать вопрос</a>
					</div>
				</div>
			</form>

		</div>-->
	</div>
     <div style="float:right;"> <?php $this->widget('common.components.SocButtons'); ?></div>
		
</div>