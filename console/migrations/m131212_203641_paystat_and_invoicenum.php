<?php

class m131212_203641_paystat_and_invoicenum extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE  `order` ADD  `paystatus` VARCHAR( 10 ) NOT NULL DEFAULT  'notpay',
                                             ADD  `invoice_num` INT NULL DEFAULT NULL ;");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `order` DROP `paystatus`, DROP `invoice_num` ;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}