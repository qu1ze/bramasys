<?php
/**
 * An example of extending the provider class.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

require_once dirname(dirname(__FILE__)) . '/services/YandexOAuthService.php';

class CustomYandexService extends YandexOAuthService {

	protected function fetchAttributes() {
		$info = (array)$this->makeSignedRequest('https://login.yandex.ru/info');

		$this->attributes['id'] = $info['id'];
		$this->attributes['name'] = $info['real_name'];
		//$this->attributes['login'] = $info['display_name'];
		//$this->attributes['email'] = $info['emails'][0];
		$this->attributes['email'] = $info['default_email'];
        $names = explode(' ', $info['real_name']);
        $this->attributes['first_name'] = $names[0];
        $this->attributes['last_name'] = $names[1];
        if (!empty($info['birthday'])) {
			$this->attributes['birthday'] = date("d.m.Y",strtotime($info['birthday']));
		}
        
		//$this->attributes['gender'] = ($info['sex'] == 'male') ? 'M' : 'F';
        
        
	}
}