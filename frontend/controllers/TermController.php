<?php

class TermController extends StaticController
{	
	public function actionView($title)
	{
		$this->render('view', array(
			'model' => Term::model()->findByAttributes(array('title' => $title)),
		));
	}
	
	public function actionIndex($search_term = null)
	{
		$dataProvider=new CActiveDataProvider('Term');
		$totalCount = $dataProvider->totalItemCount;
		if(!empty($search_term))
		{
			$criteria = new CDbCriteria;
			$criteria->addSearchCondition('title', $search_term.'%', false);
			$dataProvider->setCriteria($criteria);
		}
		$count = $dataProvider->itemCount;
		$dataProvider->setPagination(array(
			'pageSize' => 30,
			));
		$this->render('index', array(
			'dataProvider' => $dataProvider,
			'totalCount' => $totalCount,
			'count' => $count,
			'search' => $search_term,
		));
	}
}
