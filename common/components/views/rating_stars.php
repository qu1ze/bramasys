<?php
$starsCount = $this->rating;
for($i = 0; $i < 5; $i++): ?>
    <div class="star <?php echo $starsCount > 0 ? 'on' : ''; ?>">
        <div style="<?php echo $starsCount > 0 && $starsCount < 1 ? 'width: '.$this->starSize * $starsCount.'px; ' : ''; ?>overflow: hidden;">
            <span style="width: <?php echo $this->starSize; ?>px;"><?php echo $i + 1; ?></span>
        </div>
    </div>
    <?php
    $starsCount--;
endfor;
?>