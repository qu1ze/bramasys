<?php
class BottomTabs extends CWidget
{
	public function init()
	{
		
	}
	
	public function getModel($key, $id)
	{
		$model = $key == 'complects' ? Complect::model() : Products::model();
		$model = $model->findByPk($id);
		return $model;
	}
	
    public function run()
    {
		$compareProducts = array();
		if(isset(Yii::app()->session['compare']))
		{
			$compareArray = Yii::app()->session['compare'];
			foreach (Yii::app()->session['compare'] as $categoryKey => $products)
			{
				foreach ($products as $key => $productId)
				{
					array_push($compareProducts, $productId);
				}
			}
		}
		else
		{
			$compareArray = array();
		}
		$this->render("_bottom_tabs", array(
				'compareProducts' => $compareProducts,
				'products' => Products::model()->findAllByPk($compareProducts),
				'compareArray' => $compareArray,
				'viewed' => isset(app()->session['viewed']) ? app()->session['viewed'] : array(),
			)
		);
		
    }
}
?>
