<?php

class DeliverysystemaddressController extends Controller
{
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
	
		$model = new DeliverySystemAddress('search');
		
		if (isset($_GET['DeliverySystemAddress']))
			$model->attributes = $_GET['DeliverySystemAddress'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
    public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('DeliverySystemAddress');		
		$es->update();		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Запись успешно отредактирована')));			
	}	
	
	public function actionAjaxDelete($id)
	{		
		if (($model = DeliverySystemAddress::model()->findByPk($id)) !== null)
		{
                if ($model->delete())
					echo json_encode(array('title' => 'Success', 'msg' => 'Запись успешно удалена'));
				else
					echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить запись'));
			
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Запись не найдена'));
	}
	/**
	 * Manages all models.
	 */
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
//	public function loadModel($id)
//	{
//		$model=DeliverySystemAddress::model()->findByPk($id);
//		if($model===null)
//			throw new CHttpException(404,'The requested page does not exist.');
//		return $model;
//	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='delivery-system-address-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
	
	
	 private function uploadFile($file)
	{
		$excelFile = new ExcelFile();
		$excelFile->file = $file;
		if(!$excelFile->validate('file', false))
		{
			return $excelFile;
		}
		$uploader = CUploadedFile::getInstance($excelFile, 'file');
		if($uploader != null)
		{
			$path = app()->basePath.DIRECTORY_SEPARATOR.'www'.DIRECTORY_SEPARATOR.md5($uploader->name.time()).'.'.$uploader->extensionName;
			$uploader->saveAs($path);
			$excelFile->file = $path;
			$excelFile->extensionName = $uploader->extensionName;
		}
		else
		{
			$excelFile->addError('file', "Не удалось загрузить файл.");
		}
		return $excelFile;
	}
	
    public function actionImport() 
    {
	    if(isset($_FILES['ExcelFile']))
		{
			$excelFile = $this->uploadFile($_FILES['ExcelFile']);
			$model = new DeliverySystemAddress();
			$model->importFromFile($excelFile);
		}
		else
		{
			$excelFile = new ExcelFile();
		}   
        $this->render('import', array(
			'excelFile' => $excelFile,
		));
    }
}
