<ul class="navigation">
<?php
$searchTree = null;
if($tag == ProductsController::getTag()):
	if(Yii::app()->controller->id == 'products' && Yii::app()->controller->isSearch):
		$searchTree = Yii::app()->controller->searchTree;
		$children = $searchTree->getChilds(Yii::app()->controller->rootCategory);
		if(!empty(Yii::app()->controller->categoryIds)):
?>
	<h4>Найденно в каталоге</h4>
<?php	endif;
	else:
		$children = $this->getChilds(Yii::app()->controller->rootCategory);
	endif;
elseif ($tag == StaticController::getTag()):
	$categoryId = $this->getCategoryId(Yii::app()->controller->categoryName);
	$parentsIds = $this->addParentsIds($categoryId);
	$category = $this->getCategory($parentsIds[0]);
	$children = $this->getChilds($category['parent']);
else:
	$children = $this->getChilds($tag);
endif;
    if(!empty($children)):
?>
<?php foreach ($children as $child): 
$doCount = false;	
?>
<?php if($child['data']['in_url'] > 0): ?>
    <li <?php echo $this->isActiveCategory($child['id']) ? 'class="active"' : ''; ?>>
        <a title="<?php echo $child['data']['title']; ?>" href="<?php echo $this->getCategoryAddress($child['data']['name']); ?><?php echo empty($_GET['search']) ? '' : "?search={$_GET['search']}"; ?>">
            <?php echo $child['data']['title']; ?>
            <img alt="arrow" src="/images/nav-item.png">
            <?php if($tag == ProductsController::getTag()): ?>
                <span class="pd-count">(<?php echo $this->countProducts($child['id'], $searchTree); ?>)</span>
            <?php endif; ?>
        </a>
<?php endif; ?>
		<?php if($tag == ProductsController::getTag()): ?>
			<?php $doCount = true; ?>
		<?php endif; ?>
		<?php $this->renderTree($child['id'], $searchTree, $doCount); ?>
<?php if($child['data']['in_url'] > 0): ?>
    </li>
<?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
</ul>