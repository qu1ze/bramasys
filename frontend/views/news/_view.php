<?php
/*<div class="read_more_wrapper">
		<p>
			<a class="read_more_link" href="<?php echo Yii::app()->createUrl('/company/news/' . $data->name) ?>">
				Читать далее —>
			</a>
		</p>
</div>
 * <div>
		<p>
			Теги: <?php echo $data->keywords ?>
		</p>
	</div>
 * */
 Yii::app()->getClientScript()->registerCssFile('/css/rating.css');
?>
<div id="container">
<div class="article-tab">
	<img src="<?php echo $data->thumb ? $data->thumb : '/images/article-img.jpg' ?>" width="140px" />
	<div class="date">
		<?php echo date('d.m.Y', $data->update_time) ?>
	</div>
	<a class="news_title_link" href="<?php echo Yii::app()->createUrl('/company/news/' . $data->name) ?>">
		<?php echo $data->title ?>
	</a>
	<div> 
		<?php echo $data->short_description ?>		
	</div>
	
	
	<div class="name_article review-product">
		
		<div class="heading-article">
                        <table>
				<tr>
					<td>
                     <div class="rate-count">
                       <div class="vote-wrap">
						<div class="comment-rating">
                            <?php $colRating = Comment::model()->count("`news_id` = $data->id AND rating != 0 AND status = 1"); ?>
                            <?php $this->widget('common.components.RatingStars', array('rating' => $data->getRating())); ?> (<?php echo $colRating;?> оц.)
                        
                        </div>
                           </div>
                         </div>
					</td>
					<td>
						<?php echo $data->views ?> просмотров
					</td>
					<td rowspan="2" valign="top">
						
                       
					</td>
                    
				</tr>
				<tr>
					<td>
                        <?php $colComment = Comment::model()->count("`news_id` = $data->id AND status = 1 AND parent_id is NULL"); ?>
                        <?php if($colComment<1):?>
                        <a href="<?php echo Yii::app()->createUrl('/company/news/' . $data->name)."#comment"; ?>">
                        Оставить отзыв
                        <?php else:?>
                        <a href="<?php echo Yii::app()->createUrl('/company/news/' . $data->name)."#comment"; ?>">
                        <?php echo $colComment;?> отзывов
                        <?php endif;?>
                        </a>
					</td>
					<td>
                                            <?php $subject = urlencode($data->title)?>
						<?php $body = urlencode(Yii::app()->getBaseUrl(true) . '/company/news/' . $data->name)?>
						<a href="mailto:?subject=<?php echo $subject?>&body=<?php echo $body?>">
							<img src="/images/mail-to.png">
						</a>    
					</td>				
				</tr>
			</table>
		
		</div>
	</div>

</div>
</div>