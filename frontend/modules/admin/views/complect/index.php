<?php
cs()->registerCoreScript('yiiactiveform');

$grid_id = 'complect-table';
$gridView = $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $grid_id,
	'type' => 'bordered striped',
	'dataProvider' => $complect->search(),
	'filter' => $complect,
	'afterAjaxUpdate' => 'js:function(){
		jQuery(".tooltip").remove();
		jQuery("a[rel=tooltip]").tooltip();
	}',
	'template' => "{items}\n{pager}",
	'columns' => array(
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'name',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/complect/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'category_id',
			'filter' => $this->getCategories(),
			'editable' => array(
				'type' => 'select', 
				'source' => $this->getCategories(true),
				'url' => app()->createUrl('/admin/products/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
		array(
			'name' => 'complectProductsField',
		),
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'full_description',
            'editable' => array(
                'type' => 'text',
                'url' => app()->createUrl('/admin/complect/ajaxUpdate'),
                'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
            )
        ),
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'discount',
            'editable' => array(
                'type' => 'text',
                'url' => app()->createUrl('/admin/complect/ajaxUpdate'),
                'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
            )
        ),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('nowrap' => 'nowrap'),			
						'template' => '{delete} {update} {setItems} {productPage}',
			'buttons'=>array
			(
				'setItems' => array
				(
					'label'=>'Задать продукты',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/admin/box.png',
					'url'=>'Yii::app()->createUrl("admin/complect/setItems", array("id"=>$data->primaryKey))',
				),
				'productPage' => array
				(
					'label'=>'Страница комплекта',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/admin/product.png',
					'url'=>'Yii::app()->createUrl("/complect/$data->primaryKey")',
				),
			),
			'updateButtonUrl'=>'Yii::app()->controller->createUrl("update", array("id" => $data->primaryKey))',
			'deleteButtonUrl'=>'Yii::app()->controller->createUrl("ajaxDelete", array("id" => $data->primaryKey))',
			'header' => EHtml::addGridButton($this->createAbsoluteUrl('/admin/complect/ajaxCreate'), 'Добавить', $height=350, 'icon-question-sign'),
			'deleteConfirmation' => 'Вы уверенны что хотите удалить?',
			'afterDelete' => 'function(link,success,data){
				if(success){data = $.parseJSON(data);brama.ui.notify(data.title, data.msg, false);}
				else{brama.ui.notify("Oops!", data.responseText )};
			}',
		)
	)
),true);

echo str_replace('&lt;br&gt;','<br>', $gridView);
?>