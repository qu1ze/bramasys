<?php

class m130731_065704_add_faq_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `faq` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `question` text NOT NULL,
			  `answer` text NOT NULL,
			  `category_id` int(11) NOT NULL,
			  `create_time` int(11) NOT NULL,
			  `update_time` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->dropTable('faq');
	}

}