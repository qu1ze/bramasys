<?php
/* @var $order Order */
Yii::app()->getClientScript()->registerCssFile('/css/profile.css');
$this->breadcrumbs=array(
	'Мои заказы',
);

Yii::app()->clientScript->registerScript("show-my-order-list", "
        $('.order-list-href').on('click', function(){
			var list = $(this).parent().children('.order-list');
			if($(list).css('display') == 'none')
			{
				$(list).slideDown();
				$(this).text('Скрыть');
			}
			else
			{
				$(list).slideUp();
				$(this).text('Посмотреть список товаров');
			}
        });
	", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript("show-my-order-info", "
        $('.order-info-href').on('click', function(){
            //;
        });
	", CClientScript::POS_READY);

$this->renderPartial('_side'); ?>
<div id="container">
	<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
	?>
	<h3>Мои заказы</h3>
	<table class="cart-table" style="width: 100%;">
	<thead>
		<tr class="header">
            <td style="width: 90px;">Дата заказа</td>
			<td style="width: 80px;">№ заказа</td>
			<td>Наименование товара</td>
			<td style="width: 220px;">Подробности заказа</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($orders as $order): ?>
			<tr>
                <td><?php echo $order->create_time;?></td>
				<td><?php echo $order->id;?></td>
				<td>
					<div class="order-list" style="display: none;">
						<?php
						foreach (OrderProduct::model()->findAllByAttributes(array('order_id' => $order->id)) as $product): 
						$model = $product->is_complect ? Complect::model()->findByPk($product->product_id) : Products::model()->findByPk($product->product_id);
						?>
						<a style="display: inline;" href="<?php echo $model->link; ?>"><?php echo $model->categoryName; ?> / <?php echo $model->name; ?></a>
						<hr>
						<?php endforeach; ?>
					</div>
					<a class="order-list-href">Посмотреть список товаров</a>
				</td>
				<td>
                    Тип доставки:<?php echo $order->deliveryType->title;?><br>
                    Тип оплаты:<?php echo $order->paymentType->title;?>
                </td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
</div>