<?php
class AppCategoryController extends CWidget
{
    private $categoryTree = null;
    private $model;
    private $itemsCount = 0;
    
    public function init()
    {
        $this->model = Category::model();
        $this->categoryTree = $this->getTree();
    }
    
    public function renderTree($id, Tree $tree = null, $count = true, $itemsPerCol = 0, $level = 0)
    {
        $children = $tree === null ? $this->getChilds($id) : $tree->getChilds($id);
        if(!empty($children))
        {
			$child = end($children);
			$parent = $this->getCategory($child['parent']);
			if($parent['data']['in_url'] < 1)
			{
				$level--;
			}
            if($itemsPerCol > 0 && $parent['data']['in_url'] > 0)
                echo '<table><tr><td><ul>';
            else if($parent['data']['in_url'] > 0)
                echo '<ul>';
            foreach ($children as $child)
            {
				if($child['data']['in_url'] > 0)
				{
					if($itemsPerCol > 0 && $this->itemsCount >= $itemsPerCol && $level < 1)
					{
						$this->itemsCount = 0;
	                    //if($parent['data']['in_url'] > 0)
							echo '</ul></td><td><ul>';
					}
					else if($itemsPerCol > 0 &&  $this->itemsCount - $itemsPerCol <= 2 && $this->countChildren($child['id']) > 4)
					{
						$this->itemsCount = 0;
						echo '</ul></td><td><ul>';
					}
                    $class = '';
                    if ($this->isActiveCategory($child['id']))
                        $class = 'active';
					if($level < 1 && $level > -1) {
						echo '<li class="'.$class.' subtitle">';
                    }
					else {

                        echo '<li class="'.$class.'">';
                    }
					echo '<a title="'.$child['data']['title'].'" href="'.$this->getCategoryAddress($child['data']['name']).(empty($_GET['search']) ? '' : "?search={$_GET['search']}").'">';
					echo $child['data']['title'];
					$this->itemsCount++;
					echo '<img alt="arrow" src="/images/nav-item.png">';
					if($count)
						echo '<span class="pd-count">('.$this->countProducts($child['id'], $tree).')</span>';
                    echo '</a>';
				}
				$this->renderTree($child['id'], $tree, $count, $itemsPerCol, $level + 1);
				if($child['data']['in_url'] > 0)
					echo '</li>';
            }
            if($itemsPerCol > 0 && $parent['data']['in_url'] > 0)
                echo '</ul></td></tr></table>';
            else if($parent['data']['in_url'] > 0)
                echo '</ul>';
        }
        if($level < 1)
            $this->itemsCount = 0;
    }
        
    public function renderNavigationTree($controller)
    {
		$controller .= "Controller";
		$getTag = "getTag";
        $this->render('_navigation',array(
            'cssFile'=>'/css/filter.css',
			'tag' => $controller::$getTag(),
        ));
    }
    
    public function renderCatalogTree()
    {
        $this->render('_menu_catalog');
    }
    
    public function renderMenu($name)
    {
		if($name !== -1 && !is_bool($name))
        $this->render('_menu', array(
            'name' => $name
            ));
    }

    public function isActiveCategory($categoryIdOrName)
    {
        if(!is_numeric($categoryIdOrName))
            if(is_bool($categoryIdOrName = $this->getCategoryId($categoryIdOrName)))
                return false;
        $category = $this->getCategory($categoryIdOrName);
        $url = Yii::app()->request->getPathInfo();
        $parts = explode('/', $url);

        if(end($parts) == $category['data']['name']){
            return true;
        }
        return false;
    }

	public function isInUrl($categoryIdOrName)
	{
		if(!is_numeric($categoryIdOrName))
			if(is_bool($categoryIdOrName = $this->getCategoryId($categoryIdOrName)))
				return false;
		$category = $this->getCategory($categoryIdOrName);
		return $category['data']['in_url'];
	}
	
	public function isComplect($categoryIdOrName)
	{
		if(is_numeric($categoryIdOrName))
		{
			$category = $this->getCategory($categoryIdOrName);
			$categoryIdOrName = $category['data']['name'];
		}
		if(substr($categoryIdOrName, strlen($categoryIdOrName) - 10, 10) == '_complects')
			return true;
		return false;
	}
	
    public function getChilds($id = -1)
    {
        return $this->categoryTree->getChilds($id);
    }
    
	public function getTag($categoryId)
	{
		$parents = $this->addParentsIds($categoryId, true);
		$root = $this->categoryTree->getItem($parents[0]);
		return isset($root['parent']) ? $root['parent'] : false;
	}
	
	public function isStatic($categoryIdOrName)
	{
		if(!is_numeric($categoryIdOrName))
			if(is_bool($categoryIdOrName = $this->getCategoryId($categoryIdOrName)))
				return false;
		return $this->getTag($categoryIdOrName) === 'static';
	}
	
	public function isCatalog($categoryId)
	{
		if(!is_numeric($categoryId))
			if(is_bool($categoryId = $this->getCategoryId($categoryId)))
				return false;
		return $this->getTag($categoryId) === 'catalog';
	}
	
	public function getCategoryAddressById($categoryId)
    {
		$tag = $this->getTag($categoryId);
		$addressExtension = ($tag != ProductsController::getTag() && $tag != StaticController::getTag()) ? $tag.'/' : '';
        $url = '';
		$outBool = false;
		$category = &$this->categoryTree->getItemRef($outBool, $categoryId);
		if(!empty($category['data']['link']))
		{
			$url = $category['data']['link'];
		}
		else
		{
			$categories = $this->getNames($this->addParentsIds($categoryId));
			foreach ($categories as $categoryName)
			{
				$url .= $categoryName.'/';
			}
			$url = app()->getBaseUrl(true).'/'.$addressExtension.rtrim($url, '/');
			$category['data']['link'] = $url;
		}
        return $url;
    }
	
    public function getCategoryAddress($ultimateCategory, $categories = array())
    {
		$tag = $this->getTag($this->getCategoryId($ultimateCategory));
		$addressExtension = ($tag != ProductsController::getTag() && $tag != StaticController::getTag()) ? $tag.'/' : '';
        $url = '';
        if(!empty($categories))
        {
            foreach ($categories as $categoryName)
            {
                $url .= $categoryName.'/';
                if($categoryName == $ultimateCategory)
                {
                    break;
                }
            }
            $url = app()->getBaseUrl(true).'/'.$addressExtension.rtrim($url, '/');
        }
        else
		{
			$categoryId = $this->getCategoryId($ultimateCategory);
			$outBool = false;
			$category = &$this->categoryTree->getItemRef($outBool, $categoryId);
			if(!empty($category['data']['link']))
			{
				$url = $category['data']['link'];
			}
			else
			{
				$categories = $this->getNames($this->addParentsIds($categoryId));
				foreach ($categories as $categoryName)
				{
					$url .= $categoryName.'/';
				}
				$url = app()->getBaseUrl(true).'/'.$addressExtension.rtrim($url, '/');
				$category['data']['link'] = $url;
			}
        }
        return $url;
    }
    
    public function getTitle($id)
    {
        $categoryItem = $this->categoryTree->getItem($id);
        return $categoryItem['data']['title'];
    }

    public function getCategoryBcs($ultimateCategory, $lastLink = false)
    {
        $categoriesIds = $this->addParentsIds($this->getCategoryId($ultimateCategory));
        $categories = $this->getNames($categoriesIds);
        $breadcrumbs = array();
        foreach ($categories as $category) 
        {
            $categoryItem = $this->categoryTree->getItem(array_shift($categoriesIds));
            if(!$lastLink && $category == $ultimateCategory)
            {
                $breadcrumbs[0] = $categoryItem['data']['title'];
                break;
            }
            $breadcrumbs[$categoryItem['data']['title']] = array($this->getCategoryAddress($category, $categories));
        }
        return $breadcrumbs;
    }
    
	public function getCategory($categoryId)
	{
		return $this->categoryTree->getItem($categoryId);
	}
	
    public function getChildrenIds($categoryId)
    {
        $categoryChildren = array();
        $children = $this->categoryTree->getChilds($categoryId);
        if (!empty($children))
        {
            foreach ($children as $value)
            {
                array_push($categoryChildren, $value['id']);
                if ($this->categoryTree->hasChilds($value['id']))
                {
                    $categoryChildren = array_merge($categoryChildren, $this->getChildrenIds($value['id']));
                }
            }
        }
        return $categoryChildren;
    }
    public function getNames($categoryIds)
    {
        if(!empty($categoryIds) && is_array($categoryIds))
        {
            $categories = array();
            foreach ($categoryIds as $value)
            {
                $category = $this->categoryTree->getItem($value);
                if(!is_array($category))
                {
                    return array();
                }
                array_push($categories, $category['data']['name']);
            }
            return $categories;
        }
        return array();
    }
       
    public function getCategoryId($ultimateCategory = "")
    {
        if (empty($ultimateCategory))
        {
            return -1;
        }
        else
        {
            $category = $this->model->find("name = '$ultimateCategory'");
            if (is_object($category))
            {
                return $category->id;
            }
        }
        return false;
    }
    
    public function addParentsIds($ultimateId, $allCategories = false)
    {
        $categories = array();
        array_push($categories, $ultimateId);
        $category = $this->categoryTree->getItem($ultimateId);
        if(is_array($category) && is_numeric($category['parent']) && is_numeric($category['id']))
        {
            while (is_numeric($category['parent']))
            {
                $category = $this->categoryTree->getItem($category['parent']);
				if($category['data']['in_url'] > 0 || $allCategories)
					array_push($categories, $category['id']);
            }
        }
        return array_reverse($categories);
    }

    public function countChildren($id, $countOnlyInUrl = true)
    {
        $childrenCount = 0;
        $children = $this->categoryTree->getChilds($id);
        foreach ($children as $child)
        {
			if($countOnlyInUrl)
			{
				if($child['data']['in_url'] > 0)
					$childrenCount++;
			}
			else
			{
				$childrenCount++;
			}
            $childrenCount += $this->countChildren($child['id'], $countOnlyInUrl);
        }
        return $childrenCount;
    }
    
    public function countProducts($id = null, & $tree = null)
    {
		$id == null ? $id = ProductsController::getTag() : 1;
		if($tree === null)
		{
			$tree = $this->categoryTree;
			
		}
        $productsCount = 0;
		$outBool = false;
		$category = & $tree->getItemRef($outBool, $id);
		if(!empty($category['data']['count']))
		{
			return $category['data']['count'];
		}
		else
		{
			if(is_numeric($id))
			{
				if($this->isComplect($id))
					$productsCount += Complect::model()->count("category_id = $id");
				else
					$productsCount += Products::model()->count("category = $id AND hide != 1");
			}
			$children = $tree->getChilds($id);
			foreach ($children as $child)
			{
				$productsCount += $this->countProducts($child['id'], $tree);
			}
			$category['data']['count'] = $productsCount;
		}
        return $productsCount;
    }
    
    private function getTree()
    {
        $categoryTree = new Tree();
        $categoryTree->addItem(-1, 0, array('name' => 'root', 'title' => 'root'));
        $categoryTags = CategoryTag::model()->findAll();
        $categories = $this->model->findAll(array('order' => 'order_number'));
        foreach ($categoryTags as $categoryTag)
        {
            $categoryTree->addItem($categoryTag->name, -1, array(
                'name' => CHtml::encode($categoryTag->name), 
                'title' => CHtml::encode($categoryTag->name),
                ));
        }
        $categoryTags = CHtml::listData($categoryTags, 'id', 'name');
        foreach ($categories as $category)
        {
            if ($category->parent != null)
                $parentId = $category->parent;
            else
                $parentId = $categoryTags[$category->tag];
            $categoryTree->addItem($category->id, $parentId, array(
                'name' => $category->name,
                'title' => $category->title,
				'in_url' => $category->in_url,
				'link' => null,
				'count' => null,
				'image' => $category->image_url,
				'isMain' => is_numeric($parentId) ? false : true,
				'extendedExtra' => null,//TODO:extended extra.
				'filter' => false,//TODO:category filter.
                ));
        }
        return $categoryTree;
    }
	
	private function getChildsRecursive(Tree $categoryTree, $categoryId, $filterArray = array(), $counts = array(), $level = 0)
	{
		$tag = $this->getTag($categoryId);
		foreach ($this->categoryTree->getChilds($categoryId) as $subChild)
		{
			if ($level != 0)
				$parentId = $categoryId;
			else
				$parentId = $tag;
			if(empty($filterArray) || in_array($subChild['id'], (array)$filterArray))
			{
				if(!empty($counts) && isset($counts[$subChild['id']]))
					$subChild['data']['count'] = $counts[$subChild['id']];
				$categoryTree->addItem($subChild['id'], $parentId, $subChild['data']);
				$this->getChildsRecursive($categoryTree, $subChild['id'], $filterArray, $counts, $level + 1);
			}
		}
		return $categoryTree;
	}
	
	public function buildTree($categoryIds, $counts = array())
    {
		$tag = '';
		if(isset($categoryIds[0]))
		{
			$tag = $this->getTag($categoryIds[0]);
		}
        $categoryTree = new Tree();
		$categoryTree->addItem(-1, 0, array('name' => 'root', 'title' => 'root'));
		$categoryTree->addItem($tag, -1, array(
			'name' => $tag, 
			'title' => $tag,
			));
		$catalogChildren = $this->categoryTree->getChilds($tag);
		foreach ($catalogChildren as $child)
		{
			if(in_array($child['id'], $categoryIds))
			{
				$categoryTree = $this->getChildsRecursive($categoryTree, $child['id'], $categoryIds, $counts);
			}
		}
        return $categoryTree;
    }
}