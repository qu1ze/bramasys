<div id="container">
<?php
$this->breadcrumbs = app()->categoryController->getCategoryBcs($this->categoryName);

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
?>
	<div style="padding-top: 20px;">
		<form id="support-search" action="<?php echo Yii::app()->request->url; ?>">
			<table>
				<thead>
					<tr>
						<td>Категория</td><td>Бренд</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo CHtml::dropDownList('forCategory', $forCategory, $forCategories, array('class' => 'support-input'));?></td>
						<td><?php echo CHtml::dropDownList('brand', $brand, $brands, array('class' => 'support-input'));?></td>
					</tr>
				</tbody>
			</table>
			<div>
				<input type="text" placeholder="Введите текст" class="support-input" id="support-search-input" name="search" value="<?php echo $search;?>">
				<input type="submit" value="Найти" id="btn">
			</div>
		</form>
		<hr>
		<table class="support-table">
			<?php foreach ($supportFiles as $supportFile):?>
				<tr>
					<td class="img-link"><a href="<?php echo $supportFile->fullPath; ?>"><img width="50" src="<?php echo $supportFile->image; ?>"><br>(скачать)</a></td>
					<td><a href="<?php echo $supportFile->pageUrl; ?>"><h5><?php echo $supportFile->name;?></h5></a><p><?php echo $supportFile->description;?></p></td>
				</tr>
			<?php endforeach;?>
		</table>
	</div>
</div>
