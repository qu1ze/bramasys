<?php

class DefaultController extends Controller
{
	public function filters()
	{
		return array('accessControl');
	}
	
//	public function accessRules()
//	{
//		return array(
//			array('allow', // Allow all actions for logged in users ("@")
//				'actions' => array('index', 'test'),
//				'users' => array('@'),
//			),
//			array('deny'), // Deny anything else
//		);
//	}
			
	public function actionIndex()
	{
		$this->render('index');
	}
	
//	public function actionTest()
//	{
//		throw new CHttpException(404, 'Message');
//	}
}