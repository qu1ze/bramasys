<?php

/**
 * This is the model class for table "phone".
 *
 * The followings are the available columns in table 'phone':
 * @property integer $id
 * @property integer $type
 * @property string $value
 * @property integer $client_id
 */
class Phone extends ActiveRecord
{
	const PHONE_TYPE_MOBILE = 1;
	const PHONE_TYPE_HOME = 2;
	const PHONE_TYPE_WORK = 3;
	
	private $phoneTypes = array(
		self::PHONE_TYPE_MOBILE => 'Мобильный',
		self::PHONE_TYPE_HOME => 'Домашний',
		self::PHONE_TYPE_WORK => 'Рабочий',
	);
	
	public function getPhoneTypes($type = null)
	{
		return is_null($type) ? $this->phoneTypes : $this->phoneTypes[$type];
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Phone the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'phone';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, value, client_id', 'required'),
            array('value', 'filter', 'filter'=>'trim'),
			array('type, client_id', 'numerical', 'integerOnly'=>true),
			array('type', 'in', 'range'=>  array_keys($this->phoneTypes)),
			//array('value', 'length', 'max'=>14),
            array('value', 'validateIsPhone'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, value, client_id', 'safe', 'on'=>'search'),
		);
	}

    public function validateIsPhone($attribute)
    {
        $value = $this->getAttribute($attribute);
        $phoneParts = $this->getPhoneParts($value);
        if(empty($phoneParts['number']))
            $this->addError('value', 'Номер телефона введен не верно');
        if(empty($phoneParts['code']) || !is_numeric($phoneParts['code']))
            $this->addError('value', 'Неправильный код города или оператора');
        $numericPhone = (string)$this->getNumericFromNumber($phoneParts['number']);
        if($this->containsIllegalChars($phoneParts['number']))
            $this->addError('value', 'Номер телефона содержит не допустимые символы');
        else if(!is_numeric($numericPhone))
            $this->addError('value', 'Номер телефона содержит не допустимые символы');
        if(strlen($numericPhone) != 7)
            $this->addError('value', 'Номер телефона должен содержать 7-м цыфр');
        if(!$this->hasErrors())
            $this->setAttribute($attribute, "({$phoneParts['code']})".substr($numericPhone, 0 , 3)."-".substr($numericPhone, 3 , 2)."-".substr($numericPhone, 5, 2));
    }

    public function isPhone($value)
    {
        $phoneParts = $this->getPhoneParts($value);
        if(!empty($phoneParts['number']) && !empty($phoneParts['code']))
        {
            if(!is_numeric($phoneParts['code']) || $this->containsIllegalChars($phoneParts['number']) || !is_numeric($this->getNumericFromNumber($phoneParts['number'])))
                return false;
        }
        return true;
    }

    public function getNumericFromNumber($value)
    {
        return preg_replace('/-|\s/', '', $value);
    }

    public function containsIllegalChars($value)
    {
        $matches = array();
        preg_match('/[^0123456789-]/', $value, $matches);
        if(isset($matches[0]))
            return true;
        return false;
    }

    /**
     * @param string $value phone number that should be like '(code)nubmer' or 'number'.
     * @return array parts of phone number.
     * array['code'] - town or provider.
     * array['number'] - phone number.
     */

    public function getPhoneParts($value)
    {
        $matches = preg_split('#\(|\)#', $value);
        if(sizeof($matches) < 3)
        {
            $phoneParts['number'] = isset($matches[0]) ? trim($matches[0]) : null;
        }
        else
        {
            $phoneParts['code'] = isset($matches[1]) ? trim($matches[1]) : null;
            $phoneParts['number'] = isset($matches[2]) ? trim($matches[2]) : null;
        }
        return $phoneParts;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Тип',
			'value' => 'Номер телефона',
			'client_id' => 'Client',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('client_id',$this->client_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}