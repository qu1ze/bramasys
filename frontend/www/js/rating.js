// JavaScript Document
/*************************************************
Star Rating System
First Version: 21 November, 2006
Author: Ritesh Agrawal
Inspriation: Will Stuckey's star rating system (http://sandbox.wilstuckey.com/jquery-ratings/)
Demonstration: http://php.scripts.psu.edu/rja171/widgets/rating.php
Usage: $('#rating').rating('www.url.to.post.com', {maxvalue:5, curvalue:0});

arguments
url : required -- post changes to 
options
	maxvalue: number of stars
	curvalue: number of selected stars
	

************************************************/

jQuery.fn.rating = function(options) {
	
	var settings = {
        maxvalue  : 5,   // max number of stars
        curvalue  : 0    // number of selected stars
    };
   
   if(options) {
       jQuery.extend(settings, options);
    };

   var container = jQuery(this);
	
	jQuery.extend(container, {
            averageRating: settings.curvalue,
        });

	for(var i= 0; i <= settings.maxvalue ; i++){
		var size = i
        if (i != 0) {
             var div = '<div class="star"><a href="#'+i+'">'+i+'</a></div>';
			 container.append(div);

        }
 
		

	}
	
	var stars = jQuery(container).children('.star');
	
    stars
	        .mouseover(function(){
                event.drain();
                event.fill(this);
            })
            .mouseout(function(){
                event.drain();
                event.reset();
            })
            .focus(function(){
                event.drain();
                event.fill(this)
            })
            .blur(function(){
                event.drain();
                event.reset();
            });

    stars.click(function(){
		if(settings.maxvalue != 1){
            settings.curvalue = stars.index(this) + 1;
			return false;
		}
		else {
			settings.curvalue = (settings.curvalue == 0) ? 1 : 0;
			$(this).toggleClass('on');
			return false;
		}
		return true;
			
    });
        
	var event = {
		fill: function(el){ // fill to the current mouse position.
			var index = stars.index(el) + 1;
			stars.children('a').css('width', '100%').end()
			var parentId = '#' + stars.parent().attr('id');
			$(parentId + ' .star:lt(' + index + ')').addClass('hover').end();
		},
		drain: function() { // drain all the stars.
			stars
				.filter('.on').removeClass('on').end()
				.filter('.hover').removeClass('hover').end();
		},
		reset: function(){ // Reset the stars to the default index.
			var parentId = '#' + stars.parent().attr('id');
			$(parentId + ' .star:lt(' + settings.curvalue + ')').addClass('on').end();
		}
	}        
	event.reset();
	
	return(this);	

}