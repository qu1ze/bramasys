<?php 
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/css/rating.css");
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/rating.js");

Yii::app()->clientScript->registerScript("commentFormShow","

	if ((window.location.hash != '#tab=customer-reviews')&&($('#comment_count').attr('count')>6) ) 
    {
      $('.comment-show').show();
      $('.afterfive').hide();
    }
    else 
    {
      $('.afterfive').show()
      $('.comment-show').hide();
    }
   $('.comment-form-show-link').on('click', '.comment-form-show', function () {
		$(this).toggleClass('comment-form-hide');
		$(this).toggleClass('comment-form-show');
		$(this).text('Скрыть')
		commentId = $(this).attr('comment-id');
		$('#comment #post_comment:not(.response_form)').slideDown();
		$('body').animate({
			scrollTop: $('#comment #post_comment:not(.response_form)').offset().top
		}, 200);
                 $('#up_hr').hide();
                $('#down_hr').show();
	 });
   $('.comment-form-show-link').on('click', '.comment-show', function () {
		$('.afterfive').show();
		$(this).hide();
               
	 });
	     
	 $('.comment-form-show-link').on('click', '.comment-form-hide', function () {
		$(this).toggleClass('comment-form-hide');
		$(this).toggleClass('comment-form-show');
		$(this).text('Оставить отзыв')
		commentId = $(this).attr('comment-id');
		$('#comment #post_comment').slideUp();
                $('#down_hr').hide();
                $('#up_hr').show();
	 });
	", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("initRating","
         $(window).load (function () {
            $('#rating').rating({maxvalue:5});
         });
        ");
Yii::app()->clientScript->registerScript("post","
         $('#comment').on('click', '.post-btn', function () {
			var form = $(this).parent();
                        $(form).css('opacity','0.2');
                        $(form).parent().css('background','url(/images/spinner.gif) no-repeat center center');
                        
                        $(form).children('.alert').fadeOut();
			var errorList = $(form).children('.alert').children('.error-list');
			var errorLine = $($(errorList).children()[0]).clone();
			$(errorList).children().remove()
			$(errorList).append(errorLine);
                        
                        $(form).children('#Comment_rating').val($(form).children('#rating').children('.on').length);
			var commentArray = $(form).serializeArray();
			if($(form).attr('comment-id'))
			{
				commentArray.push({name: 'commentId', value: $(form).attr('comment-id') });
			}
			$.ajax({
				url: '/comment/ajaxCreate',
				data: commentArray,
				dataType: 'json',
				type: 'post',
				success: function(data){
                                $(form).css('opacity','1');
                                $(form).parent().css('background','none');
                        
					if(typeof(data) == 'object' && data.title == 'Success')
					{
						var newComment = $('#commentMainBlock').children(0).clone();
						var container = $(newComment).children('table').children('tbody').children('tr').children('.comment-container');
						$(newComment).attr('id', 'comment' + data.data['id']);
						
						if(data.data['rating'] > 0)
						{
							var commentRating = $('#commentRatingBlock').children(0).clone();
							var starsCount = data.data['rating'];
							for(var i = 0; i < 5; i++)
							{
								var commentStar = $('#commentStarBlock').children(0).clone();
								$(commentStar).children('span').text(i + 1);
								if(starsCount > 0)
								{
									$(commentStar).toggleClass('on');
									starsCount--;
								}
								$(commentRating).append($(commentStar));
							}
						}
						$(container).prepend($(commentRating));
						
						var commentTitle = $('#commentTitleBlock').children(0).clone();
						$(commentTitle).prepend(data.data['author_name']);
						$(commentTitle).children('.comment-post-date').text(data.data['post_date']);
						$(container).prepend($(commentTitle));
						
						$(container).children('.comment-content').children('span').text(data.data['content']);
						
						if(data.data['pros'])
						{
							var commentPros = $('#commentProsBlock').children(0).clone();
							$(commentPros).children('span').text(data.data['pros']);
							$(container).append($(commentPros));
						}
						
						if(data.data['cons'])
						{
							var commentCons = $('#commentConsBlock').children(0).clone();
							$(commentCons).children('span').text(data.data['cons']);
							$(container).append($(commentCons));
						}
						if(data.data['status'] !== 1)
							$(container).parent().parent().parent().parent().prepend($('#commentNotApprovedBlock').children(0).clone());
						
						var commentResponse = $('#commentResponseBlock').children(0).clone();
						$(commentResponse).children('a').attr('comment-id', data.data['id']);
						$(commentResponse).children('a').toggleClass('respone-link' + data.data['id']);
						$(container).append($(commentResponse));
						
						if($(form).attr('comment-id'))
						{
							$($('#comment #comment' + $(form).attr('comment-id') + ' ul')[1]).append($(newComment));
						}
						else
						{
							$(container).parent().append($('#commentUsefulBlock').html());
							$('#comment').children('.comments').append($(newComment));
							$(newComment).children('.comment').append('<hr/>')
						}
						$(newComment).slideDown();
						$('.comment-form-hide').click();
						$('.respone-link' + $(form).attr('comment-id')).click();
					}
					else if (typeof(data.message) == 'object')
					{
						for(var field in data.message)
						{
							var newLine = $(errorLine).clone();
							$(errorList).append($(newLine).text(data.message[field][0]));
							$(newLine).fadeIn();
						}
						$(form).children('.alert').fadeIn();
					}
				}
			});
         });
        ", CClientScript::POS_READY);
?>
<div>
<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'post_comment',
		'type'=>'vertical',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
		'htmlOptions' => array('style' => 'display:none;'),
	));
?> <hr>
	<h4>Оставить отзыв:</h4>
	<div class="alert alert-block alert-error" style="display:none;"><p>Необходимо исправить следующие ошибки:</p>
	<ul class="error-list">
		<li style="display:none;"></li>
	</ul>
	</div>
	<?php $isGuest = false; if(Yii::app()->user->id == null) $isGuest = true; ?>
    <div class="guest-author-name" style="display: <?php echo $isGuest ? 'block' : 'none'?>;">
		<?php
		$model->setAttributeLabel('author_name', $model->getAttributeLabel('author_name').', или <a class="ajax-login-btn" href="'.app()->getBaseUrl(true).'/site/login">авторизируйтесь</a>');
		echo $form->textFieldRow($model, 'author_name', array('value' => 'Гость'));
		?>
    </div>
    <div class="is-login-author-name" style="display: <?php echo $isGuest ? 'none' : 'block'?>;">
		<?php echo CHtml::label($model->getAttributeLabel('author_name'), 'Comment_author_name')?>
		<?php echo "<p>".($isGuest ? '' : Yii::app()->user->getState('name'))."</p>"; ?>
    </div>
	<?php $model->setAttributeLabel('email', $model->getAttributeLabel('email').'<span class="subtext"> (для уведомления об ответе)</span>');?>
	<?php echo $form->textFieldRow($model, 'email'); ?>
	<?php echo CHtml::hiddenField("Comment[{$type}_id]", $contentId)?>
	<?php echo CHtml::label($model->getAttributeLabel('rating'), 'Comment_rating')?>
	<?php echo CHtml::hiddenField('Comment[rating]', 0)?>
	<div id="rating"></div>
	<?php echo $form->textAreaRow($model, 'content'); ?>
	<?php
    if($plus_minus){
        echo $form->textAreaRow($model, 'pros'); 
        echo $form->textAreaRow($model, 'cons'); 
        }
    ?>
	
	<?php echo CHtml::button('Добавить отзыв', array('class' => 'post-btn')); ?>
	
<br>
<?php $this->endWidget(); ?>
</div>
<div class="comment-form-show-link">
	<a style="font-size: 20px; cursor: pointer;" class="comment-form-show">Оставить отзыв</a>
  <a style="font-size: 20px; cursor: pointer; float:right" class="comment-show">Смотреть все отзывы</a>
</div>
<div id="down_hr" style="display:none" class="up-to">
            <a href="#" class="up">Вверх</a>
            <a href="#"><img src="/images/upArrow.png"></a>
        </div>