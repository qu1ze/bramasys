<?php

class m130729_220448_create_support_file_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `support_file` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`name` varchar(50) NOT NULL,
				`description` varchar(255) NOT NULL,
				`category_id` int(11) NOT NULL,
				`for_category_id` int(11) NOT NULL,
				`brand_id` int(11) NOT NULL,
				`type` tinyint(1) NOT NULL,
				`path` varchar(255) NOT NULL,
				PRIMARY KEY (`id`),
				CONSTRAINT `FK_support_file_for_category` FOREIGN KEY (`for_category_id`)
						  REFERENCES `category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
				CONSTRAINT `FK_support_file_category` FOREIGN KEY (`category_id`)
						  REFERENCES `category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
				CONSTRAINT `FK_support_file_brand` FOREIGN KEY (`brand_id`)
						  REFERENCES `brand` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->dropTable('support_file');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}