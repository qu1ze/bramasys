<?php
cs()->registerCoreScript('yiiactiveform');

$grid_id = 'staticpage-table';
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $grid_id,
	'type' => 'bordered striped',
	'dataProvider' => $model->search(),
	'filter' => $model,	
	'afterAjaxUpdate' => 'js:function(){
		jQuery(".tooltip").remove();
		jQuery("a[rel=tooltip]").tooltip();				
	}',
	'template' => "{items}\n{pager}",
	'columns' => array(
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'name',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/staticpage/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'content',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/staticpage/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			),
			'htmlOptions' => array('style' => '
				overflow: hidden;
				max-width: 150px;
				width: 150px;
				text-overflow: ellipsis;
				white-space: nowrap;
			'),
		),
		array(
			'class' => 'bootstrap.widgets.TbEditableColumn',			
			'name' => 'category_id',
			'filter' => $categories,
			'value' => '$data->category->title',
			'editable' => array(
				'type' => 'select',
				'source' => $categories,
				'url' => app()->createUrl('/admin/staticpage/ajaxUpdate'),				
				'success' => 'js:function(data){
					if(typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if(typeof data == "object" && data.success) {
				    	brama.ui.updateViews("g:'.$grid_id.'");
					}
				}'
			)
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('nowrap' => 'nowrap'),			
			'template' => '{delete} {update}',	
			'updateButtonUrl'=>'Yii::app()->controller->createUrl("update", array("id" => $data->primaryKey))',
			'deleteButtonUrl'=>'Yii::app()->controller->createUrl("ajaxDelete", array("id" => $data->primaryKey))',
			'deleteConfirmation' => 'Вы уверенны что хотите удалить?',
			'afterDelete' => 'function(link,success,data){
				if(success){data = $.parseJSON(data);brama.ui.notify(data.title, data.msg, false);}
				else{brama.ui.notify("Oops!", data.responseText )};
			}',
		)
	)
));

