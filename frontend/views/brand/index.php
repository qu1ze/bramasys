<div id="container">
<?php
$this->breadcrumbs = CMap::mergeArray(
    app()->categoryController->getCategoryBcs($this->categoryName, true),
    array(
        $model->name,
    ));

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
?>
	<div class="partners">
		<h1><?php echo $model->name; ?></h1>
		<div class="content_partners">
			<div class="partners-group" style="display: inline-block;">
				<span class="imgUrl" style="float:left; margin: 10px;"><img width="150" src="<?php echo $model->img_url; ?>"></span>
				<p style="font: 16px 'PTSN Regular'"><?php echo $model->full_description; ?></p>
			</div>
			 <div style="float:left;"> <?php $this->widget('common.components.SocButtons'); ?></div>
		
                        <?php if (!empty($model->url)): ?> 
                           <a style="float: right;" href="<?php echo (substr($model->url,0,4)=='http'?'':'http://').$model->url; ?>"><?php echo $model->url; ?></a>
			<?php endif; ?>
                         <div class="arrow-up" style="clear: both;">
				<a href="#">Вверх<img src="/images/arrowup.jpg"></a>
			</div>
		</div>
	</div>
</div>