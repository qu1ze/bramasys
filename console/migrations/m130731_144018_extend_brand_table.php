<?php

class m130731_144018_extend_brand_table extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE `brand`
			ADD `short_description` VARCHAR(255) DEFAULT NULL,
			ADD `full_description` TEXT DEFAULT NULL,
			ADD `image_url` VARCHAR(255) DEFAULT NULL,
			ADD `url` VARCHAR(255) DEFAULT NULL,
			ADD `partner` TINYINT(1) DEFAULT 0;
			");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `brand`
			DROP `short_description`,
			DROP `full_description`,
			DROP `image_url`,
			DROP `url`,
			DROP `partner`;
			");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}