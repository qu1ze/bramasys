<?php
/**
 * create.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 11/16/12
 * Time: 5:28 PM
 */
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'ajax-brand-form',
	'type' => 'vertical',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=> true,
		'afterValidate'=>'js:function(f,d,e){
			if(!e)
			{
				var url = "'.$this->createUrl('/admin/brand/ajaxCreate').'";
		        $.ajax({
		            url: url,
		            data: f.serialize(),
		            dataType: "json",
		            type: "post",
		            success: function(d){						
		                if(typeof(d) == "object"){
							brama.ui.notify(d.title, d.msg, false);
						}
		                $.colorbox.close();
		                $("#brand-table").yiiGridView("update");					  						
		            }
		        });
			}
			return false;
		}'
	)
)); ?>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->textFieldRow($model, 'name');?>
<?php echo $form->textAreaRow($model , 'full_description', array('class' => 'full_desc_field'));?>
<?php echo $form->textAreaRow($model , 'short_description', array('class' => 'desc_field'));?>
<?php echo $form->textFieldRow($model, 'image_url');?>
<?php echo $form->textFieldRow($model, 'url');?>
<?php echo $form->checkBoxRow($model, 'partner'); ?>

<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Создать')); ?>

<?php $this->endWidget(); ?>
