<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
    ?>
<div id="ordering">
    <div class="header-text">
        <h1 class="h3">Оформление заказа</h1>
       </div>
    
<div class="confirm-order">
	<?php if($order->payment_type_id == 1) $payment=2;
else $payment = $order->payment_type_id;
echo $this->renderPartial('confirmPayment'.$payment, array(
							'order' => $order,
							));

?>
  </div>

    </div>