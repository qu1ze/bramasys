<?php
/**
 * An example of extending the provider class.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

//require_once dirname(dirname(__FILE__)) . '/services/GoogleOpenIDService.php';

class CustomGoogleService extends GoogleOAuthService {

	//protected $jsArguments = array('popup' => array('width' => 450, 'height' => 450));

	protected $scope = 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me';
	

	/**
	 * http://developers.facebook.com/docs/reference/api/user/
	 *
	 * @see FacebookOAuthService::fetchAttributes()
	 */
	protected function fetchAttributes() {
		$info = (array)$this->makeSignedRequest('https://www.googleapis.com/oauth2/v2/userinfo');

		$this->attributes['id'] = $info['id'];
		$this->attributes['name'] = $info['name'];

		if (!empty($info['link'])) {
			$this->attributes['url'] = $info['link'];
		}
        if (!empty($info['email'])) {
			$this->attributes['email'] = $info['email'];
		}
        if (!empty($info['given_name'])) {
			$this->attributes['first_name'] = $info['given_name'];
		}
        if (!empty($info['family_name'])) {
			$this->attributes['last_name'] = $info['family_name'];
		}
        
        if (!empty($info['birthday'])) {
			$this->attributes['birthday'] = date("d.m.Y",strtotime($info['birthday']));
		}
        
        
        $this->attributes['all'] = $info;

		/*if (!empty($info['gender']))
			$this->attributes['gender'] = $info['gender'] == 'male' ? 'M' : 'F';
		
		if (!empty($info['picture']))
			$this->attributes['photo'] = $info['picture'];
		
		$info['given_name']; // first name
		$info['family_name']; // last name
		$info['birthday']; // format: 0000-00-00
		$info['locale']; // format: en*/
	}

	
}