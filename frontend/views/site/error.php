<?php
/**
 * error.php
 *
 * General view file to display error messages
 * Change to suit your needs.
 *
 * @see errHandler at the main.php configuration file
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 8:27 PM
 */
Yii::app()->getClientScript()->registerCssFile('/css/error.css');

Yii::app()->clientScript->registerScript("focusToSearch","
	$('#search-link').click(function (){
		$('#search-input').focus();
	});
", CClientScript::POS_READY);

$categories = app()->categoryController->getChilds(ProductsController::getTag());
//$this->pageTitle .= ' - Error';
?>

<div id="container" style="margin-left: 210px;">
<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
?>
	<p class="red">Ошибка 404</p>
	<p class="bold">Страница не найдена</p>
	<p>Воспользуйтесь <a id="search-link" href="#">строкой поиска</a> вверху страницы.</p><br>
	<p>То, что вы ищете, возможно, находится в нашем каталоге</p>
	<div class="category-link">
		<?php foreach ($categories as $category): ?>
		<a href="<?php echo app()->categoryController->getCategoryAddress($category['data']['name']);?>"><?php echo $category['data']['title'];?></a>
		<?php endforeach; ?>
	</div>
	<img style="margin-left: 200px;" src="/images/404.jpg" alt="Ошибка 404">
</div>

<div class="error_page" style="display: none;">
	<div><?php echo CHtml::encode($message)." | in file $file | line is $line"; ?></div>

	<small>(Error <?php echo $code ?>)</small>
</div>