<?php

class m130516_205144_create_slides_table extends CDbMigration
{
	public function up()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `slides` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `url` varchar(255) NOT NULL,
                  `image_url` varchar(255) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
	}

	public function down()
	{
            $this->dropTable("slides");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}