<?php

class m130710_101427_add_unique_to_tables extends CDbMigration
{
	public function up()
	{
		$this->execute("
                ALTER TABLE  `category`
					DROP INDEX  `id`,
					ADD UNIQUE  `name` (`name`);
				ALTER TABLE  `brand` ADD UNIQUE  `name` (`name`);
				ALTER TABLE  `category_metadata` ADD UNIQUE  `url` (`url`);
				ALTER TABLE  `category_spec` ADD UNIQUE  `category_spec_unique` (`spec_id`, `category_id`);
				ALTER TABLE  `category_tag` ADD UNIQUE  `name` (`name`);
				ALTER TABLE  `complect_product` ADD UNIQUE `complect_product_unique` (`complect_id`, `product_id`);
				ALTER TABLE  `products` DROP INDEX `brand`, ADD INDEX `brand` (`brand`);
				ALTER TABLE  `products` ADD UNIQUE  `brand_model_unique` (`brand`, `model`);
				ALTER TABLE  `product_spec_value` ADD UNIQUE  `product_spec_unique` (`product_id`, `spec_id`);
				ALTER TABLE  `specification` ADD UNIQUE  `name` (`name`);
            ");
	}

	public function down()
	{
		$this->execute("
                ALTER TABLE  `category`
					DROP INDEX  `name`,
					ADD UNIQUE  `id` (`id`);
				ALTER TABLE  `brand` DROP INDEX  `name`;
				ALTER TABLE  `category_metadata` DROP INDEX  `url`;
				ALTER TABLE  `category_spec` DROP INDEX  `category_spec_unique`;
				ALTER TABLE  `category_tag` DROP INDEX  `name`;
				ALTER TABLE  `complect_product` DROP INDEX `complect_product_unique`;
				ALTER TABLE  `products` DROP INDEX `brand`, ADD INDEX `brand` (`brand`, `category`);
				ALTER TABLE  `products` DROP INDEX `brand_model_unique`;
				ALTER TABLE  `product_spec_value` DROP INDEX  `product_spec_unique`;
				ALTER TABLE  `specification` DROP INDEX  `name`;
            ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}