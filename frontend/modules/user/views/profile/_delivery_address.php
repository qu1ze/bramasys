<?php
/* @var $deliveryAddress DeliveryAddress */
$key = isset($key) && $deliveryAddress->id != null ? $key : 'new';
?>

<div class="control-group" address-id="<?php echo $key === 'new' ? 'new' : $deliveryAddress->id; ?>" style="margin-bottom: 35px; <?php echo $deliveryAddress->id == null && empty($deliveryAddress->town) && empty($deliveryAddress->street) && empty($deliveryAddress->house) && empty($deliveryAddress->flat) ? 'display: none;' : ''; ?>">
	<label class="control-label"></label>
	<div class="controls">
		<div class="fieldWithSubLabel">
			<input type="text" name="DeliveryAddresses<?php echo "[$key]"; ?>[town]<?php echo $key === 'new' ? '[]' : ''; ?>" id="city" value="<?php echo $deliveryAddress->town; ?>">
			<label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('town'); ?></label>
			<?php echo CHtml::error($deliveryAddress, 'town'); ?>
		</div>
		<div class="fieldWithSubLabel sub">
			<input type="text" style="width:150px;" name="DeliveryAddresses<?php echo "[$key]"; ?>[street]<?php echo $key === 'new' ? '[]' : ''; ?>" id="street" value="<?php echo $deliveryAddress->street; ?>">
			<label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('street'); ?></label>
			<?php echo CHtml::error($deliveryAddress, 'street'); ?>
		</div>
		<div class="fieldWithSubLabel sub">
			<input type="text" style="width:52px;" name="DeliveryAddresses<?php echo "[$key]"; ?>[house]<?php echo $key === 'new' ? '[]' : ''; ?>" id="dom" value="<?php echo $deliveryAddress->house; ?>">
			<label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('house'); ?></label>
			<?php echo CHtml::error($deliveryAddress, 'house'); ?>
		</div>
		<div class="fieldWithSubLabel sub">
			<input type="text" style="width:82px;" name="DeliveryAddresses<?php echo "[$key]"; ?>[flat]<?php echo $key === 'new' ? '[]' : ''; ?>" id="flat" value="<?php echo $deliveryAddress->flat; ?>">
			<label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('flat'); ?></label>
			<?php echo CHtml::error($deliveryAddress, 'flat'); ?>
		</div>
		<div class="fieldWithSubLabel sub" style="width: 100%;">
			<input type="text" name="DeliveryAddresses<?php echo "[$key]"; ?>[additional_info]<?php echo $key === 'new' ? '[]' : ''; ?>" id="city" value="<?php echo $deliveryAddress->additional_info; ?>">
			<label class="subLabelNoLeft"><?php echo $deliveryAddress->getAttributeLabel('additional_info'); ?></label>
		</div>
	</div>
	<?php if($count > 1 || $key === 'new'): ?>
		<img class="drop" style="padding-top:30px; width: 10px;" src="/images/cross.gif"/>
	<?php endif; ?>
</div>