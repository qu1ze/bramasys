<?php
/* @var $robotsModel Robots */

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'horizontal',
));?>

	<?php echo $form->textAreaRow($robotsModel, 'content', array('style'=>'width:auto', 'rows'=>20, 'cols'=>60)); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
<?php $this->endWidget();?>