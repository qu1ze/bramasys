<?php

class m130521_141525_create_client_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
                CREATE TABLE IF NOT EXISTS `client` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`user_id` int(11) NOT NULL,
					`name` varchar(50) NOT NULL,
					`lastname` varchar(50) NOT NULL,
					`address` varchar(255) NOT NULL,
					`delivery_address` varchar(255) DEFAULT NULL,
					`phone` varchar(12) NOT NULL,
					`image` varchar(255) DEFAULT NULL,
					PRIMARY KEY (`id`),
					UNIQUE KEY `user_id` (`user_id`)
				  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
		$this->execute("
				ALTER TABLE `client`
					ADD CONSTRAINT `client_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
			");
	}

	public function down()
	{
		$this->dropTable("client");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}