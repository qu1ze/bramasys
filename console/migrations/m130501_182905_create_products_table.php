<?php

class m130501_182905_create_products_table extends CDbMigration
{
	public function up()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `products` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `brand` int(11) NOT NULL,
                  `model` varchar(255) NOT NULL,
                  `description` text NOT NULL,
                  `price` float NOT NULL,
                  `category` int(11) NOT NULL,
                  PRIMARY KEY (`id`),
                  KEY `brand` (`brand`,`category`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");	
	}

	public function down()
	{
            $this->dropTable("products");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}