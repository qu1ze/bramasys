<?php

class m130930_195033_add_img_to_complect extends CDbMigration
{
    public function up()
    {
        $this->execute("
			ALTER TABLE `complect`
				ADD `img_url` varchar(255) DEFAULT NULL;
				");
    }

    public function down()
    {
        $this->execute("
			ALTER TABLE `complect`
				DROP `img_url`;
				");
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}