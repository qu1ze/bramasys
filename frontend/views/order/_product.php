<?php
	$amount = $prod['price'] * $prod['count'];
?>
<div class="order-price product_<?php echo $withPrice ? $id : '0'?>">
<?php if($withPrice): ?>
	<div class="order-price_img">
		<a href="<?php echo $product->link; ?>"><img style="width: 72px;" src="<?php echo $product->image_url;?>"></a>
	</div>
	<div class="order-price_link">
		<a href="<?php echo $product->link; ?>"><?php echo $product->name; ?></a>
	</div>
	<div class="order-price_text product-price-cart">
		<p><?php echo CurrencySys::exchange($prod['price']).' '.CurrencySys::getLabel(); ?></p>
		<p class="cart-prod-count">x<?php echo $prod['count'] ?> шт.</p>
		<p id="firstPrice"><?php echo CurrencySys::exchange($amount).' '.CurrencySys::getLabel(); ?></p>
	</div>
<?php else: ?>
	<div class="order-price_text product-price-cart" style="display: table-cell; vertical-align: middle; width: 48px; padding-right: 10px">
		<p><?php echo $prod['count'] ?> x </p>
	</div>
	<div class="order-price_img">
		<a href="<?php echo $product->link; ?>"><img style="width: 72px;" src="<?php echo $product->image_url;?>"></a>
	</div>
	<div class="order-price_link" style="display: table-cell; vertical-align: middle;">
		<a href="<?php echo $product->link; ?>"><?php echo $product->name; ?></a>
	</div>
<?php endif; ?>
</div>