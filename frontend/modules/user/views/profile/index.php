<?php
/* @var $this DefaultController */
/* @var $client Client */

Yii::app()->getClientScript()->registerCssFile('/css/profile.css');
$this->breadcrumbs=array(
	'Личный профиль',
);
?>
<?php $this->renderPartial('_side'); ?>
<div id="container">
	<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
	?>
	<h3>Личный профиль</h3>
	<div class="profile-content">
		<div class="label-group">
			<?php echo CHtml::label($client->getAttributeLabel('name'), '', array('class' => 'profile-label'));?>
			<div class="profile-value">
				 <?php echo $client->name == $client->email ? '-' : $client->name; ?>
			</div>
		</div>
		<div class="label-group">
			<?php echo CHtml::label($client->getAttributeLabel('lastname'), '', array('class' => 'profile-label'));?>
			<div class="profile-value">
				 <?php echo $client->lastname == null ? '-' : $client->lastname; ?>
			</div>
		</div>
		<div class="label-group">
			<?php echo CHtml::label($client->getAttributeLabel('middle_name'), '', array('class' => 'profile-label'));?>
			<div class="profile-value">
				 <?php echo $client->middle_name == null ? '-' : $client->middle_name; ?>
			</div>
		</div>
		<div class="label-group">
			<?php echo CHtml::label($client->getAttributeLabel('date_of_birth'), '', array('class' => 'profile-label'));?>
			<div class="profile-value">
				 <?php echo $client->date_of_birth == null ? '-' : $client->date_of_birth; ?>
			</div>
		</div>
		<div class="label-group">
			<?php echo CHtml::label(User::model()->getAttributeLabel('email'), '', array('class' => 'profile-label'));?>
			<div class="profile-value">
				 <?php echo $client->email == null ? '-' : $client->email; ?>
			</div>
		</div>
		<div class="label-group">
			<?php echo CHtml::label($client->getAttributeLabel('phone'), '', array('class' => 'profile-label'));?>
			<div class="profile-value">
                <?php if($client->phone == null):
                    echo '-';
                else:
                    foreach ($client->phones as $phone): ?>
                        <?php echo $phone; ?><br>
                    <?php endforeach;
                endif; ?>
			</div>
		</div>
		<div class="label-group">
			<?php echo CHtml::label($client->getAttributeLabel('delivery_address'), '', array('class' => 'profile-label'));?>
			<div class="profile-value">
                <?php if($client->delivery_address == null):
                    echo '-';
                else:
                    foreach ($client->deliveryAddresses as $delivery_address): ?>
                        <?php echo $delivery_address; ?><br>
                    <?php endforeach;
                endif; ?>
			</div>
		</div>
	</div>
	
	<a class="hover_color" href="<?php echo app()->getBaseUrl(true); ?>/user/profile/edit" style="padding-top: 30px; display: inline-block;">Редактировать личный профиль</a>
	
	<h3 style="padding-top: 20px;">Пароль</h3>
	<a class="hover_color" href="<?php echo app()->getBaseUrl(true); ?>/user/profile/changepassword">Изменить пароль</a>
    <?php
    if (empty($client->identity)): ?>
    <h3><div style="text-align: left;margin-bottom: 5px;">Подключить</div></h3>
                <?php  $this->widget('commonExt.eauth.EAuthWidget', array('action' => '/site/login'));//Yii::app()->eauth->renderWidget(); ?>
         <?php endif; ?>   
</div>
