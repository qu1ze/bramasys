<?php
/**
 * EAuthUserIdentity class file.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

/**
 * EAuthUserIdentity is a base User Identity class to authenticate with EAuth.
 *
 * @package application.extensions.eauth
 */
class EAuthUserIdentity extends CBaseUserIdentity {

	const ERROR_NOT_AUTHENTICATED = 3;

	/**
	 * @var EAuthServiceBase the authorization service instance.
	 */
	protected $service;

	/**
	 * @var string the unique identifier for the identity.
	 */
	protected $id;

	/**
	 * @var string the display name for the identity.
	 */
	protected $name;

	/**
	 * Constructor.
	 *
	 * @param EAuthServiceBase $service the authorization service instance.
	 */
	public function __construct($service) {
		$this->service = $service;
	}

	/**
	 * Authenticates a user based on {@link service}.
	 * This method is required by {@link IUserIdentity}.
	 *
	 * @return boolean whether authentication succeeds.
	 */
	
    public function authenticate() {
		if ($this->service->isAuthenticated) {
			$this->name = $this->service->getAttribute('name');
            $this->setState('id', $this->service->id);
            $this->setState('identity', $this->service->id);
            $this->setState('name', $this->name);
            $this->setState('service', $this->service->serviceName);
            
            $attributes = $this->service->getAttributes();
			$this->setState('service_stat', $attributes);
            
            $this->id = $this->service->id;
			
            if(isset($this->service->email)) $this->setState('email', $this->service->email);
            //$this->setState('service_stat', $this->service);
            if(!empty(Yii::app()->user->id) && 1<Client::model()->countByAttributes(array('identity'=>$this->service->id, 'service'=>$this->service->serviceName)))
            {
                $client=Client::model()->findByPk(Yii::app()->user->id);
                $client->service=$this->service->serviceName;
                $client->identity=$this->service->id;
                $client->save();
            }
            if($client=Client::model()->findByAttributes(array('identity'=>$this->service->id, 'service'=>$this->service->serviceName)))
                {
                    $client->regenerateValidationKey();
                    $this->setState('id', $client->id);
                    $this->id = $client->id;
                    $this->setState('id', $this->id);
                    Yii::app()->user->setReturnUrl('/');
                    if(!$client->name && !$client->lastname)
                    {
                        $this->setState('name', $client->email);
                        $this->setState('lastName', '');
                    }
                    else
                    {
                        $this->setState('name', $client->name);
                        $this->setState('lastName', $client->lastname);
                    }
                    $this->setState('isAdmin', false);
                    $this->setState('vkey', $client->validation_key);
                }
			
			// You can save all given attributes in session.
			//$attributes = $this->service->getAttributes();
			//$session = Yii::app()->session;
			//$session['eauth_attributes'][$this->service->serviceName] = $attributes;

			$this->errorCode = self::ERROR_NONE;
		}
		else {
			$this->errorCode = self::ERROR_NOT_AUTHENTICATED;
		}
		return !$this->errorCode;
	}

	/**
	 * Returns the unique identifier for the identity.
	 * This method is required by {@link IUserIdentity}.
	 *
	 * @return string the unique identifier for the identity.
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Returns the display name for the identity.
	 * This method is required by {@link IUserIdentity}.
	 *
	 * @return string the display name for the identity.
	 */
	public function getName() {
		return $this->name;
	}
}
