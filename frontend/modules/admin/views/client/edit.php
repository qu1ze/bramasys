<?php
Yii::app()->clientScript->registerScript("removePhone","
    $('body').on('click', '#phones .drop', function () {
        var phone = $(this).parent();
        var id = $(phone).attr('phone-id');
        var count = $('#phones ' + '.control-group').not('[phone-id=\"new\"]').length;
        if(count < 3 && id !== 'new')
        {
            $('#phones .control-group').not('[phone-id=\"new\"]').children('.drop').remove();
        }
        if(count > 1 || id === 'new')
        {
            $(phone).fadeOut('slow', function()
            {
            $(phone).remove();
            });
        }
    });
", CClientScript::POS_END);

Yii::app()->clientScript->registerScript("addPhone","
$('#add-phone').on('click', function () {
    var phone_form = $('#phone-form').children('div').clone();
    $('#horizontalForm #phones').append($(phone_form));
    $(phone_form).slideDown();
});
", CClientScript::POS_END);

	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'options'=>array(
                        'title'=>'Заменить описание?',
                        'modal'=>true,
                        'autoOpen'=>false,
                        'buttons'=>array(
                            'Yes'=>'js:function(){$(".desc_field").val($(".desc").val()); $(this).dialog("close")}',
                            'No'=>'js:function(){$(this).dialog("close")}'
                            )
                    ),
                    'id'=>'confirmReplace',
                    
            )
        );

        echo CHtml::textArea('desc', '', array('class' => 'desc'));

        $this->endWidget('zii.widgets.jui.CJuiDialog');
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'name'); ?>
	<?php echo $form->textFieldRow($model, 'lastname'); ?>
	<?php echo $form->textFieldRow($model , 'middle_name');?>
	<?php echo $form->textFieldRow($model, 'date_of_birth');?>
    <div id="phones">
        <font style="font: 15px/25px 'PTSN Bold';">Номер телефона</font>
        <?php
        $count = 0;
        foreach ($phones as $key => $phone):?>
            <?php $this->renderPartial('_phone', array(
                'phone' => $phone,
                'key' => $key,
                'count' => $this->countOld($phones),
            )); ?>
            <?php
            $count++;
        endforeach;?>
    </div>

    <div id="phone-form" class="hide">
        <?php $this->renderPartial('_phone', array(
            'phone' => $newPhone,
            'count' => $this->countOld($phones),
        )); ?>
    </div>
    <a id="add-phone">Добавить номер</a>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	
<?php $this->endWidget(); ?>