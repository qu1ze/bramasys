<?php

/**
 * This is the model class for table "password_request".
 *
 * The followings are the available columns in table 'password_request':
 * @property integer $id
 * @property integer $client_id
 * @property string $key
 *
 * The followings are the available model relations:
 * @property Client $client
 */
class PasswordRequest extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PasswordRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'password_request';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, key', 'required'),
			array('client_id', 'numerical', 'integerOnly'=>true),
			array('key', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, client_id, key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'key' => 'Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('key',$this->key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function sendMail($email, $link, $name, $lastname, $middleName)
    {
        $mails = MailTemplate::model()->findAllByAttributes(array('name' => 'newpassword'));
        foreach($mails as $mail)
        {
            $mail->send(array(
                '$name' => $name,
                '$lastname' => $lastname,
                '$middleName' => $middleName,
                '$email' => $email,
                '$link' => $link,
            ));
        }
        return true;
    }

    protected function beforeValidate()
    {
        if($this->client !== null)
        {
            if(!$this->key)
                $this->key = md5($this->client->email + time());
        }
        return parent::beforeValidate();
    }

    protected function beforeSave()
    {
        $client = $this->client;
        if($client !== null)
        {
            $this->sendMail(
                $client->email,
                '<a href="'.app()->getBaseUrl(true).'/site/newPassword/'.$this->key.'">ссылка</a>',
                $client->name,
                $client->lastname,
                $client->middle_name
            );
        }
        return parent::beforeSave();
    }
}