<?php

class CommentController extends Controller
{
	public function actionAjaxCreate()
	{
		if(!empty($_POST['Comment'])&& Yii::app()->request->isAjaxRequest)
		{
			$status = 0;
			if(Yii::app()->user->id == null || Yii::app()->user->getState('isAdmin'))
			{
				if(Yii::app()->user->getState('isAdmin'))
				{
					$_POST['Comment']['author_name'] = Yii::app()->user->getState('name');
					$status = 1;
				}
				$comment = new Comment("fromGuest");
			}
			else
			{
				$comment = new Comment();
				$comment->author_id = Yii::app()->user->id;
			}
			if(isset($_POST['commentId']))
			{
				$comment->parent_id = $_POST['commentId'];
			}
			$comment->attributes = $_POST['Comment'];
			$comment->status = $status;
			$comment->post_date = time();
			if($comment->save())
			{
				echo json_encode(array('title' => 'Success', 'data' => array(
					'id' => $comment->id,
					'author_name' => $comment->author_name,
					'rating' => $comment->rating,
					'post_date' => $comment->post_date,
					'content' => $comment->content,
					'pros' => $comment->pros,
					'cons' => $comment->cons,
					'status' => $status,
				)));
			}
			else
			{
				echo json_encode(array('title' => 'error', 'message' => $comment->errors));
			}
			app()->end();
		}
		echo json_encode(array('title' => 'error', 'message' => 'Comment array is not set or empty'));
		app()->end();
	}
	
	public function actionChangeUsefulCount()
	{
		if(($comment = Comment::model()->findByPk($_POST['commentId'])) !== null)
		{
			$criteria = new CDbCriteria();
			$criteria->addCondition("comment_id = $comment->id");
			$criteria->addCondition("ip_address = '".Yii::app()->request->userHostAddress."'");
			if(Yii::app()->user->id !== null && !Yii::app()->user->getState('isAdmin'))
			{
				$criteria->addCondition("client_id = ".Yii::app()->user->id);
			}
			if(CommentVote::model()->find($criteria) === null)
			{
				$vote = new CommentVote();
				$vote->comment_id = $comment->id;
				$vote->ip_address = Yii::app()->request->userHostAddress;
				if(Yii::app()->user->id !== null && !Yii::app()->user->getState('isAdmin'))
				{
					$vote->client_id = Yii::app()->user->id;
				}
				$vote->save();
				$comment->useful_count += 1;
				$comment->save();
				echo json_encode(array('title' => 'Success', 'count' => $comment->useful_count));
				app()->end();
			}
		}
		echo json_encode(array('title' => 'Error'));
		app()->end();
	}
}