<?php

/**
 * This is the model class for table "order_product".
 *
 * The followings are the available columns in table 'order_product':
 * @property integer $id
 * @property integer $product_id
 * @property boolean $is_complect
 * @property integer $count
 * @property double $price
 * @property boolean $mounting
 * @property integer $order_id
 *
 * The followings are the available model relations:
 * @property Order $order
 * @property Products $product
 */
class OrderProduct extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OrderProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, count, price, order_id', 'required'),
			array('product_id, count, order_id', 'numerical', 'integerOnly'=>true),
			array('is_complect', 'boolean'),
			array('price', 'numerical'),
			array('mounting', 'boolean'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, product_id, count, price, mounting, order_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
			'complect' => array(self::BELONGS_TO, 'Complect', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Продукт',
			'is_complect' => 'В комплекте',
			'count' => 'Количество',
			'price' => 'Цена',
			'mounting' => 'Монтаж',
			'order_id' => 'Заказ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('is_complect',$this->is_complect);
		$criteria->compare('count',$this->count);
		$criteria->compare('price',$this->price);
		$criteria->compare('mounting',$this->mounting);
		$criteria->compare('order_id',$this->order_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}