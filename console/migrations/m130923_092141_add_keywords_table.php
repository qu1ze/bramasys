<?php

class m130923_092141_add_keywords_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
		CREATE TABLE IF NOT EXISTS `keyword` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `keyword` varchar(255) NOT NULL,
		  `object_id` int(11) NOT NULL,
		  `object_type` int(11) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->execute("
			DROP TABLE IF EXISTS `keyword`;
		");
	}
}