<?php

class ProfileController extends Controller
{
	public function actionIndex($id = null)
	{
		$client = Client::model()->findByPk($id);
		$this->render('index', array(
			'client' => $client,
		));
	}
	
	public function actionMyOrders($id = null)
	{
		$this->render('my_orders', array(
			'orders' => Order::model()->with('carts')->findAll('`carts`.`client_id` = '.$id),
		));
	}
	
	public function actionMyCart($id = null)
	{
		$this->render('my_cart', array(
			'cart' => isset(Yii::app()->session['cart']) ? Yii::app()->session['cart'] : array(),
		));
	}
	
	public function actionBookmarks($id = null)
	{
		$this->render('bookmarks');
	}
	
	public function actionChangePassword($id = null)
	{
		$client = Client::model()->findByPk($id);
		if(isset($_POST['Client']))
		{
			$client = Client::model()->findByPk($id);
			if($client->verifyPassword($_POST['Client']['oldPassword']))
			{
				$client->newPassword = $_POST['Client']['newPassword'];
				$client->passwordConfirm = $_POST['Client']['passwordConfirm'];
				if($client->validate())
				{
					$client->attributes = array('password' => $_POST['Client']['newPassword']);
					$client->save();
				}
			}
			else
			{
				$client->addError('password', 'Старый пароль не верный');
			}
		}
		$this->render('change_password', array(
			'client' => $client,
		));
	}

    public function countOld(array $array)
    {
        $count = 0;
        foreach($array as $model)
        {
            $count += $model->id == null ? 0 : 1;
        }
        return $count;
    }

	public function actionEdit($id = null)
	{		
		$client = Client::model()->findByPk($id);
		$deliveryAddresses = DeliveryAddress::model()->findAllByAttributes(array('client_id' => $id));
		$phones = Phone::model()->findAllByAttributes(array('client_id' => $id));
        $hasSubErrors = false;
		if(isset($_POST['Client']))
		{
			if(isset($_POST['DeliveryAddresses']))
			{
				$oldDeliveryAddresses = $deliveryAddresses; //old addresses, all value, that not unseted, will be deleted
				$deliveryAddressesValues = $_POST['DeliveryAddresses']; //new values for old addresses
				$newDeliveryAddresses = isset($_POST['DeliveryAddresses']['new']) ? $_POST['DeliveryAddresses']['new'] : array(); //new addresses, that must be inserted into db, and $deliveryAddresses
				$deliveryAddresses = array();
				foreach ($deliveryAddressesValues as $key => $deliveryAddress)
				{
					if($key !== 'new')
					{
						if(isset($oldDeliveryAddresses[$key]))
						{
							$deliveryAddresses[$key] = $oldDeliveryAddresses[$key];
							unset($oldDeliveryAddresses[$key]);
							$deliveryAddresses[$key]->attributes = $deliveryAddress;
							$deliveryAddresses[$key]->save();
                            $hasSubErrors = $deliveryAddresses[$key]->hasErrors();
						}
					}
				}
				foreach ($oldDeliveryAddresses as $oldDeliveryAddress)
				{
					$oldDeliveryAddress->delete();
				}
				for ($i = 0; $i < sizeof(end($newDeliveryAddresses)); $i++)
				{
					$newDeliveryAddress = new DeliveryAddress();
					$newDeliveryAddress->client_id = $id;
					$newDeliveryAddress->town = $newDeliveryAddresses['town'][$i];
					$newDeliveryAddress->street = $newDeliveryAddresses['street'][$i];
					$newDeliveryAddress->house = $newDeliveryAddresses['house'][$i];
					$newDeliveryAddress->flat = $newDeliveryAddresses['flat'][$i];
					$newDeliveryAddress->additional_info = $newDeliveryAddresses['additional_info'][$i];
					$newDeliveryAddress->save();
                    if(!empty($newDeliveryAddresses['town'][$i]) || !empty($newDeliveryAddresses['street'][$i]) || !empty($newDeliveryAddresses['house'][$i]) || !empty($newDeliveryAddresses['flat'][$i]))
                        $hasSubErrors = $newDeliveryAddress->hasErrors();
					array_push($deliveryAddresses, $newDeliveryAddress);
				}
			}
			if(isset($_POST['Phones']))
			{
				$oldPhones = $phones; //old phones, all value, that not unseted, will be deleted
				$phonesValues = $_POST['Phones']; //new values for old phones
				$newPhones = isset($_POST['Phones']['new']) ? $_POST['Phones']['new'] : array(); //new phones, that must be inserted into db, and $phones
				$phones = array();
				
				foreach ($phonesValues as $key => $phone)
				{
					if($key !== 'new')
					{
						if(isset($oldPhones[$key]))
						{
							$phones[$key] = $oldPhones[$key];
							unset($oldPhones[$key]);
							$phones[$key]->attributes = $phone;
							$phones[$key]->save();
                            $hasSubErrors = $phones[$key]->hasErrors();
						}
					}
				}
				foreach ($oldPhones as $oldPhone)
				{
					$oldPhone->delete();
				}
				for ($i = 0; $i < sizeof(end($newPhones)); $i++)
				{
					$newPhone = new Phone();
					$newPhone->client_id = $id;
					$newPhone->type = $newPhones['type'][$i];
					$newPhone->value = $newPhones['value'][$i];
					$newPhone->save();
                    if(!empty($newPhones['value'][$i]))
                        $hasSubErrors = $newPhone->hasErrors();
					array_push($phones, $newPhone);
				}
			}
			$image = $client->image;
            $client->attributes=$_POST['Client'];
			$uploader = CUploadedFile::getInstance($client,'image');
			if($uploader != null)
				$client->image = md5(time().$uploader->name).'.'.$uploader->extensionName;
			else
				$client->image = $image;
            if($client->save())
			{
				if($uploader != null)
				{
					$path = app()->basePath.DIRECTORY_SEPARATOR.
							'www'.DIRECTORY_SEPARATOR.
							'avatars'.DIRECTORY_SEPARATOR.md5($id);
					if(!is_dir($path))
						mkdir($path);
					$uploader->saveAs($path.DIRECTORY_SEPARATOR.$client->image);
				}
                if(!$hasSubErrors)
				    $this->redirect('/user');
                else
                    $client->addError('phone', 'Необходимо исправить ощибки в менеджере адресов и телефонов');
			}
        }
		$this->render('edit', array(
			'client' => $client,
			'deliveryAddresses' => $deliveryAddresses,
			'newDeliveryAddress' => new DeliveryAddress(),
			'phones' => $phones,
			'newPhone' => new Phone(),
		));
	}
	
	public function actionRegistration()
	{
	$model = new Client('register');
        $phone = new Phone();
		// Проверка на регистрацию пользователя из существующего аккаунта
	if(Yii::app()->user->name == "Guest" || ( Yii::app()->user->id==Yii::app()->user->getState('identity') && Yii::app()->user->getState('identity')!==null))
            {
            if(Yii::app()->user->getState('identity')!==null)
            {
                $service_stat=Yii::app()->user->getState('service_stat');
                //var_dump($service_stat);exit;
                $model->name = $service_stat['first_name'];
                $model->lastname =  $service_stat['last_name'];
                if(isset($service_stat['birthday'])) $model->date_of_birth = $service_stat['birthday'];
                
                $model->identity = Yii::app()->user->getState('identity');
                $model->service = Yii::app()->user->getState('service');
                if(Yii::app()->user->getState('email')) $model->email = Yii::app()->user->getState('email');
            }
			// Проведение регистрации
		if (isset($_POST['Client']))
			{
				if(isset($_POST['Client']['newPassword'])) $_POST['Client']['password'] = $_POST['Client']['newPassword'];
                if(Yii::app()->user->getState('identity')!==null){
                    $pass = $model->genPass(8);
                    $_POST['Client']['password'] = $pass;
                    $_POST['Client']['passwordConfirm'] = $pass;
                    $_POST['Client']['newPassword'] = $pass;
                }
                    $model->attributes = $_POST['Client'];
                    $model->validate();
                if(!empty($_POST['Phone']['value']))
                {
                    $phone->type = $_POST['Phone']['type'];
                    $phone->value = $_POST['Phone']['value'];
                    $phone->validate(array('value'));
                    $model->addErrors($phone->getErrors('value'));
                }
		if(!$phone->hasErrors() && $model->save())
		   {
                    if(isset($_POST['Phone']['value']))
                    {
                        $phone->client_id = $model->id;
                        $phone->save();
                    }

					$login = new LoginForm;
					$login->username = $_POST['Client']['email'];
					$login->password = $_POST['Client']['password'];
					
					$login->login();
					$this->redirect('/user');
				}
                
			}
            
            $login = new LoginForm;
            if(!$model->hasErrors())
            {
                
			if (isset($_POST['LoginForm'])) {
				$login->attributes = $_POST['LoginForm'];
				if ($login->validate() && $login->login())
					$this->redirect(Yii::app()->user->returnUrl);
				}
            }
            if(Yii::app()->user->getState('identity')!==null)
            {
           						
			$this->render('register_serv', array('model' => $model, 'login' => $login, 'phone' => $phone));
            }  
            else 
            {
			
				
			$this->render('register', array('model' => $model, 'login' => $login, 'phone' => $phone));
            }
		}
		else
		{
			$this->redirect('/user');
		}
	}

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
        else
        {
            $this->render('error');
        }
    }
    public function actionAjaxphonevalidation($value)
    {
        $phone = new Phone();
        $phone->type=1;
        $phone->value = $value;
        $phone->validate(array('value'));
         $error =  $phone->getError('value');
        if ($error) echo json_encode(array('title'=>'error', 'error'=>$error));
        else echo json_encode(array('title'=>'ok'));
    }
    
}
