<?php

class m130804_072140_add_news_tables_models extends CDbMigration
{
	public function up()
	{
		$this->execute("CREATE TABLE IF NOT EXISTS `news_product` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `news_id` int(11) NOT NULL,
			  `product_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
		
		$this->execute("
			CREATE TABLE IF NOT EXISTS `news` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` varchar(255) NOT NULL,
			  `title` varchar(255) NOT NULL,
			  `text` text,
			  `views` int(11),
			  `thumb` varchar(255),
			  `create_time` int(11) NOT NULL,
			  `update_time` int(11) NOT NULL,
			  `short_description` text,
			  `keywords` varchar(255),
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
		
		$this->execute("
			CREATE TABLE IF NOT EXISTS `related_news` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `news_id` int(11) NOT NULL,
			  `related_news_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->dropTable('news');
		$this->dropTable('news_product');
		$this->dropTable('related_news');
	}
}