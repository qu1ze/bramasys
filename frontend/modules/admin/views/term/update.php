<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'title');?>
	<?php echo $form->textAreaRow($model, 'definition');?>
	<?php echo $form->ckEditorRow($model, 'content', array('class'=>'span4', 'rows'=>5)); ?>
	<?php echo $form->textAreaRow($model, 'references');?>
	<?php echo $form->textFieldRow($model, 'photo');?>
	<?php echo $form->textFieldRow($model, 'author');?>
	<?php echo $form->textFieldRow($model, 'source');?>
	<?php echo $form->textFieldRow($model, 'source_date');?>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	
<?php $this->endWidget(); ?>