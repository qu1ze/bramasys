<?php 
    Yii::app()->clientScript->registerScript("getFile", "
        $('body').on('change', '#ExcelFile_file', function(){
			var parts = $(this).val().split('/');
			if(parts.length == 1)
				parts = $(this).val().split('\\\\');
			var fileName = parts[parts.length - 1];
			if(fileName == '')
			{
				fileName = 'Выбрать файл…';
			}
			$(this).parent().children('span').html(fileName);
        });
    ", CClientScript::POS_READY);

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'horizontal',
	'htmlOptions' => array('enctype' => 'multipart/form-data', 'style' => 'margin: 0px'),
));?>
	<div class="alert alert-success" <?php echo isset($_FILES['ExcelFile']) && !$excelFile->hasErrors() ? '' : 'style="display: none;"' ?>>
		Города и отделения успешно импортированы
	</div>
	<?php echo $form->errorSummary($excelFile); ?>
	<span class="btn btn-success fileinput-button"><span>Выбрать файл…</span><?php echo CHtml::fileField('ExcelFile[file]'); ?></span>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Импортировать')); ?>
<?php $this->endWidget();?>