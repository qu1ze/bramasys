<?php

class RelatedarticlesController extends Controller
{		
	public function actionCreate()
	{
		$model = new RelatedArticle; 
		
		//$this->performAjaxValidation($model);
		
		if (isset($_POST['RelatedArticle']))
		{	
             if (isset($_POST['RelatedArticle']['related_article_s']) && is_array($_POST['RelatedArticle']['related_article_s']))
                if($model->saveRrelateds($_POST['RelatedArticle']['related_article_s'],$_POST['RelatedArticle']['article_id']))
                    $this->redirect(array('/admin/relatedarticles'));
		}
				
		$this->render('create', array(
			'model' => $model, 			
		));
	}	
	
	

	public function actionIndex()
	{		
		$model = new RelatedArticle('search');
		
		if (isset($_GET['RelatedArticle']))
			$model->attributes = $_GET['RelatedArticle'];
													
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = RelatedArticle::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/relatedarticles'));				
				
		if (isset($_POST['RelatedArticle']))
		{	
           if (isset($_POST['RelatedArticle']['related_article_s']) && is_array($_POST['RelatedArticle']['related_article_s'])){
                if($model->saveRrelateds($_POST['RelatedArticle']['related_article_s'],$_POST['RelatedArticle']['article_id'],true)){
                    $this->redirect(array('/admin/relatedarticles'));
           }}
		}
		$mass = RelatedArticle::model()->findAllByAttributes(array('article_id' => $model->article_id)); 
        foreach ($mass as $value)
            $model->related_article_s[]=$value->related_article;
        
		$this->render('update', array(
			'model' => $model			
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('RelatedArticle');	
		$es->update();		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Связь для статей успешно отредактирована')));			
	}	
	
	public function actionAjaxDelete($id)
	{		
		if (($model = RelatedArticle::model()->findByPk($id)) !== null)
		{			
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Связь для статей успешно удалена'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить связь для статей'));
			
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Связь не найдена'));
	}
	
}