<?php
    Yii::app()->clientScript->registerScript("auto_desc", "
        $('.full_desc_field').on('change', function(){
            var full_desc_field = $(this);
            var desc_field = $('.desc_field');
            if (full_desc_field.val())
            {
                var desc = full_desc_field.val().substring(0, 255);
                if (desc_field.val() != desc)
                {
                    if (!desc_field.val())
                        desc_field.val(desc);
                    else
                    {
                        $('.desc').val(desc);
                        $('#confirmReplace').dialog('open');
                    }
                }
            }
        });
    ", CClientScript::POS_READY);
?>
<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
	
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
				'options'=>array(
					'title'=>'Заменить описание?',
					'modal'=>true,
					'autoOpen'=>false,
					'buttons'=>array(
						'Yes'=>'js:function(){$(".desc_field").val($(".desc").val()); $(this).dialog("close")}',
						'No'=>'js:function(){$(this).dialog("close")}'
						)
				),
				'id'=>'confirmReplace',

		)
	);

	echo CHtml::textArea('desc', '', array('class' => 'desc'));

	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model , 'brand', $brands);?>
	<?php echo $form->textFieldRow($model, 'model'); ?>
    <?php echo $form->textFieldRow($model, 'excel_id'); ?>
	<?php echo $form->ckEditorRow($model, 'full_description', array('class'=>'full_desc_field', 'rows'=>5)); ?>
	<?php echo $form->textAreaRow($model , 'description', array('class' => 'desc_field'));?>
	<?php echo $form->textFieldRow($model, 'old_price'); ?>
	<?php echo $form->textFieldRow($model, 'price'); ?>
	<?php echo $form->dropDownListRow($model, 'category', $categories, $categories_options); ?>
	<?php echo $form->textFieldRow($model, 'quantity'); ?>
	<?php echo $form->checkBoxRow($model, 'price_of_the_week'); ?>
	<?php echo $form->checkBoxRow($model, 'top_seller'); ?>
	<?php echo $form->checkBoxRow($model, 'novelty'); ?>
	<?php echo $form->checkBoxRow($model, 'hide'); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'type' => 'danger',
			'size' => 'normal',
			'label' => 'Удалить',
			'url' => Yii::app()->createUrl('/admin/products/delete', array('id' => $model->id))
		)); 
	?>
	
	
<?php $this->endWidget(); ?>
<h2>Product galley</h2>
<?php
if ($model->galleryBehavior->getGallery() === null) {
	echo '<p>До того как добавить фото, вы должны сохранить продукт</p>';
} else {
	$this->widget('GalleryManager', array(
		'gallery' => $model->galleryBehavior->getGallery(),
	));
}
?>

