<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textAreaRow($model, 'content'); ?>
	<?php echo $form->textAreaRow($model , 'pros');?>
	<?php echo $form->textAreaRow($model , 'cons');?>
	<?php echo $form->checkBoxRow($model, 'status'); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить')); ?>
	<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'type' => 'danger',
			'size' => 'normal',
			'label' => 'Удалить',
			'url' => Yii::app()->createUrl('/admin/comment/delete', array('id' => $model->id))
		)); 
	?>
	
<?php $this->endWidget(); ?>

