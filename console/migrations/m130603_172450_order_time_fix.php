<?php

class m130603_172450_order_time_fix extends CDbMigration
{
	public function up()
	{
		$this->execute("
                ALTER TABLE  `order` CHANGE  `create_time`  `create_time` INT(11) NOT NULL
            ");
	}

	public function down()
	{
		$this->execute("
			ALTER TABLE  `order` CHANGE  `create_time`  `create_time` TIMESTAMP NOT NULL
                ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}