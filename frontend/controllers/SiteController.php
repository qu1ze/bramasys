<?php
/**
 * SiteController.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class SiteController extends Controller {

	public function accessRules() {
		return array(
			// not logged in users should be able to login and view captcha images as well as errors
			array('allow', 'actions' => array('index', 'captcha', 'login', 'error', 'KK')),
			// logged in users can do whatever they want to
			array('allow', 'users' => array('@')),
			// not logged in users can't do anything except above
			array('deny'),
		);
	}

	/**
	 * Declares class-based actions.
	 * @return array
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
		);
	}

	/* open on startup */
	public function actionIndex() {
		$criteria_news = new CDbCriteria();
		$criteria_news->order = 'id DESC';
		$criteria_news->limit = 3;
		$last_news = News::model()->findAll($criteria_news);
		
		$criteria_articles = new CDbCriteria();
		$criteria_articles->order = 'id DESC';
		$criteria_articles->limit = 3;
		$last_articles = Article::model()->findAll($criteria_articles);
		
		$this->render('index', array(
			'last_news' => $last_news,
			'last_articles' => $last_articles,
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
		else
		{
			$this->render('error');
		}
	}

	public function actionAjaxLogin()
	{
		$model = new LoginForm;
		if (isset($_POST['LoginForm']))
		{
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate() && $model->login())
			{
				$isAdmin = Yii::app()->user->getState('isAdmin');
				$name = Yii::app()->user->getState('name');
				$lastname = Yii::app()->user->getState('lastName');
				echo json_encode(array('title' => 'Success', 'isAdmin' => $isAdmin, 'name' => $name, 'lastname' => $lastname,));
				app()->end();
			}
		}
		echo json_encode(array('title' => 'error', 'message' => $model->errors));
		app()->end();
	}
	
	/**
	 * Displays the login page
	 */
	public function actionLogin() {
		$model = new LoginForm;
//        if(empty(Yii::app()->user->id) && Yii::app()->user->getState('identity')!==null )
//               $this->redirect("/user/profile/registration"); 
        
        $serviceName = Yii::app()->request->getQuery('service');
		if (isset($serviceName)) {
			/** @var $eauth EAuthServiceBase */
			Yii::app()->user->setReturnUrl('/');
            $eauth = Yii::app()->eauth->getIdentity($serviceName);
			$eauth->redirectUrl = $this->createAbsoluteUrl('/user');//Yii::app()->user->returnUrl;
			//$eauth->cancelUrl = $this->createAbsoluteUrl('site/login');

			try {
				if ($eauth->authenticate()) {
					//var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes());
					$identity = new EAuthUserIdentity($eauth);

					// successful authentication
					if ($identity->authenticate()) {
						Yii::app()->user->login($identity);
						//var_dump($identity->id, $identity->name, Yii::app()->user->id, Yii::app()->user->getState('identity'));exit;
                        if(Yii::app()->user->id == Yii::app()->user->getState('identity'))
                            $eauth->redirectUrl = "/user/profile/registration";
						// special redirect with closing popup window
						$eauth->redirect();
					}
					else {
						// close popup window and redirect to cancelUrl
						$eauth->cancel();
					}
				}

				// Something went wrong, redirect to login page
				$this->redirect(array('site/login'));
			}
			catch (EAuthException $e) {
				// save authentication error to session
				Yii::app()->user->setFlash('error', 'EAuthException: '.$e->getMessage());

				// close popup window and redirect to cancelUrl
				$eauth->redirect($eauth->getCancelUrl());
			}
		}
        
        
		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
				//$this->redirect(array('/admin'));
//			else
//				var_dump($model->getErrors());
			}
            
            
		// display the login form
		$this->render('login', array('model' => $model));
		}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionRegistration() {
		$model = new User('register');
		// Проверка на регистрацию пользователя из существующего аккаунта
		if(Yii::app()->user->name == "Guest")
		{
			// Проведение регистрации
			if (isset($_POST['User'])) {
				$_POST['User']['password'] = $_POST['User']['newPassword'];
				$model->attributes = $_POST['User'];
				if($model->save())
					$this->redirect(Yii::app()->homeUrl);
			}
			$this->render('register', array('model' => $model));
		}
		else
		{
			$this->redirect(Yii::app()->homeUrl);
		}
	}

	public function actionSetCurrency($id)
	{
		if(Currency::model()->findByPk($id) === null)
		{
			if($id != CurrencySys::DEFAULT_CURRENCY || Yii::app()->request->cookies['currency']->value == $id)
			{
				echo json_encode(array(
					'title'=>'Error',
					));
				app()->end();
			}
		}
		Yii::app()->request->cookies['currency'] = new CHttpCookie('currency', $id);
		echo json_encode(array(
				'title'=>'Success',
				));
			
	}
	
	public function actionAjaxCallback()
	{
		$model = new CallbackForm;
		if (isset($_POST['CallbackForm']))
		{
			$model->attributes = $_POST['CallbackForm'];
			if ($model->validate())
			{
				if($model->send())
				{
					echo json_encode(array('title' => 'Success',));
					app()->end();
				}
			}
		}
		echo json_encode(array('title' => 'error', 'message' => $model->errors));
		app()->end();
	}
	
	public function actionAjaxMounting()
	{
		$model = new MountingForm;
		if (isset($_POST['MountingForm']))
		{
			$model->attributes = $_POST['MountingForm'];
			if ($model->validate())
			{
				if($model->send())
				{
					echo json_encode(array('title' => 'Success',));
					app()->end();
				}
			}
		}
		echo json_encode(array('title' => 'error', 'message' => $model->errors));
		app()->end();
	}
	
	public function actionAjaxGetSpecDesc($id)
	{
		$spec = Specification::model()->findByPk($id);
		if($spec !== null)
		{
			$desc = empty($spec->description) ? 'Описание отсутствует' : $spec->description;
			echo json_encode(array('title' => 'Success', 'desc' => $desc));
		}
		else
			echo json_encode(array('title' => 'Error'));
		app()->end();
	}

    public function actionAjaxSetPassword()
    {
        if (isset($_POST['email']))
        {
            $model = Client::model()->findByAttributes(array('email' => $_POST['email']));
            if ($model !== null)
            {

                $passwordRequest = PasswordRequest::model()->findByAttributes(array('client_id' => $model->id));
                if($passwordRequest === null)
                {
                    $passwordRequest = new PasswordRequest();
                    $passwordRequest->client_id = $model->id;
                }
                if($passwordRequest->save())
                {
                    $mashes = explode('@', $_POST['email']);
                    echo json_encode(array('title' => 'Success', 'message' => $mashes[1]));
                    app()->end();
                }
            }
        }
        echo json_encode(array('title' => 'error', 'message' => 'Вы не регистрировались под этим email или ввели его не верно. Если вы уверенны в том что ввели - свяжитесь с администрацией'));
        app()->end();
    }

    public function actionNewPassword($key)
    {
        $passwordRequest = PasswordRequest::model()->findByAttributes(array('key' => $key));
        if($passwordRequest !== null)
        {
            $client = $passwordRequest->client;
            if(isset($_POST['Client']))
            {
                $client->newPassword = $_POST['Client']['newPassword'];
                $client->passwordConfirm = $_POST['Client']['passwordConfirm'];
                if($client->validate())
                {
                    $client->attributes = array('password' => $_POST['Client']['newPassword']);
                    if($client->save())
                    {
                        $passwordRequest->delete();
                        $this->redirect('/site/login');
                    }
                }
            }
            $this->render('new_password', array('client' => $client));
        }
        else
            throw new CHttpException(404);
    }
}