<?php

class m131217_221206_article_path extends CDbMigration
{
	public function up()
	{
		$this->execute("UPDATE `category` SET  `name` =  'stati-publikatcii-obzory' WHERE   `name` =  'articles';");
	}

	public function down()
	{
		$this->execute("UPDATE `category` SET  `name` =  'articles' WHERE   `name` =  'stati-publikatcii-obzory';");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}