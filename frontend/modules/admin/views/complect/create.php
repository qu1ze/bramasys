<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	));
?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'name'); ?>
    <?php echo $form->textFieldRow($model, 'img_url', array('class' => 'image_url_field')); ?>
    <?php echo CHtml::image($model->img_url, '', array('class' => 'prod_image', 'style' => 'margin-bottom:10px; margin-left:180px;'));?>
	<?php echo $form->dropDownListRow($model, 'category_id', $this->getCategories()); ?>
    <?php echo $form->textFieldRow($model, 'actual_id');?>
    <?php echo $form->ckEditorRow($model, 'full_description', array('class'=>'full_desc_field', 'rows'=>5)); ?>
    <?php echo $form->textFieldRow($model, 'discount');?>

	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Создать')); ?>
	
<?php $this->endWidget(); ?>