<?php

class m130707_170535_create_static_page_table extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `static_page` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` varchar(255) DEFAULT NULL,
			  `content` text NOT NULL,
			  `category_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  CONSTRAINT `FK_category` FOREIGN KEY (`category_id`)
						  REFERENCES `category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		$this->dropTable('static_page');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}