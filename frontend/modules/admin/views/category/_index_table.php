<?php
$grid_id = 'category-table';
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $grid_id,
	'type' => 'bordered striped',
	'dataProvider' => $model->search(),
	'filter' => $model,	
	'afterAjaxUpdate' => 'js:function(){
		jQuery(".tooltip").remove();
		jQuery("a[rel=tooltip]").tooltip();				
	}',
	'template' => "{items}\n{pager}",
	'columns' => array(		
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'name',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/category/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'title',
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/category/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),
		array(
			'class' => 'bootstrap.widgets.TbEditableColumn',			
			'name' => 'parent',				
			'filter' => Category::getListData(),
			'editable' => array(
				'type' => 'select',
				'source' => Category::getListData(true),				
				'url' => app()->createUrl('/admin/category/ajaxUpdate'),				
				'success' => 'js:function(data){
					if(typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if(typeof data == "object" && data.success) {
				    	brama.ui.updateViews("g:'.$grid_id.'");
					}
				}'
			)
		),		
		array(
			'class' => 'bootstrap.widgets.TbEditableColumn',			
			'name' => 'tag',				
			'filter' => CategoryTag::getListData(),
			'editable' => array(
				'type' => 'select',
				'source' => CategoryTag::getListData(true),
				'url' => app()->createUrl('/admin/category/ajaxUpdate'),				
				'success' => 'js:function(data){
					if(typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if(typeof data == "object" && data.success) {
				    	brama.ui.updateViews("g:'.$grid_id.'");
					}
				}'
			)
		),	
		array(			
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'order_number',					
			'editable' => array(
				'type' => 'text', 
				'url' => app()->createUrl('/admin/category/ajaxUpdate'),
				'success' => 'js: function(data){
					if (typeof data == "object") {
						brama.ui.notify(data.title, data.msg);
					}
					if (typeof data == "object" && data.success) {
						brama.ui.updateViews("g:'. $grid_id .'");
					}
				}',
			)
		),				
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('nowrap' => 'nowrap'),			
			'template' => '{delete} {update} {changeSpec}',
                        'buttons'=>array
                        (
                            'changeSpec' => array
                            (
                                'label'=>'Изменить хар-ки',
                                'imageUrl'=>Yii::app()->request->baseUrl.'/images/admin/specs.png',
                                'url'=>'Yii::app()->createUrl("admin/category/changeSpec", array("id"=>$data->primaryKey))',
                            ),
                        ),	
			'updateButtonUrl'=>'Yii::app()->controller->createUrl("update", array("id" => $data->primaryKey))',
			'deleteButtonUrl'=>'Yii::app()->controller->createUrl("ajaxDelete", array("id" => $data->primaryKey))',
			'deleteConfirmation' => 'Вы уверенны что хотите удалить?',
			'afterDelete' => 'function(link,success,data){
				if(success){data = $.parseJSON(data);brama.ui.notify(data.title, data.msg, false);}
				else{brama.ui.notify("Oops!", data.responseText )};
			}',
		)
	)
));

