<div id="container">
<?php
$this->breadcrumbs = app()->categoryController->getCategoryBcs($this->categoryName);

    $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>' > ',
		'homeLink' => CHtml::link('Главная', app()->getBaseUrl(true).'/'),
		'normalizeUrl'=>false,
    ));
?>
	
<?php if($model !== null):?>
	<h1><?php echo $model->name;?></h1>
	<?php echo $model->content;?>
<?php endif;?>
<?php if($this->rootCategoryName == 'uslugi'):?>
	<input type="button" id="mounting-btn" value="Заказать монтаж">
	<?php $this->widget('common.components.Mounting'); ?>
<?php endif;?>
</div>
