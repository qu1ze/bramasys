<?php

class StaticPageController extends Controller
{		
	private $tagsExcludeList = array(
		'catalog',
		'articles',
	);
	
	protected function beforeAction($action)
	{
		return parent::beforeAction($action);
	}
	
	public function getCategoriesList($allStatic = true)
	{
		$tags = array();
		foreach (CategoryTag::model()->findAll() as $tag)
		{
			if(!in_array($tag->name, $this->tagsExcludeList))
				array_push($tags, $tag->name);
		}
		
		$categories = array();
		if(!$allStatic)
		{
			$staticPages = StaticPage::model()->findAll();
			if(!empty($staticPages))
			{
				foreach ($staticPages as $staticPage)
				{
					array_push($categories, $staticPage->category_id);
				}
			}
		}
		foreach (Category::model()->findAllByAttributes(array('in_url' => '0')) as $category)
		{
			array_push($categories, $category->id);
		}
		
		$categoryList = array();
		foreach($tags as $tag)
		{
			$categoryIds = app()->categoryController->getChildrenIds($tag);
			foreach (array_diff($categoryIds, $categories) as $categoryId)
			{
				$categoryList[$categoryId] = app()->categoryController->getTitle($categoryId);
			}
		}
		
		return $categoryList;
	}
	
	public function actionCreate()
	{
		$model = new StaticPage(); 
				
		if (isset($_POST['StaticPage']))
		{			
			$model->attributes = $_POST['StaticPage'];
			if ($model->save())
				$this->redirect(array('/admin/staticpage'));
		}
		

		$this->render('create', array(
			'model' => $model, 
			'categories' => $this->getCategoriesList(false),
		));
	}	

	public function actionIndex()
	{		
		$model = new StaticPage('search');
		
		if (isset($_GET['StaticPage']))
			$model->attributes = $_GET['StaticPage'];
													
		$this->render('index', array(
			'model' => $model,
			'categories' => $this->getCategoriesList(),
		));
	}
	
	public function actionUpdate($id)
	{
		if (($model = StaticPage::model()->findByPk($id)) === null)
			$this->redirect(array('/admin/staticpage'));				
				
		if (isset($_POST['StaticPage']))
		{			
			$model->attributes = $_POST['StaticPage'];
			if ($model->save())
				$this->redirect(array('/admin/staticpage'));
		}
				
		$this->render('update', array(
			'model' => $model,
			'categories' => $this->getCategoriesList(false) + array($model->category_id => $model->category->title),
		));
	}
	
	public function actionAjaxUpdate()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver('StaticPage');
		
		$es->update();
		
		die(json_encode(array('Success' => true, 'title'=>'Success', 'msg'=> 'Страница успешно создана')));			
	}
	
	public function actionDelete($id)
	{		
		if ($id)
		{
				StaticPage::model()->deleteByPk( $id );					
				Yii::app()->user->setFlash('success', "Страница успешно удалена");
		}
		
		$this->redirect(array('/admin/staticpage'));
	}
	
	public function actionAjaxDelete($id)
	{		
		if (($model = StaticPage::model()->findByPk($id)) !== null)
		{
			if ($model->delete())
				echo json_encode(array('title' => 'Success', 'msg' => 'Страница успешно удалена'));
			else
				echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить страницу'));
		} else
			echo json_encode(array('title' => 'Error', 'msg' => 'Не возможно удалить: Страница не найдена'));
	}
	
}