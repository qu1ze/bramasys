	<?php foreach($products as $product): ?>
	<?php $inCart = isset(Yii::app()->session['cart'][$product->id]);?>
	<div class="tab-product pd<?php echo $product->id; ?>">
		<div class="pd-qty-block"><input hidden value="1"></div>
		<div class="code pd-code"  style="display: none;">Код товара: <span><?php echo $product->id;?></span></div>
		<div class="thumb">
			<a href="<?php echo $product->link;?>">
				<span class="img-vert">
					<img style="width: 120px;max-height: 150px" src="<?php echo $product->image_url;?>" alt="">
				</span>
			</a>
		</div>
		<div class="name"><?php echo CHtml::link(CHtml::encode($product->brand0->name).' '.CHtml::encode($product->model), array('view', 'brand' => CHtml::encode($product->brand0->name), 'model' => CHtml::encode($product->model), 'id' => CHtml::encode($product->id))); ?></div>
		<div class="price"><?php echo CurrencySys::exchange($product->price).' '.CurrencySys::getLabel(); ?></div>
        <?php if ($product->quantity != null):?>
            <?php if ($product->quantity > 0):?>
                <div id="btn" class="btn-buy-<?php echo $product->id; ?> enabled" style="<?php echo $product->inCart ? 'display: none;' : ''?>">
                    <a data-is_complect="0" id="<?php echo $product->cartPattern; ?>" href="#" class="btn-buy-buy index-buy-but">Купить</a>
                </div>
                <div class="btn-buy-<?php echo $product->id; ?> disabled" id="<?php echo $product->cartPattern; ?>"  style="<?php echo $product->inCart ? '' : 'display: none;'?>">
                    <a data-is_complect="0" href="#" data-modif-id="2" class="btn-buy-isset disabled" data-controls-modal="">Уже в корзине</a>
                </div>
            <?php else:?>
                <a class="catalog btn-buy-order" data-is_complect="0" href="#" id="pd<?php echo $complect->id; ?>" style="font-size: 20px; line-height: 30px; margin-left: 28px;">Заказать</a>
            <?php endif;?>
        <?php else:?>
            <a class="catalog btn-buy-soon" data-is_complect="0" href="#" id="pd<?php echo $product->id; ?>" style="font-size: 15px; padding: 5px; line-height:23px; margin-left: 28px;">Уведомить о наличии</a>
        <?php endif;?>
	</div>
	<?php endforeach; ?>