<?php

class m131208_155152_delivery_system_address extends CDbMigration
{
	public function up()
	{
        $this->execute("
            CREATE TABLE IF NOT EXISTS `delivery_system_address` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `delivery_type_id` int(11) NOT NULL,
            `town` varchar(150) NOT NULL,
            `office` varchar(100) NOT NULL,
            `address` varchar(150) NOT NULL,
            PRIMARY KEY (`id`),
            KEY `delivery_type_id` (`delivery_type_id`,`town`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
          
            ALTER TABLE  `delivery_system_address` ADD CONSTRAINT  `delivery_type_fk` FOREIGN KEY (  `delivery_type_id` ) REFERENCES `bramasys`.`delivery_type` (
            `id`
            ) ON DELETE CASCADE ON UPDATE CASCADE ;          
          ");
	}

	public function down()
	{
		 $this->dropTable('delivery_system_address');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}