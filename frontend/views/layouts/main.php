<?php
/**
 * main.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:31 AM
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/normalize.min.css">
	<?php
	$controller = Yii::app()->controller;
	$productMetadata = array();
	$metadata = null;
	$additional = isset($_GET['Products_page']) ? 'Страница '.$_GET['Products_page'].'. ' : '';
	$this->pageTitle = $additional.'Bramasys';
	switch ($controller->id)
	{
		case 'products':
		case 'complect':
			$this->pageTitle = $additional.$controller->categoryTitle;
			if($controller->action->id == "view" && isset($_GET['id']))
			{
				$product = $controller->id == 'complect' ? Complect::model()->findByPk($_GET['id']) : Products::model()->findByPk($_GET['id']);
				if($product !== null)
				{
					$this->pageTitle = $product->metaTitle;
					$productMetadata['description'] = $product->metaDescription;
					$productMetadata['keywords'] = $product->metaKeywords;
				}
			}
		default:
			$metadata = CategoryMetadata::model()->findByAttributes(array('url' => "/".Yii::app()->request->pathInfo));
			if($metadata !== null)
			{
				$this->pageTitle = $additional.$metadata->title;
			}
			$categories = explode('/', Yii::app()->request->pathInfo);
			if(sizeof($categories) > 2 && $categories[sizeof($categories) - 2] == 'page')
			{
				$address = '';
				for ($i = 0; $i < sizeof($categories) - 2; $i++)
					$address .= "/".$categories[$i];
				$metadata = CategoryMetadata::model()->findByAttributes(array('url' => $address));
				if($metadata !== null)
				{
					$this->pageTitle = !empty($metadata->title) ? $additional.$metadata->title : $this->pageTitle;
				}
			}
			break;
	}
	?>
	<title><?php echo h($this->pageTitle); /* using shortcut for CHtml::encode */ ?></title>
	<meta name="description" content="<?php echo !empty($metadata->description) ? $metadata->description : (!empty($productMetadata['description']) ? $productMetadata['description'] : '')?>">
	<meta name="keywords" content="<?php echo !empty($metadata->keywords) ? $metadata->keywords : (!empty($productMetadata['keywords']) ? $productMetadata['keywords'] : '')?>">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css"/>-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.min.css"/>
	 <?php if (app()->controller->id != 'site' || app()->controller->action->id != 'index'):?>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/category-drop.css">
        <?php endif;?>
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico">
		<!--[if lte IE 6] -->
	<link href="/css/style_ie.css" rel="stylesheet" type="text/css">
	<!--[if IE] -->
	<!-- ????????? scripts -->
</head>
<body>
    	<script type="text/javascript" src="//vk.com/js/api/openapi.js?63"></script>
	<script type="text/javascript">
		VK.init({apiId: 3219761, onlyWidgets: true});
	</script>
       
	<!-- Google + button script -->
	<script type="text/javascript">
		window.___gcfg = {lang: 'ru'};
		(function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			po.src = 'https://apis.google.com/js/plusone.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();
	</script>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<div class="container" id="wrapper">
	
		<header id="header">
			<div id="header-middle">
				<div id="header_middle_top">
					<ul class="top_menu">
						<li>
							<a title="Оплата и доставка" href="<?php echo app()->getBaseUrl(true); ?>/oplata_dostavka">Оплата и доставка</a>
						</li>
						<li>
							<a title="Возврат и гарантия" href="<?php echo app()->getBaseUrl(true); ?>/vozvrat_garantiya">Возврат и гарантия</a>
						</li>
						<li>
							<a title="Кредит" href="<?php echo app()->getBaseUrl(true); ?>/kredit">Кредит</a>
						</li>
					</ul>
					<?php $this->widget('common.components.UserSignMenu'); ?>
				</div>
				<?php echo $this->renderPartial('//layouts/_header'); ?>			
			</div>
		</header>
		
		<div id="content">
			<?php
			try
			{
				$tag = static::getTag();
			}
			catch(Exception $e)
			{
				$tag = '';
			}
			if($tag == 'catalog' || $tag == 'articles' || $tag == 'static'): ?>
			<aside id="sideLeft">
				<?php app()->categoryController->renderNavigationTree(Yii::app()->controller->id); ?>
			</aside>
                         <script>
                             function ajaxreqest(url, hash ){
                             
                                $('#content-ajax').css('opacity','0.2');
                                $('#content-ajax').parent().css('background','url(/images/spinner.gif) no-repeat top center');
                                $.get(url , function(data,textStatus, xhr) {
                                   if (window.location.pathname != xhr.getResponseHeader('ajaxLoc'))
                                   {
                                       window.location.hash=':'+hash;
                                       window.location.pathname = xhr.getResponseHeader('ajaxLoc');
                                   }
                                   else
                                   {
				  $('#content-ajax').html(data);
                  console.log(xhr.isRejected);
                  
                  window.location.hash=':'+hash;
                                  $('#content-ajax').css('opacity','1');
                                  $('#content-ajax').parent().css('background','none');
                                  if (typeof window['initSlider'] == 'function') initSlider();
                              }
			  }).fail(function (jqXHR) {
    console.log(jqXHR.status);
});  
                             }
                            </script>
			<div id="content-ajax">
                           
                            
                        <?php endif; ?>
			<?php echo $content; ?>
			<?php
			$isMainCategory = false;
            if((Yii::app()->controller->id == 'products' || Yii::app()->controller->id == 'complect') && Yii::app()->controller->action->id == 'index'):
			if(!Yii::app()->controller->isSearch ):
			$categoryVars = app()->categoryController->getCategory(Yii::app()->controller->categoryIds[0]);
			$isMainCategory = $categoryVars['data']['isMain'];
			endif;
			if(!$isMainCategory):
			?>
                            <aside id="sideRight" style="display: none">
				<?php $this->widget('common.components.ProductFilter'); ?>
				<?php $this->widget('common.components.TopProducts', array('categoriesIds' => Yii::app()->controller->categoryIds)); ?>
			</aside>
			<?php endif; endif; ?>
                        </div>    
			<br>
			<div style="margin-bottom: 15px; width: 100%; display: inline-block;">
				<?php echo !empty($metadata) && !isset($_GET['Products_page']) ? $metadata->seo : ''?>
			</div>
         
		</div>
        
	</div>
	 
	<footer id="footer">
		<?php echo $this->renderPartial('//layouts/_footer'); ?>
	</footer>
	<?php $this->widget('common.components.BottomTabs'); ?>
	<?php /* <!-- Google Analytics -->
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-38629514-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	</script>*/
	?>
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M86TQ3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M86TQ3');</script>
<!-- End Google Tag Manager -->
    
<?php /* 
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter21796246 = new Ya.Metrika({id:21796246,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						trackHash:true});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="//mc.yandex.ru/watch/21796246" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	*/ ?>
	<!-- BEGIN JIVOSITE CODE {literal} -->
	<script type='text/javascript'>
		(function(){ var widget_id = '21614';
			var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
	<!-- {/literal} END JIVOSITE CODE -->
	<?php $this->widget('common.components.SpecPopup'); ?>

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<script type="text/javascript">
      $("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/main.min.css' type='text/css' media='screen'>");
      $("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/spec_popup.css' type='text/css' media='screen'>");
      $("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.gritter.css' type='text/css' media='screen'>");
      $("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/colorbox.min.css' type='text/css' media='screen'>");
     </script> 

       
      
       <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.gritter.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.colorbox.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modernizr-2.6.2.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js"></script>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

</body></html>