<ul>
<?php 
	$children = $this->getChilds(ProductsController::getTag());
	if(!empty($children)):
?>
<?php foreach ($children as $child): ?>
    <li>
        <a title="<?php echo $child['data']['title']; ?>" href="<?php echo $this->getCategoryAddress($child['data']['name']); ?>">
            <?php echo $child['data']['title']; ?>
        </a>
        <div class="catalog-subcategories">
            <?php
                $count = $this->countChildren($child['id']);
				$columnNumber = 1;
                if($count > 10)
                    if($count > 20)
                        $columnNumber = 3;
                    else
                        $columnNumber = 2;
                $this->renderTree($child['id'], null, false, $count/$columnNumber); 
            ?>
        </div>
    </li>
    <?php endforeach; ?>
<?php endif; ?>
</ul>